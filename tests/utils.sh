# Unit test utilities
SRCDIR="$TESTDIR/../.."

do_test() {
	echo "Running $1 ($2) ..."
	perun-com -o per-test -mainclass=$2 $1 || exit 1
	./per-test || exit 1
}