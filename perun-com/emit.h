/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EMIT_H_
#define EMIT_H_

#include "parse.h"
#include "expr.h"
#include "stmt.h"
#include "hashtab.h"

/**
 * Variable sections.
 */
enum
{
	PER_VSECT_PRIMITIVE,
	PER_VSECT_MANAGED,
	PER_VSECT_ARRAY
};

/**
 * Typedef all the structs here.
 */
typedef struct EmitFunc_ EmitFunc;
typedef struct TempVar_ TempVar;
typedef struct Insn_ Insn;
typedef struct Block_ Block;
typedef struct PerunVar_ PerunVar;
typedef struct ExecContext_ ExecContext;
typedef struct ExprValue_ ExprValue;

/**
 * Represents a Perun variable.
 */
struct PerunVar_
{
	/**
	 * The variable type.
	 */
	Type *type;
	
	/**
	 * Name of the C temporary variable holding the value.
	 */
	char *cname;
	
	/**
	 * Token associated with the definition.
	 */
	Token *tok;
	
	/**
	 * Set to 1 if the variable is final.
	 */
	int isFinal;
};

/**
 * Represents an execution context.
 */
struct ExecContext_
{
	/**
	 * Parent context or NULL.
	 */
	ExecContext *parent;
	
	/**
	 * Set to '1' if a 'this' value is available in this context. In this case,
	 * it is assumed to be stored in the C variable "thisptr".
	 */
	int hasThis;
	
	/**
	 * Hash table mapping variable names to the corresponding PerunVar*.
	 */
	HashTable *vars;
	
	/**
	 * Return type.
	 */
	Type *retType;
	
	/**
	 * Targets for the "break" and "continue" statements. These are translated into a
	 * "goto", to the specified label.
	 */
	const char* breakTarget;
	const char* continueTarget;
};

/**
 * Represents the value of an expression.
 */
struct ExprValue_
{
	/**
	 * Type of value. NULL indicates error.
	 */
	Type *type;
	
	/**
	 * Value of the expression written as a C expression. If there are no side effects,
	 * then this will be an expression which may or may not be used; if it does have side
	 * effects then the evaluation would have been emitted to the block, and this will be
	 * a variable name. If the value is managed or array, this expression MUST name a temporary
	 * variable, which currently holds a reference.
	 */
	char *cval;
	
	/**
	 * Token associated with the value.
	 */
	Token *tok;
	
	/**
	 * If type == NULL, the error message.
	 */
	char *errmsg;
};

/**
 * Represents a temporary variable for C translation.
 */
struct TempVar_
{
	/**
	 * Next variable.
	 */
	TempVar *next;
	
	/**
	 * Variable type as a string, e.g. "int32_t".
	 */
	const char *type;
	
	/**
	 * Variable name.
	 */
	const char *name;
	
	/**
	 * Variable section (PER_VSECT_*).
	 */
	int section;
};

/**
 * Represents an instruction (C statement).
 */
struct Insn_
{
	/**
	 * Code (might be an empty string).
	 */
	char *code;
	
	/**
	 * Next instruction in this block.
	 */
	Insn *next;
	
	/**
	 * If this is not NULL, then this instruction will be followed
	 * by a C compound statement (before 'next').
	 */
	Block *block;
};

/**
 * Represents a block of instructions.
 */
struct Block_
{
	/**
	 * The function which this block is a part of.
	 */
	EmitFunc *func;
	
	/**
	 * First and last instructions in the block.
	 */
	Insn *first;
	Insn *last;
};

/**
 * Represents a function to be emitted in C.
 */
struct EmitFunc_
{
	/**
	 * Next number to use for a temporary.
	 */
	int nextTemp;
	
	/**
	 * Temporary variables which need to be emitted.
	 */
	TempVar *vars;
	
	/**
	 * Set to 1 if variables need to be made volatile due to try/catch
	 * statements.
	 */
	int volatileVars;
	
	/**
	 * Code in this function.
	 */
	Block *code;
	
	/**
	 * String containing the type name (in C) to be returned.
	 */
	char *retType;
};

/**
 * Information about an accessible static field. This specifies the mangled name,
 * the expanded type, flags, and diagnostic information. This is used during a search
 * for static members of classes whilst compiling.
 */
typedef struct
{
	/**
	 * Mangled name of the field. A value of NULL here indicates that no field
	 * was found.
	 */
	const char *mangled;
	
	/**
	 * The expanded type.
	 */
	Type *type;
	
	/**
	 * Flags.
	 */
	int flags;
	
	/**
	 * Token where the field was defined.
	 */
	Token *tok;
	
	/**
	 * The class template which defines this field.
	 */
	ClassTemplate *definer;
} StaticFieldInfo;

/**
 * Information about an accessible dynamic field. This specifies the mangled name,
 * the expanded type, flags, and diagnostic information. This is used during a search
 * for dynamic members of classes whilst compiling.
 */
typedef struct
{
	/**
	 * Mangled name of the field. A value of NULL here indicates that no field
	 * was found.
	 */
	const char *mangled;
	
	/**
	 * The expanded type.
	 */
	Type *type;
	
	/**
	 * Flags.
	 */
	int flags;
	
	/**
	 * Token where the field was defined.
	 */
	Token *tok;
	
	/**
	 * The class template which defines this field.
	 */
	ClassTemplate *definer;
} DynamicFieldInfo;

/**
 * Get the output file.
 */
FILE* emitGetOutputFile();

/**
 * Get the template being compiled.
 */
ClassTemplate* emitGetInTemplate();

/**
 * Create a new temporary variable, and return its name.
 */
char* makeTempVar(EmitFunc *func, const char *type, int managed);

/**
 * Create a new emitter function.
 */
EmitFunc* newEmitFunc(const char *retType);

/**
 * Add an instruction to a block. If 'newBlockOut' is not NULL, then a block is created.
 */
void emitInsn(Block *block, Block **newBlockOut, const char *format, ...);

/**
 * Create a new execution context which is not the child of an existing context.
 */
ExecContext* newRootContext(int hasThis, Type *type);

/**
 * Create a new execution context which is the child of an existing context.
 */
ExecContext* newChildContext(ExecContext *parent);

/**
 * Add a new variable to the specified context, which has the specified type and is allocated to
 * the specified C variable. Returns NULL on success, or a token pointer if already defined.
 */
Token* defineVar(ExecContext *ctx, Type *type, const char *name, const char *cname, Token *tok, int isFinal);

/**
 * Find a constructor of the specified type which can take the specified arguments. If not founds,
 * returns NULL and prints errors to the console. 'tok' is a token to associate with errors.
 */
ClassConstructor* findConstructor(Token *tok, Type *type, ExprValue *args, int numArgs);

/**
 * Compile expressions into the specified block. Return the appropriate ExprValue.
 */
ExprValue emitNormalAssignment(ExecContext *ctx, Block *block, PostfixExpression *postfix, ExprValue value);
ExprValue emitNewExpression(ExecContext *ctx, Block *block, NewExpression *newExpr);
ExprValue emitImportExpression(ExecContext *ctx, Block *block, ImportExpression *importExpr);
ExprValue emitPrimaryExpression(ExecContext *ctx, Block *block, PrimaryExpression *primary);
ExprValue emitPostfixExpression(ExecContext *ctx, Block *block, PostfixExpression *postfix);
ExprValue emitUnaryExpression(ExecContext *ctx, Block *block, UnaryExpression *unary);
ExprValue emitCastExpression(ExecContext *ctx, Block *block, CastExpression *cast);
ExprValue emitMultiplicativeExpression(ExecContext *ctx, Block *block, MultiplicativeExpression *mul);
ExprValue emitAdditiveExpression(ExecContext *ctx, Block *block, AdditiveExpression *add);
ExprValue emitShiftExpression(ExecContext *ctx, Block *block, ShiftExpression *shift);
ExprValue emitRelationalExpression(ExecContext *ctx, Block *block, RelationalExpression *rel);
ExprValue emitEqualityExpression(ExecContext *ctx, Block *block, EqualityExpression *equality);
ExprValue emitBitwiseANDExpression(ExecContext *ctx, Block *block, BitwiseANDExpression *bitwiseAND);
ExprValue emitBitwiseXORExpression(ExecContext *ctx, Block *block, BitwiseXORExpression *bitwiseXOR);
ExprValue emitBitwiseORExpression(ExecContext *ctx, Block *block, BitwiseORExpression *bitwiseOR);
ExprValue emitLogicalANDExpression(ExecContext *ctx, Block *block, LogicalANDExpression *logicalAND);
ExprValue emitLogicalORExpression(ExecContext *ctx, Block *block, LogicalORExpression *logicalOR);
ExprValue emitConditionalExpression(ExecContext *ctx, Block *block, ConditionalExpression *cond);
ExprValue emitAssignmentExpression(ExecContext *ctx, Block *block, AssignmentExpression *assign);
ExprValue emitExpression(ExecContext *ctx, Block *block, Expression *expr);

/**
 * Compile statements into the specified block. Prints errors to the console if anything is wrong.
 */
void emitReturnStatement(ExecContext *ctx, Block *block, ReturnStatement *rets);
void emitStatement(ExecContext *ctx, Block *block, Statement *st);
void emitCompoundStatement(ExecContext *ctx, Block *block, CompoundStatement *stmt);

/**
 * Translate a Perun compilation unit to C, outputting the code to the specified file.
 */
void emitPerun(CompilationUnit *unit, FILE *fp);

/**
 * Add a directory to the resource search path.
 */
void addResourcePath(const char *dirname);

#endif
