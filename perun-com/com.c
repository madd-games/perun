/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#ifdef _WIN32
#include <windows.h>
#define getpid GetCurrentProcessId
#else
#include <unistd.h>
#endif

#include "parse.h"
#include "mangle.h"
#include "emit.h"
#include "catalog.h"
#include "cci.h"
#include "buildvars.h"
#include "lex.h"
#include "opt.h"
#include "console.h"

static int stopAtCatalog;
static char *stopAtDeps;
static int stopAtTokens;
static int stopAtDefs;
static int stopAtEmit;
static int stopAtCompile;
static char* outputFileName;
static char* mainClass;
static char* tokenTagFile;
char* depFileName;
static int makeShared;
int noLibPerun;
int verbose;
static const char **extraLibs;
static const char **staticLibs;
static int numExtraLibs;
static int numStaticLibs;
static int noDefaultClassPath;

void addExtraLib(const char *lib)
{
	char *buf = (char*) malloc(strlen(lib) + 8);
	sprintf(buf, "-l%s", lib);
	
	int index = numExtraLibs++;
	extraLibs = (const char**) realloc(extraLibs, sizeof(void*) * numExtraLibs);
	extraLibs[index] = strdup(buf);
};

void addLibPath(const char *path)
{
	char *buf = (char*) malloc(strlen(path) + 8);
	sprintf(buf, "-L%s", path);
	
	int index = numExtraLibs++;
	extraLibs = (const char**) realloc(extraLibs, sizeof(void*) * numExtraLibs);
	extraLibs[index] = strdup(buf);
};

void addStaticLib(const char *lib)
{
	char *buf = (char*) malloc(strlen(lib) + 8);
	sprintf(buf, "%s", lib);
	
	int index = numStaticLibs++;
	staticLibs = (const char**) realloc(staticLibs, sizeof(void*) * numStaticLibs);
	staticLibs[index] = strdup(buf);
};

static char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

typedef struct TempFileName_
{
	struct TempFileName_ *prev;
	char *name;
} TempFileName;
TempFileName *nameStack = NULL;

void generateRandomName(char *buffer, const char *fmt)
{
	static int counter = 1;
	int fileID = getpid() * 1000 + (counter++);
	sprintf(buffer, fmt, fileID);
	
	TempFileName *tmp = (TempFileName*) malloc(sizeof(TempFileName));
	tmp->prev = nameStack;
	tmp->name = strdup(buffer);
	nameStack = tmp;
};

void cleanupTemps()
{
	TempFileName *tmp;
	for (tmp=nameStack; tmp!=NULL; tmp=tmp->prev)
	{
		remove(tmp->name);
	};
}

void addBuildVar(const char *spec)
{
	char *eqpos = strchr(spec, '=');
	if (eqpos == NULL)
	{
		bvarSet(spec, "");
	}
	else
	{
		char *work = strdup(spec);
		eqpos = strchr(work, '=');
		*eqpos = 0;
		bvarSet(work, eqpos+1);
		free(work);
	};
};

void dumpTokenTagsIfRequested(Token *toklist)
{
	if (tokenTagFile != NULL)
	{
		FILE *fp = fopen(tokenTagFile, "w");
		if (fp == NULL)
		{
			fprintf(stderr, "perun-com: failed to open tag file `%s': %s\n", tokenTagFile, strerror(errno));
			exit(1);
		};
		
		fprintf(fp, "<tags>\n");
		
		Token *tok;
		for (tok=toklist; tok->type!=TOK_END; tok=tok->next)
		{
			fprintf(fp, "\t<token position=\"%s:%d:%d:%d\">\n", tok->filename, tok->lineno, tok->col, (int) strlen(tok->value));
			
			TokenTag *tag;
			for (tag=tok->tags; tag!=NULL; tag=tag->next)
			{
				char *spec;
				
				if (tag->otherToken != NULL)
				{
					spec = (char*) malloc(strlen(tag->otherToken->filename) + 256);
					sprintf(spec, "%s:%d:%d:%d", tag->otherToken->filename, tag->otherToken->lineno, tag->otherToken->col,
								(int) strlen(tag->otherToken->value));
				}
				else
				{
					spec = strdup("");
				};
				
				fprintf(fp, "\t\t<%s>%s;%s</%s>\n", tag->type, spec, tag->param != NULL ? tag->param : "", tag->type);
				free(spec);
			};
			
			fprintf(fp, "\t</token>\n");
		};
		
		fprintf(fp, "</tags>\n");
		fclose(fp);
	};
};

int main(int argc, char *argv[])
{
	atexit(cleanupTemps);
	bvarInit();
	
	OptionList optlist;
	optInit(&optlist);
	int showVersion = 0;
	optAdd(&optlist, "-T", "",
		"Stop at the tokenization stage, and dump the tokens to standard output.",
		OPT_FLAG, &stopAtTokens);
	optAdd(&optlist, "-fdump-tags", "<TAG-FILE>",
		"(Only applicable with -fdump-emit or later stages) After code generation is completed, dump token tags to the specified file.",
		OPT_STRING, &tokenTagFile);
	optAdd(&optlist, "-fdump-emit", "",
		"Stop at the C code emission stage, and dump it to standard output.",
		OPT_FLAG, &stopAtEmit);
	optAdd(&optlist, "-fdump-defs", "",
		"Stop at the definition stage, and dump definitions to standard output.",
		OPT_FLAG, &stopAtDefs);
	optAdd(&optlist, "-fdump-catalog", "",
		"Stop at the cataloging stage, and dump the catalog.",
		OPT_FLAG, &stopAtCatalog);
	optAdd(&optlist, "-fdump-deps", "",
		"Stop at the cataloging stage, and dump the dependency graph for the specified class.",
		OPT_STRING, &stopAtDeps);
	optAdd(&optlist, "-c", "",
		"Stop at the C compilation stage, and output the object file.",
		OPT_FLAG, &stopAtCompile);
	optAdd(&optlist, "-o", "<OUTPUT-FILE>",
		"Specify the output file name.",
		OPT_SHORT_STRING, &outputFileName);
	optAdd(&optlist, "-D", "<TARGET-NAME>",
		"Instead of compiling, write a make-compatible rule to standard output,\n"
		"listing the dependencies of the compilation, with target TARGET-NAME.",
		OPT_SHORT_STRING, &depFileName);
	optAdd(&optlist, "-shared", "",
		"Produce a shared library (instead of an executable).",
		OPT_FLAG, &makeShared);
	optAdd(&optlist, "-mainclass", "<MAIN-CLASS-FULLNAME>",
		"Specify the fullname of the main class. Required when producing executables.",
		OPT_STRING, &mainClass);
	optAdd(&optlist, "-Werror", "",
		"Report all warnings as errors.",
		OPT_FLAG, &conWerror);
	optAdd(&optlist, "-B", "<NAME>=<VALUE>",
		"Set a build variable to the specified value.",
		OPT_LIST, addBuildVar);
	optAdd(&optlist, "-C", "<DIRECTORY>",
		"Add the specified directory to the class search path.",
		OPT_LIST, addClassPath);
	optAdd(&optlist, "-fno-default-classpath", "",
		"Do not use the default class search path.",
		OPT_FLAG, &noDefaultClassPath);
	optAdd(&optlist, "-fno-libperun", "",
		"Do not automatically link in the Perun standard library.",
		OPT_FLAG, &noLibPerun);
	optAdd(&optlist, "-L", "<PATH>",
		"Add the specified directory to the library search path.",
		OPT_LIST, addLibPath);
	optAdd(&optlist, "-l", "<LIBRARY>",
		"Link in the specified library.",
		OPT_LIST, addExtraLib);
	optAdd(&optlist, "-S", "<LIBRARY>",
		"Link in the specified Perun static library.",
		OPT_LIST, addStaticLib);
	optAdd(&optlist, "-R", "<PATH>",
		"Add the specified directory to the resource search path.",
		OPT_LIST, addResourcePath);
	optAdd(&optlist, "-v", "",
		"Be more verbose (useful for debugging).",
		OPT_FLAG, &verbose);
	optAdd(&optlist, "--version", "",
		"Write the compiler version to stdout and exit.",
		OPT_FLAG, &showVersion);
	char **files = optParse(argc, argv, &optlist);
	if (files == NULL)
	{
		return 1;
	};
	
	if (showVersion)
	{
		printf("%s: Madd Perun Compiler %s\n", argv[0], PERUN_VERSION);
		printf("Copyright (c) 2019, Madd Games. All rights reserved.\n");
		return 0;
	};
	
	if (files[0] == NULL)
	{
		fprintf(stderr, "%s: no input files\n", argv[0]);
		fprintf(stderr, "Use `%s --help' to get help.\n", argv[0]);
		return 1;
	};

	initParser();

	if (!noDefaultClassPath)
	{
		addClassPath(PERUN_CLASSPATH_MAIN);
		addClassPath(PERUN_CLASSPATH_PLATFORM);
	};
	
	LinkRequest ldreq;
	int linking = 0;
	if (stopAtCatalog || stopAtDeps || stopAtTokens || stopAtDefs || depFileName != NULL || stopAtEmit || stopAtCompile)
	{
		if (files[1] != NULL)
		{
			fprintf(stderr, "%s: multiple files specified, but i'm not linking!\n", argv[0]);
			return 1;
		};
	}
	else
	{
		linking = 1;
		if (outputFileName == NULL)
		{
			outputFileName = "perun-program";
		};
		
		if (!makeShared && mainClass == NULL)
		{
			fprintf(stderr, "%s: linking an executable, but the main class is not specified\n", argv[0]);
			return 1;
		};
		
		cciInitLink(&ldreq, makeShared, outputFileName);
	};
	
	int i;
	for (i=0; files[i] != NULL; i++)
	{
		const char *sourceFile = files[i];
		
		if (strlen(sourceFile) > 4 && memcmp(&sourceFile[strlen(sourceFile)-4], ".per", 4) == 0)
		{
			// Perun source file
			doClassCatalog(sourceFile);
			if (conGetError() != 0)
			{
				return 1;
			};
			
			if (stopAtCatalog)
			{
				dumpCatalog();
				return 0;
			};
			
			if (stopAtDeps)
			{
				dumpDeps(stopAtDeps);
				return 0;
			};
			
			FILE *fp = fopen(sourceFile, "r");
			if (fp == NULL)
			{
				fprintf(stderr, "%s: cannot open source file `%s': %s\n", argv[0], sourceFile, strerror(errno));
				return 1;
			};
			
			char *data = readWholeFile(fp);
			if (data == NULL)
			{
				fprintf(stderr, "%s: failed to read `%s'\n", argv[0], sourceFile);
				return 1;
			};
			
			fclose(fp);
			
			Token *toklist = parsePerun(data, sourceFile);
			if (toklist == NULL)
			{
				return 1;
			};
			
			if (stopAtTokens)
			{
				lexDumpTokenList(toklist);
				return 0;
			};
			
			if (depFileName != NULL)
			{
				printf("%s: %s", depFileName, files[0]);
			};
			
			CompilationUnit *unit = compilePerun(toklist, 1);
			if (unit == NULL || conGetError())
			{
				return 1;
			};
			
			if (stopAtDefs)
			{
				printf("There are %d class definitions in this unit:\n\n", unit->numClasses);
				
				int j;
				for (j=0; j<unit->numClasses; j++)
				{
					ClassTemplate *tem = unit->classList[j];
					printf("  %s %s class %s:\n", protNames[tem->protLevel], getQualifNames(tem->flags), tem->fullname);
					printf("    Template parameters:\n");
					
					int k;
					for (k=0; k<tem->numTemParams; k++)
					{
						TemplateParameterSpec *spec = &tem->temParams[k];
						printf("      %s extends %s\n", spec->name, getTypeName(spec->minBase));
					};
					
					printf("    Direct explicit parents:\n");
					
					for (k=0; k<tem->numParents; k++)
					{
						printf("      %s\n", getTypeName(tem->parents[k]));
					};
					
					printf("    Fields:\n");
					
					ClassField *field;
					for (field=tem->fields; field!=NULL; field=field->next)
					{
						printf("     %s %s %s %s; // %s\n", protNames[field->prot], getQualifNames(field->flags),
									getTypeName(field->type), field->name, field->mangled);
					};
					
					printf("    Constructors:\n");
					ClassConstructor *ctor;
					for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
					{
						printf("     %s (%s); // %s\n", protNames[ctor->prot], getArgumentListFormatted(ctor->args), ctor->mangled);
					};
					
					printf("    Methods:\n");
					
					ClassMethod *method;
					for (method=tem->methods; method!=NULL; method=method->next)
					{
						printf("     %s %s %s %s(%s); // %s", protNames[method->prot], getQualifNames(method->flags),
									getTypeName(method->retType), method->name,
									getArgumentListFormatted(method->args), method->mangled);
						if (method->implementor != NULL)
						{
							printf(" (implemented by %s)", method->implementor);
						};
						printf("\n");
					};
				};
				
				return 0;
			};
			
			if (stopAtEmit)
			{
				emitPerun(unit, stdout);
				dumpTokenTagsIfRequested(toklist);
				return conGetError();
			};
			
			char *objectName;
			if (stopAtCompile)
			{
				objectName = outputFileName;
				if (objectName == NULL)
				{
					objectName = "perun-class.o";
				};
			}
			else
			{
				char buffer[256];
				generateRandomName(buffer, "%d.o");

				objectName = strdup(buffer);
			};
			
			char ctempfile[256];
			generateRandomName(ctempfile, "%d.c");
			
			FILE *cfile = fopen(ctempfile, "w");
			if (cfile == NULL)
			{
				fprintf(stderr, "failed to open temporary file `%s' for writing: %s\n", ctempfile, strerror(errno));
				return 1;
			};
			
			emitPerun(unit, cfile);
			fclose(cfile);
			
			dumpTokenTagsIfRequested(toklist);

			if (depFileName != NULL)
			{
				printf("\n");
				return 0;
			};
			
			if (conGetError())
			{
				remove(ctempfile);
				return 1;
			};
			
			if (cciCompile(ctempfile, objectName, makeShared) != 0)
			{
				return 1;
			};
			
			remove(ctempfile);
			
			if (stopAtCompile)
			{
				return 0;
			};
			
			cciAddLink(&ldreq, objectName);
		}
		else if (strlen(sourceFile) > 2 && strcmp(&sourceFile[strlen(sourceFile)-2], ".o") == 0)
		{
			if (!linking)
			{
				fprintf(stderr, "%s: object file specified in input file list, but i'm not linking\n", argv[0]);
				return 1;
			};
			
			cciAddLink(&ldreq, sourceFile);
		}
		else
		{
			fprintf(stderr, "%s: cannot detect file type based on extension of `%s'\n", argv[0], sourceFile);
			return 1;
		};
	};
	
	// if we are linking an executable, we must create the "global prolog"
	char prologName[256];
	if (!makeShared)
	{
		generateRandomName(prologName, "%d.c");
		
		FILE *fp = fopen(prologName, "w");
		if (fp == NULL)
		{
			fprintf(stderr, "%s: failed to open temporary file `%s' for writing: %s\n", argv[0], prologName, strerror(errno));
			return 1;
		};
		
		fprintf(fp, "// global prolog file, generated by the Madd Perun Compiler\n");
		fprintf(fp, "#include <perun_abi.h>\n\n");
		
		char *mainName = mangleName("Per_FD?4main", mainClass);
		fprintf(fp, "void %s();\n\n", mainName);
		
		fprintf(fp, "int main(int argc, char *argv[], char *envp[])\n");
		fprintf(fp, "{\n");
		fprintf(fp, "\tPer_Init(argc, argv, envp);\n");
		fprintf(fp, "\t%s();\n", mainName);
		fprintf(fp, "\tPer_Sys_ExitThread();\n");
		fprintf(fp, "\treturn 0;\n");
		fprintf(fp, "};\n");
		fclose(fp);
		
		cciAddLink(&ldreq, prologName);
	};
	
	for (i=0; i<numExtraLibs; i++)
	{
		cciAddLink(&ldreq, extraLibs[i]);
	};
	
	for (i=0; i<numStaticLibs; i++)
	{
		if (cciAddStatic(&ldreq, staticLibs[i]) != 0)
		{
			return 1;
		};
	};
	
	if (cciLink(&ldreq) == 0)
	{
		return 0;
	};
	
	return 1;
};
