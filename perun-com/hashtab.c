/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashtab.h"

static unsigned long hash(const char *key_)
{
	const unsigned char *key = (const unsigned char*) key_;
	
	unsigned long h = 0, g;
	while (*key)
	{
		h = (h << 4) + *key++;
		if (0 != (g = (h & 0xf0000000))) h ^= g >> 24;
		h &= 0x0fffffff;
	}
	return h;
}

HashTable* hashtabNew()
{
	HashTable *tab = (HashTable*) malloc(sizeof(HashTable));
	memset(tab, 0, sizeof(HashTable));
	return tab;
};

void hashtabSet(HashTable *tab, const char *key, void *value)
{
	unsigned long bi = hash(key) & (HASH_NUM_BUCKETS-1);
	HashEntry **hook = &tab->buckets[bi];
	
	while (*hook != NULL)
	{
		if (strcmp((*hook)->key, key) == 0)
		{
			(*hook)->value = value;
			return;
		};
		
		hook = &((*hook)->next);
	};
	
	HashEntry *newent = (HashEntry*) malloc(sizeof(HashEntry));
	memset(newent, 0, sizeof(HashEntry));
	newent->key = strdup(key);
	newent->value = value;
	*hook = newent;
};

void* hashtabGet(HashTable *tab, const char *key)
{
	unsigned long bi = hash(key) & (HASH_NUM_BUCKETS-1);
	
	HashEntry *ent;
	for (ent=tab->buckets[bi]; ent!=NULL; ent=ent->next)
	{
		if (strcmp(ent->key, key) == 0)
		{
			return ent->value;
		};
	};
	
	fprintf(stderr, "perun-com: hashtabGet(): cannot find key `%s'\n", key);
	abort();
};

int hashtabHasKey(HashTable *tab, const char *key)
{
	unsigned long bi = hash(key) & (HASH_NUM_BUCKETS-1);
	
	HashEntry *ent;
	for (ent=tab->buckets[bi]; ent!=NULL; ent=ent->next)
	{
		if (strcmp(ent->key, key) == 0)
		{
			return 1;
		};
	};
	
	return 0;
};

void hashtabUnset(HashTable *tab, const char *key)
{
	unsigned long bi = hash(key) & (HASH_NUM_BUCKETS-1);
	HashEntry **hook = &tab->buckets[bi];
	
	while (*hook != NULL)
	{
		if (strcmp((*hook)->key, key) == 0)
		{
			HashEntry *ent = *hook;
			*hook = ent->next;
			free(ent->key);
			free(ent);
			return;
		};
		
		hook = &((*hook)->next);
	};
};

void hashtabDelete(HashTable *tab)
{
	int i;
	for (i=0; i<HASH_NUM_BUCKETS; i++)
	{
		while (tab->buckets[i] != NULL)
		{
			HashEntry *ent = tab->buckets[i];
			tab->buckets[i] = ent->next;
			free(ent->key);
			free(ent);
		};
	};
	
	free(tab);
};

HashTable* hashtabClone(HashTable *tab)
{
	HashTable *new = hashtabNew();
	
	int i;
	for (i=0; i<HASH_NUM_BUCKETS; i++)
	{
		HashEntry *ent;
		for (ent=tab->buckets[i]; ent!=NULL; ent=ent->next)
		{
			hashtabSet(new, ent->key, ent->value);
		};
	};
	
	return new;
};

void hashtabIterInit(HashIterator *iter, HashTable *tab)
{
	iter->tab = tab;
	iter->bucket = 0;
	iter->ent = tab->buckets[0];

	while (iter->ent == NULL)
	{
		iter->bucket++;
		if (iter->bucket == HASH_NUM_BUCKETS) break;
		iter->ent = iter->tab->buckets[iter->bucket];
	};
};

void hashtabIterNext(HashIterator *iter)
{
	iter->ent = iter->ent->next;
	
	while (iter->ent == NULL)
	{
		iter->bucket++;
		if (iter->bucket == HASH_NUM_BUCKETS) break;
		iter->ent = iter->tab->buckets[iter->bucket];
	};
};