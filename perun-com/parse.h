/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PARSE_H_
#define PARSE_H_

#include "lex.h"
#include "hashtab.h"

/**
 * Class qualifiers.
 */
#define	PER_Q_STATIC				(1 << 0)
#define	PER_Q_ABSTRACT				(1 << 1)
#define	PER_Q_FINAL				(1 << 2)
#define	PER_Q_OVERRIDE				(1 << 3)

/**
 * Token types.
 */
enum
{
	/**
	 * Keywords.
	 */
	TOK_PER_NAMESPACE = TOK_USER_BASE,
	TOK_PER_IMPORT,
	TOK_PER_PRIVATE,
	TOK_PER_PROTECTED,
	TOK_PER_PUBLIC,
	TOK_PER_STATIC,
	TOK_PER_ABSTRACT,
	TOK_PER_FINAL,
	TOK_PER_OVERRIDE,
	TOK_PER_CLASS,
	TOK_PER_EXTENDS,
	TOK_PER_TRUE,
	TOK_PER_FALSE,
	TOK_PER_NULL,
	TOK_PER_IS,
	TOK_PER_NEW,
	TOK_PER_THIS,
	TOK_PER_IF,
	TOK_PER_ELSE,
	TOK_PER_WHILE,
	TOK_PER_FOR,
	TOK_PER_BREAK,
	TOK_PER_CONTINUE,
	TOK_PER_RETURN,
	TOK_PER_TRY,
	TOK_PER_CATCH,
	TOK_PER_THROW,
	TOK_PER_DO,
	TOK_PER_DESTRUCTOR,
	TOK_PER_EXTERN,
	TOK_PER_FUNCTION,
	TOK_PER_LAMBDA,
	TOK_PER_FOREACH,
	TOK_PER_TYPEDEF,

	/**
	 * Future reserved keywords.
	 */
	TOK_PER_CONST,
	TOK_PER_EXPORT,
	TOK_PER_SWITCH,
	TOK_PER_CASE,
	TOK_PER_DEFAULT,
	TOK_PER_ENUM,
	TOK_PER_STRUCT,
	TOK_PER_AUTO,
	
	/**
	 * Type names.
	 */
	TOK_PER_VOID,
	TOK_PER_BOOL,
	TOK_PER_FLOAT,
	TOK_PER_DOUBLE,
	TOK_PER_SBYTE,
	TOK_PER_BYTE,
	TOK_PER_SWORD,
	TOK_PER_WORD,
	TOK_PER_SDWORD,
	TOK_PER_INT,
	TOK_PER_DWORD,
	TOK_PER_SQWORD,
	TOK_PER_QWORD,
	TOK_PER_LONG,
	TOK_PER_SPTR,
	TOK_PER_PTR,
	
	/**
	 * Constants.
	 */
	TOK_PER_CONST_INT,
	TOK_PER_CONST_UINT,
	TOK_PER_CONST_LONG,
	TOK_PER_CONST_ULONG,
	TOK_PER_CONST_FLOAT,
	TOK_PER_CONST_DOUBLE,
	TOK_PER_CONST_STRING,
	
	/**
	 * Operators.
	 */
	TOK_PER_DOT,
	TOK_PER_SEMICOLON,
	TOK_PER_COLON,
	TOK_PER_LARROW,
	TOK_PER_RARROW,
	TOK_PER_COMMA,
	TOK_PER_SHL,
	TOK_PER_SHR,
	TOK_PER_LBRACE,
	TOK_PER_RBRACE,
	TOK_PER_AST,					/* asterisk (*) */\
	TOK_PER_DIV,
	TOK_PER_MOD,
	TOK_PER_ARRAY,					/* [] */
	TOK_PER_LPAREN,					/* ( */
	TOK_PER_RPAREN,					/* ) */
	TOK_PER_ASSIGN,					/* = */
	TOK_PER_ASSIGN_ADD,
	TOK_PER_ASSIGN_SUB,
	TOK_PER_ASSIGN_MUL,
	TOK_PER_ASSIGN_DIV,
	TOK_PER_ASSIGN_SHL,
	TOK_PER_ASSIGN_SHR,
	TOK_PER_ASSIGN_MOD,
	TOK_PER_ASSIGN_AND,
	TOK_PER_ASSIGN_OR,
	TOK_PER_ASSIGN_XOR,
	TOK_PER_COND,					/* ? */
	TOK_PER_LOR,					/* || */
	TOK_PER_LAND,					/* && */
	TOK_PER_OR,					/* | */
	TOK_PER_AND,					/* & */
	TOK_PER_XOR,					/* ^ */
	TOK_PER_EQ,					/* == */
	TOK_PER_NEQ,					/* != */
	TOK_PER_LAMBDA_ARROW,				/* => */
	TOK_PER_LESS_EQ,				/* <= */
	TOK_PER_MORE_EQ,				/* >= */
	TOK_PER_ADD,
	TOK_PER_SUB,
	TOK_PER_INC,					/* ++ */
	TOK_PER_DEC,					/* -- */
	TOK_PER_INV,					/* ~ */
	TOK_PER_NOT,					/* ! */
	TOK_PER_LSQ,					/* [ */
	TOK_PER_RSQ,					/* ] */
	TOK_PER_BVAR,					/* $ */
	TOK_PER_HASH,					/* # */
	TOK_PER_TYPE_ACCESS,				/* :: */
	
	/**
	 * Other stuff.
	 */
	TOK_PER_ID,
};

/**
 * Protection levels.
 */
enum
{
	PER_PROT_PRIVATE,
	PER_PROT_PROTECTED,
	PER_PROT_PUBLIC
};
extern const char* protNames[];

/**
 * Perun "type kinds".
 */
enum
{
	PER_TYPE_VOID,
	PER_TYPE_BOOL,
	PER_TYPE_FLOAT,
	PER_TYPE_DOUBLE,
	PER_TYPE_BYTE,
	PER_TYPE_SBYTE,
	PER_TYPE_WORD,
	PER_TYPE_SWORD,
	PER_TYPE_DWORD,
	PER_TYPE_SDWORD,
	PER_TYPE_QWORD,
	PER_TYPE_SQWORD,
	PER_TYPE_PTR,
	PER_TYPE_SPTR,
	PER_TYPE_MANAGED,
	PER_TYPE_ARRAY,
	PER_TYPE_TEMPLATE_PARAM,
	PER_TYPE_NULL,
	PER_TYPE_METHOD,
	PER_TYPE_CLASS,
	PER_TYPE_FUNCREF,
};

/**
 * Typedef all the structs here.
 */
typedef struct ClassField_ ClassField;
typedef struct ClassMethod_ ClassMethod;
typedef struct ClassSuperCall_ ClassSuperCall;
typedef struct ClassConstructor_ ClassConstructor;
typedef struct ClassTemplate_ ClassTemplate;
typedef struct Type_ Type;
typedef struct TemplateParameterSpec_ TemplateParameterSpec;
typedef struct ArgumentSpecificationList_ ArgumentSpecificationList;
typedef struct MethodSignature_ MethodSignature;
typedef struct TypeDef_ TypeDef;

/**
 * ArgumentSpecificationList ::=
 * 	TypeSpecification Identifier [',' ArgumentSpecificationList]
 */
struct ArgumentSpecificationList_
{
	Type *type;
	char *name;
	Token *tok;
	ArgumentSpecificationList *next;
};

/**
 * Template parameter specifier.
 */
struct TemplateParameterSpec_
{
	/**
	 * Name of the template parameter.
	 */
	const char *name;
	
	/**
	 * Minimum base type.
	 */
	Type *minBase;
	
	/**
	 * The token associated with the specification.
	 */
	Token *tok;
};

/**
 * Describes a field member of a class.
 */
struct ClassField_
{
	/**
	 * Next field.
	 */
	ClassField *next;
	
	/**
	 * Name of the field.
	 */
	const char *name;
	
	/**
	 * Type of the field.
	 */
	Type *type;
	
	/**
	 * Protection level (PER_PROT_*).
	 */
	int prot;
	
	/**
	 * Flags (qualifiers, PER_Q_*).
	 */
	int flags;
	
	/**
	 * For static fields, mangled name of the field.
	 * For non-static fields, mangled name of the Field Offset.
	 */
	const char *mangled;
	
	/**
	 * Token associated with the field.
	 */
	Token *tok;
	
	/**
	 * Initializer expression, or NULL if there isn't any.
	 */
	struct Expression_ *initExpr;
};

/**
 * Describes a method member of a class.
 */
struct ClassMethod_
{
	/**
	 * Next method.
	 */
	ClassMethod *next;
	
	/**
	 * Name of the method.
	 */
	const char *name;
	
	/**
	 * Return type.
	 */
	Type *retType;
	
	/**
	 * List of arguments.
	 */
	ArgumentSpecificationList *args;
	
	/**
	 * Protection level (PER_PROT_*).
	 */
	int prot;
	
	/**
	 * Flags (qualifiers, PER_Q_*).
	 */
	int flags;
	
	/**
	 * Set to 1 if this method overrides another.
	 */
	int overrides;
	
	/**
	 * Token associated with this method.
	 */
	Token *tok;
	
	/**
	 * Name of the virtual indicator, if this method is non-static.
	 * If static, then the mangled name of the function.
	 */
	const char *mangled;
	
	/**
	 * If this is a virtual method, this is the name of the static
	 * equivalent (its "implementor"). Otherwise, NULL. It is also NULL
	 * for abstract methods.
	 */
	const char *implementor;
	
	/**
	 * Method body or NULL (static methods only, including implementors).
	 */
	struct CompoundStatement_ *body;
	
	/**
	 * Source class template.
	 */
	ClassTemplate *tem;
};

/**
 * Describes a superconstructor call.
 */
struct ClassSuperCall_
{
	/**
	 * Next call.
	 */
	ClassSuperCall *next;
	
	/**
	 * The parent type.
	 */
	Type *parent;
	
	/**
	 * Token associated with the call.
	 */
	Token *tok;
	
	/**
	 * Argument passed.
	 */
	struct ArgumentExpressionList_ *args;
};

/**
 * Describes a constructor for a class.
 */
struct ClassConstructor_
{
	/**
	 * Next constructor.
	 */
	ClassConstructor *next;
	
	/**
	 * List of arguments.
	 */
	ArgumentSpecificationList *args;
	
	/**
	 * List of superconstructor calls.
	 */
	ClassSuperCall *superCalls;
	
	/**
	 * Protection level (PER_PROT_*).
	 */
	int prot;
	
	/**
	 * Token associated with this constructor.
	 */
	Token *tok;
	
	/**
	 * Constructor body, or NULL if not given.
	 */
	struct CompoundStatement_ *body;
	
	/**
	 * Mangled name of the constructor.
	 */
	const char *mangled;
};

/**
 * Describes a type definition inside a class.
 */
struct TypeDef_
{
	/**
	 * Next type definition.
	 */
	TypeDef *next;
	
	/**
	 * Token associated with this type definition.
	 */
	Token *tok;
	
	/**
	 * Protection level (PER_PROT_*).
	 */
	int prot;
	
	/**
	 * Name of this type.
	 */
	const char *name;
	
	/**
	 * The non-expanded type.
	 */
	Type *base;
};

/**
 * Describes a class template.
 */
struct ClassTemplate_
{
	/**
	 * Set to 1 if the class is complete.
	 */
	int complete;
	
	/**
	 * Fullname of the class.
	 */
	const char *fullname;

	/**
	 * Mangled name of the definition.
	 */
	const char *mangledName;

	/**
	 * Mangled name of the static lock.
	 */
	const char *staticLockName;
	
	/**
	 * Mangled name of the static initializer.
	 */
	const char *staticInitName;
	
	/**
	 * List of template parameters.
	 */
	TemplateParameterSpec *temParams;
	int numTemParams;
	
	/**
	 * List of parent classes. "std.rt.Object" is implicit and does not have to be
	 * on this list.
	 */
	Type **parents;
	int numParents;
	
	/**
	 * Class access level (PER_PROT_*).
	 */
	int protLevel;
	
	/**
	 * Class flags (bitwise-OR of PER_Q_*).
	 */
	int flags;
	
	/**
	 * A token associated with the definition.
	 */
	Token *tok;
	
	/**
	 * List of fields defined in this class (not its parents!)
	 */
	ClassField* fields;
	ClassField* lastField;
	
	/**
	 * List of methods defined in this class (not its parents!)
	 */
	ClassMethod *methods;
	ClassMethod *lastMethod;
	
	/**
	 * List of constructors.
	 */
	ClassConstructor *ctors;
	ClassConstructor *lastCtor;
	
	/**
	 * List of type definitions.
	 */
	TypeDef *typeDefs;
	TypeDef *lastTypeDef;
	
	/**
	 * Type name table used within the class (for generic type names).
	 */
	HashTable *typeNameTable;
	
	/**
	 * Destructor if defined, or NULL; and the token on which it was defined.
	 */
	struct CompoundStatement_ *dtor;
	Token *dtorTok;
};

/**
 * Method signature lists for the "method" type.
 */
struct MethodSignature_
{
	/**
	 * Next in the list.
	 */
	MethodSignature *next;
	
	/**
	 * Token associated with this signature.
	 */
	Token *tok;
	
	/**
	 * Set to nonzero if static.
	 */
	int isStatic;
	
	/**
	 * Mangled name of the implementor (if static) or the virtual
	 * function indicator (if virtual).
	 */
	const char *mangledName;
	
	/**
	 * Non-mangled method name, for diagnostic messages.
	 */
	const char *name;
	
	/**
	 * The return type.
	 */
	Type* retType;
	
	/**
	 * Number of arguments accepted by this signature, and their types.
	 */
	int numArgs;
	Type **argTypes;
};

/**
 * Type specifier.
 */
struct Type_
{
	/**
	 * The type-kind.
	 */
	int kind;
	
	/**
	 * For managed types, the fullname of the template class.
	 * For PER_TYPE_CLASS, the fullname of the class in question.
	 */
	const char *fullname;
	
	/**
	 * For arrays, the element type.
	 */
	Type *elementType;
	
	/**
	 * For template parameters, the minimum base type.
	 */
	Type *minBase;
	
	/**
	 * For managed types, hashtable mapping a template parameter name to the type
	 * used for it (Type*). Primitives types have already been converted to implicit
	 * managed wrappers prior to being placed in this list!
	 */
	HashTable *temParamTypes;
	
	/**
	 * For PER_TYPE_METHOD, the list of signatures.
	 */
	MethodSignature *sigs;
	
	/**
	 * For PER_TYPE_METHOD, the C expression evaluating to the "this"
	 * value. If this is NULL, then the method is static.
	 */
	char* thisExpr;
	
	/**
	 * For PER_TYPE_FUNCREF, the return type of the function.
	 */
	Type* retType;
	
	/**
	 * For PER_TYPE_FUNCREF, the number and types of arguments.
	 */
	int numArgs;
	Type** argTypes;
};

/**
 * Represents a compilation unit.
 */
typedef struct
{
	/**
	 * Token associated with the unit.
	 */
	Token *tok;
	
	/**
	 * Array of classes defined in the unit.
	 */
	ClassTemplate **classList;
	int numClasses;
} CompilationUnit;

/**
 * Describes a mapping between a primitive type and its implicit managed wrapper.
 */
typedef struct
{
	int prim;
	const char *man;
	const char *wrapFunc;
	const char *unwrapFunc;
} ManagedTypeMapping;

/**
 * The 'std.rt.Object' type.
 */
extern Type* typeObject;

/**
 * Initialize the parser system.
 */
void initParser();

/**
 * Return a list of tokens given a Perun source file name. On error, returns NULL and reports errors
 * to the console.
 */
Token* parsePerun(const char *data, const char *filename);

/**
 * Parse an argument specification list; prints the error to console on error and sets *setOnError.
 */
ArgumentSpecificationList* parseArgumentSpecificationList(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner, int *setOnError);

/**
 * Compile a compilation unit.
 */
CompilationUnit* compilePerun(Token *toklist, int mainTarget);

/**
 * Make a primitive type specification.
 */
Type* makePrimitiveType(int kind);

/**
 * Make an array type out of the specified element type.
 */
Type* makeArrayType(Type *elementType);

/**
 * Make a simple managed type (without template params).
 */
Type* makeSimpleManagedType(const char *fullname);

/**
 * Parse a type specification. 'inTemplate' is the class inside of which the type was referenced. 'typeNameTable' is the table
 * of type names. 'scanner' is a pointer to the token-pointer. It is shifted if the type was parsed successfully. The type is
 * then returned, or NULL if the specification is not valid.
 */
Type* parseTypeSpec(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner);

/**
 * Return a string describing the specified Type. The string is on the heap and should later be passed
 * to free().
 */
char* getTypeName(Type *type);

/**
 * Return a string describing the specified set of qualifiers. On the heap, call free() on it later.
 */
char* getQualifNames(int qualifs);

/**
 * Get a class by name. Returns the template if it is already in the class table. If not, finds the class
 * source code and adds it to the table, then returns it. If not found, prints an error to the console
 * and returns NULL. The token is the one with which to associate the import.
 */
ClassTemplate* findClassByName(Token *tok, const char *name);

/**
 * Get the type resulting from parametrizing the specified type with the specified template parameters.
 */
Type* expandType(Type *base, HashTable *params);

/**
 * Get information about an implicit managed type wrapper by primitive type kind; return NULL if not found.
 */
ManagedTypeMapping* getManagedWrapperByPrimitive(int prim);

/**
 * Returns nonzero if 'sub' is a subclass of 'parent'.
 */
int isSubtype(Type *parent, Type *sub);

/**
 * Returns nonzero if the specified type is managed.
 */
int isManagedType(Type *type);

/**
 * Returns nonzero if the specified type is an unsigned integer type.
 */
int isUnsignedIntegerType(Type *type);

/**
 * Returns nonzero if the specified type is a signed integer type.
 */
int isSignedIntegerType(Type *type);

/**
 * Returns nonzero if the specified type is an integer type.
 */
int isIntegerType(Type *type);

/**
 * Returns nonzero if the specified type is a floating-point type.
 */
int isFloatingType(Type *type);

/**
 * Returns nonzero if the specified type is a primitive type.
 */
int isPrimitiveType(Type *type);

/**
 * Returns nonzero if 'a' and 'b' refer to exactly the same data type.
 */
int isSameType(Type *a, Type *b);

/**
 * Returns nonzero if 'a' and 'b' are similar types (cannot differentiate overloads of a single method name).
 */
int areSimilarTypes(Type *a, Type *b);

/**
 * Returns nonzero if 'a' and 'b' are argument specification lists for matching method signatures.
 */
int isSameSignature(ArgumentSpecificationList *a, ArgumentSpecificationList *b);

/**
 * Get the "this" type for the specified class template.
 */
Type* getThisType(ClassTemplate *tem);

/**
 * Create a string describing an argument list. It is returned on the heap.
 */
char* getArgumentListFormatted(ArgumentSpecificationList *list);

/**
 * Parse a protection specifier token, returning PER_PROT_* on success, -1 if not a valid protection specifier.
 */
int parseProt(Token *tok);

/**
 * Parse qualifiers and shift the token scanner.
 */
int parseQualifs(Token **scanner);

#endif
