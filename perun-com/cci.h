/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CCI_H_
#define CCI_H_

/**
 * Represents a linking request.
 */
typedef struct
{
	char **argv;
	int argc;
} LinkRequest;

/**
 * Compile a C file (containing Perun code) into the specified object file. Return 0 on
 * success, -1 on error.
 */
int cciCompile(const char *csrc, const char *outfile, int shared);

/**
 * Begin a linking request. 'makeShared' specifies whether or not we are producing a dynamic
 * library. 'outputFile' is the name of the file we are outputting.
 */
void cciInitLink(LinkRequest *req, int makeShared, const char *outputFile);

/**
 * Add the specified object file (or C file) to the link request.
 */
void cciAddLink(LinkRequest *req, const char *filename);

/**
 * Add the specified Perun static library to the link. Return 0 on success, otherwise return
 * nonzero and print the error message.
 */
int cciAddStatic(LinkRequest *req, const char *filename);

/**
 * Perform the final link. Return 0 if successful, -1 on error.
 */
int cciLink(LinkRequest *req);

#endif
