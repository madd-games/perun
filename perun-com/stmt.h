/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STMT_H_
#define STMT_H_

#include "parse.h"
#include "expr.h"
#include "lex.h"

/**
 * Typedef all the structs here.
 */
typedef struct CompoundStatement_ CompoundStatement;
typedef struct StatementList_ StatementList;
typedef struct Statement_ Statement;
typedef struct NullStatement_ NullStatement;
typedef struct VariableDefinition_ VariableDefinition;
typedef struct IfStatement_ IfStatement;
typedef struct WhileStatement_ WhileStatement;
typedef struct ForStatement_ ForStatement;
typedef struct BreakStatement_ BreakStatement;
typedef struct ContinueStatement_ ContinueStatement;
typedef struct ReturnStatement_ ReturnStatement;
typedef struct TryCatchStatement_ TryCatchStatement;
typedef struct CatchList_ CatchList;
typedef struct ThrowStatement_ ThrowStatement;
typedef struct DoWhileStatement_ DoWhileStatement;
typedef struct ExpressionStatement_ ExpressionStatement;
typedef struct ForEachStatement_ ForEachStatement;

/**
 * ExpressionStatement ::=
 * 	Expression ';'
 */
struct ExpressionStatement_
{
	Expression *expr;
};

/**
 * DoWhileStatement ::=
 * 	'do' Statement 'while' '(' Expression ')'
 */
struct DoWhileStatement_
{
	Statement *body;
	Expression *ctrl;
	Token *tok;		// 'while'
};

/**
 * ThrowStatement ::=
 * 	'throw' Expression ';'
 */
struct ThrowStatement_
{
	Token *tok;
	Expression *expr;
};

/**
 * CatchList ::=
 * 	'catch' '(' TypeSpecification Identifier ')' [CatchList]
 */
struct CatchList_
{
	Token *tok;
	Type *type;
	Token *id;
	Statement *body;
	CatchList *next;
};

/**
 * TryCatchStatement ::=
 * 	'try' Statement CatchList
 */
struct TryCatchStatement_
{
	Token *tok;
	Statement *body;
	CatchList *catchList;
};

/**
 * ReturnStatement ::=
 * 	'return' [Expression] ';'
 */
struct ReturnStatement_
{
	Token *tok;
	Expression *expr;
};

/**
 * BreakStatement ::=
 * 	'break' ';'
 */
struct BreakStatement_
{
	Token *tok;
};

/**
 * ContinueStatement ::=
 * 	'continue' ';'
 */
struct ContinueStatement_
{
	Token *tok;
};

/**
 * ForStatement ::=
 * 	'for' '(' [Expression] ';' [Expression] ';' [Expression] ')' Statement
 */
struct ForStatement_
{
	Token *tok;
	Expression *init;
	Expression *cond;
	Expression *iter;
	Statement *body;
};

/**
 * WhileStatement ::=
 * 	'while' '(' Expression ')' Statement
 */
struct WhileStatement_
{
	Token *tok;
	Expression *ctrl;
	Statement *body;
};

/**
 * IfStatement ::=
 * 	'if' '(' Expression ')' Statement ['else' Statement]
 */
struct IfStatement_
{
	Token *tok;
	Expression *ctrl;
	Statement *body;
	Statement *other;
};

/**
 * ForEachStatement ::=
 * 	'foreach' '(' TypeSpecification Identifier ':' Expression ')' Statement
 */
struct ForEachStatement_
{
	Token *tok;
	Type *type;
	Token *id;
	Expression *expr;
	Statement *body;
};

/**
 * VariableDefinition ::=
 * 	TypeSpecification Identifier ['=' Expression] ';'
 */
struct VariableDefinition_
{
	Type *type;
	Token *id;
	Expression *init;
};

/**
 * NullStatement ::=
 * 	';'
 */
struct NullStatement_
{
	Token *tok;
};

/**
 * Statement ::=
 * 		CompoundStatement
 * 	|	NullStatement
 * 	|	VariableDefinition
 * 	|	IfStatement
 * 	|	WhileStatement
 * 	|	ForStatement
 * 	|	BreakStatement
 * 	|	ContinueStatement
 * 	|	ReturnStatement
 * 	|	TryCatchStatement
 * 	|	ThrowStatement
 * 	|	DoWhileStatement
 * 	|	ForEachStatement
 * 	|	ExpressionStatement
 */
struct Statement_
{
	// one of these is non-NULL:
	CompoundStatement *comp;
	NullStatement *null;
	VariableDefinition *vardef;
	IfStatement *ifs;
	WhileStatement *whiles;
	ForStatement *fors;
	BreakStatement *breaks;
	ContinueStatement *conts;
	ReturnStatement *rets;
	TryCatchStatement *trys;
	ThrowStatement *throws;
	DoWhileStatement *dos;
	ForEachStatement *foreach;
	ExpressionStatement *xs;
};

/**
 * StatementList ::=
 * 	Statement [StatementList]
 */
struct StatementList_
{
	Statement *st;
	StatementList *next;
};

/**
 * CompoundStatement ::=
 * 	'{' [StatementList] '}'
 */
struct CompoundStatement_
{
	StatementList *head;
};

/**
 * Represents a parser context.
 */
typedef struct
{
	/**
	 * The current token we are on.
	 */
	Token *tok;
	
	/**
	 * Token on which an error occured, and the error string, if applicable.
	 * 'errmsg' being NULL means there was no error.
	 */
	Token *errtok;
	char *errmsg;
	
	/**
	 * If set to 1, then the parser has aborted: no more error messages shall be
	 * reported.
	 */
	int aborted;
	
	/**
	 * The class definition inside of which the expression was found.
	 */
	ClassTemplate *inTemplate;
	
	/**
	 * The type name table.
	 */
	HashTable *typeNameTable;
} StmtParserContext;

/**
 * Initialize a statement parser context.
 */
void initStmtParser(StmtParserContext *ctx, Token *tok, ClassTemplate *inTemplate, HashTable *typeNameTable);

/**
 * These functions each accept a production of the statement grammar. They return the
 * corresponding parse tree node on success, or NULL on error. If an error occurs, then
 * an error message is also set.
 * 
 * If a production is accepted, the token in the context is shifted forward, past the
 * end of the production. If the production is not accepted, the token pointer shall
 * remain unchanged.
 */
ExpressionStatement* parseExpressionStatement(StmtParserContext *ctx);
DoWhileStatement* parseDoWhileStatement(StmtParserContext *ctx);
ThrowStatement* parseThrowStatement(StmtParserContext *ctx);
TryCatchStatement* parseTryCatchStatement(StmtParserContext *ctx);
ReturnStatement* parseReturnStatement(StmtParserContext *ctx);
BreakStatement* parseBreakStatement(StmtParserContext *ctx);
ContinueStatement* parseContinueStatement(StmtParserContext *ctx);
ForStatement* parseForStatement(StmtParserContext *ctx);
WhileStatement* parseWhileStatement(StmtParserContext *ctx);
IfStatement* parseIfStatement(StmtParserContext *ctx);
VariableDefinition* parseVariableDefinition(StmtParserContext *ctx);
NullStatement* parseNullStatement(StmtParserContext *ctx);
Statement* parseStatement(StmtParserContext *ctx);
CompoundStatement* parseCompoundStatement(StmtParserContext *ctx);

#endif
