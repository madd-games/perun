/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CATALOG_H_
#define CATALOG_H_

#include "lex.h"
#include "hashtab.h"

/**
 * Linked list of class fullnames, for the catalog.
 */
typedef struct ClassNameList_
{
	struct ClassNameList_ *next;
	const char *fullname;
} ClassNameList;

/**
 * Information about a class, extracted from the cataloging pass.
 */
typedef struct ClassInfo_ ClassInfo;
struct ClassInfo_
{
	/**
	 * Fullname of the class.
	 */
	const char *fullname;
	
	/**
	 * Points to classes which this one depends on.
	 */
	ClassInfo **deps;
	int numDeps;
	
	/**
	 * Number of template parameters and their names.
	 */
	int numTemplateParams;
	const char **paramNames;
};

/**
 * Add the specified directory to the class path.
 */
void addClassPath(const char *dir);

/**
 * Perform class cataloging. The specified path is the class being compiled,
 * and the catalog is produced based on that file. Return 0 on success, -1
 * on error. If there is indeed an error, a message will be printed to console
 * already.
 */
int doClassCatalog(const char *mainPath);

/**
 * Dump the class catalog.
 */
void dumpCatalog();

/**
 * Analyse a specific class and return information about it. Returns NULL if the
 * resolution was impossible due to a circular dependency.
 */
ClassInfo* doAnalysis(const char *className);

/**
 * Dump the dependencies of the specified class in "dot" format.
 */
void dumpDeps(const char *className);

/**
 * Get the list of classes in a specified namespace, or NULL if there aren't any.
 */
ClassNameList* getClassNamesInNamespaces(const char *namespace);

/**
 * Get the path to a class, or NULL if it doesn't exist.
 */
char* getClassFileName(const char *fullname);

#endif
