/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef LEX_H_
#define LEX_H_

#include <stdlib.h>
#include <stddef.h>
#include <inttypes.h>

/**
 * Matcher flags (must start from bit 8).
 */
#define	LEX_MATCH_NOGREED			(1 << 8)		/* '?' attached */
#define	LEX_MATCH_ASTERISK			(1 << 9)		/* '*' attached */
#define	LEX_MATCH_PLUS				(1 << 10)		/* '+' attached */

/**
 * Matcher types (bottom 8 bits of 'mode').
 */
#define	LEX_MATCH_EXACT				0
#define	LEX_MATCH_SET				1
#define	LEX_MATCH_BRACKET			2
#define	LEX_MATCH_END				3

/**
 * Types of tokens.
 */
#define	TOK_ENDLIST				-1		/* indicates end of TokenSpec list */
#define	TOK_END					0		/* automatically appended to end of list */
#define	TOK_WHITESPACE				1		/* whitespace: discard token */
#define	TOK_USER_BASE				10		/* 10+ = user-defined tokens */

/**
 * Types of tags.
 */
#define	TAG_DEFINE_VAR				"define_var"
#define	TAG_CLASS_NAME				"class_name"
#define	TAG_DEFINED_AT				"defined_at"
#define	TAG_VAR_NAME				"var_name"
#define	TAG_FIELD_NAME				"field_name"
#define	TAG_METHOD_NAME				"method_name"
#define	TAG_DEFINE_FIELD			"define_field"
#define	TAG_DEFINE_CLASS			"define_class"
#define	TAG_DEFINE_METHOD			"define_method"

/**
 * Describes an element of a regular expression.
 */
struct Regex_;
typedef union
{
	/**
	 * Type in bottom 8 bits, and then flags (such as LEX_MATCH_NOGREED, LEX_MATCH_ASTERISK, etc).
	 */
	int					mode;
	
	struct
	{
		int				mode;			/* & 0xFF == LEX_MATCH_EXACT */
		char				c;			/* character to match */
	} exact;
	
	struct
	{
		int				mode;			/* & 0xFF == LEX_MATCH_SET */
		unsigned char			bitmap[32];		/* bitmap of bytes that should match */
	} set;
	
	struct
	{
		int				mode;			/* & 0xFF == LEX_MATCH_BRACKET */
		struct Regex_*			first;
	} bracket;
} Matcher;

/**
 * Describes a compiled expression.
 */
typedef struct Regex_
{
	/**
	 * List of matchers.
	 */
	Matcher*				matcherList;
	size_t					numMatchers;
	
	/**
	 * The regex below this one, in a stack. Used internally by lexCompileRegex(), and the value
	 * must be considered meaningless and unsafe to use afterwards!
	 */
	struct Regex_*				down;
	
	/**
	 * Next regex; used to link chains of options in brackets.
	 */
	struct Regex_*				next;
} Regex;

/**
 * Token type specification. These are used as terminal symbols.
 */
typedef struct
{
	/**
	 * Token type number. User-defined types begin at 10. TOK_ENDLIST indicates end of the
	 * TokenSpec list.
	 */
	int					type;
	
	/**
	 * Regular expression for this token type.
	 */
	const char*				expr;
	
	/**
	 * Compiled expression; this is ignored on input and compiled internally
	 * by bnfCompileGrammar().
	 */
	Regex*					regex;
} TokenSpec;

/**
 * Token tag. Each token has a linked list of zero or more tags, which describe its semantic
 * properties. These are generated during the actual compilation/code generation stage, rather
 * than by the parser, and can be dumped by the user to get semantic information about a
 * project.
 */
struct Token_;
typedef struct TokenTag_
{
	/**
	 * Tag type; one of the TAG_* macros. This value is never freed because it's supposed to
	 * be one of a set of constants.
	 */
	const char*				type;
	
	/**
	 * The token with which this tag is associated (may be NULL).
	 */
	struct Token_*				otherToken;
	
	/**
	 * String argument (or NULL).
	 */
	char*					param;
	
	/**
	 * Next tag.
	 */
	struct TokenTag_*			next;
} TokenTag;

/**
 * Token in a linked list. The last entry (automatically appended) will have the type TOK_END,
 * empty string as the value, and actual end of file for the filename/lineno/col parameters;
 * and its 'next' pointer points back to itself!
 */
typedef struct Token_
{
	/**
	 * Link.
	 */
	struct Token_*				next;
	
	/**
	 * The name of the file and the position where this token came from.
	 */
	char*					filename;
	int					lineno;
	int					col;
	
	/**
	 * Type of token.
	 */
	int					type;
	
	/**
	 * Value (contents of the token which were matched).
	 */
	char*					value;
	
	/**
	 * Type of the previous token (even if it was TOK_WHITESPACE). If this is the first
	 * token, this is set to TOK_END. This is basically used for context-sensitive grammars
	 * and stuff.
	 */
	int					prevType;
	
	/**
	 * The token from which this one was expanded, if applicable; NULL otherwise.
	 */
	struct Token_*				parent;
	
	/**
	 * Tags associated with this token.
	 */
	struct TokenTag_*			tags;
} Token;

/**
 * Compile a regular expression and return the description, which may later be used to match.
 * Returns NULL if the expression is invalid.
 */
Regex* lexCompileRegex(const char *spec);

/**
 * Delete a compiled expression.
 */
void lexDeleteRegex(Regex *regex);

/**
 * Match a compiled regular expression against a string. Returns -1 if there is no match; otherwise,
 * returns the number of characters matched (which may be 0).
 */
ssize_t lexMatch(Regex *regex, const char *str);

/**
 * Convert an escape sequence character into the correct character value. Returns -1 if not
 * recognised.
 */
int lexGetCharByEscape(char escape);

/**
 * Parse a string token (C-style), and store in the given buffer. The buffer must have a size at
 * least equal to the token. Returns 0 on success, or a character indicating a broken escape
 * sequence, or -1 if the string is invalid (no quotes).
 */
char lexParseString(const char *token, char *buffer);

/**
 * Compile token specifications.
 */
void lexCompileTokenSpecs(TokenSpec *specs);

/**
 * Tokenize an input string, coming from the specified file. Returns the token list on success.
 * On failure, prints an error message and returns NULL.
 */
Token* lexTokenize(TokenSpec *specs, const char *data, const char *filename);

/**
 * Print a diagnostic about a token.
 */
void lexDiag(Token *tok, int level, const char *format, ...);

/**
 * Print out a list of tokens to standard output.
 */
void lexDumpTokenList(Token *tok);

/**
 * Delete a list of tokens.
 */
void lexDeleteTokenList(Token *tok);

/**
 * Add a tag to the token.
 */
void lexTag(Token *tok, const char *type, Token *otherToken, const char *param);

#endif