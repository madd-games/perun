/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN COpaNTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "parse.h"
#include "mangle.h"
#include "expr.h"
#include "stmt.h"
#include "catalog.h"
#include "makedeps.h"
#include "buildvars.h"
#include "console.h"

/**
 * Preprocessor state.
 */
typedef struct PreprocState_ PreprocState;
struct PreprocState_
{
	PreprocState* down;
	int truth;
	int inConditionalBlock;
};

/**
 * Token specifications for Perun.
 */
static TokenSpec perTokens[] = {
	// KEYWORD MUST GO FIRST
	{TOK_PER_NAMESPACE,				"namespace"},
	{TOK_PER_IMPORT,				"import"},
	{TOK_PER_PRIVATE,				"private"},
	{TOK_PER_PROTECTED,				"protected"},
	{TOK_PER_PUBLIC,				"public"},
	{TOK_PER_STATIC,				"static"},
	{TOK_PER_ABSTRACT,				"abstract"},
	{TOK_PER_FINAL,					"final"},
	{TOK_PER_OVERRIDE,				"override"},
	{TOK_PER_CLASS,					"class"},
	{TOK_PER_EXTENDS,				"extends"},
	{TOK_PER_TRUE,					"true"},
	{TOK_PER_FALSE,					"false"},
	{TOK_PER_NULL,					"null"},
	{TOK_PER_IS,					"is"},
	{TOK_PER_NEW,					"new"},
	{TOK_PER_THIS,					"this"},
	{TOK_PER_IF,					"if"},
	{TOK_PER_ELSE,					"else"},
	{TOK_PER_WHILE,					"while"},
	{TOK_PER_FOR,					"for"},
	{TOK_PER_BREAK,					"break"},
	{TOK_PER_CONTINUE,				"continue"},
	{TOK_PER_RETURN,				"return"},
	{TOK_PER_TRY,					"try"},
	{TOK_PER_CATCH,					"catch"},
	{TOK_PER_THROW,					"throw"},
	{TOK_PER_DO,					"do"},
	{TOK_PER_DESTRUCTOR,				"destructor"},
	{TOK_PER_EXTERN,				"extern"},
	{TOK_PER_FUNCTION,				"function"},
	{TOK_PER_LAMBDA,				"lambda"},
	{TOK_PER_FOREACH,				"foreach"},
	{TOK_PER_TYPEDEF,				"typedef"},
	
	// FUTURE RESERVED KEYWORDS
	{TOK_PER_CONST,					"const"},
	{TOK_PER_EXPORT,				"export"},
	{TOK_PER_SWITCH,				"switch"},
	{TOK_PER_CASE,					"case"},
	{TOK_PER_DEFAULT,				"default"},
	{TOK_PER_ENUM,					"enum"},
	{TOK_PER_STRUCT,				"struct"},
	{TOK_PER_AUTO,					"auto"},
	
	// TYPE NAMES
	{TOK_PER_VOID,					"void"},
	{TOK_PER_BOOL,					"bool"},
	{TOK_PER_FLOAT,					"float"},
	{TOK_PER_DOUBLE,				"double"},
	{TOK_PER_BYTE,					"byte_t"},
	{TOK_PER_SBYTE,					"sbyte_t"},
	{TOK_PER_WORD,					"word_t"},
	{TOK_PER_SWORD,					"sword_t"},
	{TOK_PER_DWORD,					"dword_t"},
	{TOK_PER_SDWORD,				"sdword_t"},
	{TOK_PER_QWORD,					"qword_t"},
	{TOK_PER_SQWORD,				"sqword_t"},
	{TOK_PER_INT,					"int"},
	{TOK_PER_LONG,					"long"},
	{TOK_PER_PTR,					"ptr_t"},
	{TOK_PER_SPTR,					"sptr_t"},
	
	// CONSTANTS
	{TOK_PER_CONST_INT,				"(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)"},
	{TOK_PER_CONST_UINT,				"(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU]"},
	{TOK_PER_CONST_LONG,				"(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[lL]"},
	{TOK_PER_CONST_ULONG,				"(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU][lL]"},
	{TOK_PER_CONST_DOUBLE,				"[0-9]*\\.[0-9]+([eE][-+][0-9]+)?"},
	{TOK_PER_CONST_FLOAT,				"[0-9]*\\.[0-9]+([eE][-+][0-9]+)?[Ff]"},
	{TOK_PER_CONST_STRING,				"\\\"(\\\\.|.)*?\\\""},
	
	// OPERATORS
	{TOK_PER_DOT,					"\\."},
	{TOK_PER_COMMA,					"\\,"},
	{TOK_PER_SEMICOLON,				"\\;"},
	{TOK_PER_COLON,					"\\:"},
	{TOK_PER_LARROW,				"\\<"},
	{TOK_PER_RARROW,				"\\>"},
	{TOK_PER_SHL,					"\\<\\<"},
	{TOK_PER_SHR,					"\\>\\>"},
	{TOK_PER_LBRACE,				"\\{"},
	{TOK_PER_RBRACE,				"\\}"},
	{TOK_PER_AST,					"\\*"},
	{TOK_PER_DIV,					"\\/"},
	{TOK_PER_MOD,					"\\%"},
	{TOK_PER_ARRAY,					"\\[\\]"},
	{TOK_PER_LPAREN,				"\\("},
	{TOK_PER_RPAREN,				"\\)"},
	{TOK_PER_ASSIGN,				"\\="},
	{TOK_PER_ASSIGN_ADD,				"\\+\\="},
	{TOK_PER_ASSIGN_AND,				"\\&\\="},
	{TOK_PER_ASSIGN_DIV,				"\\/\\="},
	{TOK_PER_ASSIGN_MOD,				"\\%\\="},
	{TOK_PER_ASSIGN_MUL,				"\\*\\="},
	{TOK_PER_ASSIGN_OR,				"\\|\\="},
	{TOK_PER_LAMBDA_ARROW,				"\\=\\>"},
	{TOK_PER_ASSIGN_SHL,				"\\<\\<\\="},
	{TOK_PER_ASSIGN_SHR,				"\\>\\>\\="},
	{TOK_PER_ASSIGN_SUB,				"\\-\\="},
	{TOK_PER_ASSIGN_XOR,				"\\^\\="},
	{TOK_PER_COND,					"\\?"},
	{TOK_PER_LOR,					"\\|\\|"},
	{TOK_PER_LAND,					"\\&\\&"},
	{TOK_PER_OR,					"\\|"},
	{TOK_PER_AND,					"\\&"},
	{TOK_PER_XOR,					"\\^"},
	{TOK_PER_EQ,					"\\=\\="},
	{TOK_PER_NEQ,					"\\!\\="},
	{TOK_PER_LESS_EQ,				"\\<\\="},
	{TOK_PER_MORE_EQ,				"\\>\\="},
	{TOK_PER_ADD,					"\\+"},
	{TOK_PER_SUB,					"\\-"},
	{TOK_PER_INC,					"\\+\\+"},
	{TOK_PER_DEC,					"\\-\\-"},
	{TOK_PER_INV,					"\\~"},
	{TOK_PER_NOT,					"\\!"},
	{TOK_PER_LSQ,					"\\["},
	{TOK_PER_RSQ,					"\\]"},
	{TOK_PER_BVAR,					"\\$"},
	{TOK_PER_HASH,					"\\#"},
	{TOK_PER_TYPE_ACCESS,				"\\:\\:"},
	
	// OTHER STUFF
	{TOK_PER_ID,					"[_a-zA-Z][_0-9a-zA-Z]*"},
	
	// WHITESPACE
	{TOK_WHITESPACE,				"[ \t\n]+"},
	{TOK_WHITESPACE,				"\x0C"},
	{TOK_WHITESPACE,				"\\/\\*.*?(\\*\\/)"},
	{TOK_WHITESPACE,				"\\/\\/.*?\n"},

	// LIST TERMINATOR
	{TOK_ENDLIST},
};

/**
 * List of mappings between primitive types and their implicit managed wrappers, and the function name
 * to wrap and unwrap.
 */
ManagedTypeMapping managedTypeMappings[] = {
	{PER_TYPE_BYTE,		"std.rt.UnsignedInteger",	"Per_WrapUnsigned",		"Per_UnwrapUnsigned"},
	{PER_TYPE_SBYTE,	"std.rt.Integer",		"Per_WrapSigned",		"Per_UnwrapSigned"},
	{PER_TYPE_WORD,		"std.rt.UnsignedInteger",	"Per_WrapUnsigned",		"Per_UnwrapUnsigned"},
	{PER_TYPE_SWORD,	"std.rt.Integer",		"Per_WrapSigned",		"Per_UnwrapSigned"},
	{PER_TYPE_DWORD,	"std.rt.UnsignedInteger",	"Per_WrapUnsigned",		"Per_UnwrapUnsigned"},
	{PER_TYPE_SDWORD,	"std.rt.Integer",		"Per_WrapSigned",		"Per_UnwrapSigned"},
	{PER_TYPE_QWORD,	"std.rt.UnsignedInteger",	"Per_WrapUnsigned",		"Per_UnwrapUnsigned"},
	{PER_TYPE_SQWORD,	"std.rt.Integer",		"Per_WrapSigned",		"Per_UnwrapSigned"},
	{PER_TYPE_PTR,		"std.rt.UnsignedInteger",	"Per_WrapUnsigned",		"Per_UnwrapUnsigned"},
	{PER_TYPE_SPTR,		"std.rt.Integer",		"Per_WrapSigned",		"Per_UnwrapSigned"},
	{PER_TYPE_BOOL,		"std.rt.Boolean",		"Per_WrapBoolean",		"Per_UnwrapBoolean"},
	{PER_TYPE_FLOAT,	"std.rt.Float",			"Per_WrapFloat",		"Per_UnwrapFloat"},
	{PER_TYPE_DOUBLE,	"std.rt.Float",			"Per_WrapFloat",		"Per_UnwrapFloat"},
	
	// LIST TERMINATOR
	{0,			NULL}
};

const char* protNames[] = {
	"private",
	"protected",
	"public"
};

/**
 * Hash table mapping class fullnames to their definitions (ClassTemplate*).
 */
HashTable* classTable;

/**
 * The 'Object' type.
 */
Type* typeObject;

void initParser()
{
	classTable = hashtabNew();
	
	typeObject = (Type*) calloc(1, sizeof(Type));
	typeObject->kind = PER_TYPE_MANAGED;
	typeObject->fullname = "std.rt.Object";
};

static int isPreprocConditionTrue(const char *value, const char *expected)
{
	// NOTE: this function MUST return 0 or 1, not just zero vs nonzero
	if (value == NULL)
	{
		// variable does not exist, not true
		return 0;
	};
	
	if (expected == NULL)
	{
		// variable exists and we aren't expecting any specific value, so true
		return 1;
	};
	
	// otherwise, they must be the same
	return strcmp(value, expected) == 0;
};

static void emitToken(Token **firstPtr, Token **lastPtr, Token *tok)
{
	Token *newtok = (Token*) malloc(sizeof(Token));
	memcpy(newtok, tok, sizeof(Token));
	newtok->next = NULL;
	
	if (*firstPtr == NULL)
	{
		(*firstPtr) = (*lastPtr) = newtok;
	}
	else
	{
		(*lastPtr)->next = newtok;
		*lastPtr = newtok;
	};
};

static Token* preprocess(Token *chain)
{
	Token *first = NULL;
	Token *last = NULL;
	
	PreprocState *state = (PreprocState*) malloc(sizeof(PreprocState));
	state->truth = 1;
	state->inConditionalBlock = 0;
	state->down = NULL;
	
	Token *tok;
	for (tok=chain; tok->type!=TOK_END; tok=tok->next)
	{
		if (tok->type == TOK_PER_LBRACE)
		{
			PreprocState *newState = (PreprocState*) malloc(sizeof(PreprocState));
			newState->truth = state->truth;
			newState->inConditionalBlock = 0;
			newState->down = state;
			state = newState;
			
			if (state->truth) emitToken(&first, &last, tok);
		}
		else if (tok->type == TOK_PER_RBRACE)
		{
			if (state->down == NULL)
			{
				lexDiag(tok, CON_ERROR, "unexpected `}'");
				return NULL;
			};
			
			if (!state->inConditionalBlock && state->truth)
			{
				emitToken(&first, &last, tok);
			};
			
			PreprocState *prevState = state;
			state = prevState->down;
			free(prevState);
		}
		else if (tok->type == TOK_PER_HASH)
		{
			tok = tok->next;
			
			int negate = 0;
			if (tok->type == TOK_PER_NOT)
			{
				negate = 1;
				tok = tok->next;
			};
			
			if (tok->type != TOK_PER_ID)
			{
				lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
				return NULL;
			};
			const char *varname = tok->value;
			tok = tok->next;
			
			const char *expectedValue = NULL;
			if (tok->type == TOK_PER_EQ)
			{
				tok = tok->next;
				expectedValue = tok->value;
				tok = tok->next;
			};
			
			if (tok->type != TOK_PER_LBRACE)
			{
				lexDiag(tok, CON_ERROR, "expected `{', not `%s'", tok->value);
				return NULL;
			};
			
			const char *value = bvarGet(varname);
			
			PreprocState *newState = (PreprocState*) malloc(sizeof(PreprocState));
			newState->truth = (isPreprocConditionTrue(value, expectedValue) ^ negate) && state->truth;
			newState->inConditionalBlock = 1;
			newState->down = state;
			state = newState;
		}
		else
		{
			if (state->truth) emitToken(&first, &last, tok);
		};
	};
	
	if (state->down != NULL)
	{
		lexDiag(tok, CON_ERROR, "unterminated brace");
		return NULL;
	};
	
	free(state);
	
	// emit the end token
	emitToken(&first, &last, tok);
	last->next = last;
	return first;
};

Token* parsePerun(const char *data, const char *filename)
{
	if (perTokens[0].regex == NULL)
	{
		lexCompileTokenSpecs(perTokens);
	};
	
	Token *result = lexTokenize(perTokens, data, filename);
	if (result != NULL)
	{
		result = preprocess(result);
	};
	
	return result;
};

int parseProt(Token *tok)
{
	switch (tok->type)
	{
	case TOK_PER_PRIVATE:			return PER_PROT_PRIVATE;
	case TOK_PER_PROTECTED:			return PER_PROT_PROTECTED;
	case TOK_PER_PUBLIC:			return PER_PROT_PUBLIC;
	default:				return -1;
	};
};

int parseQualifs(Token **scanner)
{
	int flags = 0;
	
	while (1)
	{
		switch ((*scanner)->type)
		{
		case TOK_PER_STATIC:
			flags |= PER_Q_STATIC;
			break;
		case TOK_PER_ABSTRACT:
			flags |= PER_Q_ABSTRACT;
			break;
		case TOK_PER_FINAL:
			flags |= PER_Q_FINAL;
			break;
		case TOK_PER_OVERRIDE:
			flags |= PER_Q_OVERRIDE;
			break;
		default:
			return flags;
		};
		
		*scanner = (*scanner)->next;
	};
};

static int isTemplateParameterName(ClassTemplate *tem, const char *name)
{
	int i;
	for (i=0; i<tem->numTemParams; i++)
	{
		TemplateParameterSpec *spec = &tem->temParams[i];
		if (strcmp(spec->name, name) == 0)
		{
			return 1;
		};
	};
	
	return 0;
};

ManagedTypeMapping* getManagedWrapperByPrimitive(int prim)
{
	ManagedTypeMapping *map;
	for (map=managedTypeMappings; map->prim!=0; map++)
	{
		if (map->prim == prim) return map;
	};
	
	return NULL;
};

static void wrapPrimitiveType(Type *type)
{
	// if the specified type is primitive, convert it to its managed wrapper
	ManagedTypeMapping *map = managedTypeMappings;
	for (; map->prim!=0; map++)
	{
		if (map->prim == type->kind)
		{
			type->kind = PER_TYPE_MANAGED;
			type->fullname = map->man;
			type->temParamTypes = hashtabNew();
			break;
		};
	};
};

Type* expandType(Type *base, HashTable *params)
{
	if (base->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		return hashtabGet(params, base->fullname);
	};
	
	if (base->kind == PER_TYPE_ARRAY)
	{
		return makeArrayType(expandType(base->elementType, params));
	};
	
	if (base->kind == PER_TYPE_FUNCREF)
	{
		Type *result = (Type*) calloc(1, sizeof(Type));
		result->kind = PER_TYPE_FUNCREF;
		result->retType = expandType(base->retType, params);
		result->numArgs = base->numArgs;
		result->argTypes = (Type**) malloc(sizeof(void*) * base->numArgs);
		
		int i;
		for (i=0; i<base->numArgs; i++)
		{
			result->argTypes[i] = expandType(base->argTypes[i], params);
		};
		
		return result;
	};
	
	if (base->kind != PER_TYPE_MANAGED)
	{
		return base;
	};
	
	Type *ex = (Type*) calloc(1, sizeof(Type));
	ex->kind = PER_TYPE_MANAGED;
	ex->fullname = base->fullname;
	ex->temParamTypes = hashtabNew();
	
	const char *key;
	HASHTAB_FOREACH(base->temParamTypes, Type*, key, par)
	{
		hashtabSet(ex->temParamTypes, key, expandType(par, params));
	};
	
	return ex;
};

int isSubtype(Type *parent, Type *sub)
{
	if ((parent->kind == PER_TYPE_TEMPLATE_PARAM || parent->kind == PER_TYPE_MANAGED || parent->kind == PER_TYPE_ARRAY) && sub->kind == PER_TYPE_NULL)
	{
		return 1;
	};
	
	if (parent->kind == PER_TYPE_FUNCREF)
	{
		if (sub->kind == PER_TYPE_NULL)
		{
			return 1;
		};
		
		return isSameType(parent, sub);
	};
	
	if (parent->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		if (sub->kind != PER_TYPE_TEMPLATE_PARAM) return 0;
		return (strcmp(parent->fullname, sub->fullname) == 0);
	};
	
	if (sub->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		return isSubtype(parent, sub->minBase);
	};
	
	if (parent->kind == PER_TYPE_ARRAY)
	{
		if (sub->kind != PER_TYPE_ARRAY) return 0;
		return isSubtype(parent->elementType, sub->elementType);
	};
	
	if (parent->kind != PER_TYPE_MANAGED || sub->kind != PER_TYPE_MANAGED)
	{
		return (parent->kind == sub->kind);
	};
	
	// std.rt.Object is implicitly the parent of all classes
	if (strcmp(parent->fullname, "std.rt.Object") == 0)
	{
		return 1;
	};
	
	ClassTemplate *tem = (ClassTemplate*) hashtabGet(classTable, parent->fullname);
	
	// check if this is the class itself
	if (strcmp(tem->fullname, sub->fullname) == 0)
	{
		// all template parameters must also be subclasses relative to the parent
		int i;
		for (i=0; i<tem->numTemParams; i++)
		{
			if (!isSubtype(hashtabGet(parent->temParamTypes, tem->temParams[i].name), hashtabGet(sub->temParamTypes, tem->temParams[i].name)))
			{
				return 0;
			};
		};
		
		return 1;
	};
	
	// go through the list of parents and recursively check for subclass
	tem = (ClassTemplate*) hashtabGet(classTable, sub->fullname);
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *candidate = expandType(tem->parents[i], sub->temParamTypes);
		if (isSubtype(parent, candidate)) return 1;
	};
	
	return 0;
};

int isHalfSimilarType(Type *parent, Type *sub)
{
	if (parent->kind == PER_TYPE_TEMPLATE_PARAM || sub->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		return 1;
	};
	
	if (parent->kind == PER_TYPE_FUNCREF)
	{
		return isSameType(parent, sub);
	};
	
	if (parent->kind != PER_TYPE_MANAGED || sub->kind != PER_TYPE_MANAGED)
	{
		return (parent->kind == sub->kind);
	};
	
	// std.rt.Object is implicitly the parent of all classes
	if (strcmp(parent->fullname, "std.rt.Object") == 0)
	{
		return 1;
	};
	
	ClassTemplate *tem = hashtabGet(classTable, parent->fullname);
	
	// check if this is the class itself
	if (strcmp(tem->fullname, sub->fullname) == 0)
	{
		// all template parameters must also be subclasses relative to the parent
		int i;
		for (i=0; i<tem->numTemParams; i++)
		{
			if (!isSubtype(hashtabGet(parent->temParamTypes, tem->temParams[i].name), hashtabGet(sub->temParamTypes, tem->temParams[i].name)))
			{
				return 0;
			};
		};
		
		return 1;
	};
	
	// go through the list of parents and recursively check for subclass
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *candidate = expandType(tem->parents[i], parent->temParamTypes);
		if (isSubtype(candidate, sub)) return 1;
	};
	
	return 0;
};

int areSimilarTypes(Type *a, Type *b)
{
	return isHalfSimilarType(a, b) || isHalfSimilarType(b, a);
};

Type* makePrimitiveType(int kind)
{
	Type *type = (Type*) calloc(1, sizeof(Type));
	type->kind = kind;
	return type;
};

Type* makeArrayType(Type *elementType)
{
	Type *type = (Type*) calloc(1, sizeof(Type));
	type->kind = PER_TYPE_ARRAY;
	type->elementType = elementType;
	return type;
};

Type* makeSimpleManagedType(const char *fullname)
{
	Type *type = (Type*) calloc(1, sizeof(Type));
	type->kind = PER_TYPE_MANAGED;
	type->fullname = fullname;
	type->temParamTypes = hashtabNew();
	return type;
};

Type* parseTypeSpec(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner);

static Type* parseRootTypeSpec(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner)
{
	Token *tok = *scanner;
	Type *type = (Type*) calloc(1, sizeof(Type));
	
	// first check if this is a primitive type
	switch (tok->type)
	{
	case TOK_PER_VOID:
		type->kind = PER_TYPE_VOID;
		*scanner = tok->next;
		return type;
	case TOK_PER_FLOAT:
		type->kind = PER_TYPE_FLOAT;
		*scanner = tok->next;
		return type;
	case TOK_PER_DOUBLE:
		type->kind = PER_TYPE_DOUBLE;
		*scanner = tok->next;
		return type;
	case TOK_PER_BYTE:
		type->kind = PER_TYPE_BYTE;
		*scanner = tok->next;
		return type;
	case TOK_PER_SBYTE:
		type->kind = PER_TYPE_SBYTE;
		*scanner = tok->next;
		return type;
	case TOK_PER_WORD:
		type->kind = PER_TYPE_WORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_SWORD:
		type->kind = PER_TYPE_SWORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_DWORD:
		type->kind = PER_TYPE_DWORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_SDWORD:
	case TOK_PER_INT:
		type->kind = PER_TYPE_SDWORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_QWORD:
		type->kind = PER_TYPE_QWORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_SQWORD:
	case TOK_PER_LONG:
		type->kind = PER_TYPE_SQWORD;
		*scanner = tok->next;
		return type;
	case TOK_PER_PTR:
		type->kind = PER_TYPE_PTR;
		*scanner = tok->next;
		return type;
	case TOK_PER_SPTR:
		type->kind = PER_TYPE_SPTR;
		*scanner = tok->next;
		return type;
	case TOK_PER_BOOL:
		type->kind = PER_TYPE_BOOL;
		*scanner = tok->next;
		return type;
	};
	
	// function reference type
	if (tok->type == TOK_PER_FUNCTION)
	{
		tok = tok->next;
		type->kind = PER_TYPE_FUNCREF;
		
		if (tok->type != TOK_PER_LARROW)
		{
			lexDiag(tok, CON_ERROR, "expected `<' after `function', not `%s'", tok->value);
			free(type);
			return NULL;
		};
		tok = tok->next; // skip over the '<'
		
		Type *retType = parseTypeSpec(inTemplate, typeNameTable, &tok);
		if (retType == NULL)
		{
			free(type);
			return NULL;
		};
		
		type->retType = retType;
		
		if (tok->type == TOK_PER_COLON)
		{
			tok = tok->next;
			
			while (1)
			{
				Token *argtok = tok;
				
				Type *argtype = parseTypeSpec(inTemplate, typeNameTable, &tok);
				if (argtype == NULL)
				{
					free(type);
					return NULL;
				};
				
				if (argtype->kind == PER_TYPE_VOID)
				{
					lexDiag(argtok, CON_ERROR, "argument type cannot be void");
					free(type);
					return NULL;
				};
				
				int index = type->numArgs++;
				type->argTypes = (Type**) realloc(type->argTypes, sizeof(void*) * type->numArgs);
				type->argTypes[index] = argtype;
				
				if (tok->type == TOK_PER_ID)
				{
					// optional argument name, skip
					tok = tok->next;
				};
				
				if (tok->type == TOK_PER_COMMA)
				{
					tok = tok->next;	// skip over the ','
				}
				else if (tok->type == TOK_PER_RARROW)
				{
					break;
				}
				else
				{
					lexDiag(tok, CON_ERROR, "expected `,' or '>', not `%s'", tok->value);
					free(type);
					return NULL;
				};
			};
		};
		
		if (tok->type != TOK_PER_RARROW)
		{
			lexDiag(tok, CON_ERROR, "expected `>' not `%s'", tok->value);
			free(type);
			return NULL;
		};
		tok = tok->next;
		
		*scanner = tok;
		return type;
	};
	
	// otherwise, it must be a managed type
	if (tok->type != TOK_PER_ID)
	{
		free(type);
		return NULL;
	};
	
	const char *typeName = tok->value;
	
	if (isTemplateParameterName(inTemplate, typeName))
	{
		*scanner = tok->next;
		type->kind = PER_TYPE_TEMPLATE_PARAM;
		type->fullname = typeName;

		int i;
		for (i=0; i<inTemplate->numTemParams; i++)
		{
			TemplateParameterSpec *spec = &inTemplate->temParams[i];
			if (strcmp(spec->name, typeName) == 0)
			{
				type->minBase = spec->minBase;
				
				lexTag(tok, TAG_CLASS_NAME, NULL, NULL);
				lexTag(tok, TAG_DEFINED_AT, spec->tok, NULL);
			};
		};

		return type;
	};
	
	if (!hashtabHasKey(typeNameTable, typeName))
	{
		free(type);
		return NULL;
	};

	// TODO: add the type to a "post-check list" to verify that the types given in parameters are
	// in fact subtypes of the minimum base types
	ClassTemplate *tem = (ClassTemplate*) hashtabGet(typeNameTable, typeName);
	ClassInfo *info = doAnalysis(tem->fullname);
	
	lexTag(tok, TAG_CLASS_NAME, NULL, tem->fullname);
	lexTag(tok, TAG_DEFINED_AT, tem->tok, NULL);
	
	type->kind = PER_TYPE_MANAGED;
	type->fullname = tem->fullname;
	type->temParamTypes = hashtabNew();
	
	tok = tok->next;
	
	if (info->numTemplateParams != 0)
	{
		if (tok->type != TOK_PER_LARROW)
		{
			lexDiag(tok, CON_ERROR, "expected `<', not `%s', because `%s' requires template parameters", tok->value, tem->fullname);
			hashtabDelete(type->temParamTypes);
			free(type);
			return NULL;
		};
		
		tok = tok->next;
		
		int i;
		for (i=0; i<info->numTemplateParams; i++)
		{
			const char *name = info->paramNames[i];
			
			Token *subtok = tok;
			
			Type *subtype = parseTypeSpec(inTemplate, typeNameTable, &tok);
			if (subtype == NULL)
			{
				lexDiag(tok, CON_ERROR, "expected a type specification, not `%s'", tok->value);
				return NULL;
			};
			
			if (subtype->kind == PER_TYPE_ARRAY)
			{
				lexDiag(subtok, CON_ERROR, "a template parameter cannot be an array type (have `%s')", getTypeName(subtype));
				return NULL;
			};
			
			if (subtype->kind == PER_TYPE_VOID)
			{
				lexDiag(subtok, CON_ERROR, "a template parameter cannot be the `void' type");
				return NULL;
			};
			
			wrapPrimitiveType(subtype);
		
			hashtabSet(type->temParamTypes, name, subtype);
			
			if (i == info->numTemplateParams-1)
			{
				if (tok->type != TOK_PER_RARROW)
				{
					lexDiag(tok, CON_ERROR, "expected `>' not `%s'", tok->value);
					if (tok->type == TOK_PER_SHR)
					{
						lexDiag(tok, CON_NOTE, "if closing multiple arrow brackets, you must write `> >` with a space, instead of `>>'");
					};
					return NULL;
				};
			}
			else
			{
				if (tok->type != TOK_PER_COMMA)
				{
					lexDiag(tok, CON_ERROR, "expected `,' not `%s'", tok->value);
					return NULL;
				};
			};
			
			tok = tok->next;
		};
	};
	
	*scanner = tok;
	return type;
};

static Type* resolveTypeAccess(ClassTemplate *inTemplate, Type *type, Token *tok)
{
	ClassTemplate *tem = findClassByName(tok, type->fullname);
	
	int minProtLevel;
	if (strcmp(inTemplate->fullname, type->fullname) == 0)
	{
		// same class, allow access to private types
		minProtLevel = PER_PROT_PRIVATE;
	}
	else
	{
		if (isSubtype(type, getThisType(inTemplate)))
		{
			// we are in a class defining a subtype of 'type', so allow access
			// to protected types
			minProtLevel = PER_PROT_PROTECTED;
		}
		else
		{
			// only public
			minProtLevel = PER_PROT_PUBLIC;
		};
	};
	
	// first check the type itself
	TypeDef *def;
	for (def=tem->typeDefs; def!=NULL; def=def->next)
	{
		if (strcmp(def->name, tok->value) == 0 && def->prot >= minProtLevel)
		{
			lexTag(tok, TAG_CLASS_NAME, NULL, NULL);
			lexTag(tok, TAG_DEFINED_AT, def->tok, NULL);
			
			return expandType(def->base, type->temParamTypes);
		};
	};
	
	// now recursively check parents
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		Type *candidate = resolveTypeAccess(inTemplate, parent, tok);
		if (candidate != NULL) return candidate;
	};
	
	// finally, conclude the type does not exist
	return NULL;
};

static Type* parseSingularTypeSpec(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner)
{
	Type *result = parseRootTypeSpec(inTemplate, typeNameTable, scanner);
	
	while ((*scanner)->type == TOK_PER_TYPE_ACCESS)
	{
		Token *tok = *scanner;
		
		if (result->kind != PER_TYPE_MANAGED)
		{
			lexDiag(tok, CON_ERROR, "the `::' operator can only be applied to managed types, not `%s'", getTypeName(result));
			return NULL;
		};
		
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			return NULL;
		};
		
		Type *resolved = resolveTypeAccess(inTemplate, result, tok);
		if (resolved == NULL)
		{
			lexDiag(tok, CON_ERROR, "the type `%s' does not have an accessible member type named `%s'", getTypeName(result), tok->value);
			return NULL;
		};
		
		result = resolved;
		tok = tok->next;
		*scanner = tok;
	};
	
	return result;
};

Type* parseTypeSpec(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner)
{
	Type *result = parseSingularTypeSpec(inTemplate, typeNameTable, scanner);
	
	while ((*scanner)->type == TOK_PER_ARRAY)
	{
		(*scanner) = (*scanner)->next;
		
		Type *sub = (Type*) calloc(1, sizeof(Type));
		sub->kind = PER_TYPE_ARRAY;
		sub->elementType = result;
		result = sub;
	};
	
	return result;
};

ArgumentSpecificationList* parseArgumentSpecificationList(ClassTemplate *inTemplate, HashTable *typeNameTable, Token **scanner, int *setOnError)
{
	Token *tok = *scanner;
	
	Type *type = parseTypeSpec(inTemplate, typeNameTable, &tok);
	if (type == NULL)
	{
		return NULL;
	};
	
	if (type->kind == PER_TYPE_VOID)
	{
		lexDiag(*scanner, CON_ERROR, "an argument cannot have type `void'");
		*setOnError = 1;
		return NULL;
	};
	
	if (tok->type != TOK_PER_ID)
	{
		lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
		*setOnError = 1;
		return NULL;
	};
	
	lexTag(tok, TAG_DEFINE_VAR, NULL, tok->value);
	lexTag(tok, TAG_VAR_NAME, NULL, tok->value);
	
	ArgumentSpecificationList *list = (ArgumentSpecificationList*) calloc(1, sizeof(ArgumentSpecificationList));
	list->type = type;
	list->tok = tok;
	list->name = strdup(tok->value);
	
	tok = tok->next;
	
	if (tok->type == TOK_PER_COMMA)
	{
		tok = tok->next;
		
		list->next = parseArgumentSpecificationList(inTemplate, typeNameTable, &tok, setOnError);
		if (list->next == NULL)
		{
			if (!(*setOnError))
			{
				lexDiag(tok, CON_ERROR, "expected a type specification, not `%s'", tok->value);
				*setOnError = 1;
			};
			
			return NULL;
		};
	};
	
	*scanner = tok;
	return list;
};

static char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

static void importWildcard(Token *importToken, HashTable *typeNameTable, const char *namespace)
{
	ClassNameList *scan;
	for (scan=getClassNamesInNamespaces(namespace); scan!=NULL; scan=scan->next)
	{
		const char *fullname = scan->fullname;
		char *lastDot = strrchr(fullname, '.');
		const char *shortname = lastDot + 1;
		
		ClassTemplate *importClass = findClassByName(importToken, fullname);
		if (importClass != NULL)
		{
			hashtabSet(typeNameTable, shortname, importClass);
		};
	};
};

ClassTemplate* findClassByName(Token *tok, const char *fullname)
{
	if (hashtabHasKey(classTable, fullname))
	{
		return (ClassTemplate*) hashtabGet(classTable, fullname);
	};

	char *filename = getClassFileName(fullname);
	if (filename == NULL)
	{
		lexDiag(tok, CON_ERROR, "failed to find a definition of `%s'", fullname);
		return NULL;
	};
	
	ClassInfo *info = doAnalysis(fullname);
	if (info == NULL)
	{
		lexDiag(tok, CON_ERROR, "failed to analyze `%s'", fullname);
		abort();
	};
	
	int i;
	for (i=0; i<info->numDeps; i++)
	{
		ClassInfo *dep = info->deps[i];
		if (findClassByName(tok, dep->fullname) == NULL)
		{
			return NULL;
		};
	};
	
	FILE *fp = fopen(filename, "r");
	if (fp == NULL)
	{
		lexDiag(tok, CON_ERROR, "could not open `%s' whilst looking for a definition of `%s': %s", filename, fullname, strerror(errno));
		return NULL;
	};
	
	char *data = readWholeFile(fp);
	fclose(fp);
	
	declareMakeDep(filename);
	
	Token *toklist = parsePerun(data, filename);
	if (toklist == NULL)
	{
		return NULL;
	};
	
	// add the imported class as an incomplete type for now
	ClassTemplate *inc = (ClassTemplate*) calloc(1, sizeof(ClassTemplate));
	inc->fullname = strdup(fullname);
	hashtabSet(classTable, fullname, inc);
	
	CompilationUnit *unit = compilePerun(toklist, 0);
	if (unit == NULL)
	{
		return NULL;
	};
	
	ClassTemplate *result = (ClassTemplate*) hashtabGet(classTable, fullname);
	if (!result->complete || result->protLevel != PER_PROT_PUBLIC)
	{
		lexDiag(unit->tok, CON_ERROR, "the file `%s' did not define a public class `%s' as expected", filename, fullname);
		return NULL;
	};
	
	return result;
};

int isSameType(Type *a, Type *b)
{
	if (a->kind != b->kind) return 0;
	if (a->kind == PER_TYPE_TEMPLATE_PARAM) return (strcmp(a->fullname, b->fullname) == 0);
	if (a->kind == PER_TYPE_FUNCREF)
	{
		if (!isSameType(a->retType, b->retType))
		{
			return 0;
		};
		
		if (a->numArgs != b->numArgs)
		{
			return 0;
		};
		
		int i;
		for (i=0; i<a->numArgs; i++)
		{
			if (!isSameType(a->argTypes[i], b->argTypes[i]))
			{
				return 0;
			};
		};
		
		return 1;
	};
	if (a->kind != PER_TYPE_MANAGED) return 1;
	if (a->kind == PER_TYPE_ARRAY) return isSameType(a->elementType, b->elementType);
	if (strcmp(a->fullname, b->fullname) != 0) return 0;
	
	ClassTemplate *tem = (ClassTemplate*) hashtabGet(classTable, a->fullname);
	int i;
	for (i=0; i<tem->numTemParams; i++)
	{
		const char *name = tem->temParams[i].name;
		
		Type *subA = (Type*) hashtabGet(a->temParamTypes, name);
		Type *subB = (Type*) hashtabGet(b->temParamTypes, name);
		
		if (!isSameType(subA, subB)) return 0;
	};
	
	return 1;
};

Type* getThisType(ClassTemplate *tem)
{
	Type *type = (Type*) calloc(1, sizeof(Type));
	type->kind = PER_TYPE_MANAGED;
	type->fullname = tem->fullname;
	type->temParamTypes = hashtabNew();
	
	int i;
	for (i=0; i<tem->numTemParams; i++)
	{
		TemplateParameterSpec *spec = &tem->temParams[i];
		
		Type *sub = (Type*) calloc(1, sizeof(Type));
		sub->kind = PER_TYPE_TEMPLATE_PARAM;
		sub->fullname = strdup(spec->name);
		sub->minBase = spec->minBase;
		
		hashtabSet(type->temParamTypes, spec->name, sub);
	};
	
	return type;
};

static int isIncompatiblyParametrized(Type *candidate, Type *with)
{
	// if they are a different kind (template parameter vs specific class),
	// they are always compatible
	if (candidate->kind != with->kind) return 0;
	
	// if they refer to a different class, they are compatible
	if (strcmp(candidate->fullname, with->fullname) != 0) return 0;
	
	// if same class, they are compatible only if all template parameters
	// are the same in both cases
	ClassTemplate *tem = (ClassTemplate*) hashtabGet(classTable, candidate->fullname);
	int i;
	for (i=0; i<tem->numTemParams; i++)
	{
		const char *name = tem->temParams[i].name;
		
		Type *a = (Type*) hashtabGet(candidate->temParamTypes, name);
		Type *b = (Type*) hashtabGet(with->temParamTypes, name);
		
		if (!isSameType(a, b)) return 1;
	};
	
	// otherwise, compatible if all ancestors also are
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], candidate->temParamTypes);
		if (isIncompatiblyParametrized(parent, with)) return 1;
	};
	
	return 0;
};

/**
 * Determine if a field with a specific name can be defined in the given class. This checks if there is already
 * a method or field with that name in either the class, or in one of its parents.
 * 
 * Returns nonzero if the field can be defined, otherwise 0. The error about the reason is displayed to the console
 * by this function!
 * 
 * 'nameToken' is the token with which errors will be associated.
 */
static int canDefineField(ClassTemplate *tem, const char *fieldName, Token *nameToken, int minProt)
{
	ClassField *field;
	for (field=tem->fields; field!=NULL; field=field->next)
	{
		if (strcmp(field->name, fieldName) == 0 && field->prot >= minProt)
		{
			lexDiag(nameToken, CON_ERROR, "field name `%s' is already in use", fieldName);
			lexDiag(field->tok, CON_NOTE, "first defined here");
			return 0;
		};
	};
	
	ClassMethod *method;
	for (method=tem->methods; method!=NULL; method=method->next)
	{
		if (strcmp(method->name, fieldName) == 0 && method->prot >= minProt)
		{
			lexDiag(nameToken, CON_ERROR, "field name `%s' is already in use for a method", fieldName);
			lexDiag(method->tok, CON_NOTE, "defined here");
			return 0;
		};
	};
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		ClassTemplate *sub = (ClassTemplate*) hashtabGet(classTable, tem->parents[i]->fullname);
		if (!canDefineField(sub, fieldName, nameToken, PER_PROT_PROTECTED)) return 0;
	};
	
	// std.rt.Object is an implicit parent
	if (strcmp(tem->fullname, "std.rt.Object") != 0)
	{
		ClassTemplate *obj = (ClassTemplate*) hashtabGet(classTable, "std.rt.Object");
		if (!canDefineField(obj, fieldName, nameToken, PER_PROT_PROTECTED)) return 0;
	};
	
	return 1;
};

/**
 * Same as the above, except checks if a method can be defined based on the fact that a field does not exist with that name.
 * It does NOT check override and stuff!
 */
static int canDefineMethod(ClassTemplate *tem, const char *fieldName, Token *nameToken, int minProt)
{
	ClassField *field;
	for (field=tem->fields; field!=NULL; field=field->next)
	{
		if (strcmp(field->name, fieldName) == 0 && field->prot >= minProt)
		{
			lexDiag(nameToken, CON_ERROR, "cannot define method `%s' because a field with this name exists", fieldName);
			lexDiag(field->tok, CON_NOTE, "first defined here");
			return 0;
		};
	};
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		ClassTemplate *sub = (ClassTemplate*) hashtabGet(classTable, tem->parents[i]->fullname);
		if (!canDefineMethod(sub, fieldName, nameToken, PER_PROT_PROTECTED)) return 0;
	};
	
	// std.rt.Object is an implicit parent
	if (strcmp(tem->fullname, "std.rt.Object") != 0)
	{
		ClassTemplate *obj = (ClassTemplate*) hashtabGet(classTable, "std.rt.Object");
		if (!canDefineMethod(obj, fieldName, nameToken, PER_PROT_PROTECTED)) return 0;
	};
	
	return 1;
};

int isManagedType(Type *type)
{
	return (type->kind == PER_TYPE_MANAGED || type->kind == PER_TYPE_TEMPLATE_PARAM || type->kind == PER_TYPE_NULL);
};

int isUnsignedIntegerType(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_BYTE:
	case PER_TYPE_WORD:
	case PER_TYPE_DWORD:
	case PER_TYPE_QWORD:
	case PER_TYPE_PTR:
		return 1;
	default:
		return 0;
	};
};

int isSignedIntegerType(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_SBYTE:
	case PER_TYPE_SWORD:
	case PER_TYPE_SDWORD:
	case PER_TYPE_SQWORD:
	case PER_TYPE_SPTR:
		return 1;
	default:
		return 0;
	};
};

int isIntegerType(Type *type)
{
	return isUnsignedIntegerType(type) || isSignedIntegerType(type);
};

int isFloatingType(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_FLOAT:
	case PER_TYPE_DOUBLE:
		return 1;
	default:
		return 0;
	};
};

int isPrimitiveType(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_BOOL:
	case PER_TYPE_BYTE:
	case PER_TYPE_DOUBLE:
	case PER_TYPE_DWORD:
	case PER_TYPE_FLOAT:
	case PER_TYPE_PTR:
	case PER_TYPE_QWORD:
	case PER_TYPE_SBYTE:
	case PER_TYPE_SDWORD:
	case PER_TYPE_SPTR:
	case PER_TYPE_SQWORD:
	case PER_TYPE_SWORD:
	case PER_TYPE_WORD:
		return 1;
	default:
		return 0;
	};
};

int isSameSignature(ArgumentSpecificationList *a, ArgumentSpecificationList *b)
{
	while (a != NULL && b != NULL)
	{
		if (!areSimilarTypes(a->type, b->type))
		{
			return 0;
		};
		
		a = a->next;
		b = b->next;
	};
	
	// both must be NULL at the end
	return (a == b);
};

HashTable* getTypeTableFor(ClassTemplate *temTarget, Type *type)
{
	if (strcmp(type->fullname, temTarget->fullname) == 0) return type->temParamTypes;
	
	ClassTemplate *tem = (ClassTemplate*) hashtabGet(classTable, type->fullname);
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		HashTable *cand = getTypeTableFor(temTarget, parent);
		if (cand != NULL) return cand;
	};
	
	return NULL;
};

int isIdenticalSignature(ArgumentSpecificationList *a, ArgumentSpecificationList *b, ClassTemplate *aClass, Type *bType)
{
	while (a != NULL && b != NULL)
	{
		if (a->type->kind == PER_TYPE_TEMPLATE_PARAM && b->type->kind == PER_TYPE_TEMPLATE_PARAM)
		{
			if (!isSameType(a->type->minBase, b->type->minBase))
			{
				return 0;
			};
		}
		else if (a->type->kind == PER_TYPE_TEMPLATE_PARAM)
		{
			HashTable *typeTable = getTypeTableFor(aClass, bType);
			assert(typeTable != NULL);
			
			Type *effectiveType = (Type*) hashtabGet(typeTable, a->type->fullname);
			if (!isSameType(effectiveType, b->type))
			{
				return 0;
			};
		}
		else if (!isSameType(a->type, b->type))
		{
			return 0;
		};
		
		a = a->next;
		b = b->next;
	};
	
	// both must be NULL at the end
	return (a == b);
};

ClassMethod* getMethodWithSignature(ClassTemplate *tem, const char *name, ArgumentSpecificationList *list)
{
	ClassMethod *method;
	for (method=tem->methods; method!=NULL; method=method->next)
	{
		if ((method->flags & PER_Q_STATIC) == 0 && strcmp(method->name, name) == 0 && isSameSignature(method->args, list))
		{
			return method;
		};
	};
	
	// now check all the parents
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *type = tem->parents[i];
		ClassTemplate *parent = (ClassTemplate*) hashtabGet(classTable, type->fullname);
		
		method = getMethodWithSignature(parent, name, list);
		if (method != NULL) return method;
	};
	
	// also check std.rt.Object
	if (strcmp(tem->fullname, "std.rt.Object") != 0)
	{
		ClassTemplate *obj = (ClassTemplate*) hashtabGet(classTable, "std.rt.Object");
		return getMethodWithSignature(obj, name, list);
	};
	
	// couldn't find one
	return NULL;
};

char* getArgumentListFormatted(ArgumentSpecificationList *list)
{
	char *result = strdup("");
	
	for (; list!=NULL; list=list->next)
	{
		char *typeName = getTypeName(list->type);
		result = (char*) realloc(result, strlen(result) + strlen(typeName) + strlen(list->name) + 16);
		strcat(result, typeName);
		free(typeName);
		strcat(result, " ");
		strcat(result, list->name);
		
		if (list->next != NULL)
		{
			strcat(result, ", ");
		};
	};
	
	return result;
};

void getMethodMap(ClassTemplate *tem, HashTable *methodMap)
{
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		getMethodMap(findClassByName(NULL, tem->parents[i]->fullname), methodMap);
	};
	
	ClassMethod *method;
	for (method=tem->methods; method!=NULL; method=method->next)
	{
		if ((method->flags & PER_Q_STATIC) == 0)
		{
			char *provMangled = mangleName("_*", method->args);
			hashtabSet(methodMap, provMangled, method);
			free(provMangled);
		};
	};
};

Type* getReturnTypeInContext(ClassMethod *method, Type *type)
{
	ClassTemplate *tem = findClassByName(NULL, type->fullname);
	
	ClassMethod *scan;
	for (scan=tem->methods; scan!=NULL; scan=scan->next)
	{
		if (method == scan) return expandType(method->retType, type->temParamTypes);
	};
	
	// not in this class, so check parents
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		Type *cand = getReturnTypeInContext(method, parent);
		if (cand != NULL) return cand;
	};
	
	if (strcmp(type->fullname, "std.rt.Object") != 0)
	{
		return getReturnTypeInContext(method, typeObject);
	};
	
	return NULL;
};

CompilationUnit* compilePerun(Token *tok, int mainTarget)
{
	// NamespaceSpecifiction ::= 'namespace' NamespaceSpecifier ';'
	if (tok->type != TOK_PER_NAMESPACE)
	{
		lexDiag(tok, CON_ERROR, "expected a namespace specification, not `%s'", tok->value);
		return NULL;
	};
	
	Token *namespaceTok = tok;
	
	tok = tok->next;
	if (tok->type != TOK_PER_ID)
	{
		lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
		return NULL;
	};
	
	char *namespace = strdup(tok->value);
	tok = tok->next;
	
	while (tok->type == TOK_PER_DOT)
	{
		tok = tok->next;
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			return NULL;
		};
		
		int len = strlen(namespace) + strlen(tok->value) + 2;
		char *newNamespace = (char*) malloc(len);
		sprintf(newNamespace, "%s.%s", namespace, tok->value);
		free(namespace);
		namespace = newNamespace;
		
		tok = tok->next;
	};
	
	if (tok->type != TOK_PER_SEMICOLON)
	{
		lexDiag(tok, CON_ERROR, "expected `.' or `;', not `%s'", tok->value);
		return NULL;
	};
	
	tok = tok->next;
	
	// maps type names to the corresponding class template
	HashTable *typeNameTable = hashtabNew();
	
	// import the current package as well as 'std.rt.*'
	importWildcard(namespaceTok, typeNameTable, namespace);
	importWildcard(namespaceTok, typeNameTable, "std.rt");
	
	// import list
	while (tok->type == TOK_PER_IMPORT)
	{
		Token *importToken = tok;
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			return NULL;
		};
		
		char *importName = strdup(tok->value);
		const char *shortname = strdup(importName);
		int wildcard = 0;
		
		tok = tok->next;
		
		while (tok->type == TOK_PER_DOT)
		{
			tok = tok->next;
			
			if (tok->type == TOK_PER_ID)
			{
				char *newName = (char*) malloc(strlen(importName) + strlen(tok->value) + 2);
				sprintf(newName, "%s.%s", importName, tok->value);
				free(importName);
				importName = newName;
				shortname = tok->value;
				
				tok = tok->next;
			}
			else if (tok->type == TOK_PER_AST)
			{
				wildcard = 1;
				tok = tok->next;
				break;
			}
			else
			{
				lexDiag(tok, CON_ERROR, "expected an identifier or `*', not `%s'", tok->value);
				return NULL;
			};
		};
		
		if (tok->type != TOK_PER_SEMICOLON)
		{
			lexDiag(tok, CON_ERROR, "expected `;', not `%s'", tok->value);
			return NULL;
		};
		
		tok = tok->next;
		
		if (wildcard)
		{
			importWildcard(importToken, typeNameTable, importName);
		}
		else
		{
			ClassTemplate *importClass = findClassByName(importToken, importName);
			
			if (importClass == NULL)
			{
				return NULL;
			};
			
			hashtabSet(typeNameTable, shortname, importClass);
		};
		
		free(importName);
	};
	
	CompilationUnit *unit = (CompilationUnit*) calloc(1, sizeof(CompilationUnit));
	unit->tok = namespaceTok;
	
	// ToplevelDefinitionList
	while (tok->type != TOK_END)
	{
		if (tok->type == TOK_PER_SEMICOLON)
		{
			tok = tok->next;
			continue;
		};
		
		int prot = parseProt(tok);
		if (prot == -1)
		{
			lexDiag(tok, CON_ERROR, "expected an access specifier, not `%s'", tok->value);
			return NULL;
		};
		
		tok = tok->next;
		
		Token *qtoks = tok;
		int flags = parseQualifs(&tok);
		
		if (flags != 0 && flags != PER_Q_STATIC && flags != PER_Q_FINAL && flags != PER_Q_ABSTRACT)
		{
			lexDiag(qtoks, CON_ERROR, "mutually-exclusive class qualifiers specified");
			return NULL;
		};
		
		if (tok->type != TOK_PER_CLASS)
		{
			lexDiag(tok, CON_ERROR, "expected a class qualifier or `class', not `%s'", tok->value);
			return NULL;
		};
		
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			return NULL;
		};
		
		lexTag(tok, TAG_DEFINE_CLASS, NULL, NULL);
		lexTag(tok, TAG_CLASS_NAME, NULL, NULL);
		
		const char *shortname = tok->value;
		Token *deftok = tok;
		tok = tok->next;
		
		char *fullname = (char*) malloc(strlen(namespace) + strlen(shortname) + 2);
		sprintf(fullname, "%s.%s", namespace, shortname);
		
		ClassTemplate *tem = (ClassTemplate*) calloc(1, sizeof(ClassTemplate));
		tem->complete = 1;
		tem->fullname = fullname;
		tem->protLevel = prot;
		tem->flags = flags;
		tem->tok = deftok;
		tem->typeNameTable = typeNameTable;
		
		// template parameter list
		if (tok->type == TOK_PER_LARROW)
		{
			while (1)
			{
				tok = tok->next;
				
				if (tok->type != TOK_PER_ID)
				{
					lexDiag(tok, CON_ERROR, "expected a template parameter name, not `%s'", tok->value);
					return NULL;
				};
				
				const char *parname = tok->value;
				if (isTemplateParameterName(tem, parname))
				{
					lexDiag(tok, CON_ERROR, "duplicate name `%s' for template parameter", tok->value);
					return NULL;
				};
				
				Token *templParamNameToken = tok;
				lexTag(tok, TAG_DEFINE_CLASS, NULL, NULL);
				lexTag(tok, TAG_CLASS_NAME, NULL, NULL);
				
				tok = tok->next;
				
				int index = tem->numTemParams++;
				tem->temParams = (TemplateParameterSpec*) realloc(tem->temParams, sizeof(TemplateParameterSpec) * tem->numTemParams);
				TemplateParameterSpec *spec = &tem->temParams[index];
				
				spec->name = parname;
				spec->minBase = typeObject;
				spec->tok = templParamNameToken;
				
				if (tok->type == TOK_PER_EXTENDS)
				{
					tok = tok->next;
					Token *spectok = tok;
					
					Type *minBase = parseTypeSpec(tem, typeNameTable, &tok);
					if (minBase == NULL)
					{
						lexDiag(tok, CON_ERROR, "expected a type specifier, not `%s'", tok->value);
						return NULL;
					};
					
					if (minBase->kind != PER_TYPE_MANAGED)
					{
						lexDiag(spectok, CON_ERROR, "the minimum base type for a template parameter `%s' must be a managed non-array type, not `%s'",
										parname, getTypeName(minBase));
						return NULL;
					};
					
					spec->minBase = minBase;
				};
				
				if (tok->type == TOK_PER_COMMA)
				{
					continue;
				}
				else if (tok->type == TOK_PER_RARROW)
				{
					tok = tok->next;
					break;
				}
				else
				{
					lexDiag(tok, CON_ERROR, "expected `>' or `,', not `%s'", tok->value);
					if (tok->type == TOK_PER_SHR)
					{
						lexDiag(tok, CON_NOTE, "if closing multiple arrow brackets, you must write `> >` with a space, instead of `>>'");
					};
					return NULL;
				};
			};
		};
		
		// the class becomes available before we parse the 'extends' body
		hashtabSet(classTable, fullname, tem);
		
		if (hashtabHasKey(typeNameTable, shortname))
		{
			ClassTemplate *tem = (ClassTemplate*) hashtabGet(typeNameTable, shortname);
			if (strcmp(tem->fullname, fullname) != 0)
			{
				lexDiag(deftok, CON_ERROR, "attempting to re-use shortname `%s' for class `%s' (currently refers to `%s')\n",
						shortname, fullname, tem->fullname);
				return NULL;
			};
		};
		
		hashtabSet(typeNameTable, shortname, tem);
		
		// 'extends' clause
		if (tok->type == TOK_PER_EXTENDS)
		{
			do
			{
				tok = tok->next;
				Token *partok = tok;
				
				Type *parent = parseTypeSpec(tem, typeNameTable, &tok);
				if (parent == NULL)
				{
					lexDiag(partok, CON_ERROR, "expected a type specification, not `%s'", partok->value);
					return NULL;
				};
				
				if (parent->kind != PER_TYPE_MANAGED)
				{
					lexDiag(partok, CON_ERROR, "parents of a class must be managed non-array types, not `%s'", getTypeName(parent));
					return NULL;
				};
				
				ClassTemplate *parentClass = (ClassTemplate*) hashtabGet(classTable, parent->fullname);
				if (parentClass->flags & PER_Q_FINAL)
				{
					lexDiag(partok, CON_ERROR, "the class `%s' is defined as final, and cannot be the parent of another class", parent->fullname);
					return NULL;
				};
				
				// make sure it does not clash with an incompatibly parametrized ancestor
				int i;
				for (i=0; i<tem->numParents; i++)
				{
					if (isIncompatiblyParametrized(parent, tem->parents[i]))
					{
						lexDiag(partok, CON_ERROR, "the class `%s' cannot have a parent with type `%s' because it is parametrized in a manner incompatible with another ancestor", tem->fullname, getTypeName(parent));
						return NULL;
					};
				};
				
				int index = tem->numParents++;
				tem->parents = (Type**) realloc(tem->parents, sizeof(void*) * tem->numParents);
				tem->parents[index] = parent;
			} while (tok->type == TOK_PER_COMMA);
		};
		
		tem->mangledName = mangleName("Per_CD?", fullname);
		tem->staticLockName = mangleName("Per_SL?", fullname);
		tem->staticInitName = mangleName("Per_SI?", fullname);
		
		// class body
		if (tok->type != TOK_PER_LBRACE)
		{
			lexDiag(tok, CON_ERROR, "expected `{' not `%s'", tok->value);
			return NULL;
		};
		
		Token *closingBrace;
		
		tok = tok->next;
		while (1)
		{
			if (tok->type == TOK_PER_RBRACE)
			{
				closingBrace = tok;
				tok = tok->next;
				break;
			}
			else if (tok->type == TOK_PER_SEMICOLON)
			{
				tok = tok->next;
				continue;
			}
			else
			{
				if (tok->type == TOK_PER_DESTRUCTOR)
				{
					Token *dtorTok = tok;
					tok = tok->next;
					
					if (tem->dtor != NULL)
					{
						lexDiag(dtorTok, CON_ERROR, "multiple destructors defined for `%s'", tem->fullname);
						lexDiag(tem->dtorTok, CON_NOTE, "first defined here");
						return NULL;
					};
					
					StmtParserContext sctx;
					initStmtParser(&sctx, tok, tem, typeNameTable);
					
					CompoundStatement *dtor = parseCompoundStatement(&sctx);
					if (dtor == NULL || sctx.aborted)
					{
						lexDiag(sctx.errtok, CON_ERROR, "%s", sctx.errmsg);
						return NULL;
					};
					
					tok = sctx.tok;
					tem->dtor = dtor;
					tem->dtorTok = dtorTok;
					continue;
				};
				
				int prot = parseProt(tok);
				if (prot == -1)
				{
					lexDiag(tok, CON_ERROR, "expected an access specifier, `destructor' or `;', not `%s'", tok->value);
					return NULL;
				};
				
				tok = tok->next;
				
				if (strcmp(tok->value, shortname) == 0 && tok->next->type == TOK_PER_LPAREN)
				{
					// constructor
					if (tem->flags & PER_Q_STATIC)
					{
						lexDiag(tok, CON_ERROR, "class `%s' is static, and cannot have constructors", tem->fullname);
						return NULL;
					};
					
					Token *reftok = tok;
					
					tok = tok->next->next;
					
					int error = 0;
					ArgumentSpecificationList *args = parseArgumentSpecificationList(tem, typeNameTable, &tok, &error);
					if (error)
					{
						return NULL;
					};
					
					if (tok->type != TOK_PER_RPAREN)
					{
						lexDiag(tok, CON_ERROR, "expected `)' not `%s'", tok->value);
						return NULL;
					};
					
					tok = tok->next;
					
					ClassSuperCall *superCalls = NULL;
					if (tok->type == TOK_PER_COLON)
					{
						// get the list of constructor invocations
						do
						{
							// skip the ',' or the initial ':'
							tok = tok->next;
							
							if (tok->type != TOK_PER_ID)
							{
								lexDiag(tok, CON_ERROR, "expected a parent name (identifier), not `%s'", tok->value);
								return NULL;
							};
							
							Token *nameTok = tok;
							
							const char *parentShortName = tok->value;
							if (!hashtabHasKey(typeNameTable, parentShortName))
							{
								lexDiag(tok, CON_ERROR, "`%s' is not a class shortname", tok->value);
								return NULL;
							};
							
							ClassTemplate *temParent = (ClassTemplate*) hashtabGet(typeNameTable, parentShortName);
							
							// find the parent type (in the parent types list) with the same fullname
							Type *parentType = NULL;
							int i;
							for (i=0; i<tem->numParents; i++)
							{
								Type *cand = tem->parents[i];
								if (strcmp(cand->fullname, temParent->fullname) == 0)
								{
									parentType = cand;
									break;
								};
							};
							
							if (parentType == NULL)
							{
								lexDiag(tok, CON_ERROR, "class `%s' is not a parent of `%s'", temParent->fullname, tem->fullname);
								return NULL;
							};
							
							// OK, now we can get to the argument list
							tok = tok->next;
							
							if (tok->type != TOK_PER_LPAREN)
							{
								lexDiag(tok, CON_ERROR, "expected `(' not `%s'", tok->value);
								return NULL;
							};
							
							tok = tok->next;
							
							ExprParserContext xctx;
							initExprParser(&xctx, tok, tem, typeNameTable);
							ArgumentExpressionList *args = parseArgumentExpressionList(&xctx);
							tok = xctx.tok;
							
							// (args is allowed to be NULL)
							if (tok->type != TOK_PER_RPAREN)
							{
								lexDiag(tok, CON_ERROR, "expected `)' not `%s'", tok->value);
								return NULL;
							};
							
							tok = tok->next;
							
							// check if this one is already explicitly constructed
							ClassSuperCall *call;
							for (call=superCalls; call!=NULL; call=call->next)
							{
								if (strcmp(call->parent->fullname, parentType->fullname) == 0)
								{
									lexDiag(nameTok, CON_ERROR, "class `%s' is already constructed", call->parent->fullname);
									lexDiag(call->tok, CON_NOTE, "superconstructor first called here");
									return NULL;
								};
							};
							
							// add to list
							call = (ClassSuperCall*) calloc(1, sizeof(ClassSuperCall));
							call->next = superCalls;
							superCalls = call;
							call->parent = parentType;
							call->tok = nameTok;
							call->args = args;
						} while (tok->type == TOK_PER_COMMA);
					};
					
					CompoundStatement *body;
					if (tok->type == TOK_PER_SEMICOLON)
					{
						body = NULL;
						tok = tok->next;
					}
					else
					{
						StmtParserContext sctx;
						initStmtParser(&sctx, tok, tem, typeNameTable);
						
						body = parseCompoundStatement(&sctx);
						if (body == NULL || sctx.aborted)
						{
							lexDiag(sctx.errtok, CON_ERROR, "%s", sctx.errmsg);
							return NULL;
						};
						
						tok = sctx.tok;
					};
					
					// check if we already have a constructor with this signature
					ClassConstructor *ctor;
					for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
					{
						if (isSameSignature(args, ctor->args))
						{
							lexDiag(reftok, CON_ERROR, "constructor %s(%s) is already defined", tem->fullname, getArgumentListFormatted(args));
							lexDiag(ctor->tok, CON_NOTE, "first defined here");
							return NULL;
						};
					};
					
					// we don't, so create it
					ctor = (ClassConstructor*) calloc(1, sizeof(ClassConstructor));
					ctor->args = args;
					ctor->prot = prot;
					ctor->tok = reftok;
					ctor->body = body;
					ctor->superCalls = superCalls;
					ctor->mangled = mangleName("Per_CC?*", tem->fullname, args);
					
					lexTag(reftok, TAG_DEFINE_METHOD, NULL, reftok->value);
					lexTag(reftok, TAG_METHOD_NAME, NULL, reftok->value);
					
					if (tem->ctors == NULL)
					{
						tem->ctors = tem->lastCtor = ctor;
					}
					else
					{
						tem->lastCtor->next = ctor;
						tem->lastCtor = ctor;
					};
				}
				else if (tok->type == TOK_PER_TYPEDEF)
				{
					// type definition
					tok = tok->next;
					
					if (tok->type != TOK_PER_ID)
					{
						lexDiag(tok, CON_ERROR, "expected an identifier after `type', not `%s'", tok->value);
						return NULL;
					};
					
					Token *tokTypeName = tok;
					tok = tok->next;
					
					if (tok->type != TOK_PER_ASSIGN)
					{
						lexDiag(tok, CON_ERROR, "expected `=', not `%s'", tok->value);
						return NULL;
					};
					tok = tok->next;
					
					Type *base = parseTypeSpec(tem, typeNameTable, &tok);
					if (base == NULL)
					{
						lexDiag(tok, CON_ERROR, "expected a type specification, not `%s'", tok->value);
						return NULL;
					};
					
					if (tok->type != TOK_PER_SEMICOLON)
					{
						lexDiag(tok, CON_ERROR, "expected `;', not `%s'", tok->value);
						return NULL;
					};
					tok = tok->next;
					
					// check if already exists
					TypeDef *def;
					for (def=tem->typeDefs; def!=NULL; def=def->next)
					{
						if (strcmp(def->name, tokTypeName->value) == 0)
						{
							lexDiag(tokTypeName, CON_ERROR, "type `%s' is already defined in this class", def->name);
							lexDiag(def->tok, CON_NOTE, "first definted here");
							return NULL;
						};
					};
					
					// add to the list
					def = (TypeDef*) calloc(1, sizeof(TypeDef));
					def->tok = tokTypeName;
					def->prot = prot;
					def->name = tokTypeName->value;
					def->base = base;
					
					lexTag(tokTypeName, TAG_DEFINE_CLASS, NULL, tokTypeName->value);
					lexTag(tokTypeName, TAG_CLASS_NAME, NULL, tokTypeName->value);
					
					if (tem->typeDefs == NULL)
					{
						tem->typeDefs = tem->lastTypeDef = def;
					}
					else
					{
						tem->lastTypeDef->next = def;
						tem->lastTypeDef = def;
					};
				}
				else
				{
					Token *tokQualifs = tok;
					int flags = parseQualifs(&tok);
					
					Type *type = parseTypeSpec(tem, typeNameTable, &tok);
					if (type == NULL)
					{
						lexDiag(tok, CON_ERROR, "expected a type specification or qualifier, not `%s'", tok->value);
						return NULL;
					};
					
					if (tok->type != TOK_PER_ID)
					{
						lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
						return NULL;
					};
					
					const char *fieldName = tok->value;
					Token *nameToken = tok;
					tok = tok->next;
					
					if (tok->type == TOK_PER_LPAREN)
					{
						tok = tok->next;		// consume the '('
						
						int allowedFlags = PER_Q_STATIC | PER_Q_FINAL | PER_Q_ABSTRACT | PER_Q_OVERRIDE;
						if ((flags & allowedFlags) != flags)
						{
							lexDiag(tokQualifs, CON_ERROR, "invalid qualifiers given to a method");
							return NULL;
						};

						if (((tem->flags & PER_Q_STATIC) != 0) && ((flags & PER_Q_STATIC) == 0))
						{
							lexDiag(tokQualifs, CON_ERROR, "all members of a static class must themselves be static");
							return NULL;
						};
						
						if ((flags & PER_Q_STATIC) && (flags & PER_Q_ABSTRACT))
						{
							lexDiag(tokQualifs, CON_ERROR, "a method cannot be both abstract and static");
							return NULL;
						};
						
						if (!canDefineMethod(tem, fieldName, nameToken, PER_PROT_PRIVATE))
						{
							// (error printed by canDefineMethod() already)
							return NULL;
						};
						
						lexTag(nameToken, TAG_METHOD_NAME, NULL, nameToken->value);
						lexTag(nameToken, TAG_DEFINE_METHOD, NULL, nameToken->value);
						
						int error = 0;
						ArgumentSpecificationList *args = parseArgumentSpecificationList(tem, typeNameTable, &tok, &error);
						if (error)
						{
							return NULL;
						};
						
						if (tok->type != TOK_PER_RPAREN)
						{
							lexDiag(tok, CON_ERROR, "expected `)' not `%s'", tok->value);
							return NULL;
						};
						
						tok = tok->next;
						
						const char *externName = NULL;
						if (tok->type == TOK_PER_EXTERN)
						{
							tok = tok->next;
							
							if (tok->type != TOK_PER_ID)
							{
								lexDiag(tok, CON_ERROR, "expected an identifier after `extern', not `%s'",
									tok->value);
								return NULL;
							};
							
							externName = tok->value;
							tok = tok->next;
						};
						
						CompoundStatement *body;
						if (tok->type == TOK_PER_SEMICOLON)
						{
							body = NULL;
							tok = tok->next;
						}
						else
						{
							if (flags & PER_Q_ABSTRACT)
							{
								lexDiag(tok, CON_ERROR, "expected `;' not `%s' (because the method is abstract)", tok->value);
								return NULL;
							};
							
							StmtParserContext sctx;
							initStmtParser(&sctx, tok, tem, typeNameTable);
							
							body = parseCompoundStatement(&sctx);
							if (body == NULL || sctx.aborted)
							{
								lexDiag(sctx.errtok, CON_ERROR, "%s", sctx.errmsg);
								return NULL;
							};
							
							tok = sctx.tok;
						};
						
						// check if this method is virtual, and if so, generate the "static copy"
						int virtual = ((flags & PER_Q_STATIC) == 0);
						ArgumentSpecificationList *staticArgs = (ArgumentSpecificationList*) calloc(1, sizeof(ArgumentSpecificationList));
						staticArgs->type = getThisType(tem);
						staticArgs->name = strdup("this");
						staticArgs->next = args;
						
						// check if a method in the current class already defines a method with
						// this signature
						ClassMethod *scan;
						for (scan=tem->methods; scan!=NULL; scan=scan->next)
						{
							if (strcmp(scan->name, fieldName) == 0)
							{
								if (isSameSignature(scan->args, args))
								{
									lexDiag(nameToken, CON_ERROR, "a method with this signature is already defined in class `%s'", tem->fullname);
									lexDiag(scan->tok, CON_NOTE, "first defined here");
									return NULL;
								};
								
								if (virtual)
								{
									if (isSameSignature(scan->args, staticArgs))
									{
										lexDiag(nameToken, CON_ERROR,
											"a method with the corresponding static signature is already defined in class `%s'",
											tem->fullname);
										lexDiag(scan->tok, CON_NOTE, "the conflicting method was first defined here");
										return NULL;
									};
								};
							}; 
						};
						
						// check if we are overriding
						ClassMethod *slave = NULL;
						if (virtual)
						{
							slave = getMethodWithSignature(tem, fieldName, args);
							if (slave == NULL)
							{
								if (flags & PER_Q_OVERRIDE && mainTarget)
								{
									lexDiag(nameToken, CON_ERROR, "requested an override of %s(%s) but no method with this signature exists", fieldName, getArgumentListFormatted(args));
									return NULL;
								};
							}
							else
							{
								// check that the signatures are IDENTICAL
								if (!isIdenticalSignature(slave->args, args, slave->tem, getThisType(tem)))
								{
									lexDiag(nameToken, CON_ERROR, "method %s(%s) conflicts with an existing similar method %s(%s)",
										fieldName, getArgumentListFormatted(args),
										slave->name, getArgumentListFormatted(slave->args));
									lexDiag(slave->tok, CON_NOTE, "first defined here");
									return NULL;
								};
								
								Type *retType = getReturnTypeInContext(slave, getThisType(tem));
								assert(retType != NULL);
								
								if (!isSubtype(retType, type))
								{
									lexDiag(nameToken, CON_ERROR, "method %s(%s) conflicts with an existing method with the same signature but incompatible return type %s(%s)",
										fieldName, getArgumentListFormatted(args),
										slave->name, getArgumentListFormatted(slave->args));
									lexDiag(slave->tok, CON_NOTE, "first defined here");
									return NULL;
								};
								
								if ((flags & PER_Q_OVERRIDE) == 0)
								{
									lexDiag(nameToken, CON_ERROR, "method %s(%s) conflicts with an existing signature; did you forget the `override' qualifier?", fieldName, getArgumentListFormatted(args));
									lexDiag(slave->tok, CON_NOTE, "first defined here");
									return NULL;
								};
								
								if (slave->flags & PER_Q_FINAL)
								{
									lexDiag(nameToken, CON_ERROR, "attempted to override final method %s(%s)",
										fieldName, getArgumentListFormatted(args));
									lexDiag(slave->tok, CON_NOTE, "first defined here");
									return NULL;
								};
								
								if (slave->prot != prot)
								{
									lexDiag(nameToken, CON_ERROR, "overriden method %s(%s) has an incompatibly protection level",
										fieldName, getArgumentListFormatted(args));
									lexDiag(slave->tok, CON_NOTE, "first defined here");
									return NULL;
								};
							};
						};
						
						// add to method list
						ClassMethod *method = (ClassMethod*) calloc(1, sizeof(ClassMethod));
						method->name = strdup(fieldName);
						method->retType = type;
						method->prot = prot;
						method->args = args;
						method->flags = flags;
						method->tok = nameToken;
						method->tem = tem;
						if (slave != NULL) method->overrides = 1;
						
						if (flags & PER_Q_STATIC)
						{
							if (externName == NULL)
							{
								method->mangled = mangleName("Per_FD##??*",
									isManagedType(type) ? 'M' : 0,
									(type->kind == PER_TYPE_ARRAY) ? 'A' : 0,
									tem->fullname, fieldName, args);
							}
							else
							{
								method->mangled = strdup(externName);
							};
							
							method->body = body;
						}
						else
						{
							if (slave == NULL)
							{
								method->mangled = mangleName("Per_VF##??*",
									isManagedType(type) ? 'M' : 0,
									(type->kind == PER_TYPE_ARRAY) ? 'A' : 0,
									tem->fullname, fieldName, args);
							}
							else
							{
								method->mangled = strdup(slave->mangled);
							};

							if ((flags & PER_Q_ABSTRACT) == 0)
							{
								if (externName == NULL)
								{
									method->implementor = mangleName("Per_FD##??*",
										isManagedType(type) ? 'M' : 0,
										(type->kind == PER_TYPE_ARRAY) ? 'A' : 0,
										tem->fullname, fieldName, staticArgs);
								}
								else
								{
									method->implementor = strdup(externName);
								};
							};
						};
						
						if (tem->methods != NULL)
						{
							tem->lastMethod->next = method;
							tem->lastMethod = method;
						}
						else
						{
							tem->methods = tem->lastMethod = method;
						};
						
						// add the static one
						if (virtual && (flags & PER_Q_ABSTRACT) == 0)
						{
							method = (ClassMethod*) calloc(1, sizeof(ClassMethod));
							method->name = strdup(fieldName);
							method->retType = type;
							method->prot = prot;
							method->args = staticArgs;
							method->flags = PER_Q_STATIC;
							method->tok = nameToken;
							method->tem = tem;
							if (externName == NULL)
							{
								method->mangled = mangleName("Per_FD##??*",
									isManagedType(type) ? 'M' : 0,
									(type->kind == PER_TYPE_ARRAY) ? 'A' : 0,
									tem->fullname, fieldName, staticArgs);
							}
							else
							{
								method->mangled = strdup(externName);
							};
							method->body = body;
							
							if (tem->methods != NULL)
							{
								tem->lastMethod->next = method;
								tem->lastMethod = method;
							}
							else
							{
								tem->methods = tem->lastMethod = method;
							};
						};
					}
					else
					{
						int allowedFlags = PER_Q_STATIC | PER_Q_FINAL;
						if ((flags & allowedFlags) != flags)
						{
							lexDiag(tokQualifs, CON_ERROR, "only `static' and `final' qualifiers are allowed on a field");
							return NULL;
						};
						
						if (type->kind == PER_TYPE_VOID)
						{
							lexDiag(tokQualifs, CON_ERROR, "a field cannot have the `void' type");
							return NULL;
						};
						
						if (((tem->flags & PER_Q_STATIC) != 0) && ((flags & PER_Q_STATIC) == 0))
						{
							lexDiag(tokQualifs, CON_ERROR, "all members of a static class must themselves be static");
							return NULL;
						};
						
						if (!canDefineField(tem, fieldName, nameToken, PER_PROT_PRIVATE))
						{
							// (error printed by canDefineField() already)
							return NULL;
						};
						
						ClassField *field = (ClassField*) calloc(1, sizeof(ClassField));
						field->name = fieldName;
						field->type = type;
						field->prot = prot;
						field->flags = flags;
						field->tok = nameToken;
						
						lexTag(nameToken, TAG_FIELD_NAME, NULL, nameToken->value);
						lexTag(nameToken, TAG_DEFINE_FIELD, NULL, nameToken->value);
						
						if (flags & PER_Q_STATIC)
						{
							if (isManagedType(type))
							{
								field->mangled = mangleName("Per_SFM??", tem->fullname, fieldName);
							}
							else
							{
								field->mangled = mangleName("Per_SF??", tem->fullname, fieldName);
							};
						}
						else
						{
							if (isManagedType(type))
							{
								field->mangled = mangleName("Per_FOM??", tem->fullname, fieldName);
							}
							else
							{
								field->mangled = mangleName("Per_FO??", tem->fullname, fieldName);
							};
						};
						
						if (tem->lastField == NULL)
						{
							tem->fields = tem->lastField = field;
						}
						else
						{
							tem->lastField->next = field;
							tem->lastField = field;
						};
						
						if (tok->type == TOK_PER_ASSIGN)
						{
							tok = tok->next;
							
							ExprParserContext xctx;
							initExprParser(&xctx, tok, tem, typeNameTable);
							
							Expression *expr = parseExpression(&xctx);
							if (expr == NULL || xctx.aborted)
							{
								lexDiag(xctx.errtok, CON_ERROR, "%s", xctx.errmsg);
								return NULL;
							};
							
							tok = xctx.tok;
							
							field->initExpr = expr;
						};
						
						if (tok->type != TOK_PER_SEMICOLON)
						{
							lexDiag(tok, CON_ERROR, "expected `;' not `%s'", tok->value);
							return NULL;
						};
						
						tok = tok->next;	// skip the ';'
					};
				};
			};
		};
		
		// if there is no default constructor, then define a blank one
		int haveDefault = 0;
		ClassConstructor *ctor;
		for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
		{
			if (ctor->args == NULL)
			{
				haveDefault = 1;
				break;
			};
		};
		
		if (!haveDefault)
		{
			ctor = (ClassConstructor*) calloc(1, sizeof(ClassConstructor));
			ctor->next = tem->ctors;
			tem->ctors = ctor;
			
			ctor->prot = PER_PROT_PUBLIC;
			ctor->tok = tem->tok;
			ctor->body = (CompoundStatement*) calloc(1, sizeof(CompoundStatement));
			ctor->mangled = mangleName("Per_CC?", fullname);
		};
		
		// if this is a non-abstract class, verify that it overrides any abstract methods
		// TODO: move this to emitting, otherwise it just breaks half the time
		(void)closingBrace;
#if 0
		if ((tem->flags & PER_Q_ABSTRACT) == 0 && mainTarget)
		{
			// hash table mapping the virtual indicator names (Per_VF*) to the ClassMethod
			// which defines them
			HashTable *methodMap = hashtabNew();
			getMethodMap(findClassByName(NULL, "std.rt.Object"), methodMap);
			getMethodMap(tem, methodMap);
			
			const char *mname;
			HASHTAB_FOREACH(methodMap, ClassMethod*, mname, method)
			{
				(void)mname;
				
				if (method->flags & PER_Q_ABSTRACT)
				{
					// an abstract method which was not overriden
					lexDiag(closingBrace, CON_ERROR, "non-abstract class `%s' does not override abstract method %s(%s); override the method, or declare the class abstract", tem->fullname, method->name, getArgumentListFormatted(method->args));
					lexDiag(method->tok, CON_NOTE, "defined here");
				};
			};
		};
#endif
		
		// add this class to the compilation unit
		int index = unit->numClasses++;
		unit->classList = (ClassTemplate**) realloc(unit->classList, sizeof(void*) * unit->numClasses);
		unit->classList[index] = tem;
	};
	
	return unit;
};

char* getTypeName(Type *type)
{
	if (type->kind == PER_TYPE_ARRAY)
	{
		char *sub = getTypeName(type->elementType);
		sub = (char*) realloc(sub, strlen(sub)+3);
		strcat(sub, "[]");
		return sub;
	}
	else if (type->kind == PER_TYPE_MANAGED)
	{
		char *buf = strdup(type->fullname);
		
		if (hashtabHasKey(classTable, type->fullname))
		{
			ClassTemplate *tem = (ClassTemplate*) hashtabGet(classTable, type->fullname);
			if (tem->numTemParams != 0)
			{
				buf = (char*) realloc(buf, strlen(buf)+2);
				strcat(buf, "<");
				
				int i;
				for (i=0; i<tem->numTemParams; i++)
				{
					const char *pname = tem->temParams[i].name;
					Type *subtype = (Type*) hashtabGet(type->temParamTypes, pname);
					
					char *subname = getTypeName(subtype);
					buf = (char*) realloc(buf, strlen(buf) + strlen(subname) + 4);
					strcat(buf, subname);
					
					free(subname);
					
					if (i == tem->numTemParams-1)
					{
						strcat(buf, "> ");
					}
					else
					{
						strcat(buf, ", ");
					};
				};
			};
		};
		
		return buf;
	}
	else if (type->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		return strdup(type->fullname);
	}
	else if (type->kind == PER_TYPE_NULL)
	{
		return strdup("<null>");
	}
	else if (type->kind == PER_TYPE_METHOD)
	{
		return strdup("<method>");
	}
	else if (type->kind == PER_TYPE_CLASS)
	{
		return strdup("<class>");
	}
	else if (type->kind == PER_TYPE_FUNCREF)
	{
		char *retType = getTypeName(type->retType);
		char *result = (char*) malloc(strlen(retType) + 16);
		sprintf(result, "function<%s", retType);
		free(retType);
		
		if (type->numArgs != 0)
		{
			char *newResult = (char*) malloc(strlen(result) + 8);
			sprintf(newResult, "%s : ", result);
			free(result);
			result = newResult;
		};
		
		int i;
		for (i=0; i<type->numArgs; i++)
		{
			char *argType = getTypeName(type->argTypes[i]);
			char *newResult = (char*) malloc(strlen(result) + strlen(argType) + 8);
			sprintf(newResult, "%s%s", result, argType);
			free(result);
			free(argType);
			result = newResult;
			
			if (i != type->numArgs-1)
			{
				strcat(result, ", ");
			};
		};
		
		result = (char*) realloc(result, strlen(result)+2);
		strcat(result, ">");
		return result;
	}
	else
	{
		static const char *primTypeNames[] = {	"void", "bool", "float", "double", "byte_t", "sbyte_t", "word_t", "sword_t",
							"dword_t", "int", "qword_t", "long", "ptr_t", "sptr_t"
							};
		return strdup(primTypeNames[type->kind]);
	};
};

char* getQualifNames(int qualifs)
{
	static const char *qualifNames[] = {"static", "abstract", "final", NULL};
	int size = 1;
	
	int i;
	for (i=0; qualifNames[i]!=NULL; i++)
	{
		size += strlen(qualifNames[i]) + 1;
	};
	
	char *buffer = (char*) malloc(size);
	buffer[0] = 0;
	
	for (i=0; qualifNames[i]!=NULL; i++)
	{
		if (qualifs & (1 << i))
		{
			strcat(buffer, qualifNames[i]);
			strcat(buffer, " ");
		};
	};
	
	return (char*) realloc(buffer, strlen(buffer) + 1);
};
