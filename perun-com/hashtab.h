/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HASHTAB_H_
#define HASHTAB_H_

/**
 * Number of buckets in hash tables. Note that this MUST be a power
 * of 2!
 */
#define	HASH_NUM_BUCKETS			128

/**
 * A nice macro to iterate over a hash table.
 */
#define	HASHTAB_FOREACH(tab, type, k, x) HashIterator iter_##x; type x; for (hashtabIterInit(&iter_##x, tab); iter_##x.ent != NULL && ((x = (type) iter_##x.ent->value), (k = iter_##x.ent->key), 1); hashtabIterNext(&iter_##x))

/**
 * Hash table bucket entry.
 */
typedef struct HashEntry_
{
	struct HashEntry_*			next;
	
	/**
	 * Key, on the heap (release using free()).
	 */
	char*					key;
	
	/**
	 * Value.
	 */
	void*					value;
} HashEntry;

/**
 * Hash table. Maps key strings to arbitrary pointers.
 */
typedef struct
{
	/**
	 * Buckets containing entries.
	 */
	HashEntry*				buckets[HASH_NUM_BUCKETS];
} HashTable;

/**
 * Represents an iterator through a hashtable, used by hashtabFirst() and hashtabNext().
 */
typedef struct
{
	/**
	 * The hash table being iterated over.
	 */
	HashTable *tab;
	
	/**
	 * Bucket index of current element.
	 */
	int bucket;
	
	/**
	 * Current entry.
	 */
	HashEntry *ent;
} HashIterator;

/**
 * Create a new, empty hash table.
 */
HashTable* hashtabNew();

/**
 * Set a value in a hash table. If the key already exists, it is updated.
 */
void hashtabSet(HashTable *tab, const char *key, void *value);

/**
 * Get a value from a hash table. If the key does not exist, this function aborts!
 * If it is not certain whether the key exists, always call hashtabHasKey() before!
 * 
 * Note that NULL may be a perfectly valid value, if it was set as such by hashtabSet().
 */
void* hashtabGet(HashTable *tab, const char *key);

/**
 * Test if a hash table contains a key. Returns TRUE if the key exists, otherwise FALSE.
 */
int hashtabHasKey(HashTable *tab, const char *key);

/**
 * Delete a key from a hash table. Does nothing if the key already doesn't exist.
 */
void hashtabUnset(HashTable *tab, const char *key);

/**
 * Delete the whole hash table. Do not use the 'tab' pointer afterwards!
 */
void hashtabDelete(HashTable *tab);

/**
 * Make a shallow copy of a hash table.
 */
HashTable* hashtabClone(HashTable *tab);

/**
 * Initialize the hash iterator.
 */
void hashtabIterInit(HashIterator *iter, HashTable *tab);

/**
 * Move an iterator to the next element.
 */
void hashtabIterNext(HashIterator *iter);

#endif