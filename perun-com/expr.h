/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EXPR_H_
#define EXPR_H_

#include "parse.h"
#include "lex.h"

/**
 * Typedef all the structs here.
 */
typedef struct Expression_ Expression;
typedef struct AssignmentExpression_ AssignmentExpression;
typedef struct ConditionalExpression_ ConditionalExpression;
typedef struct UnaryExpression_ UnaryExpression;
typedef struct LogicalORExpression_ LogicalORExpression;
typedef struct LogicalANDExpression_ LogicalANDExpression;
typedef struct BitwiseORExpression_ BitwiseORExpression;
typedef struct BitwiseXORExpression_ BitwiseXORExpression;
typedef struct BitwiseANDExpression_ BitwiseANDExpression;
typedef struct EqualityExpression_ EqualityExpression;
typedef struct RelationalExpression_ RelationalExpression;
typedef struct ShiftExpression_ ShiftExpression;
typedef struct AdditiveExpression_ AdditiveExpression;
typedef struct MultiplicativeExpression_ MultiplicativeExpression;
typedef struct CastExpression_ CastExpression;
typedef struct PostfixExpression_ PostfixExpression;
typedef struct PrimaryExpression_ PrimaryExpression;
typedef struct SubscriptPostfix_ SubscriptPostfix;
typedef struct CallPostfix_ CallPostfix;
typedef struct MemberPostfix_ MemberPostfix;
typedef struct NewExpression_ NewExpression;
typedef struct ImportExpression_ ImportExpression;
typedef struct ArgumentExpressionList_ ArgumentExpressionList;
typedef struct InstancePostfix_ InstancePostfix;
typedef struct LambdaExpression_ LambdaExpression;

/**
 * LambdaExpression ::=
 * 	'lambda' '<' TypeSpecification '>' '(' [ArgumentSpecificationList] ')' '=>' Expression
 */
struct LambdaExpression_
{
	Token* tok;
	Type* retType;
	ArgumentSpecificationList *args;
	Expression* expr;
};

/**
 * NewExpression ::=
 * 		'new' TypeSpecification '(' ArgumentExpressionList ')'
 * 	|	'new' TypeSpecification '[' Expression ']'
 * 	|	'new' TypeSpecification '{' ArgumentExpressionList '}'
 */
struct NewExpression_
{
	Token *tok;
	Type *type;
	
	// if 'type' is non-array, one or zero of these are non-NULL.
	// 'size' is non-NULL if it's a fixed array initializer
	ArgumentExpressionList *args;
	Expression *size;
	
	// if 'type' is an array, this one is non-NULL:
	ArgumentExpressionList *inits;
};

/**
 * ImportExpression ::=
 * 		'import' '(' String ')'
 */
struct ImportExpression_
{
	Token *str;
};

/**
 * PrimaryExpression ::=
 * 		'(' Expression ')'
 *	|	Constant
 * 	|	Identifier
 *	|	BuildVar
 * 	|	NewExpression
 * 	|	ImportExpression
 */
struct PrimaryExpression_
{
	Token *firstTok;
	Expression *expr;
	Token *tok;
	Token *bvar;
	NewExpression *newExpr;
	ImportExpression *importExpr;
	LambdaExpression *lambdaExpr;
};

/**
 * SubscriptPostfix ::=
 * 	'[' Expression ']'
 */
struct SubscriptPostfix_
{
	Expression *expr;
};

/**
 * ArgumentExpressionList ::=
 * 	AssignmentExpression [',' ArgumentExpressionList]
 */
struct ArgumentExpressionList_
{
	AssignmentExpression *assign;
	ArgumentExpressionList *next;
};

/**
 * CallPostfix ::=
 * 	'(' [ArgumentExpressionList] ')'
 */
struct CallPostfix_
{
	ArgumentExpressionList *head;
};

/**
 * MemberPostfix ::=
 * 	'.' Identifier
 */
struct MemberPostfix_
{
	Token *id;
};

/**
 * InstancePostfix ::=
 * 	'is' TypeSpecification
 */
struct InstancePostfix_
{
	Token *tok;
	Type *type;
};

/**
 * PostfixExpression ::=
 * 	PrimaryExpression [PostfixChain]
 * PostfixChain ::=
 * 	Postfix [PostfixChain]
 */
struct PostfixExpression_
{
	PostfixExpression *left;
	
	// ONE of these:
	PrimaryExpression *primary;
	SubscriptPostfix *subscript;
	CallPostfix *call;
	MemberPostfix *member;
	InstancePostfix *inst;
	Token *op;			// '++' or '--'
};

/**
 * UnaryExpression ::=
 * 		PostfixExpression
 * 	|	'++' PostfixExpression
 * 	|	'--' PostfixExpression
 * 	|	UnaryOperator CastExpression
 */
struct UnaryExpression_
{
	// this:
	PostfixExpression *postfix;
	
	// OR this:
	Token *op;
	
	// with this:
	PostfixExpression *sub;
	
	// OR this:
	CastExpression *cast;
};

/**
 * CastExpression ::=
 * 		'(' TypeSpecification ')' CastExpression
 *	|	UnaryExpression
 */
struct CastExpression_
{
	// either this:
	Type *type;
	CastExpression *sub;
	
	// OR this:
	UnaryExpression *unary;
};

/**
 * MultiplicativeExpression ::=
 * 	CastExpression [MultiplicativeOperator MultiplicativeExpression]
 */
struct MultiplicativeExpression_
{
	CastExpression *cast;
	Token *op;
	MultiplicativeExpression *next;
};

/**
 * AdditiveExpression ::=
 * 	MultiplicativeExpression [AdditiveOperator AdditiveExpression]
 */
struct AdditiveExpression_
{
	MultiplicativeExpression *mul;
	Token *op;
	AdditiveExpression *next;
};

/**
 * ShiftExpression ::=
 * 	AdditiveExpression [ShiftOperator ShiftExpression]
 */
struct ShiftExpression_
{
	AdditiveExpression *add;
	Token *op;
	ShiftExpression *next;
};

/**
 * RelationalExpression ::=
 * 	ShiftExpression [RelationalOperator RelationalExpression]
 */
struct RelationalExpression_
{
	ShiftExpression *shift;
	Token *op;
	RelationalExpression *next;
};

/**
 * EqualityExpression ::=
 * 	RelationalExpression [EqualityOperator EqualityExpression]
 */
struct EqualityExpression_
{
	RelationalExpression *rel;
	Token *op;
	EqualityExpression *next;
};

/**
 * BitwiseANDExpression ::=
 * 	EqualityExpression ['&' BitwiseANDExpression]
 */
struct BitwiseANDExpression_
{
	EqualityExpression *equality;
	Token *op;		/* & */
	BitwiseANDExpression *next;
};

/**
 * BitwiseXORExpression ::=
 * 	BitwiseANDExpression ['^' BitwiseXORExpression]
 */
struct BitwiseXORExpression_
{
	BitwiseANDExpression *bitwiseAND;
	Token *op;		/* ^ */
	BitwiseXORExpression *next;
};

/**
 * BitwiseORExpression ::=
 * 	BitwiseXORExpression ['|' BitwiseORExpression]
 */
struct BitwiseORExpression_
{
	BitwiseXORExpression *bitwiseXOR;
	Token *op;		/* | */
	BitwiseORExpression *next;
};

/**
 * LogicalANDExpression ::=
 * 	BitwiseORExpression ['&&' LogicalANDExpression]
 */
struct LogicalANDExpression_
{
	BitwiseORExpression *bitwiseOR;
	Token *op;		/* && */
	LogicalANDExpression *next;
};

/**
 * LogicalORExpression ::=
 * 	LogicalANDExpression ['||' LogicalORExpression]
 */
struct LogicalORExpression_
{
	LogicalANDExpression *logicalAND;
	Token *op;		/* || */
	LogicalORExpression *next;
};

/**
 * ConditionalExpression ::=
 * 	LogicalORExpression ['?' Expression ':' ConditionalExpression]
 */
struct ConditionalExpression_
{
	LogicalORExpression *logicalOR;
	Expression *exprTrue;
	ConditionalExpression *exprFalse;
	Token *op;
};

/**
 * AssignmentExpression ::=
 * 		ConditionalExpression
 *	|	PostfixExpression AssignmentOperator AssignmentExpression
 */
struct AssignmentExpression_
{
	// this:
	ConditionalExpression *cond;
	
	// OR:
	PostfixExpression *left;
	Token *op;
	AssignmentExpression *right;
};

/**
 * Expression ::=
 * 	AssignmentExpression [',' Expression]
 */
struct Expression_
{
	AssignmentExpression *assign;
	Expression *next;
};

/**
 * Represents a parser context.
 */
typedef struct
{
	/**
	 * The current token we are on.
	 */
	Token *tok;
	
	/**
	 * Token on which an error occured, and the error string, if applicable.
	 * 'errmsg' being NULL means there was no error.
	 */
	Token *errtok;
	char *errmsg;
	
	/**
	 * If set to 1, then the parser has aborted: no more error messages shall be
	 * reported.
	 */
	int aborted;
	
	/**
	 * The class definition inside of which the expression was found.
	 */
	ClassTemplate *inTemplate;
	
	/**
	 * The type name table.
	 */
	HashTable *typeNameTable;
} ExprParserContext;

/**
 * Initialize an expression parser context.
 */
void initExprParser(ExprParserContext *ctx, Token *tok, ClassTemplate *inTemplate, HashTable *typeNameTable);

/**
 * These functions each accept a production of the expression grammar. They return the
 * corresponding parse tree node on success, or NULL on error. If an error occurs, then
 * an error message is also set.
 * 
 * If a production is accepted, the token in the context is shifted forward, past the
 * end of the production. If the production is not accepted, the token pointer shall
 * remain unchanged.
 */
LambdaExpression* parseLambdaExpression(ExprParserContext *ctx);
NewExpression* parseNewExpression(ExprParserContext *ctx);
ImportExpression* parseImportExpression(ExprParserContext *ctx);
PrimaryExpression* parsePrimaryExpression(ExprParserContext *ctx);
ArgumentExpressionList* parseArgumentExpressionList(ExprParserContext *ctx);
SubscriptPostfix* parseSubscriptPostfix(ExprParserContext *ctx);
CallPostfix* parseCallPostfix(ExprParserContext *ctx);
MemberPostfix* parseMemberPostfix(ExprParserContext *ctx);
InstancePostfix* parseInstancePostfix(ExprParserContext *ctx);
PostfixExpression* parsePostfix(ExprParserContext *ctx);
PostfixExpression* parsePostfixExpression(ExprParserContext *ctx);
UnaryExpression* parseUnaryExpression(ExprParserContext *ctx);
CastExpression* parseCastExpression(ExprParserContext *ctx);
MultiplicativeExpression* parseMultiplicativeExpression(ExprParserContext *ctx);
AdditiveExpression* parseAdditiveExpression(ExprParserContext *ctx);
ShiftExpression* parseShiftExpression(ExprParserContext *ctx);
RelationalExpression* parseRelationalExpression(ExprParserContext *ctx);
EqualityExpression* parseEqualityExpression(ExprParserContext *ctx);
BitwiseANDExpression* parseBitwiseANDExpression(ExprParserContext *ctx);
BitwiseXORExpression* parseBitwiseXORExpression(ExprParserContext *ctx);
BitwiseORExpression* parseBitwiseORExpression(ExprParserContext *ctx);
LogicalANDExpression* parseLogicalANDExpression(ExprParserContext *ctx);
LogicalORExpression* parseLogicalORExpression(ExprParserContext *ctx);
ConditionalExpression* parseConditionalExpression(ExprParserContext *ctx);
AssignmentExpression* parseAssignmentExpression(ExprParserContext *ctx);
Expression* parseExpression(ExprParserContext *ctx);

#endif
