/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef _WIN32
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#endif

#include "cci.h"

extern int noLibPerun;
extern int verbose;

int cciCompile(const char *csrc, const char *outfile, int shared)
{
#ifdef _WIN32
	const char *template = "\"cc\" \"-c\" \"%s\" \"-o\" \"%s\" \"-w\" -I/mbsroot/include -O3";
	if (shared) template = "\"cc\" \"-c\" \"%s\" \"-o\" \"%s\" \"-w\" \"-fPIC\" -I/mbsroot/include -O3";
	
	char *cmdline = (char*) malloc(strlen(template) + strlen(csrc) + strlen(outfile) + 1);
	sprintf(cmdline, template, csrc, outfile);
	
	if (verbose)
	{
		printf("Invoking C compiler using: %s\n", cmdline);
	};
	
	STARTUPINFO sinfo;
	memset(&sinfo, 0, sizeof(STARTUPINFO));
	sinfo.cb = sizeof(sinfo);
	PROCESS_INFORMATION pinfo;
	memset(&pinfo, 0, sizeof(PROCESS_INFORMATION));
	
	if (!CreateProcess(NULL, cmdline, NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, NULL, NULL, &sinfo, &pinfo))
	{
		free(cmdline);
		fprintf(stderr, "FATAL: CreateProcess() failed\n");
		return -1;
	};
	
	WaitForSingleObject(pinfo.hProcess, INFINITE);
	
	DWORD exitCode;
	GetExitCodeProcess(pinfo.hProcess, &exitCode);
	
	CloseHandle(pinfo.hProcess);
	CloseHandle(pinfo.hThread);
	
	if (exitCode != 0)
	{
		free(cmdline);
		fprintf(stderr, "FATAL: C compiler error has occured.\n");
		return -1;
	};
	
	free(cmdline);
	return 0;
#else
	pid_t pid = fork();
	if (pid == -1)
	{
		perror("fork failed");
		return -1;
	}
	else if (pid == 0)
	{
		int fd = open("perun-error.log", O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (fd == -1)
		{
			fprintf(stderr, "failed to open perun-error.log: %s\n", strerror(errno));
			_exit(1);
		};
		
		close(1);
		close(2);
		assert(dup(fd) != -1);
		assert(dup(fd) != -1);
		
		const char *pic = NULL;
		if (shared) pic = "-fPIC";

		execl("/usr/bin/env", "/usr/bin/env", PERUN_CC, "-c", csrc, "-o", outfile, "-O3", "-w", pic, NULL);
		perror("exec");
		_exit(1);
	}
	else
	{
		int status;
		waitpid(pid, &status, 0);
		
		if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
		{
			remove("perun-error.log");
			return 0;
		}
		else
		{
			fprintf(stderr, "FATAL: C compiler error has occured. See perun-error.log for details.\n");
			return -1;
		};
	};
#endif
};

static void addArg(LinkRequest *req, const char *arg)
{
	int index = req->argc++;
	req->argv = (char**) realloc(req->argv, sizeof(void*) * req->argc);
	if (arg == NULL)
	{
		req->argv[index] = NULL;
	}
	else
	{
		req->argv[index] = strdup(arg);
	};
};

void cciInitLink(LinkRequest *req, int makeShared, const char *outputFile)
{
#ifdef _WIN32
	req->argv = NULL;
	req->argc = 0;
#else
	req->argv = (char**) malloc(sizeof(void*) * 1);
	req->argv[0] = strdup("/usr/bin/env");
	req->argc = 1;
#endif

	addArg(req, PERUN_CC);
	if (makeShared) addArg(req, "-shared");
	addArg(req, "-o");
	addArg(req, outputFile);
};

void cciAddLink(LinkRequest *req, const char *filename)
{
	addArg(req, filename);
};

int cciAddStatic(LinkRequest *req, const char *libname)
{
	char *flagsPath = (char*) malloc(strlen(PERUN_STATICLIB_PATH) + strlen(libname) + 64);
	sprintf(flagsPath, "%s/lib%s.ldflags", PERUN_STATICLIB_PATH, libname);
	
	FILE *fp = fopen(flagsPath, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "perun-com: cannot read `%s' when linking in `-s%s'", flagsPath, libname);
		return -1;
	};
	
	char linebuf[1024];
	char *line = fgets(linebuf, 1024, fp);
	
	if (line == NULL)
	{
		fprintf(stderr, "perun-com: cannot read `%s' when linking in `-s%s'", flagsPath, libname);
		return -1;
	};
	
	fclose(fp);
	
	char *newline = strchr(line, '\n');
	if (newline != NULL) *newline = 0;
	
	char *tok;
	for (tok=strtok(line, " "); tok!=NULL; tok=strtok(NULL, " "))
	{
		addArg(req, tok);
	};
	
	return 0;
};

int cciLink(LinkRequest *req)
{
	// link against the Perun standard library if not instructed otherwise
	if (!noLibPerun) addArg(req, "-lperun");

#ifdef _WIN32
	addArg(req, "-I/mbsroot/include");
#endif

	// terminate the argument list
	addArg(req, NULL);

	if (verbose)
	{
		printf("About to invoke linker, with the following arguments:\n[");
		
		int i;
		for (i=0; req->argv[i]!=NULL; i++)
		{
			printf("'%s'", req->argv[i]);
			if (req->argv[i+1] != NULL)
			{
				printf(", ");
			};
		};
		
		printf("]\n");
	};

#ifdef _WIN32
	char *cmdline = strdup("");
	int i;
	for (i=0; req->argv[i]!=NULL; i++)
	{
		char *newLine = (char*) malloc(strlen(cmdline) + strlen(req->argv[i]) + 16);
		sprintf(newLine, "%s\"%s\" ", cmdline, req->argv[i]);
		
		free(cmdline);
		cmdline = newLine;
	};

	if (verbose)
	{
		printf("Invoking linker using: %s\n", cmdline);
	};

	STARTUPINFO sinfo;
	memset(&sinfo, 0, sizeof(STARTUPINFO));
	sinfo.cb = sizeof(sinfo);
	PROCESS_INFORMATION pinfo;
	memset(&pinfo, 0, sizeof(PROCESS_INFORMATION));
	
	if (!CreateProcess(NULL, cmdline, NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, NULL, NULL, &sinfo, &pinfo))
	{
		free(cmdline);
		fprintf(stderr, "FATAL: CreateProcess() failed\n");
		return -1;
	};
	
	WaitForSingleObject(pinfo.hProcess, INFINITE);
	
	DWORD exitCode;
	GetExitCodeProcess(pinfo.hProcess, &exitCode);
	
	CloseHandle(pinfo.hProcess);
	CloseHandle(pinfo.hThread);
	
	if (exitCode == 0)
	{
		return 0;
	};
	
	return -1;
#else
	pid_t pid = fork();
	if (pid == -1)
	{
		perror("fork failed");
		return -1;
	}
	else if (pid == 0)
	{
		execv("/usr/bin/env", req->argv);
		perror("exec");
		_exit(1);
	}
	else
	{
		int status;
		waitpid(pid, &status, 0);
		
		if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
		{
			return 0;
		}
		else
		{
			return -1;
		};
	};
#endif
};
