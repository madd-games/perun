/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "catalog.h"
#include "parse.h"
#include "hashtab.h"
#include "console.h"

/**
 * Class path.
 */
const char **classPath;
int numClassPath;

/**
 * Hash table mapping class fullnames to the files which are supposed to define
 * the classes.
 */
HashTable *hashtabClassFiles;

/**
 * Hash table mapping namespace names to a list of classes in that namespace (ClassNameList*).
 */
HashTable* hashtabNamespaces;

/**
 * Hash table mapping class fullnames to ClassInfo* about them.
 */
HashTable* hashtabClassInfo;

static char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

void addClassPath(const char *dir)
{
	int index = numClassPath++;
	classPath = (const char**) realloc(classPath, sizeof(void*) * numClassPath);
	classPath[index] = dir;
};

static void doClassSearch(const char *dirname, const char *currentNamespace, int depth)
{
	if (depth >= 16) return;
	
	DIR *dirp = opendir(dirname);
	if (dirp == NULL)
	{
		return;
	};
	
	struct dirent *ent;
	while ((ent = readdir(dirp)))
	{
		if (ent->d_name[0] == '.') continue;
		
		char *itemPath = (char*) malloc(strlen(dirname) + strlen(ent->d_name) + 2);
		sprintf(itemPath, "%s/%s", dirname, ent->d_name);
		
		struct stat st;
		if (stat(itemPath, &st) != 0)
		{
			free(itemPath);
			continue;
		};
		
		if (S_ISDIR(st.st_mode))
		{
			// subdirectory, so search it too
			char *namespace;
			if (currentNamespace == NULL)
			{
				namespace = strdup(ent->d_name);
			}
			else
			{
				namespace = (char*) malloc(strlen(currentNamespace) + strlen(ent->d_name) + 2);
				sprintf(namespace, "%s.%s", currentNamespace, ent->d_name);
			};
			
			doClassSearch(itemPath, namespace, depth+1);
			free(namespace);
			free(itemPath);
		}
		else if (strlen(ent->d_name) > 4 && strcmp(&ent->d_name[strlen(ent->d_name)-4], ".per") == 0 && currentNamespace != NULL)
		{
			// Perun class file
			char *className;
			className = (char*) malloc(strlen(currentNamespace) + strlen(ent->d_name) + 2);
			sprintf(className, "%s.%s", currentNamespace, ent->d_name);
			
			// remove ".per" from the end of the class name
			className[strlen(className)-4] = 0;
			
			// store in the class path hashtable
			hashtabSet(hashtabClassFiles, className, itemPath);
			
			// also add to the list for that namespace
			ClassNameList *ent = (ClassNameList*) calloc(1, sizeof(ClassNameList));
			ent->fullname = className;
			
			if (hashtabHasKey(hashtabNamespaces, currentNamespace))
			{
				ent->next = (ClassNameList*) hashtabGet(hashtabNamespaces, currentNamespace);
			};
			
			hashtabSet(hashtabNamespaces, currentNamespace, ent);
		};
	};
	
	closedir(dirp);
};

void dumpCatalog()
{
	// print out namespace lists
	fprintf(stderr, "Namespaces:\n");
	const char *key;
	HASHTAB_FOREACH(hashtabNamespaces, ClassNameList*, key, list)
	{
		fprintf(stderr, "  Namespace `%s' contains these classes:\n", key);
		
		for (; list!=NULL; list=list->next)
		{
			fprintf(stderr, "    `%s'\n", list->fullname);
		};
	};
	
	// now print the class file locations
	fprintf(stderr, "\nClass file locations:\n");
	HASHTAB_FOREACH(hashtabClassFiles, char*, key, path)
	{
		fprintf(stderr, "  %s -> %s\n", key, path);
	};
};

void mapClass(HashTable *shortNames, const char *fullname)
{
	char *last = strrchr(fullname, '.');
	hashtabSet(shortNames, last+1, strdup(fullname));
};

void mapNamespace(HashTable *shortNames, const char *namespace)
{
	if (hashtabHasKey(hashtabNamespaces, namespace))
	{
		ClassNameList *list = (ClassNameList*) hashtabGet(hashtabNamespaces, namespace);
		for (; list!=NULL; list=list->next)
		{
			mapClass(shortNames, list->fullname);
		};
	};
};

char* parseTentativeTypeSpec(HashTable *shortNames, Token **scanner)
{
	Token *tok = *scanner;
	
	if (tok->type != TOK_PER_ID)
	{
		lexDiag(tok, CON_ERROR, "expected a class name, not `%s'", tok->value);
		exit(1);
	};
	
	if (!hashtabHasKey(shortNames, tok->value))
	{
		lexDiag(tok, CON_ERROR, "the identifier `%s' does not name a class", tok->value);
		exit(1);
	};
	
	char *fullname = (char*) hashtabGet(shortNames, tok->value);
	tok = tok->next;
	
	*scanner = tok;
	
	if (tok->type == TOK_PER_LARROW)
	{
		tok = tok->next;
		
		int depth = 0;
		while (1)
		{
			if (tok->type == TOK_PER_RARROW)
			{
				if (depth == 0)
				{
					*scanner = tok->next;
					break;
				};
				
				depth--;
			}
			else if (tok->type == TOK_PER_LARROW)
			{
				depth++;
			}
			else if (tok->type == TOK_END)
			{
				lexDiag(tok, CON_ERROR, "unterminated arrow bracket");
				exit(1);
			};
			
			tok = tok->next;
		};
	};
	
	return fullname;
};

ClassInfo* doAnalysis(const char *className);
void addDependency(ClassInfo *info, const char *depname, Token *tok)
{
	ClassInfo *dep = doAnalysis(depname);
	if (dep == NULL)
	{
		lexDiag(tok, CON_ERROR, "circular dependency between `%s' and `%s'", info->fullname, depname);
		exit(1);
	};
	
	int index = info->numDeps++;
	info->deps = (ClassInfo**) realloc(info->deps, sizeof(void*) * info->numDeps);
	info->deps[index] = dep;
};

ClassInfo* doAnalysis(const char *className)
{
	if (hashtabHasKey(hashtabClassInfo, className))
	{
		return (ClassInfo*) hashtabGet(hashtabClassInfo, className);
	};
	
	// mark this class as being currently in analysis
	hashtabSet(hashtabClassInfo, className, NULL);
	
	// if we're performing analysis, it is assumed that the class is available in the path,
	// so if it isn't, then it is safe to let hashtabGet() abort.
	const char *filename = (const char*) hashtabGet(hashtabClassFiles, className);

	char *expectedNamespace = strdup(className);
	char *finalDot = strrchr(expectedNamespace, '.');
	*finalDot = 0;
	
	FILE *fp = fopen(filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "cannot open %s: %s\n", filename, strerror(errno));
		abort();
	};
	
	char *data = readWholeFile(fp);
	if (data == NULL)
	{
		fprintf(stderr, "failed to read %s\n", filename);
		abort();
	};
	
	fclose(fp);
	
	Token *tok = parsePerun(data, filename);
	if (tok == NULL)
	{
		// don't need to abort, just let the tokenization error remain in the console
		// and exit with an error
		exit(1);
	};
	
	Token *namespaceTok = tok;
	if (tok->type != TOK_PER_NAMESPACE)
	{
		lexDiag(tok, CON_ERROR, "expected `namespace', not `%s'", tok->value);
		exit(1);
	};
	
	tok = tok->next;
	
	if (tok->type != TOK_PER_ID)
	{
		lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
		exit(1);
	};
	
	char *namespace = strdup(tok->value);
	tok = tok->next;
	
	while (tok->type == TOK_PER_DOT)
	{
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			exit(1);
		};
		
		char *newns = (char*) malloc(strlen(namespace) + strlen(tok->value) + 2);
		sprintf(newns, "%s.%s", namespace, tok->value);
		free(namespace);
		namespace = newns;
		
		tok = tok->next;
	};
	
	if (tok->type != TOK_PER_SEMICOLON)
	{
		lexDiag(tok, CON_ERROR, "expected `;' or `.', not `%s'", tok->value);
		exit(1);
	};
	
	tok = tok->next;
	
	if (strcmp(namespace, expectedNamespace) != 0)
	{
		lexDiag(namespaceTok, CON_ERROR, "bad namespace declaration, expected `%s'", expectedNamespace);
		exit(1);
	};
	
	// get the import list. we map a type name to a string containing the fullname of the class
	// it refers to.
	HashTable *shortNames = hashtabNew();
	
	// import the namespace inside of which the current class is defined, followed by `std.rt', followed
	// by everything on the explicit import list.
	mapNamespace(shortNames, expectedNamespace);
	mapNamespace(shortNames, "std.rt");
	while (tok->type == TOK_PER_IMPORT)
	{
		Token *importToken = tok;
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			exit(1);
		};
		
		char *importName = strdup(tok->value);
		int wildcard = 0;
		
		tok = tok->next;
		
		while (tok->type == TOK_PER_DOT)
		{
			tok = tok->next;
			
			if (tok->type == TOK_PER_ID)
			{
				char *newName = (char*) malloc(strlen(importName) + strlen(tok->value) + 2);
				sprintf(newName, "%s.%s", importName, tok->value);
				free(importName);
				importName = newName;
				
				tok = tok->next;
			}
			else if (tok->type == TOK_PER_AST)
			{
				wildcard = 1;
				tok = tok->next;
				break;
			}
			else
			{
				lexDiag(tok, CON_ERROR, "expected an identifier or `*', not `%s'", tok->value);
				exit(1);
			};
		};
		
		if (tok->type != TOK_PER_SEMICOLON)
		{
			lexDiag(tok, CON_ERROR, "expected `;', not `%s'", tok->value);
			exit(1);
		};
		
		tok = tok->next;

		if (wildcard)
		{
			mapNamespace(shortNames, importName);
		}
		else
		{
			if (!hashtabHasKey(hashtabClassFiles, importName))
			{
				lexDiag(importToken, CON_ERROR, "cannot find class `%s'", importName);
				exit(1);
			};
			
			mapClass(shortNames, importName);
		};
		
		free(importName);
	};
	
	// parse top-level definitions
	while (tok->type != TOK_END)
	{
		if (tok->type == TOK_PER_SEMICOLON)
		{
			tok = tok->next;
			continue;
		};
		
		int prot = parseProt(tok);
		if (prot == -1)
		{
			lexDiag(tok, CON_ERROR, "expected an access specifier, not `%s'", tok->value);
			exit(1);
		};
		
		tok = tok->next;
		
		Token *qtoks = tok;
		int flags = parseQualifs(&tok);
		
		if (flags != 0 && flags != PER_Q_STATIC && flags != PER_Q_FINAL && flags != PER_Q_ABSTRACT)
		{
			lexDiag(qtoks, CON_ERROR, "mutually-exclusive class qualifiers specified");
			exit(1);
		};
		
		if (tok->type != TOK_PER_CLASS)
		{
			lexDiag(tok, CON_ERROR, "expected a class qualifier or `class', not `%s'", tok->value);
			exit(1);
		};
		
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			exit(1);
		};
		
		const char *shortname = tok->value;
		//Token *deftok = tok;
		tok = tok->next;
		
		char *fullname = (char*) malloc(strlen(namespace) + strlen(shortname) + 2);
		sprintf(fullname, "%s.%s", namespace, shortname);
		
		ClassInfo *info = (ClassInfo*) calloc(1, sizeof(ClassInfo));
		info->fullname = fullname;
		
		// template parameter list
		if (tok->type == TOK_PER_LARROW)
		{
			while (1)
			{
				tok = tok->next;
				
				if (tok->type != TOK_PER_ID)
				{
					lexDiag(tok, CON_ERROR, "expected a template parameter name, not `%s'", tok->value);
					exit(1);
				};
				
				const char *parname = tok->value;
				tok = tok->next;
				
				int index = info->numTemplateParams++;
				info->paramNames = (const char**) realloc(info->paramNames, sizeof(void*) * info->numTemplateParams);
				info->paramNames[index] = strdup(parname);
				
				if (tok->type == TOK_PER_EXTENDS)
				{
					tok = tok->next;
					Token *deftok = tok;
					
					char *depname = parseTentativeTypeSpec(shortNames, &tok);
					addDependency(info, depname, deftok);
				};
				
				if (tok->type == TOK_PER_COMMA)
				{
					continue;
				}
				else if (tok->type == TOK_PER_RARROW)
				{
					tok = tok->next;
					break;
				}
				else
				{
					lexDiag(tok, CON_ERROR, "expected `>' or `,', not `%s'", tok->value);
					if (tok->type == TOK_PER_SHR)
					{
						lexDiag(tok, CON_NOTE, "if closing multiple arrow brackets, you must write `> >` with a space, instead of `>>'");
					};
					exit(1);
				};
			};
		};

		// 'extends' clause
		if (tok->type == TOK_PER_EXTENDS)
		{
			do
			{
				tok = tok->next;
				Token *partok = tok;
				
				char *depname = parseTentativeTypeSpec(shortNames, &tok);
				addDependency(info, depname, partok);
			} while (tok->type == TOK_PER_COMMA);
		};
		
		if (tok->type != TOK_PER_LBRACE)
		{
			lexDiag(tok, CON_ERROR, "expected `{' not `%s'", tok->value);
			exit(1);
		};
		
		tok = tok->next;
		int depth = 0;
		
		while (1)
		{
			if (tok->type == TOK_PER_LBRACE)
			{
				depth++;
			}
			else if (tok->type == TOK_PER_RBRACE)
			{
				if (depth == 0)
				{
					tok = tok->next;
					break;
				};
				
				depth--;
			}
			else if (tok->type == TOK_END)
			{
				lexDiag(tok, CON_ERROR, "unterminated brace");
				exit(1);
			};
			
			tok = tok->next;
		};
		
		hashtabSet(hashtabClassInfo, fullname, info);
	};
	
	if (hashtabGet(hashtabClassInfo, className) == NULL)
	{
		lexDiag(namespaceTok, CON_ERROR, "file `%s' did not define class `%s' as expected", filename, className);
		exit(1);
	};
	
	return (ClassInfo*) hashtabGet(hashtabClassInfo, className);
};

int doClassCatalog(const char *mainPath)
{
	hashtabClassFiles = hashtabNew();
	hashtabNamespaces = hashtabNew();
	hashtabClassInfo = hashtabNew();
	
	// first determine what class the specified source file is supposed to contain, based on
	// its file name and the namespace declared within
	FILE *fp = fopen(mainPath, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "cannot open %s: %s\n", mainPath, strerror(errno));
		return -1;
	};
	
	char *data = readWholeFile(fp);
	if (data == NULL)
	{
		fprintf(stderr, "failed to read %s\n", mainPath);
		return -1;
	};
	
	fclose(fp);
	
	Token *tok = parsePerun(data, mainPath);
	if (tok == NULL)
	{
		return 1;
	};
	
	if (tok->type != TOK_PER_NAMESPACE)
	{
		lexDiag(tok, CON_ERROR, "expected `namespace', not `%s'", tok->value);
		return -1;
	};
	
	tok = tok->next;
	
	if (tok->type != TOK_PER_ID)
	{
		lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
		return -1;
	};
	
	char *namespace = strdup(tok->value);
	tok = tok->next;
	
	while (tok->type == TOK_PER_DOT)
	{
		tok = tok->next;
		
		if (tok->type != TOK_PER_ID)
		{
			lexDiag(tok, CON_ERROR, "expected an identifier, not `%s'", tok->value);
			return -1;
		};
		
		char *newns = (char*) malloc(strlen(namespace) + strlen(tok->value) + 2);
		sprintf(newns, "%s.%s", namespace, tok->value);
		free(namespace);
		namespace = newns;
		
		tok = tok->next;
	};
	
	if (tok->type != TOK_PER_SEMICOLON)
	{
		lexDiag(tok, CON_ERROR, "expected `;' or `.', not `%s'", tok->value);
		return -1;
	};
	
	const char *pos = mainPath;
	char *slashPos = strrchr(pos, '/');
	if (slashPos != NULL) pos = slashPos + 1;
	slashPos = strrchr(pos, '\\');
	if (slashPos != NULL) pos = slashPos + 1;
	
	char *name = strdup(pos);
	char *dotPos = strchr(name, '.');
	if (dotPos != NULL) *dotPos = 0;
	
	char *fullname = (char*) malloc(strlen(namespace) + strlen(name) + 2);
	sprintf(fullname, "%s.%s", namespace, name);
	free(namespace);
	free(name);
	
	hashtabSet(hashtabClassFiles, fullname, strdup(mainPath));
	
	int i;
	for (i=0; i<numClassPath; i++)
	{
		doClassSearch(classPath[i], NULL, 0);
	};
	
	doAnalysis(fullname);
	free(fullname);
	return 0;
};

void dumpDeps(const char *className)
{
	ClassInfo *info = doAnalysis(className);
	assert(info != NULL);
	
	printf("strict digraph {\n");
	
	const char *fullname;
	HASHTAB_FOREACH(hashtabClassInfo, ClassInfo *, fullname, subinfo)
	{
		printf("\t_%p [label=\"%s\"];\n", subinfo, fullname);
		
		int i;
		for (i=0; i<subinfo->numDeps; i++)
		{
			printf("\t_%p -> _%p;\n", subinfo->deps[i], subinfo);
		};
	};
	
	printf("}\n");
};

ClassNameList* getClassNamesInNamespaces(const char *namespace)
{
	if (hashtabHasKey(hashtabNamespaces, namespace))
	{
		return (ClassNameList*) hashtabGet(hashtabNamespaces, namespace);
	}
	else
	{
		return NULL;
	};
};

char* getClassFileName(const char *fullname)
{
	if (hashtabHasKey(hashtabClassFiles, fullname))
	{
		return (char*) hashtabGet(hashtabClassFiles, fullname);
	}
	else
	{
		return NULL;
	};
};
