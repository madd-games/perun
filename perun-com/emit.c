/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "emit.h"
#include "mangle.h"
#include "buildvars.h"
#include "makedeps.h"
#include "lex.h"
#include "console.h"
#include "hashtab.h"

extern HashTable* classTable;
const char **resPath;
int numResPath;

void addResourcePath(const char *dirname)
{
	int index = numResPath++;
	resPath = (const char**) realloc(resPath, sizeof(void*) * numResPath);
	resPath[index] = dirname;
};

static char* readWholeFile(FILE *fp)
{
	char *result = strdup("");
	char linebuf[2048];
	char *line;
	
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *newBuffer = (char*) malloc(strlen(result) + strlen(line) + 1);
		sprintf(newBuffer, "%s%s", result, line);
		free(result);
		result = newBuffer;
	};
	
	return result;
};

static FILE* openResFile(const char *name)
{
	int i;
	for (i=0; i<numResPath; i++)
	{
		char *path = (char*) malloc(strlen(resPath[i]) + strlen(name) + 2);
		sprintf(path, "%s/%s", resPath[i], name);
		
		FILE *fp = fopen(path, "r");
		if (fp != NULL)
		{
			declareMakeDep(path);
			free(path);
			return fp;
		};
		
		free(path);
	};
	
	return NULL;
};

static const char* getEquivalentCType(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_ARRAY:		return "void*";
	case PER_TYPE_BOOL:		return "unsigned char";
	case PER_TYPE_BYTE:		return "uint8_t";
	case PER_TYPE_DOUBLE:		return "double";
	case PER_TYPE_DWORD:		return "uint32_t";
	case PER_TYPE_FLOAT:		return "float";
	case PER_TYPE_MANAGED:		return "Per_Object*";
	case PER_TYPE_SPTR:		return "ptrdiff_t";
	case PER_TYPE_PTR:		return "size_t";
	case PER_TYPE_QWORD:		return "uint64_t";
	case PER_TYPE_SBYTE:		return "int8_t";
	case PER_TYPE_SDWORD:		return "int32_t";
	case PER_TYPE_SQWORD:		return "int64_t";
	case PER_TYPE_SWORD:		return "int16_t";
	case PER_TYPE_TEMPLATE_PARAM:	return "Per_Object*";
	case PER_TYPE_VOID:		return "void";
	case PER_TYPE_WORD:		return "uint16_t";
	case PER_TYPE_FUNCREF:		return "void*";
	default:
		fprintf(stderr, "FATAL: Cannot translate Perun type to C\n");
		abort();
		return NULL;
	};
};

EmitFunc* newEmitFunc(const char *retType)
{
	EmitFunc *func = (EmitFunc*) calloc(1, sizeof(EmitFunc));
	func->code = (Block*) calloc(1, sizeof(Block));
	func->code->func = func;
	func->retType = strdup(retType);
	return func;
};

void emitInsn(Block *block, Block **newBlockOut, const char *format, ...)
{
	va_list ap, aq;
	va_start(ap, format);
	va_copy(aq, ap);
	
	Insn *insn = (Insn*) calloc(1, sizeof(Insn));

	char buffer[256];
	int count = vsnprintf(buffer, 256, format, ap);
	va_end(ap);
	
	if (count < 256)
	{
		insn->code = strdup(buffer);
	}
	else
	{
		insn->code = (char*) malloc((size_t)count + 1);
		vsnprintf(insn->code, (size_t)count + 1, format, aq);
	};
	
	if (newBlockOut != NULL)
	{
		Block *new = (Block*) calloc(1, sizeof(Block));
		new->func = block->func;
		insn->block = new;
		*newBlockOut = new;
	};
	
	if (block->first == NULL)
	{
		block->first = block->last = insn;
	}
	else
	{
		block->last->next = insn;
		block->last = insn;
	};
	
	va_end(aq);
};

char* makeTempVar(EmitFunc *func, const char *type, int section)
{
	TempVar *var = (TempVar*) calloc(1, sizeof(TempVar));
	var->type = strdup(type);
	char *name = (char*) malloc(64);
	var->name = name;
	sprintf(name, "tmp_%d", func->nextTemp++);
	var->section = section;
	
	var->next = func->vars;
	func->vars = var;
	
	return name;
};

static void outputBlock(FILE *fp, Block *block, int level)
{
	char tabs[level+1];
	memset(tabs, 0, level+1);
	memset(tabs, '\t', level);
	
	fprintf(fp, "%s{\n", tabs);
	
	Insn *insn;
	for (insn=block->first; insn!=NULL; insn=insn->next)
	{
		fprintf(fp, "\t%s%s\n", tabs, insn->code);
		if (insn->block != NULL) outputBlock(fp, insn->block, level+1);
	};
	
	fprintf(fp, "%s}\n", tabs);
};

static void outputFunc(FILE *fp, EmitFunc *func)
{
	fprintf(fp, "{\n");
	
	const char *volatilePrefix = "";
	if (func->volatileVars)
	{
		volatilePrefix = " volatile";
	};
	
	TempVar *var;
	for (var=func->vars; var!=NULL; var=var->next)
	{
		if (var->section != PER_VSECT_PRIMITIVE)
		{
			fprintf(fp, "\t%s%s %s = NULL;\n", var->type, volatilePrefix, var->name);
		}
		else
		{
			fprintf(fp, "\t%s%s %s;\n", var->type, volatilePrefix, var->name);
		};
	};
	
	if (strcmp(func->retType, "void") != 0)
	{
		fprintf(fp, "\t%s retval = (%s) 0;\n", func->retType, func->retType);
	};
	
	// do the exception context handling and stuff
	fprintf(fp, "\n");
	
	fprintf(fp, "\tPer_Object** exRefs[] = {");
	for (var=func->vars; var!=NULL; var=var->next)
	{
		if (var->section == PER_VSECT_MANAGED)
		{
			fprintf(fp, "&%s, ", var->name);
		};
	};
	
	fprintf(fp, "NULL};\n");
	fprintf(fp, "\tvoid** exArrays[] = {");
	for (var=func->vars; var!=NULL; var=var->next)
	{
		if (var->section == PER_VSECT_ARRAY)
		{
			fprintf(fp, "&%s, ", var->name);
		};
	};
	fprintf(fp, "NULL};\n\n");
	fprintf(fp, "\tPer_ExceptionContext mainCtx;\n");
	fprintf(fp, "\tPer_Enter(&mainCtx);\n");
	fprintf(fp, "\tmainCtx.xcRefs = exRefs;\n");
	fprintf(fp, "\tmainCtx.xcArrays = exArrays;\n\n");
	
	// output the function body
	outputBlock(fp, func->code, 1);
	
	// and the context leaving/returning
	fprintf(fp, "\n\tend:\n");
	fprintf(fp, "\tPer_Leave(&mainCtx);\n");
	for (var=func->vars; var!=NULL; var=var->next)
	{
		if (var->section == PER_VSECT_MANAGED)
		{
			fprintf(fp, "\tPer_Down(%s);\n", var->name);
		}
		else if (var->section == PER_VSECT_ARRAY)
		{
			fprintf(fp, "\tPer_ArrayDown(%s);\n", var->name);
		};
	};
	
	if (strcmp(func->retType, "void") != 0)
	{
		fprintf(fp, "\treturn retval;\n");
	};
	
	fprintf(fp, "}\n\n");
};

static char* formatMsg(const char *fmt, ...)
{
	va_list ap, aq;
	va_start(ap, fmt);
	va_copy(aq, ap);
	
	char buffer[256];
	int count = vsnprintf(buffer, 256, fmt, ap);
	va_end(ap);
	
	if (count < 256)
	{
		va_end(aq);
		return strdup(buffer);
	}
	else
	{
		char *out = (char*) malloc((size_t)count + 1);
		vsnprintf(out, (size_t)count + 1, fmt, aq);
		va_end(aq);
		return out;
	};
};

ExprValue makeCCast(ExprValue value, Type *destType)
{
	value.cval = formatMsg("((%s) %s)", getEquivalentCType(destType), value.cval);
	value.type = destType;
	return value;
};

ExprValue implicitCast(ExecContext *ctx, Block *block, ExprValue value, Type *destType)
{
	if (isSubtype(destType, value.type))
	{
		// the value is already a subtype of destType; we don't need to do
		// anything more
		return value;
	};
	
	// a class can be implicitly converted to std.rt.Class
	if (value.type->kind == PER_TYPE_CLASS && destType->kind == PER_TYPE_MANAGED && strcmp(destType->fullname, "std.rt.Class") == 0)
	{
		ClassTemplate *tem = findClassByName(value.tok, value.type->fullname);
		char *tmpvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
		emitInsn(block, NULL, "Per_Down(%s); %s = Per_WrapClass(&%s);", tmpvar, tmpvar, tem->mangledName);
		value.type = destType;
		value.cval = formatMsg("(%s)", tmpvar);
		return value;
	};
	
	// if the destination type is managed, and the source type primitive, see if there
	// is an implicit managed wrapper
	if (isManagedType(destType) && isPrimitiveType(value.type) && destType->kind != PER_TYPE_NULL)
	{
		ManagedTypeMapping *map = getManagedWrapperByPrimitive(value.type->kind);
		if (map != NULL && strcmp(map->man, destType->fullname) == 0)
		{
			 char *tmpvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
			 emitInsn(block, NULL, "Per_Down(%s); %s = %s(%s);", tmpvar, tmpvar, map->wrapFunc, value.cval);
			 value.type = destType;
			 value.cval = formatMsg("(%s)", tmpvar);
			 return value;
		};
	};
	
	// and the reverse
	if (isPrimitiveType(destType) && isManagedType(value.type) && value.type->kind != PER_TYPE_NULL)
	{
		ManagedTypeMapping *map = getManagedWrapperByPrimitive(destType->kind);
		if (map != NULL && strcmp(map->man, value.type->fullname) == 0)
		{
			value.type = destType;
			value.cval = formatMsg("(%s(%s))", map->unwrapFunc, value.cval);
			return value;
		};
	};
	
	// convert signed integer types to wider signed integer types
	switch (value.type->kind)
	{
	case PER_TYPE_SBYTE:
		if (destType->kind == PER_TYPE_SWORD) return makeCCast(value, destType);
	case PER_TYPE_SWORD:
		if (destType->kind == PER_TYPE_SDWORD) return makeCCast(value, destType);
	case PER_TYPE_SDWORD:
		if (destType->kind == PER_TYPE_SQWORD) return makeCCast(value, destType);
	case PER_TYPE_SQWORD:
		if (destType->kind == PER_TYPE_SPTR) return makeCCast(value, destType);
	};
	
	// convert unsigned integer types to wider unsigned integer types
	switch (value.type->kind)
	{
	case PER_TYPE_BYTE:
		if (destType->kind == PER_TYPE_WORD) return makeCCast(value, destType);
	case PER_TYPE_WORD:
		if (destType->kind == PER_TYPE_DWORD) return makeCCast(value, destType);
	case PER_TYPE_DWORD:
		if (destType->kind == PER_TYPE_QWORD) return makeCCast(value, destType);
	case PER_TYPE_QWORD:
		if (destType->kind == PER_TYPE_PTR) return makeCCast(value, destType);
	};
	
	// float can be implicitly converted to double
	if (value.type->kind == PER_TYPE_FLOAT && destType->kind == PER_TYPE_DOUBLE)
	{
		return makeCCast(value, destType);
	};
	
	// the 'null' type can be converted to any managed or array type
	if (value.type->kind == PER_TYPE_NULL)
	{
		if (destType->kind == PER_TYPE_MANAGED || destType->kind == PER_TYPE_ARRAY
			|| destType->kind == PER_TYPE_TEMPLATE_PARAM || destType->kind == PER_TYPE_FUNCREF)
		{
			// the conversion is a no-op
			value.type = destType;
			return value;
		};
	};
	
	// a method can be converted to a function reference if it has an exactly matching signature
	if (value.type->kind == PER_TYPE_METHOD && destType->kind == PER_TYPE_FUNCREF)
	{
		MethodSignature *sig;
		for (sig=value.type->sigs; sig!=NULL; sig=sig->next)
		{
			if (sig->numArgs == destType->numArgs && sig->isStatic && isSameType(sig->retType, destType->retType))
			{
				int i;
				
				for (i=0; i<sig->numArgs; i++)
				{
					if (!isSameType(sig->argTypes[i], destType->argTypes[i]))
					{
						break;
					};
				};
				
				if (i == sig->numArgs)
				{
					value.type = destType;
					value.cval = formatMsg("(&%s)", sig->mangledName);
					return value;
				};
			};
		};
		
		value.type = NULL;
		value.errmsg = formatMsg("the specified method has no signature suitable for function reference type `%s'", getTypeName(destType));
		return value;
	};
	
	// in any other case, an implicit conversion is not possible
	value.errmsg = formatMsg("cannot implicitly convert a value of type `%s' to type `%s'", getTypeName(value.type), getTypeName(destType));
	value.type = NULL;
	return value;
};

int isImplicitlyCastable(ExprValue value, Type *destType)
{
	if (isSubtype(destType, value.type))
	{
		// the value is already a subtype of destType; we don't need to do
		// anything more
		return 1;
	};

	// a class can be implicitly converted to std.rt.Class
	if (value.type->kind == PER_TYPE_CLASS && destType->kind == PER_TYPE_MANAGED && strcmp(destType->fullname, "std.rt.Class") == 0)
	{
		return 1;
	};

	// if the destination type is managed, and the source type is primitive,
	// see if there is an implicit managed wrapper
	if (isManagedType(destType) && isPrimitiveType(value.type))
	{
		ManagedTypeMapping *mapping = getManagedWrapperByPrimitive(value.type->kind);
		if (mapping == NULL) return 0;
		if (strcmp(mapping->man, destType->fullname) == 0) return 1;
		return 0;
	};
	
	// also the reverse
	if (isPrimitiveType(destType) && isManagedType(value.type))
	{
		if (value.type->kind == PER_TYPE_NULL) return 0;
		
		ManagedTypeMapping *mapping = getManagedWrapperByPrimitive(destType->kind);
		if (mapping == NULL) return 0;
		if (strcmp(mapping->man, value.type->fullname) == 0) return 1;
		return 0;
	};
	
	// convert signed integer types to wider signed integer types
	switch (value.type->kind)
	{
	case PER_TYPE_SBYTE:
		if (destType->kind == PER_TYPE_SWORD) return 1;
	case PER_TYPE_SWORD:
		if (destType->kind == PER_TYPE_SDWORD) return 1;
	case PER_TYPE_SDWORD:
		if (destType->kind == PER_TYPE_SQWORD) return 1;
	};
	
	// convert unsigned integer types to wider unsigned integer types
	switch (value.type->kind)
	{
	case PER_TYPE_BYTE:
		if (destType->kind == PER_TYPE_WORD) return 1;
	case PER_TYPE_WORD:
		if (destType->kind == PER_TYPE_DWORD) return 1;
	case PER_TYPE_DWORD:
		if (destType->kind == PER_TYPE_QWORD) return 1;
	};
	
	// float can be implicitly converted to double
	if (value.type->kind == PER_TYPE_FLOAT && destType->kind == PER_TYPE_DOUBLE)
	{
		return 1;
	};
	
	// the 'null' type can be converted to any managed or array type
	if (value.type->kind == PER_TYPE_NULL)
	{
		if (destType->kind == PER_TYPE_MANAGED || destType->kind == PER_TYPE_ARRAY
			|| destType->kind == PER_TYPE_TEMPLATE_PARAM || destType->kind == PER_TYPE_FUNCREF)
		{
			// the conversion is a no-op
			return 1;
		};
	};

	// a method can be converted to a function reference if it has an exactly matching signature
	if (value.type->kind == PER_TYPE_METHOD && destType->kind == PER_TYPE_FUNCREF)
	{
		MethodSignature *sig;
		for (sig=value.type->sigs; sig!=NULL; sig=sig->next)
		{
			if (sig->numArgs == destType->numArgs && sig->isStatic && isSameType(sig->retType, destType->retType))
			{
				int i;
				
				for (i=0; i<sig->numArgs; i++)
				{
					if (!isSameType(sig->argTypes[i], destType->argTypes[i]))
					{
						break;
					};
				};
				
				if (i == sig->numArgs)
				{
					return 1;
				};
			};
		};
	};
	
	// in any other case, an implicit conversion is not possible
	return 0;
};

ExprValue emitPromotion(ExprValue value)
{
	switch (value.type->kind)
	{
	case PER_TYPE_SBYTE:
	case PER_TYPE_SWORD:
		return makeCCast(value, makePrimitiveType(PER_TYPE_SDWORD));
	case PER_TYPE_BYTE:
	case PER_TYPE_WORD:
		return makeCCast(value, makePrimitiveType(PER_TYPE_DWORD));
	default:
		return value;
	};
};

StaticFieldInfo findStaticField(Type *type, const char *name, ClassTemplate *inTemplate)
{
	ClassTemplate *tem = findClassByName(NULL, type->fullname);
	
	ClassField *field;
	for (field=tem->fields; field!=NULL; field=field->next)
	{
		if ((field->flags & PER_Q_STATIC) && strcmp(field->name, name) == 0)
		{
			// check access rights
			if (field->prot == PER_PROT_PRIVATE)
			{
				if (strcmp(inTemplate->fullname, type->fullname) != 0)
				{
					// attempting to access a private field in a class
					// from code in another class
					StaticFieldInfo err;
					memset(&err, 0, sizeof(StaticFieldInfo));
					return err;
				};
			};
			
			if (field->prot == PER_PROT_PROTECTED)
			{
				if (!isSubtype(type, getThisType(inTemplate)))
				{
					// attempting to access a protected field in a class
					// from a non-child
					StaticFieldInfo err;
					memset(&err, 0, sizeof(StaticFieldInfo));
					return err;
				};
			};
			
			StaticFieldInfo result;
			result.mangled = field->mangled;
			result.type = expandType(field->type, type->temParamTypes);
			result.flags = field->flags;
			result.tok = field->tok;
			result.definer = tem;
			return result;
		};
	};
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		StaticFieldInfo info = findStaticField(parent, name, inTemplate);
		if (info.mangled != NULL) return info;
	};
	
	if (strcmp(type->fullname, "std.rt.Object") == 0)
	{
		// we are in std.rt.Object and still nothing found
		StaticFieldInfo info;
		memset(&info, 0, sizeof(StaticFieldInfo));
		return info;
	};
	
	// check in std.rt.Object
	Type *objType = getThisType(findClassByName(NULL, "std.rt.Object"));
	return findStaticField(objType, name, inTemplate);
};

DynamicFieldInfo findDynamicField(Type *type, const char *name, ClassTemplate *inTemplate)
{
	ClassTemplate *tem = findClassByName(NULL, type->fullname);
	
	ClassField *field;
	for (field=tem->fields; field!=NULL; field=field->next)
	{
		if ((field->flags & PER_Q_STATIC) == 0 && strcmp(field->name, name) == 0)
		{
			// check access rights
			if (field->prot == PER_PROT_PRIVATE)
			{
				if (strcmp(inTemplate->fullname, type->fullname) != 0)
				{
					// attempting to access a private field in a class
					// from code in another class
					DynamicFieldInfo err;
					memset(&err, 0, sizeof(DynamicFieldInfo));
					return err;
				};
			};
			
			if (field->prot == PER_PROT_PROTECTED)
			{
				if (!isSubtype(type, getThisType(inTemplate)))
				{
					// attempting to access a protected field in a class
					// from a non-child
					DynamicFieldInfo err;
					memset(&err, 0, sizeof(DynamicFieldInfo));
					return err;
				};
			};
			
			DynamicFieldInfo result;
			result.mangled = field->mangled;
			result.type = expandType(field->type, type->temParamTypes);
			result.flags = field->flags;
			result.tok = field->tok;
			result.definer = tem;
			return result;
		};
	};
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		DynamicFieldInfo info = findDynamicField(parent, name, inTemplate);
		if (info.mangled != NULL) return info;
	};
	
	if (strcmp(type->fullname, "std.rt.Object") == 0)
	{
		// we are in std.rt.Object and still nothing found
		DynamicFieldInfo info;
		memset(&info, 0, sizeof(DynamicFieldInfo));
		return info;
	};
	
	// check in std.rt.Object
	Type *objType = getThisType(findClassByName(NULL, "std.rt.Object"));
	return findDynamicField(objType, name, inTemplate);
};

ExprValue makeStaticFieldValue(ExecContext *ctx, Block *block, Token *tok, StaticFieldInfo info)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(info.type)) sect = PER_VSECT_MANAGED;
	if (info.type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	char *tempvar = makeTempVar(block->func, getEquivalentCType(info.type), sect);
	emitInsn(block, NULL, "Per_StaticLock(&%s);", info.definer->staticLockName);
	
	if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", tempvar, tempvar, info.mangled, tempvar);
	}
	else if (sect == PER_VSECT_ARRAY)
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", tempvar, tempvar, info.mangled, tempvar);
	}
	else
	{
		emitInsn(block, NULL, "%s = %s;", tempvar, info.mangled);
	};
	
	emitInsn(block, NULL, "Per_StaticUnlock(&%s);", info.definer->staticLockName);
	
	lexTag(tok, TAG_FIELD_NAME, NULL, tok->value);
	lexTag(tok, TAG_DEFINED_AT, info.tok, NULL);
	
	ExprValue result;
	memset(&result, 0, sizeof(ExprValue));
	result.type = info.type;
	result.cval = strdup(tempvar);
	result.tok = tok;
	return result;
};

ExprValue makeDynamicFieldValue(ExecContext *ctx, Block *block, Token *tok, const char *subject, DynamicFieldInfo info)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(info.type)) sect = PER_VSECT_MANAGED;
	if (info.type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	char *tempvar = makeTempVar(block->func, getEquivalentCType(info.type), sect);
	emitInsn(block, NULL, "Per_Lock(%s);", subject);
	
	char *fetcher = formatMsg("(*((%s*)Per_GetFieldPtr(%s, &%s, %s)))", getEquivalentCType(info.type), subject,
					info.definer->mangledName, info.mangled);

	if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", tempvar, tempvar, fetcher, tempvar);
	}
	else if (sect == PER_VSECT_ARRAY)
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", tempvar, tempvar, fetcher, tempvar);
	}
	else
	{
		emitInsn(block, NULL, "%s = %s;", tempvar, fetcher);
	};
	
	emitInsn(block, NULL, "Per_Unlock(%s);", subject);
	
	lexTag(tok, TAG_FIELD_NAME, NULL, NULL);
	lexTag(tok, TAG_DEFINED_AT, info.tok, NULL);
	
	ExprValue result;
	memset(&result, 0, sizeof(ExprValue));
	result.type = info.type;
	result.cval = strdup(tempvar);
	result.tok = tok;
	return result;
};

int isMatchingMethod(MethodSignature *a, MethodSignature *b)
{
	if (a->numArgs != b->numArgs) return 0;
	if (!isSameType(a->retType, b->retType)) return 0;
	
	int i;
	for (i=0; i<a->numArgs; i++)
	{
		if (!isSameType(a->argTypes[i], b->argTypes[i])) return 0;
	};
	
	return 1;
};

void getMethodRecur(ExecContext *ctx, Block *block, Type *type, const char *name, const char *subject, Type *result)
{
	ClassTemplate *tem = findClassByName(NULL, type->fullname);
	ClassMethod *method;
	for (method=tem->methods; method!=NULL; method=method->next)
	{
		// skip methods which override, before we need their BASE definition to make a call,
		// not the overrides
		if (strcmp(method->name, name) == 0 && !method->overrides)
		{
			if ((method->flags & PER_Q_STATIC) == 0 && subject == NULL)
			{
				// non-static methods not allowed
				continue;
			};
			
			MethodSignature *sig = (MethodSignature*) calloc(1, sizeof(MethodSignature));
			sig->isStatic = (method->flags & PER_Q_STATIC);
			sig->mangledName = method->mangled;
			sig->name = name;
			sig->retType = expandType(method->retType, type->temParamTypes);
			sig->tok = method->tok;
			
			ArgumentSpecificationList *arg;
			for (arg=method->args; arg!=NULL; arg=arg->next)
			{
				int index = sig->numArgs++;
				sig->argTypes = (Type**) realloc(sig->argTypes, sizeof(void*) * sig->numArgs);
				sig->argTypes[index] = expandType(arg->type, type->temParamTypes);
			};
			
			MethodSignature *scan;
			for (scan=result->sigs; scan!=NULL; scan=scan->next)
			{
				if (isMatchingMethod(scan, sig))
				{
					// we already know about this method
					break;
				};
			};
	
			if (scan == NULL)
			{
				sig->next = result->sigs;
				result->sigs = sig;
			}
			else
			{
				scan->mangledName = sig->mangledName;
			};
		};
	};
	
	int i;
	for (i=0; i<tem->numParents; i++)
	{
		Type *parent = expandType(tem->parents[i], type->temParamTypes);
		getMethodRecur(ctx, block, parent, name, subject, result);
	};
};

Type* getMethod(ExecContext *ctx, Block *block, Token *tok, Type *type, const char *name, const char *subject)
{
	// subject == NULL means only static methods allowed
	
	Type *result = (Type*) calloc(1, sizeof(Type));
	result->kind = PER_TYPE_METHOD;
	
	getMethodRecur(ctx, block, type, name, subject, result);
	getMethodRecur(ctx, block, typeObject, name, subject, result);
	
	if (result->sigs == NULL)
	{
		free(result);
		return NULL;
	};
	
	if (subject != NULL) result->thisExpr = strdup(subject);
	return result;
};

void dumpMethodInfo(Type *type)
{
	if (type->kind != PER_TYPE_METHOD)
	{
		printf("This type is NOT a method type.\n");
		return;
	};
	
	MethodSignature *sig;
	for (sig=type->sigs; sig!=NULL; sig=sig->next)
	{
		printf("\t%s%s %s(", sig->isStatic ? "static " : "", getTypeName(sig->retType), sig->name);
		
		int i;
		for (i=0; i<sig->numArgs; i++)
		{
			printf("%s", getTypeName(sig->argTypes[i]));
			
			if (i != sig->numArgs-1)
			{
				printf(", ");
			};
		};
		
		printf(");\t// %s\n", sig->mangledName);
	};
	
	const char *thisExpr = "<static only>";
	if (type->thisExpr != NULL) thisExpr = type->thisExpr;
	printf("This: %s\n", thisExpr);
};

ExprValue emitGetMember(ExecContext *ctx, Block *block, ExprValue value, Token *tok, const char *name)
{
	// 'tok' is the token with which errors will be associated
	
	// when accessing members of a parameter type, convert it implicitly
	// to its minimum base type.
	Type *type = value.type;
	if (type->kind == PER_TYPE_TEMPLATE_PARAM)
	{
		type = type->minBase;
	};
	
	// in the case of arrays, the only member is 'length', and evaluates to the length
	// of the array (as uint), which we extract from the array header
	if (type->kind == PER_TYPE_ARRAY)
	{
		if (strcmp(name, "length") != 0)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.tok = tok;
			result.errmsg = formatMsg("attempting to access member `%s' of array type `%s', but the only member of an array type is `length'",
							name, getTypeName(type));
			return result;
		};
		
		lexTag(tok, TAG_FIELD_NAME, NULL, "length");
		
		char *lvar = makeTempVar(block->func, "uint32_t", PER_VSECT_PRIMITIVE);
		emitInsn(block, NULL, "%s = Per_ArrayLength(%s);", lvar, value.cval);
		
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.type = makePrimitiveType(PER_TYPE_SDWORD);
		result.cval = formatMsg("(%s)", lvar);
		result.tok = tok;
		return result;
	};
	
	// in the case of a static class type, try to find a static field with that name
	// first
	if (type->kind == PER_TYPE_CLASS)
	{
		Type *thistype = getThisType(findClassByName(NULL, type->fullname));
		StaticFieldInfo info = findStaticField(thistype, name, emitGetInTemplate());
		
		if (info.mangled == NULL)
		{
			Type *methodType = getMethod(ctx, block, tok, thistype, name, NULL);
			if (methodType == NULL)
			{
				ExprValue result;
				memset(&result, 0, sizeof(ExprValue));
				result.tok = tok;
				result.errmsg = formatMsg("class `%s' has no accessible static member named `%s'", type->fullname, name);
				return result;
			}
			else
			{
				ExprValue result;
				memset(&result, 0, sizeof(ExprValue));
				result.tok = tok;
				result.type = methodType;
				result.cval = "N/A";
				return result;
			};
		};
		
		return makeStaticFieldValue(ctx, block, tok, info);
	};
	
	// in the case of a managed type, try to find a static or dynamic field with that name
	if (type->kind == PER_TYPE_MANAGED)
	{
		// try static
		StaticFieldInfo info = findStaticField(type, name, emitGetInTemplate());
		
		if (info.mangled == NULL)
		{	
			DynamicFieldInfo dinfo = findDynamicField(type, name, emitGetInTemplate());
			
			if (dinfo.mangled == NULL)
			{
				Type *methodType = getMethod(ctx, block, tok, type, name, value.cval);
				if (methodType != NULL)
				{
					ExprValue result;
					memset(&result, 0, sizeof(ExprValue));
					result.tok = tok;
					result.type = methodType;
					result.cval = "N/A";
					return result;
				}
				else
				{
					ExprValue result;
					memset(&result, 0, sizeof(ExprValue));
					result.tok = tok;
					result.errmsg = formatMsg("class `%s' has no accessible member named `%s'", type->fullname, name);
					return result;
				};
			};
			
			return makeDynamicFieldValue(ctx, block, tok, value.cval, dinfo);
		};
		
		return makeStaticFieldValue(ctx, block, tok, info);
	};
	
	ExprValue err;
	memset(&err, 0, sizeof(ExprValue));
	err.tok = tok;
	err.errmsg = formatMsg("type `%s' does not have a member named `%s'", getTypeName(type), name);
	return err;
};

ExprValue emitCall(ExecContext *ctx, Block *block, Token *tok, ExprValue method, ExprValue *args, int numArgs)
{
	Type *origType = method.type;
	while (method.type->kind == PER_TYPE_MANAGED)
	{
		method = emitGetMember(ctx, block, method, tok, "opCall");
		if (method.type == NULL)
		{
			method.tok = tok;
			method.errmsg = formatMsg("object of type `%s' is not callable", getTypeName(origType));
			return method;
		};
	};
	
	if (method.type->kind == PER_TYPE_FUNCREF)
	{
		if (numArgs != method.type->numArgs)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = tok;
			err.errmsg = formatMsg("function reference %s expects %d arguments, but %d were given",
					getTypeName(method.type), method.type->numArgs, numArgs);
			return err;
		};
		
		// call it
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.tok = tok;
		result.type = method.type->retType;
		
		char *insn;
		if (method.type->retType->kind == PER_TYPE_VOID)
		{
			result.cval = "N/A";
			insn = strdup("");
		}
		else
		{
			int sect;
			if (method.type->retType->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
			else if (isManagedType(method.type->retType)) sect = PER_VSECT_MANAGED;
			else sect = PER_VSECT_PRIMITIVE;
			
			char *retvar = makeTempVar(block->func, getEquivalentCType(method.type->retType), sect);
			result.cval = strdup(retvar);
			
			if (sect == PER_VSECT_PRIMITIVE)
			{
				insn = formatMsg("%s = ", retvar);
			}
			else if (sect == PER_VSECT_ARRAY)
			{
				insn = formatMsg("Per_ArrayDown(%s); %s = NULL; %s = ", retvar, retvar, retvar);
			}
			else
			{
				insn = formatMsg("Per_Down(%s); %s = NULL; %s = ", retvar, retvar, retvar);
			};
		};
		
		// figure out the function pointer type
		const char *retTypeInC = getEquivalentCType(method.type->retType);
		char *ftype = (char*) malloc(strlen(retTypeInC) + 8);
		sprintf(ftype, "%s (*)(", retTypeInC);
		
		int i;
		for (i=0; i<numArgs; i++)
		{
			const char *argTypeInC = getEquivalentCType(method.type->argTypes[i]);
			ftype = (char*) realloc(ftype, strlen(ftype) + strlen(argTypeInC) + 8);
			strcat(ftype, argTypeInC);
			
			if (i != numArgs-1)
			{
				strcat(ftype, ", ");
			};
		};
		
		strcat(ftype, ")");
		
		// open the call
		char *newInsn;
		newInsn = formatMsg("%s((%s)%s)(", insn, ftype, method.cval);
		free(insn);
		insn = newInsn;
		
		for (i=0; i<numArgs; i++)
		{
			ExprValue arg = implicitCast(ctx, block, args[i], method.type->argTypes[i]);
			if (arg.type == NULL) return arg;
			
			const char *suffix = "";
			if (i != numArgs-1) suffix = ", ";
			newInsn = formatMsg("%s%s%s", insn, arg.cval, suffix);
			free(insn);
			insn = newInsn;
		};
		
		// close the bracket and emit
		emitInsn(block, NULL, "%s);", insn);
		free(insn);
		
		return result;
	};
	
	if (method.type->kind != PER_TYPE_METHOD)
	{
		ExprValue err;
		memset(&err, 0, sizeof(ExprValue));
		err.tok = tok;
		err.errmsg = formatMsg("attempting to call something which is not a method or function reference, but a value of type `%s'", getTypeName(method.type));
		return err;
	};
	
	// first try to find a signature which EXACTLY matches the types given
	MethodSignature *sig;
	for (sig=method.type->sigs; sig!=NULL; sig=sig->next)
	{
		if (sig->numArgs == numArgs)
		{
			int i;
			for (i=0; i<numArgs; i++)
			{
				if (!isSameType(sig->argTypes[i], args[i].type))
				{
					break;
				};
			};
			
			if (i == numArgs)
			{
				// all arguments match
				break;
			};
		};
	};
	
	// if we have not found a matching signature, then we try to find one signature to which
	// all arguments could be implicitly cast
	if (sig == NULL)
	{
		MethodSignature *candidate = NULL;
		int numFound = 0;
		
		for (sig=method.type->sigs; sig!=NULL; sig=sig->next)
		{
			if (sig->numArgs == numArgs)
			{
				int i;
				for (i=0; i<numArgs; i++)
				{
					if (!isImplicitlyCastable(args[i], sig->argTypes[i]))
					{
						break;
					};
				};
				
				if (i == numArgs)
				{
					// all arguments are castable
					candidate = sig;
					numFound++;
				};
			};
		};
		
		sig = candidate;
		if (numFound == 0)
		{
			// TODO: more descriptive
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = tok;
			err.errmsg = formatMsg("no matching signature for the specified types");
			return err;
		}
		else if (numFound > 1)
		{
			// TODO: more descriptive
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = tok;
			err.errmsg = formatMsg("ambiguous method call; use explicit casts to select a specific signature");
			return err;
		};
	};
	
	// signature found, let's call it
	ExprValue result;
	memset(&result, 0, sizeof(ExprValue));
	result.tok = tok;
	result.type = sig->retType;
	
	lexTag(method.tok, TAG_METHOD_NAME, NULL, method.tok->value);
	lexTag(method.tok, TAG_DEFINED_AT, sig->tok, NULL);
	
	char *insn;
	if (sig->retType->kind == PER_TYPE_VOID)
	{
		result.cval = "N/A";
		insn = strdup("");
	}
	else
	{
		int sect;
		if (sig->retType->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
		else if (isManagedType(sig->retType)) sect = PER_VSECT_MANAGED;
		else sect = PER_VSECT_PRIMITIVE;
		
		char *retvar = makeTempVar(block->func, getEquivalentCType(sig->retType), sect);
		result.cval = strdup(retvar);
		
		if (sect == PER_VSECT_PRIMITIVE)
		{
			insn = formatMsg("%s = ", retvar);
		}
		else if (sect == PER_VSECT_ARRAY)
		{
			insn = formatMsg("Per_ArrayDown(%s); %s = NULL; %s = ", retvar, retvar, retvar);
		}
		else
		{
			insn = formatMsg("Per_Down(%s); %s = NULL; %s = ", retvar, retvar, retvar);
		};
	};
	
	// open the call
	char *newInsn;
	if (sig->isStatic)
	{
		newInsn = formatMsg("%s%s(", insn, sig->mangledName);
		free(insn);
		insn = newInsn;
	}
	else
	{
		const char *suffix = "";
		if (numArgs != 0) suffix = ", ";
		newInsn = formatMsg("%s((FP_%s)Per_GetDynamicBinding(%s, &%s))(%s%s", insn, sig->mangledName, method.type->thisExpr, sig->mangledName,
											method.type->thisExpr, suffix);
		free(insn);
		insn = newInsn;
	};
	
	int i;
	for (i=0; i<numArgs; i++)
	{
		ExprValue arg = implicitCast(ctx, block, args[i], sig->argTypes[i]);
		if (arg.type == NULL) return arg;
		
		const char *suffix = "";
		if (i != numArgs-1) suffix = ", ";
		newInsn = formatMsg("%s%s%s", insn, arg.cval, suffix);
		free(insn);
		insn = newInsn;
	};
	
	// close the bracket and emit
	emitInsn(block, NULL, "%s);", insn);
	free(insn);
	
	return result;
};

ExprValue emitSingleArgCall(ExecContext *ctx, Block *block, Token *tok, ExprValue method, ExprValue arg)
{
	return emitCall(ctx, block, tok, method, &arg, 1);
};

PerunVar* getPerunVar(ExecContext *ctx, const char *name)
{
	if (ctx == NULL) return NULL;
	
	if (hashtabHasKey(ctx->vars, name))
	{
		return (PerunVar*) hashtabGet(ctx->vars, name);
	};
	
	return getPerunVar(ctx->parent, name);
};

ExprValue emitNewExpression(ExecContext *ctx, Block *block, NewExpression *newExpr)
{
	if (newExpr->size != NULL)
	{
		ExprValue size = emitExpression(ctx, block, newExpr->size);
		if (size.type == NULL) return size;
		
		size = implicitCast(ctx, block, size, makePrimitiveType(PER_TYPE_SDWORD));
		if (size.type == NULL) return size;
		
		const char *sectname;
		if (newExpr->type->kind == PER_TYPE_ARRAY)
		{
			sectname = "PER_TYPESECT_ARRAY";
		}
		else if (isManagedType(newExpr->type))
		{
			sectname = "PER_TYPESECT_MANAGED";
		}
		else
		{
			sectname = "PER_TYPESECT_PRIMITIVE";
		};
		
		char *varname = makeTempVar(block->func, "void*", PER_VSECT_ARRAY);
		emitInsn(block, NULL, "%s = Per_NewArray(%s, sizeof(%s), %s);", varname, size.cval, getEquivalentCType(newExpr->type), sectname);
		
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.type = makeArrayType(newExpr->type);
		result.cval = formatMsg("(%s)", varname);
		result.tok = newExpr->tok;
		return result;
	}
	else
	{
		if (newExpr->type->kind == PER_TYPE_ARRAY)
		{
			const char *sectname;
			if (newExpr->type->elementType->kind == PER_TYPE_ARRAY)
			{
				sectname = "PER_TYPESECT_ARRAY";
			}
			else if (isManagedType(newExpr->type->elementType))
			{
				sectname = "PER_TYPESECT_MANAGED";
			}
			else
			{
				sectname = "PER_TYPESECT_PRIMITIVE";
			};
			
			int numElements = 0;
			ArgumentExpressionList *init;
			for (init=newExpr->inits; init!=NULL; init=init->next)
			{
				numElements++;
			};
			
			char *varname = makeTempVar(block->func, "void*", PER_VSECT_ARRAY);
			emitInsn(block, NULL, "%s = Per_NewArray(%d, sizeof(%s), %s);", varname, numElements, getEquivalentCType(newExpr->type), sectname);
			
			int index = 0;
			for (init=newExpr->inits; init!=NULL; init=init->next)
			{
				ExprValue value = emitAssignmentExpression(ctx, block, init->assign);
				if (value.type == NULL) return value;
				
				value = implicitCast(ctx, block, value, newExpr->type->elementType);
				if (value.type == NULL) return value;
				
				if (newExpr->type->elementType->kind == PER_TYPE_ARRAY)
				{
					emitInsn(block, NULL, "Per_ArrayUp(%s);", value.cval);
				}
				else if (isManagedType(newExpr->type->elementType))
				{
					emitInsn(block, NULL, "Per_Up(%s);", value.cval);
				};
				
				emitInsn(block, NULL, "((%s*)%s)[%d] = %s;", getEquivalentCType(newExpr->type->elementType), varname, index++, value.cval);
			};
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = newExpr->type;
			result.cval = formatMsg("(%s)", varname);
			result.tok = newExpr->tok;
			return result;
		}
		else
		{
			Type *type = newExpr->type;
			ClassTemplate *tem = findClassByName(NULL, type->fullname);
			
			if (tem->flags & PER_Q_STATIC)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = newExpr->tok->next;
				err.errmsg = formatMsg("class `%s' is static so cannot be instantiated", type->fullname);
				return err;
			};
			
			if (tem->flags & PER_Q_ABSTRACT)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = newExpr->tok->next;
				err.errmsg = formatMsg("class `%s' is abstract so cannot be instantiated", type->fullname);
				return err;
			};
			
			// evaluate all the arguments
			int numArgs = 0;
			
			ArgumentExpressionList *arg;
			for (arg=newExpr->args; arg!=NULL; arg=arg->next)
			{
				numArgs++;
			};
			
			int k = 0;
			ExprValue argvals[numArgs];
			for (arg=newExpr->args; arg!=NULL; arg=arg->next)
			{
				ExprValue val = emitAssignmentExpression(ctx, block, arg->assign);
				if (val.type == NULL) return val;
				
				argvals[k++] = val;
			};
			
			// emit the creation itself
			char *varname = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
			emitInsn(block, NULL, "%s = Per_New(&%s);", varname, tem->mangledName);
			
			// find a matching constructor
			// TODO: check access specifier
			ClassConstructor *match = findConstructor(newExpr->tok->next, type, argvals, numArgs);
			if (match != NULL)
			{
				char *callInsn = formatMsg("%s(%s", match->mangled, varname);
				
				for (k=0; k<numArgs; k++)
				{
					char *newInsn = formatMsg("%s, %s", callInsn, argvals[k].cval);
					free(callInsn);
					callInsn = newInsn;
				};
				
				emitInsn(block, NULL, "%s);", callInsn);
				free(callInsn);
				
				ExprValue result;
				memset(&result, 0, sizeof(ExprValue));
				result.tok = newExpr->tok;
				result.type = type;
				result.cval = formatMsg("(%s)", varname);
				return result;
			}
			else
			{
				// TODO: more descriptive
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = newExpr->tok->next;
				err.errmsg = formatMsg("class `%s' does not have a matching constructor", type->fullname);
				return err;
			};
		};
	};
};

ExprValue emitImportExpression(ExecContext *ctx, Block *block, ImportExpression *importExpr)
{
	Type *strtype = (Type*) calloc(1, sizeof(Type));
	strtype->kind = PER_TYPE_MANAGED;
	strtype->fullname = strdup("std.rt.String");
	strtype->temParamTypes = hashtabNew();
	
	char *filename = (char*) malloc(strlen(importExpr->str->value));
	char c = lexParseString(importExpr->str->value, filename);
	if (c != 0)
	{
		ExprValue err;
		memset(&err, 0, sizeof(ExprValue));
		err.tok = importExpr->str;
		err.errmsg = formatMsg("invalid escape sequence: \\%c", c);
		return err;
	};
	
	FILE *fp = openResFile(filename);
	if (fp == NULL)
	{
		ExprValue err;
		memset(&err, 0, sizeof(ExprValue));
		err.tok = importExpr->str;
		err.errmsg = formatMsg("cannot find resource file `%s'", filename);
		free(filename);
		return err;
	};
	
	char *data = readWholeFile(fp);
	fclose(fp);
	
	if (data == NULL)
	{
		ExprValue err;
		memset(&err, 0, sizeof(ExprValue));
		err.tok = importExpr->str;
		err.errmsg = formatMsg("cannot read resource file `%s'", filename);
		free(filename);
		return err;
	};
	
	free(filename);
	
	char *encoded = (char*) malloc(4 * strlen(data) + 3);
	char *put = encoded;
	*put++ = '"';
	
	const char *scan = data;
	while (*scan != 0)
	{
		unsigned char c = (unsigned char) *scan++;
		sprintf(put, "\\x%02hhX", c);
		put += 4;
	};
	
	*put++ = '"';
	*put = 0;
	
	free(data);
	
	char *cvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
	emitInsn(block, NULL, "Per_Down(%s); %s = Per_MakeString(%s);", cvar, cvar, encoded);
	free(encoded);
	
	ExprValue result;
	memset(&result, 0, sizeof(ExprValue));
	result.type = strtype;
	result.tok = importExpr->str;
	result.cval = formatMsg("(%s)", cvar);
	return result;
};

ExprValue emitLambdaExpression(ExecContext *ctx, Block *block, LambdaExpression *lambdaExpr)
{
	FILE *fp = emitGetOutputFile();
	ClassTemplate *inTemplate = emitGetInTemplate();
	
	ExecContext *rootctx = newRootContext(0, lambdaExpr->retType);
	
	int argcnt = 0;
	ArgumentSpecificationList *arg;
	for (arg=lambdaExpr->args; arg!=NULL; arg=arg->next)
	{
		int argno = argcnt++;
		char cname[64];
		sprintf(cname, "arg%d", argno);
		
		Token *errtok = defineVar(rootctx, arg->type, arg->name, cname, arg->tok, 1);
		if (errtok != NULL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = arg->tok;
			err.errmsg = formatMsg("argument `%s' defined multiple times (in a lambda expression)", arg->name);
			return err;
		};
	};
	
	EmitFunc *impl = newEmitFunc(getEquivalentCType(lambdaExpr->retType));
	
	ExprValue retval = emitExpression(rootctx, impl->code, lambdaExpr->expr);
	if (retval.type == NULL) return retval;
	retval = implicitCast(rootctx, impl->code, retval, lambdaExpr->retType);
	if (retval.type == NULL) return retval;
	
	if (lambdaExpr->retType->kind != PER_TYPE_VOID)
	{
		emitInsn(impl->code, NULL, "retval = (%s);", retval.cval);
	};
	
	static int lambdaCounter = 1;
	const char *mangledLambdaName = mangleName("Per_LX$_?", lambdaCounter++, inTemplate->fullname);
	
	fprintf(fp, "static %s %s(", getEquivalentCType(lambdaExpr->retType), mangledLambdaName);
	argcnt = 0;
	for (arg=lambdaExpr->args; arg!=NULL; arg=arg->next)
	{
		int argno = argcnt++;
		fprintf(fp, "%s arg%d", getEquivalentCType(arg->type), argno);
		
		if (arg->next != NULL) fprintf(fp, ", ");
	};
	fprintf(fp, ")\n");
	outputFunc(fp, impl);
	
	Type *funcType = (Type*) calloc(1, sizeof(Type));
	funcType->kind = PER_TYPE_FUNCREF;
	funcType->retType = lambdaExpr->retType;
	
	funcType->numArgs = argcnt;
	funcType->argTypes = (Type**) malloc(sizeof(void*) * argcnt);
	argcnt = 0;
	for (arg=lambdaExpr->args; arg!=NULL; arg=arg->next)
	{
		funcType->argTypes[argcnt++] = arg->type;
	};
	
	ExprValue result;
	memset(&result, 0, sizeof(ExprValue));
	result.type = funcType;
	result.tok = lambdaExpr->tok;
	result.cval = formatMsg("(&%s)", mangledLambdaName);
	return result;
};

ExprValue emitPrimaryExpression(ExecContext *ctx, Block *block, PrimaryExpression *primary)
{
	if (primary->expr != NULL)
	{
		return emitExpression(ctx, block, primary->expr);
	}
	else if (primary->tok != NULL)
	{
		if (primary->tok->type == TOK_PER_CONST_INT)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_SDWORD);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_UINT)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_DWORD);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_FLOAT)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_FLOAT);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_LONG)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_SQWORD);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_ULONG)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_QWORD);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_DOUBLE)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_DOUBLE);
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", primary->tok->value);
			return result;
		}
		else if (primary->tok->type == TOK_PER_NULL)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_NULL);
			result.tok = primary->tok;
			result.cval = strdup("(NULL)");
			return result;
		}
		else if (primary->tok->type == TOK_PER_THIS)
		{
			if (ctx == NULL || !ctx->hasThis)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = primary->tok;
				err.errmsg = strdup("no `this' reference is available in this context");
				return err;
			};

			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = getThisType(emitGetInTemplate());
			result.tok = primary->tok;
			result.cval = strdup("(thisptr)");
			return result;
		}
		else if (primary->tok->type == TOK_PER_CONST_STRING)
		{
			Type *strtype = (Type*) calloc(1, sizeof(Type));
			strtype->kind = PER_TYPE_MANAGED;
			strtype->fullname = strdup("std.rt.String");
			strtype->temParamTypes = hashtabNew();
			
			char *cvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
			if (ctx != NULL)
			{
				static int nextID;
				int id = nextID++;
				
				FILE *fp = emitGetOutputFile();
				fprintf(fp, "static __attribute__ ((constructor)) Per_Object* _mkstr%d()\n", id);
				fprintf(fp, "{\n");
				fprintf(fp, "\tstatic Per_Object *litstr;\n");
				fprintf(fp, "\tif (litstr == NULL) litstr = Per_MakeString(%s);\n", primary->tok->value);
				fprintf(fp, "\tPer_Up(litstr);\n");
				fprintf(fp, "\treturn litstr;\n");
				fprintf(fp, "};\n\n");
				
				emitInsn(block, NULL, "Per_Down(%s); %s = _mkstr%d();\n", cvar, cvar, id);
			}
			else
			{
				
				emitInsn(block, NULL, "Per_Down(%s); %s = Per_MakeString(%s);", cvar, cvar, primary->tok->value);
			};
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = strtype;
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", cvar);
			return result;
		}
		else if (primary->tok->type == TOK_PER_TRUE)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = primary->tok;
			result.cval = strdup("(1)");
			return result;
		}
		else if (primary->tok->type == TOK_PER_FALSE)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = primary->tok;
			result.cval = strdup("(0)");
			return result;
		}
		else if (primary->tok->type == TOK_PER_ID)
		{
			PerunVar *var = getPerunVar(ctx, primary->tok->value);
			if (var == NULL)
			{
				if (ctx != NULL)
				{
					Type *thistype = getThisType(emitGetInTemplate());
					StaticFieldInfo info = findStaticField(thistype, primary->tok->value, emitGetInTemplate());
					
					if (info.mangled != NULL)
					{
						return makeStaticFieldValue(ctx, block, primary->tok, info);
					};
					
					DynamicFieldInfo dinfo = findDynamicField(thistype, primary->tok->value, emitGetInTemplate());
					if (dinfo.mangled != NULL)
					{
						if (!ctx->hasThis)
						{
							ExprValue err;
							memset(&err, 0, sizeof(ExprValue));
							err.tok = primary->tok;
							err.errmsg = formatMsg("field `%s' is dynamic, and cannot be accessed from a static method", primary->tok->value);
							return err;
						};
						
						return makeDynamicFieldValue(ctx, block, primary->tok, "thisptr", dinfo);
					};
					
					if (hashtabHasKey(emitGetInTemplate()->typeNameTable, primary->tok->value))
					{
						ClassTemplate *other = (ClassTemplate*) hashtabGet(emitGetInTemplate()->typeNameTable, primary->tok->value);
						Type *stype = (Type*) calloc(1, sizeof(Type));
						stype->kind = PER_TYPE_CLASS;
						stype->fullname = strdup(other->fullname);
						
						lexTag(primary->tok, TAG_CLASS_NAME, NULL, other->fullname);
						lexTag(primary->tok, TAG_DEFINED_AT, other->tok, NULL);
						
						emitInsn(block, NULL, "%s();", other->staticInitName);
						
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = primary->tok;
						result.type = stype;
						result.cval = "N/A";
						return result;
					};
					
					const char *subject = NULL;
					if (ctx->hasThis) subject = "thisptr";
					
					Type *methodType = getMethod(ctx, block, primary->tok, thistype, primary->tok->value, subject);
					if (methodType != NULL)
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = primary->tok;
						result.type = methodType;
						result.cval = "N/A";
						return result;
					};
				};
				
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = primary->tok;
				err.errmsg = formatMsg("identifier `%s' is not defined in the current scope", primary->tok->value);
				return err;
			};
			
			lexTag(primary->tok, TAG_VAR_NAME, NULL, primary->tok->value);
			lexTag(primary->tok, TAG_DEFINED_AT, var->tok, NULL);
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = var->type;
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", var->cname);
			return result;
		}
		else
		{
			abort(); // ???
		};
	}
	else if (primary->newExpr != NULL)
	{
		return emitNewExpression(ctx, block, primary->newExpr);
	}
	else if (primary->importExpr != NULL)
	{
		return emitImportExpression(ctx, block, primary->importExpr);
	}
	else if (primary->lambdaExpr != NULL)
	{
		return emitLambdaExpression(ctx, block, primary->lambdaExpr);
	}
	else if (primary->bvar != NULL)
	{
		const char *varname = primary->bvar->value;
		const char *value = bvarGet(varname);
		
		if (value == NULL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = primary->bvar;
			err.errmsg = formatMsg("build variable `%s' not specified", varname);
			return err;
		}
		else
		{
			Type *strtype = (Type*) calloc(1, sizeof(Type));
			strtype->kind = PER_TYPE_MANAGED;
			strtype->fullname = strdup("std.rt.String");
			strtype->temParamTypes = hashtabNew();
			
			char *cstr = (char*) malloc(4 * strlen(value) + 3);
			strcpy(cstr, "\"");
			
			const char *scan;
			for (scan=value; *scan!=0; scan++)
			{
				char tmp[5];
				sprintf(tmp, "\\x%02hhx", *scan);
				strcat(cstr, tmp);
			};
			
			strcat(cstr, "\"");
			
			char *cvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
			emitInsn(block, NULL, "Per_Down(%s); %s = Per_MakeString(%s);", cvar, cvar, cstr);
			free(cstr);
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = strtype;
			result.tok = primary->tok;
			result.cval = formatMsg("(%s)", cvar);
			return result;
		};
	}
	else
	{
		abort(); // ???
	};
};

ExprValue emitCallPostfix(ExecContext *ctx, Block *block, ExprValue subject, CallPostfix *call)
{
	int numArgs = 0;
	ArgumentExpressionList *expr;
	
	for (expr=call->head; expr!=NULL; expr=expr->next)
	{
		numArgs++;
	};
	
	ExprValue *args = (ExprValue*) calloc(numArgs, sizeof(ExprValue));
	int i = 0;
	for (expr=call->head; expr!=NULL; expr=expr->next)
	{
		ExprValue arg = emitAssignmentExpression(ctx, block, expr->assign);
		if (arg.type == NULL) return arg;
		
		args[i++] = arg;
	};
	
	return emitCall(ctx, block, subject.tok, subject, args, numArgs);
};

ExprValue emitSubscriptPostfix(ExecContext *ctx, Block *block, ExprValue subject, SubscriptPostfix *subscript)
{
	ExprValue sub = emitExpression(ctx, block, subscript->expr);
	if (sub.type == NULL) return sub;
	
	if (subject.type->kind == PER_TYPE_ARRAY)
	{
		// try to cast it to uint...
		ExprValue subAsUint = implicitCast(ctx, block, sub, makePrimitiveType(PER_TYPE_DWORD));
		if (subAsUint.type == NULL)
		{
			// not possible, so now try int instead
			sub = implicitCast(ctx, block, sub, makePrimitiveType(PER_TYPE_SDWORD));
			if (sub.type == NULL) return sub;
		}
		else
		{
			sub = subAsUint;
		};
		
		// do a bounds check
		emitInsn(block, NULL, "Per_BoundsCheck(%s, %s);", subject.cval, sub.cval);
		
		// lock the array
		emitInsn(block, NULL, "Per_ArrayLock(%s);", subject.cval);
		
		// put the value in a temporary variable
		int sect = PER_VSECT_PRIMITIVE;
		if (isManagedType(subject.type->elementType)) sect = PER_VSECT_MANAGED;
		if (subject.type->elementType->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
		
		char *tmpvar = makeTempVar(block->func, getEquivalentCType(subject.type->elementType), sect);
		if (isManagedType(subject.type->elementType))
		{
			emitInsn(block, NULL, "Per_Down(%s); %s = ((Per_Object**)%s)[%s]; Per_Up(%s);", tmpvar, tmpvar, subject.cval, sub.cval, tmpvar);
		}
		else if (subject.type->elementType->kind == PER_TYPE_ARRAY)
		{
			emitInsn(block, NULL, "Per_ArrayDown(%s); %s = ((void**)%s)[%s]; Per_ArrayUp(%s);", tmpvar, tmpvar, subject.cval, sub.cval, tmpvar);
		}
		else
		{
			emitInsn(block, NULL, "%s = ((%s*)%s)[%s];", tmpvar, getEquivalentCType(subject.type->elementType), subject.cval, sub.cval);
		};
		
		// unlock the array
		emitInsn(block, NULL, "Per_ArrayUnlock(%s);", subject.cval);
		
		// yay, result
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.tok = sub.tok;
		result.type = subject.type->elementType;
		result.cval = formatMsg("(%s)", tmpvar);
		return result;
	}
	else if (isManagedType(subject.type))
	{
		// get the opGet() function
		ExprValue opGet = emitGetMember(ctx, block, subject, subject.tok, "opGet");
		if (opGet.type == NULL) return opGet;
		
		return emitCall(ctx, block, subject.tok, opGet, &sub, 1);
	}
	else
	{
		ExprValue err;
		memset(&err, 0, sizeof(ExprValue));
		err.tok = sub.tok;
		err.errmsg = formatMsg("the subscript operator is not applicable to type `%s'", getTypeName(subject.type));
		return err;
	};
};

ExprValue emitPostfixExpression(ExecContext *ctx, Block *block, PostfixExpression *postfix)
{
	if (postfix->primary != NULL)
	{
		return emitPrimaryExpression(ctx, block, postfix->primary);
	}
	else
	{
		ExprValue subject = emitPostfixExpression(ctx, block, postfix->left);
		if (subject.type == NULL) return subject;
		
		if (postfix->member != NULL)
		{
			return emitGetMember(ctx, block, subject, postfix->member->id, postfix->member->id->value);
		}
		else if (postfix->call != NULL)
		{
			return emitCallPostfix(ctx, block, subject, postfix->call);
		}
		else if (postfix->subscript != NULL)
		{
			return emitSubscriptPostfix(ctx, block, subject, postfix->subscript);
		}
		else if (postfix->op != NULL && postfix->op->type == TOK_PER_INC)
		{
			if (isIntegerType(subject.type))
			{
				char *tmpvar = makeTempVar(block->func, getEquivalentCType(subject.type), PER_VSECT_PRIMITIVE);
				emitInsn(block, NULL, "%s = %s;", tmpvar, subject.cval);
				
				ExprValue value = subject;
				value.cval = formatMsg("(%s + 1)", tmpvar);
				
				ExprValue assign = emitNormalAssignment(ctx, block, postfix->left, value);
				if (assign.type == NULL) return assign;
				
				ExprValue result = subject;
				result.cval = formatMsg("(%s)", tmpvar);
				return result;
			}
			else
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = postfix->op;
				err.errmsg = formatMsg("post-increment operator `++' is not applicable to type `%s'", getTypeName(subject.type));
				return err;
			};
		}
		else if (postfix->op != NULL && postfix->op->type == TOK_PER_DEC)
		{
			if (isIntegerType(subject.type))
			{
				char *tmpvar = makeTempVar(block->func, getEquivalentCType(subject.type), PER_VSECT_PRIMITIVE);
				emitInsn(block, NULL, "%s = %s;", tmpvar, subject.cval);
				
				ExprValue value = subject;
				value.cval = formatMsg("(%s - 1)", tmpvar);
				
				ExprValue assign = emitNormalAssignment(ctx, block, postfix->left, value);
				if (assign.type == NULL) return assign;
				
				ExprValue result = subject;
				result.cval = formatMsg("(%s)", tmpvar);
				return result;
			}
			else
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = postfix->op;
				err.errmsg = formatMsg("post-decrement operator `--' is not applicable to type `%s'", getTypeName(subject.type));
				return err;
			};
		}
		else if (postfix->inst != NULL)
		{
			if (isManagedType(subject.type))
			{
				ClassTemplate *tem = findClassByName(postfix->inst->tok, postfix->inst->type->fullname);
				
				ExprValue result;
				memset(&result, 0, sizeof(ExprValue));
				result.tok = postfix->inst->tok;
				result.type = makePrimitiveType(PER_TYPE_BOOL);
				result.cval = formatMsg("Per_Is(%s, &%s)", subject.cval, tem->mangledName);
				return result;
			}
			else
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = postfix->inst->tok;
				err.errmsg = formatMsg("the left side of the `is' operator must be a value of a managed type, not `%s'", getTypeName(subject.type));
				return err;
			};
		}
		else
		{
			abort(); // ???
		};
	};
};

ExprValue emitUnaryExpression(ExecContext *ctx, Block *block, UnaryExpression *unary)
{
	if (unary->postfix != NULL)
	{
		return emitPostfixExpression(ctx, block, unary->postfix);
	}
	else if (unary->op->type == TOK_PER_NOT)
	{
		ExprValue value = emitCastExpression(ctx, block, unary->cast);
		if (value.type == NULL) return value;
		
		if (value.type->kind != PER_TYPE_BOOL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = unary->op;
			err.errmsg = formatMsg("the `!' operator requires an operand of type `bool', got `%s'", getTypeName(value.type));
			return err;
		};
		
		value.cval = formatMsg("(!%s)", value.cval);
		return value;
	}
	else if (unary->op->type == TOK_PER_SUB)
	{
		ExprValue value = emitCastExpression(ctx, block, unary->cast);
		if (value.type == NULL) return value;
		
		if (isManagedType(value.type))
		{
			ExprValue opNegate = emitGetMember(ctx, block, value, unary->op, "opNegate");
			if (opNegate.type == NULL) return opNegate;
			
			return emitCall(ctx, block, unary->op, opNegate, NULL, 0);
		}
		else if (isSignedIntegerType(value.type) || value.type->kind == PER_TYPE_FLOAT || value.type->kind == PER_TYPE_DOUBLE)
		{
			value = emitPromotion(value);
			if (value.type == NULL) return value;
			
			value.cval = formatMsg("(-%s)", value.cval);
			return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = unary->op;
			err.errmsg = formatMsg("unary `-' operator is not applicable to type `%s'", getTypeName(value.type));
			return err;
		};
	}
	else if (unary->op->type == TOK_PER_INV)
	{
		ExprValue value = emitCastExpression(ctx, block, unary->cast);
		if (value.type == NULL) return value;
		
		if (isManagedType(value.type))
		{
			ExprValue opInvert = emitGetMember(ctx, block, value, unary->op, "opInvert");
			if (opInvert.type == NULL) return opInvert;
			
			return emitCall(ctx, block, unary->op, opInvert, NULL, 0);
		}
		else if (isIntegerType(value.type))
		{
			value = emitPromotion(value);
			if (value.type == NULL) return value;
			
			value.cval = formatMsg("(~%s)", value.cval);
			return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = unary->op;
			err.errmsg = formatMsg("unary `~' operator is not applicable to type `%s'", getTypeName(value.type));
			return err;
		};
	}
	else if (unary->op->type == TOK_PER_INC)
	{
		ExprValue value = emitPostfixExpression(ctx, block, unary->sub);
		if (value.type == NULL) return value;
		
		if (isIntegerType(value.type))
		{
			value.cval = formatMsg("(%s + 1)", value.cval);
			return emitNormalAssignment(ctx, block, unary->sub, value);
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = unary->op;
			err.errmsg = formatMsg("pre-increment operator `++' is not applicable to type `%s'", getTypeName(value.type));
			return err;
		};
	}
	else if (unary->op->type == TOK_PER_DEC)
	{
		ExprValue value = emitPostfixExpression(ctx, block, unary->sub);
		if (value.type == NULL) return value;
		
		if (isIntegerType(value.type))
		{
			value.cval = formatMsg("(%s - 1)", value.cval);
			return emitNormalAssignment(ctx, block, unary->sub, value);
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = unary->op;
			err.errmsg = formatMsg("pre-decrement operator `--' is not applicable to type `%s'", getTypeName(value.type));
			return err;
		};
	}
	else
	{
		abort();	// ???
	};
};

int isMutuallyConvertiblePrimitive(Type *type)
{
	// "mutually convertible primitive types" are primitive types which can all be
	// cast to each other explicitly
	switch (type->kind)
	{
	case PER_TYPE_BYTE:
	case PER_TYPE_WORD:
	case PER_TYPE_DWORD:
	case PER_TYPE_QWORD:
	case PER_TYPE_SBYTE:
	case PER_TYPE_SWORD:
	case PER_TYPE_SDWORD:
	case PER_TYPE_SQWORD:
	case PER_TYPE_FLOAT:
	case PER_TYPE_DOUBLE:
	case PER_TYPE_PTR:
	case PER_TYPE_SPTR:
		return 1;
	default:
		return 0;
	};
};

ExprValue emitCastExpression(ExecContext *ctx, Block *block, CastExpression *cast)
{
	if (cast->unary != NULL)
	{
		return emitUnaryExpression(ctx, block, cast->unary);
	}
	else
	{
		ExprValue value = emitCastExpression(ctx, block, cast->sub);
		if (value.type == NULL) return value;
		
		// anything can be converted to "void"; just discard the value
		if (cast->type->kind == PER_TYPE_VOID)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.tok = value.tok;
			result.type = cast->type;		// == void
			result.cval = "N/A";
			return result;
		};
		
		// mutually convertible primitive types can all be converted between each other
		// with explicit casts
		if (isMutuallyConvertiblePrimitive(value.type) && isMutuallyConvertiblePrimitive(cast->type))
		{
			value.cval = formatMsg("((%s) %s)", getEquivalentCType(cast->type), value.cval);
			value.type = cast->type;
			return value;
		};
		
		// if converting from a managed type to another, it's a "dynamic cast"
		if (isManagedType(value.type) && isManagedType(cast->type))
		{
			// emit a check for that type at runtime, but otherwise everything stays the same
			ClassTemplate *dest = findClassByName(NULL, cast->type->fullname);
			emitInsn(block, NULL, "Per_GetRequiredComponent(%s, &%s);", value.cval, dest->mangledName);
			value.type = cast->type;
			return value;
		};
		
		// allow explicitly casting from ptr_t to function reference types
		if (value.type->kind == PER_TYPE_PTR && cast->type->kind == PER_TYPE_FUNCREF)
		{
			value.cval = formatMsg("((void*)%s)", value.cval);
			value.type = cast->type;
			return value;
		};
		
		// allow explicitly converting form function references types to ptr_t
		if (value.type->kind == PER_TYPE_FUNCREF && cast->type->kind == PER_TYPE_PTR)
		{
			value.cval = formatMsg("((size_t)%s)", value.cval);
			value.type = cast->type;
			return value;
		};
		
		// allow explicitly converting from array types to ptr_T
		if (value.type->kind == PER_TYPE_ARRAY && cast->type->kind == PER_TYPE_PTR)
		{
			value.cval = formatMsg("((size_t)%s)", value.cval);
			value.type = cast->type;
			return value;
		};
		
		// in all other cases, act as an implicit cast
		ExprValue result = implicitCast(ctx, block, value, cast->type);
		if (result.type == NULL)
		{
			result.errmsg = formatMsg("value of type `%s' cannot be converted to `%s'", getTypeName(value.type), getTypeName(cast->type));
		};
		
		return result;
	};
};

ExprValue emitMultiplicativeExpression(ExecContext *ctx, Block *block, MultiplicativeExpression *mul)
{
	ExprValue value = emitCastExpression(ctx, block, mul->cast);
	if (value.type == NULL) return value;
	
	Token *op = mul->op;
	for (mul=mul->next; mul!=NULL; mul=mul->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitCastExpression(ctx, block, mul->cast);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type) || isFloatingType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s %s %s)", value.cval, op->value, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			const char *methodName;
			if (op->type == TOK_PER_AST)
			{
				methodName = "opMul";
			}
			else if (op->type == TOK_PER_DIV)
			{
				methodName = "opDiv";
			}
			else
			{
				methodName = "opMod";
			};
			
			ExprValue opShift = emitGetMember(ctx, block, value, op, methodName);
			if (opShift.type == NULL) return opShift;
			
			if (opShift.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `%s' of class `%s' is not a method", methodName, value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opShift, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = mul->op;
	};
	
	return value;
};

ExprValue emitAdditiveExpression(ExecContext *ctx, Block *block, AdditiveExpression *add)
{
	ExprValue value = emitMultiplicativeExpression(ctx, block, add->mul);
	if (value.type == NULL) return value;
	
	Token *op = add->op;
	for (add=add->next; add!=NULL; add=add->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitMultiplicativeExpression(ctx, block, add->mul);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type) || isFloatingType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s %s %s)", value.cval, op->value, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			const char *methodName;
			if (op->type == TOK_PER_ADD)
			{
				methodName = "opAdd";
			}
			else
			{
				methodName = "opSub";
			};
			
			ExprValue opShift = emitGetMember(ctx, block, value, op, methodName);
			if (opShift.type == NULL) return opShift;
			
			if (opShift.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `%s' of class `%s' is not a method", methodName, value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opShift, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = add->op;
	};
	
	return value;
};

ExprValue emitShiftExpression(ExecContext *ctx, Block *block, ShiftExpression *shift)
{
	ExprValue value = emitAdditiveExpression(ctx, block, shift->add);
	if (value.type == NULL) return value;
	
	Token *op = shift->op;
	for (shift=shift->next; shift!=NULL; shift=shift->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitAdditiveExpression(ctx, block, shift->add);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type))
		{
			// TODO: try to convert the right value from a managed wrapper
			// to primitive type first.
			
			if (!isIntegerType(right.type))
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
				return err;
			};
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s %s %s)", value.cval, op->value, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			const char *methodName;
			if (op->type == TOK_PER_SHL)
			{
				methodName = "opShiftLeft";
			}
			else
			{
				methodName = "opShiftRight";
			};
			
			ExprValue opShift = emitGetMember(ctx, block, value, op, methodName);
			if (opShift.type == NULL) return opShift;
			
			if (opShift.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `%s' of class `%s' is not a method", methodName, value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opShift, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = shift->op;
	};
	
	return value;
};

ExprValue emitRelationalExpression(ExecContext *ctx, Block *block, RelationalExpression *rel)
{
	ExprValue value = emitShiftExpression(ctx, block, rel->shift);
	if (value.type == NULL) return value;
	
	Token *op = rel->op;
	for (rel=rel->next; rel!=NULL; rel=rel->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitShiftExpression(ctx, block, rel->shift);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type) || isFloatingType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(%s %s %s)", value.cval, op->value, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			ExprValue opCompare = emitGetMember(ctx, block, value, op, "opCompare");
			if (opCompare.type == NULL) return opCompare;
			
			if (opCompare.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `opCompare' of class `%s' is not a method", value.type->fullname);
				return err;
			};
			
			ExprValue relval = emitSingleArgCall(ctx, block, op, opCompare, right);
			if (relval.type == NULL) return relval;
			
			relval = emitPromotion(relval);
			if (relval.type == NULL) return relval;
			
			if (relval.type->kind != PER_TYPE_SDWORD && relval.type->kind != PER_TYPE_SQWORD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("the method `%s.opCompare(%s)' returned a value whose type is not a signed integer type",
						getTypeName(value.type), getTypeName(right.type));
				return err;
			};
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(%s %s 0)", relval.cval, op->value);
			value = result;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = rel->op;
	};
	
	return value;
};

ExprValue emitEqualityExpression(ExecContext *ctx, Block *block, EqualityExpression *equality)
{
	ExprValue value = emitRelationalExpression(ctx, block, equality->rel);
	if (value.type == NULL) return value;
	
	Token *op = equality->op;
	for (equality=equality->next; equality!=NULL; equality=equality->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitRelationalExpression(ctx, block, equality->rel);
		if (right.type == NULL) return right;
		
		if (isPrimitiveType(value.type) || value.type->kind == PER_TYPE_FUNCREF)
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(%s == %s)", value.cval, right.cval);
			value = result;
		}
		else if (value.type->kind == PER_TYPE_ARRAY)
		{
			if (!isSameType(value.type, right.type) && right.type->kind != PER_TYPE_NULL)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("comparing a value of array type `%s' can only be done against a value of the same type or the null constant, not `%s'",
						getTypeName(value.type), getTypeName(right.type));
				return err;
			};

			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(%s == %s)", value.cval, right.cval);
			value = result;
		}
		else if ((value.type->kind == PER_TYPE_NULL && isManagedType(right.type)) || (isManagedType(value.type) && right.type->kind == PER_TYPE_NULL))
		{
			// comparing a managed value against a 'null' constant
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(%s == %s)", value.cval, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			if (!isManagedType(right.type))
			{
				right = implicitCast(ctx, block, right, value.type);
				if (right.type == NULL) return right;
			};
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = makePrimitiveType(PER_TYPE_BOOL);
			result.tok = op;
			result.cval = formatMsg("(Per_Equals(%s, %s))", value.cval, right.cval);
			value = result;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `%s' operator cannot be applied to types `%s' and `%s'", op->value, getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		if (op->type == TOK_PER_NEQ)
		{
			value.cval = formatMsg("(!%s)", value.cval);
		};
		
		op = equality->op;
	};
	
	return value;
};

ExprValue emitBitwiseANDExpression(ExecContext *ctx, Block *block, BitwiseANDExpression *bitwiseAND)
{
	ExprValue value = emitEqualityExpression(ctx, block, bitwiseAND->equality);
	if (value.type == NULL) return value;
	
	Token *op = bitwiseAND->op;
	for (bitwiseAND=bitwiseAND->next; bitwiseAND!=NULL; bitwiseAND=bitwiseAND->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitEqualityExpression(ctx, block, bitwiseAND->equality);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s & %s)", value.cval, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			ExprValue opBitwiseAND = emitGetMember(ctx, block, value, op, "opBitwiseAND");
			if (opBitwiseAND.type == NULL) return opBitwiseAND;
			
			if (opBitwiseAND.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `opBitwiseAND' of class `%s' is not a method", value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opBitwiseAND, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `&' operator cannot be applied to operands of types `%s' and `%s'", getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = bitwiseAND->op;
	};
	
	return value;
};

ExprValue emitBitwiseXORExpression(ExecContext *ctx, Block *block, BitwiseXORExpression *bitwiseXOR)
{
	ExprValue value = emitBitwiseANDExpression(ctx, block, bitwiseXOR->bitwiseAND);
	if (value.type == NULL) return value;
	
	Token *op = bitwiseXOR->op;
	for (bitwiseXOR=bitwiseXOR->next; bitwiseXOR!=NULL; bitwiseXOR=bitwiseXOR->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitBitwiseANDExpression(ctx, block, bitwiseXOR->bitwiseAND);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s ^ %s)", value.cval, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			ExprValue opBitwiseXOR = emitGetMember(ctx, block, value, op, "opBitwiseXOR");
			if (opBitwiseXOR.type == NULL) return opBitwiseXOR;
			
			if (opBitwiseXOR.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `opBitwiseXOR' of class `%s' is not a method", value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opBitwiseXOR, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `^' operator cannot be applied to operands of types `%s' and `%s'", getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = bitwiseXOR->op;
	};
	
	return value;
};

ExprValue emitBitwiseORExpression(ExecContext *ctx, Block *block, BitwiseORExpression *bitwiseOR)
{
	ExprValue value = emitBitwiseXORExpression(ctx, block, bitwiseOR->bitwiseXOR);
	if (value.type == NULL) return value;
	
	Token *op = bitwiseOR->op;
	for (bitwiseOR=bitwiseOR->next; bitwiseOR!=NULL; bitwiseOR=bitwiseOR->next)
	{
		value = emitPromotion(value);
		if (value.type == NULL) return value;
		
		ExprValue right = emitBitwiseXORExpression(ctx, block, bitwiseOR->bitwiseXOR);
		if (right.type == NULL) return right;
		
		if (isIntegerType(value.type))
		{
			right = implicitCast(ctx, block, right, value.type);
			if (right.type == NULL) return right;
			
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.type = value.type;
			result.tok = op;
			result.cval = formatMsg("(%s | %s)", value.cval, right.cval);
			value = result;
		}
		else if (isManagedType(value.type))
		{
			ExprValue opBitwiseOR = emitGetMember(ctx, block, value, op, "opBitwiseOR");
			if (opBitwiseOR.type == NULL) return opBitwiseOR;
			
			if (opBitwiseOR.type->kind != PER_TYPE_METHOD)
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = op;
				err.errmsg = formatMsg("member `opBitwiseOR' of class `%s' is not a method", value.type->fullname);
				return err;
			};
			
			value = emitSingleArgCall(ctx, block, op, opBitwiseOR, right);
			if (value.type == NULL) return value;
		}
		else
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = op;
			err.errmsg = formatMsg("the `|' operator cannot be applied to operands of types `%s' and `%s'", getTypeName(value.type), getTypeName(right.type));
			return err;
		};
		
		op = bitwiseOR->op;
	};
	
	return value;
};

ExprValue emitLogicalANDExpression(ExecContext *ctx, Block *block, LogicalANDExpression *logicalAND)
{
	ExprValue value = emitBitwiseORExpression(ctx, block, logicalAND->bitwiseOR);
	if (value.type == NULL) return value;
	
	if (logicalAND->next != NULL)
	{
		if (value.type->kind != PER_TYPE_BOOL)
		{
			value.errmsg = formatMsg("both operands to `&&' must have type `bool', but the left operand has type `%s'", getTypeName(value.type));
			value.type = NULL;
			return value;
		};
		
		Block *mainBody;
		Block *elseBody;
		
		char *cvar = makeTempVar(block->func, "unsigned char", 0);
		emitInsn(block, &mainBody, "if (!(%s)) ", value.cval);
		emitInsn(mainBody, NULL, "%s = 0;", cvar);
		emitInsn(block, &elseBody, "else ");
		
		ExprValue right = emitLogicalANDExpression(ctx, elseBody, logicalAND->next);
		if (right.type == NULL) return right;
		
		if (right.type->kind != PER_TYPE_BOOL)
		{
			right.errmsg = formatMsg("both operands to `&&' must have type `bool', but the right operand has type `%s'", getTypeName(value.type));
			right.type = NULL;
			return right;
		};
		
		emitInsn(elseBody, NULL, "%s = %s;", cvar, right.cval);
		
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.type = value.type;
		result.tok = logicalAND->op;
		result.cval = formatMsg("(%s)", cvar);
		return result;
	};
	
	return value;
};

ExprValue emitLogicalORExpression(ExecContext *ctx, Block *block, LogicalORExpression *logicalOR)
{
	ExprValue value = emitLogicalANDExpression(ctx, block, logicalOR->logicalAND);
	if (value.type == NULL) return value;
	
	if (logicalOR->next != NULL)
	{
		if (value.type->kind != PER_TYPE_BOOL)
		{
			value.errmsg = formatMsg("both operands to `||' must have type `bool', but the left operand has type `%s'", getTypeName(value.type));
			value.type = NULL;
			return value;
		};
		
		Block *mainBody;
		Block *elseBody;
		
		char *cvar = makeTempVar(block->func, "unsigned char", 0);
		emitInsn(block, &mainBody, "if (%s) ", value.cval);
		emitInsn(mainBody, NULL, "%s = 1;", cvar);
		emitInsn(block, &elseBody, "else ");
		
		ExprValue right = emitLogicalORExpression(ctx, elseBody, logicalOR->next);
		if (right.type == NULL) return right;
		
		if (right.type->kind != PER_TYPE_BOOL)
		{
			right.errmsg = formatMsg("both operands to `||' must have type `bool', but the right operand has type `%s'", getTypeName(value.type));
			right.type = NULL;
			return right;
		};
		
		emitInsn(elseBody, NULL, "%s = %s;", cvar, right.cval);
		
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.type = value.type;
		result.tok = logicalOR->op;
		result.cval = formatMsg("(%s)", cvar);
		return result;
	};
	
	return value;
};

ExprValue emitConditionalExpression(ExecContext *ctx, Block *block, ConditionalExpression *cond)
{
	ExprValue value = emitLogicalORExpression(ctx, block, cond->logicalOR);
	if (value.type == NULL) return value;
	
	if (cond->exprTrue == NULL)
	{
		return value;
	}
	else
	{
		if (value.type->kind != PER_TYPE_BOOL)
		{
			value.errmsg = formatMsg("the subject of a conditional expression must have type `bool', but we have `%s'", getTypeName(value.type));
			value.type = NULL;
			return value;
		};
		
		ExprValue trueVal = emitExpression(ctx, block, cond->exprTrue);
		if (trueVal.type == NULL) return trueVal;
		
		ExprValue falseVal = emitConditionalExpression(ctx, block, cond->exprFalse);
		if (falseVal.type == NULL) return falseVal;
		
		falseVal = implicitCast(ctx, block, falseVal, trueVal.type);
		if (falseVal.type == NULL) return falseVal;
		
		ExprValue result;
		memset(&result, 0, sizeof(ExprValue));
		result.tok = cond->op;
		result.type = trueVal.type;
		result.cval = formatMsg("(%s ? %s : %s)", value.cval, trueVal.cval, falseVal.cval);
		return result;
	};
};

void emitStaticAssignment(ExecContext *ctx, Block *block, StaticFieldInfo info, ExprValue value)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(info.type)) sect = PER_VSECT_MANAGED;
	if (info.type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	emitInsn(block, NULL, "Per_StaticLock(&%s);", info.definer->staticLockName);
	
	if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", info.mangled, info.mangled, value.cval, value.cval);
	}
	else if (sect == PER_VSECT_ARRAY)
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", info.mangled, info.mangled, value.cval, value.cval);
	}
	else
	{
		emitInsn(block, NULL, "%s = %s;", info.mangled, value.cval);
	};
	
	emitInsn(block, NULL, "Per_StaticUnlock(&%s);", info.definer->staticLockName);
};

void emitDynamicAssignment(ExecContext *ctx, Block *block, const char *subject, DynamicFieldInfo info, ExprValue value)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(info.type)) sect = PER_VSECT_MANAGED;
	if (info.type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	emitInsn(block, NULL, "Per_Lock(%s);", subject);
	
	char *fetcher = formatMsg("(*((%s*)Per_GetFieldPtr(%s, &%s, %s)))", getEquivalentCType(info.type), subject,
					info.definer->mangledName, info.mangled);
	
	if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", fetcher, fetcher, value.cval, value.cval);
	}
	else if (sect == PER_VSECT_ARRAY)
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", fetcher, fetcher, value.cval, value.cval);
	}
	else
	{
		emitInsn(block, NULL, "%s = %s;", fetcher, value.cval);
	};
	
	emitInsn(block, NULL, "Per_Unlock(%s);", subject);
};

void emitLocalAssignment(ExecContext *ctx, Block *block, PerunVar *var, ExprValue value)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(var->type)) sect = PER_VSECT_MANAGED;
	if (var->type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", var->cname, var->cname, value.cval, value.cval);
	}
	else if (sect == PER_VSECT_ARRAY)
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", var->cname, var->cname, value.cval, value.cval);
	}
	else
	{
		emitInsn(block, NULL, "%s = %s;", var->cname, value.cval);
	};
};

ExprValue emitNormalAssignment(ExecContext *ctx, Block *block, PostfixExpression *postfix, ExprValue value)
{
	if (postfix->primary != NULL)
	{
		PrimaryExpression *primary = postfix->primary;
		if (primary->expr != NULL)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.tok = primary->firstTok;
			result.errmsg = formatMsg("the left side of an assignment cannot be a paranthesized expression");
			return result;
		}
		else if (primary->tok != NULL)
		{
			if (primary->tok->type == TOK_PER_ID)
			{
				const char *name = primary->tok->value;
				
				// see if this is a local variable
				PerunVar *var = getPerunVar(ctx, name);
				if (var != NULL)
				{
					if (var->isFinal)
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = primary->tok;
						result.errmsg = formatMsg("variable `%s' is final and its value cannot be changed", name);
						return result;
					};
					
					lexTag(primary->tok, TAG_VAR_NAME, NULL, primary->tok->value);
					lexTag(primary->tok, TAG_DEFINED_AT, var->tok, NULL);
					
					value = implicitCast(ctx, block, value, var->type);
					if (value.type == NULL) return value;
					
					emitLocalAssignment(ctx, block, var, value);
					return value;
				};
				
				// try to see if this is a static field
				Type *thistype = getThisType(emitGetInTemplate());
				StaticFieldInfo info = findStaticField(thistype, name, emitGetInTemplate());
				
				if (info.mangled != NULL)
				{
					if (info.flags & PER_Q_FINAL)
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = primary->tok;
						result.errmsg = formatMsg("field `%s' in class `%s' is final (so its value cannot be changed)", name, thistype->fullname);
						return result;
					};
					
					lexTag(primary->tok, TAG_FIELD_NAME, NULL, name);
					lexTag(primary->tok, TAG_DEFINED_AT, info.tok, NULL);
					
					value = implicitCast(ctx, block, value, info.type);
					if (value.type == NULL) return value;
					
					emitStaticAssignment(ctx, block, info, value);
					return value;
				}
				else
				{
					DynamicFieldInfo dinfo = findDynamicField(thistype, name, emitGetInTemplate());
					
					if (dinfo.mangled != NULL)
					{
						if (!ctx->hasThis)
						{
							ExprValue result;
							memset(&result, 0, sizeof(ExprValue));
							result.tok = primary->tok;
							result.errmsg = formatMsg("field `%s' is non-static, and cannot be accessed from a static context", name);
							return result;
						};
						
						if (dinfo.flags & PER_Q_FINAL)
						{
							ExprValue result;
							memset(&result, 0, sizeof(ExprValue));
							result.tok = primary->tok;
							result.errmsg = formatMsg("field `%s' in class `%s' is final (so its value cannot be changed)", name, thistype->fullname);
							return result;
						};

						lexTag(primary->tok, TAG_FIELD_NAME, NULL, name);
						lexTag(primary->tok, TAG_DEFINED_AT, info.tok, NULL);
						
						value = implicitCast(ctx, block, value, dinfo.type);
						if (value.type == NULL) return value;
						
						emitDynamicAssignment(ctx, block, "thisptr", dinfo, value);
						return value;
					}
					else
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = primary->tok;
						result.errmsg = formatMsg("identifier `%s' does not refer to an assignable field in this context", name);
						return result;
					};
				};
			}
			else
			{
				ExprValue result;
				memset(&result, 0, sizeof(ExprValue));
				result.tok = primary->tok;
				result.errmsg = formatMsg("`%s' is not assignable", primary->tok->value);
				return result;
			};
		}
		else if (primary->newExpr != NULL)
		{
			ExprValue result;
			memset(&result, 0, sizeof(ExprValue));
			result.tok = primary->newExpr->tok;
			result.errmsg = formatMsg("left side of an assignment operator is not assignable");
			return result;
		}
		else
		{
			abort(); // ???
		};
	}
	else
	{
		// first get the value on the left
		ExprValue subject = emitPostfixExpression(ctx, block, postfix->left);
		if (subject.type == NULL) return subject;
		
		if (postfix->member != NULL)
		{
			const char *name = postfix->member->id->value;
			
			// replace parameter types with their minimum base types
			Type *type = subject.type;
			if (type->kind == PER_TYPE_TEMPLATE_PARAM)
			{
				type = type->minBase;
			};
			
			// in the case of a static class type, we can only set static fields
			if (type->kind == PER_TYPE_CLASS)
			{
				Type *thistype = getThisType(findClassByName(NULL, type->fullname));
				StaticFieldInfo info = findStaticField(thistype, name, emitGetInTemplate());
						
				if (info.mangled == NULL)
				{	
					ExprValue result;
					memset(&result, 0, sizeof(ExprValue));
					result.tok = postfix->member->id;;
					result.errmsg = formatMsg("class `%s' has no accessible static field named `%s'", type->fullname, name);
					return result;
				};
				
				if (info.flags & PER_Q_FINAL)
				{
					ExprValue result;
					memset(&result, 0, sizeof(ExprValue));
					result.tok = postfix->member->id;
					result.errmsg = formatMsg("field `%s' in class `%s' is final (so its value cannot be changed)", name, type->fullname);
					return result;
				};
				
				value = implicitCast(ctx, block, value, info.type);
				if (value.type == NULL) return value;
				
				lexTag(postfix->member->id, TAG_FIELD_NAME, NULL, NULL);
				lexTag(postfix->member->id, TAG_DEFINED_AT, info.tok, NULL);
				
				emitStaticAssignment(ctx, block, info, value);
				return value;
			};
			
			if (type->kind == PER_TYPE_MANAGED)
			{
				// try static
				StaticFieldInfo info = findStaticField(type, name, emitGetInTemplate());
				
				if (info.mangled == NULL)
				{
					DynamicFieldInfo dinfo = findDynamicField(type, name, emitGetInTemplate());
					
					if (dinfo.mangled == NULL)
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = postfix->member->id;
						result.errmsg = formatMsg("class `%s' has no accessible field named `%s'", type->fullname, name);
						return result;
					};

					if (dinfo.flags & PER_Q_FINAL)
					{
						ExprValue result;
						memset(&result, 0, sizeof(ExprValue));
						result.tok = postfix->member->id;
						result.errmsg = formatMsg("field `%s' in class `%s' is final (so its value cannot be changed)", name, type->fullname);
						return result;
					};

					value = implicitCast(ctx, block, value, dinfo.type);
					if (value.type == NULL) return value;
					
					lexTag(postfix->member->id, TAG_FIELD_NAME, NULL, NULL);
					lexTag(postfix->member->id, TAG_DEFINED_AT, dinfo.tok, NULL);
					
					emitDynamicAssignment(ctx, block, subject.cval, dinfo, value);
					return value;
				};
				
				if (info.flags & PER_Q_FINAL)
				{
					ExprValue result;
					memset(&result, 0, sizeof(ExprValue));
					result.tok = postfix->member->id;
					result.errmsg = formatMsg("field `%s' in class `%s' is final (so its value cannot be changed)", name, type->fullname);
					return result;
				};
				
				value = implicitCast(ctx, block, value, info.type);
				if (value.type == NULL) return value;
				
				lexTag(postfix->member->id, TAG_FIELD_NAME, NULL, NULL);
				lexTag(postfix->member->id, TAG_DEFINED_AT, info.tok, NULL);
				
				emitStaticAssignment(ctx, block, info, value);
				return value;
			};
			
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = postfix->member->id;
			err.errmsg = formatMsg("type `%s' has no assignable property named `%s'", getTypeName(type), name);
			return err;
		}
		else if (postfix->subscript != NULL)
		{
			ExprValue sub = emitExpression(ctx, block, postfix->subscript->expr);
			if (sub.type == NULL) return sub;
			
			if (subject.type->kind == PER_TYPE_ARRAY)
			{
				// try to cast it to uint...
				ExprValue subAsUint = implicitCast(ctx, block, sub, makePrimitiveType(PER_TYPE_DWORD));
				if (subAsUint.type == NULL)
				{
					// not possible, so now try int instead
					sub = implicitCast(ctx, block, sub, makePrimitiveType(PER_TYPE_SDWORD));
					if (sub.type == NULL) return sub;
				}
				else
				{
					sub = subAsUint;
				};
				
				// try to cast the value to the required type
				value = implicitCast(ctx, block, value, subject.type->elementType);
				if (value.type == NULL) return value;
				
				// do a bounds check
				emitInsn(block, NULL, "Per_BoundsCheck(%s, %s);", subject.cval, sub.cval);
				
				// lock the array
				emitInsn(block, NULL, "Per_ArrayLock(%s);", subject.cval);
				
				// set the value
				if (isManagedType(subject.type->elementType))
				{
					emitInsn(block, NULL, "Per_Down(((Per_Object**)%s)[%s]); ((Per_Object**)%s)[%s] = %s; Per_Up(%s);",
								subject.cval, sub.cval, subject.cval, sub.cval, value.cval, value.cval);
				}
				else if (subject.type->elementType->kind == PER_TYPE_ARRAY)
				{
					emitInsn(block, NULL, "Per_Down(((void**)%s)[%s]); ((void**)%s)[%s] = %s; Per_Up(%s);",
								subject.cval, sub.cval, subject.cval, sub.cval, value.cval, value.cval);
				}
				else
				{
					emitInsn(block, NULL, "((%s*)%s)[%s] = %s;", getEquivalentCType(subject.type->elementType), subject.cval, sub.cval, value.cval);
				};
				
				// unlock the array
				emitInsn(block, NULL, "Per_ArrayUnlock(%s);", subject.cval);
				
				return value;
			}
			else if (isManagedType(subject.type))
			{
				// get the opSet() function
				ExprValue opSet = emitGetMember(ctx, block, subject, subject.tok, "opSet");
				if (opSet.type == NULL) return opSet;
				
				ExprValue args[2];
				args[0] = sub;
				args[1] = value;
				
				return emitCall(ctx, block, subject.tok, opSet, args, 2);
			}
			else
			{
				ExprValue err;
				memset(&err, 0, sizeof(ExprValue));
				err.tok = sub.tok;
				err.errmsg = formatMsg("the subscript operator is not applicable to type `%s'", getTypeName(subject.type));
				return err;
			};
		}
		else if (postfix->call != NULL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = subject.tok;
			err.errmsg = formatMsg("a call cannot be the left side of an assignment");
			return err;
		}
		else if (postfix->inst != NULL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = postfix->inst->tok;
			err.errmsg = formatMsg("the `is' operator cannot be the left side of an assignment");
			return err;
		}
		else if (postfix->op != NULL)
		{
			ExprValue err;
			memset(&err, 0, sizeof(ExprValue));
			err.tok = postfix->op;
			err.errmsg = formatMsg("the `%s' operator cannot be the left side of an assignment", postfix->op->value);
			return err;
		}
		else
		{
			abort(); // ???
		};
	};
};

ExprValue emitAssignmentExpression(ExecContext *ctx, Block *block, AssignmentExpression *assign)
{
	if (assign->cond != NULL)
	{
		return emitConditionalExpression(ctx, block, assign->cond);
	}
	else
	{
		// evaluate the right side first
		ExprValue value = emitAssignmentExpression(ctx, block, assign->right);
		if (value.type == NULL) return value;
		
		if (assign->op->type == TOK_PER_ASSIGN)
		{
			// normal assignment: the left side is assigned a value
			return emitNormalAssignment(ctx, block, assign->left, value);
		}
		else
		{
			ExprValue right = value;
			ExprValue left = emitPostfixExpression(ctx, block, assign->left);
			if (left.type == NULL) return left;
			
			if (left.type->kind == PER_TYPE_DWORD || left.type->kind == PER_TYPE_QWORD
				|| left.type->kind == PER_TYPE_SDWORD || left.type->kind == PER_TYPE_SQWORD)
			{
				// natural integer type
				right = implicitCast(ctx, block, right, left.type);
				if (right.type == NULL) return right;
				
				const char *cop;
				switch (assign->op->type)
				{
				case TOK_PER_ASSIGN_ADD:
					cop = "+";
					break;
				case TOK_PER_ASSIGN_SUB:
					cop = "-";
					break;
				case TOK_PER_ASSIGN_MUL:
					cop = "*";
					break;
				case TOK_PER_ASSIGN_DIV:
					cop = "/";
					break;
				case TOK_PER_ASSIGN_MOD:
					cop = "%";
					break;
				case TOK_PER_ASSIGN_SHL:
					cop = "<<";
					break;
				case TOK_PER_ASSIGN_SHR:
					cop = ">>";
					break;
				case TOK_PER_ASSIGN_AND:
					cop = "&";
					break;
				case TOK_PER_ASSIGN_OR:
					cop = "|";
					break;
				case TOK_PER_ASSIGN_XOR:
					cop = "^";
					break;
				default:
					memset(&value, 0, sizeof(ExprValue));
					value.tok = assign->op;
					value.errmsg = formatMsg("the `%s' operator is not applicable to type `%s'",
												assign->op->value, getTypeName(left.type));
					return value;
				};
				
				memset(&value, 0, sizeof(ExprValue));
				value.type = left.type;
				value.tok = assign->op;
				value.cval = formatMsg("(%s %s %s)", left.cval, cop, right.cval);
			}
			else if (isFloatingType(left.type))
			{
				right = implicitCast(ctx, block, right, left.type);
				if (right.type == NULL) return right;
				
				const char *cop;
				switch (assign->op->type)
				{
				case TOK_PER_ASSIGN_ADD:
					cop = "+";
					break;
				case TOK_PER_ASSIGN_SUB:
					cop = "-";
					break;
				case TOK_PER_ASSIGN_MUL:
					cop = "*";
					break;
				case TOK_PER_ASSIGN_DIV:
					cop = "/";
					break;
				default:
					memset(&value, 0, sizeof(ExprValue));
					value.tok = assign->op;
					value.errmsg = formatMsg("the `%s' operator is not applicable to type `%s'",
												assign->op->value, getTypeName(left.type));
					return value;
				};
				
				memset(&value, 0, sizeof(ExprValue));
				value.type = left.type;
				value.tok = assign->op;
				value.cval = formatMsg("(%s %s %s)", left.cval, cop, right.cval);
			}
			else if (isManagedType(left.type))
			{
				const char *methodName;
				
				switch (assign->op->type)
				{
				case TOK_PER_ASSIGN_ADD:
					methodName = "opAdd";
					break;
				case TOK_PER_ASSIGN_SUB:
					methodName = "opSub";
					break;
				case TOK_PER_ASSIGN_MUL:
					methodName = "opMul";
					break;
				case TOK_PER_ASSIGN_DIV:
					methodName = "opDiv";
					break;
				case TOK_PER_ASSIGN_MOD:
					methodName = "opMod";
					break;
				case TOK_PER_ASSIGN_SHL:
					methodName = "opShiftLeft";
					break;
				case TOK_PER_ASSIGN_SHR:
					methodName = "opShiftRight";
					break;
				case TOK_PER_ASSIGN_AND:
					methodName = "opBitwiseAND";
					break;
				case TOK_PER_ASSIGN_OR:
					methodName = "opBitwiseOR";
					break;
				case TOK_PER_ASSIGN_XOR:
					methodName = "opBitwiseXOR";
					break;
				default:
					abort();		// ???
				};
				
				ExprValue method = emitGetMember(ctx, block, left, assign->op, methodName);
				if (method.type == NULL) return method;
				
				value = emitCall(ctx, block, assign->op, method, &right, 1);
				if (value.type == NULL) return value;
			}
			else
			{
				memset(&value, 0, sizeof(ExprValue));
				value.tok = assign->op;
				value.errmsg = formatMsg("the `%s' operator is not applicable to types `%s' and `%s'",
											assign->op->value, getTypeName(left.type), getTypeName(right.type));
				return value;
			};
			
			return emitNormalAssignment(ctx, block, assign->left, value);
		};
	};
};

ExprValue emitExpression(ExecContext *ctx, Block *block, Expression *expr)
{
	ExprValue val;
	memset(&val, 0, sizeof(ExprValue));
	
	for (; expr!=NULL; expr=expr->next)
	{
		val = emitAssignmentExpression(ctx, block, expr->assign);
		if (val.type == NULL) return val;
	};
	
	return val;
};

void emitReturnStatement(ExecContext *ctx, Block *block, ReturnStatement *rets)
{
	if (rets->expr == NULL)
	{
		if (ctx->retType->kind != PER_TYPE_VOID)
		{
			lexDiag(rets->tok, CON_ERROR, "`return' with no value in method returning non-void");
		}
		else
		{
			emitInsn(block, NULL, "goto end;");
		};
	}
	else
	{
		if (ctx->retType->kind == PER_TYPE_VOID)
		{
			lexDiag(rets->tok, CON_ERROR, "`return' with a value in method returning void");
		}
		else
		{
			ExprValue val = emitExpression(ctx, block, rets->expr);
			if (val.type != NULL)
			{
				val = implicitCast(ctx, block, val, ctx->retType);
			};
			
			if (val.type == NULL)
			{
				lexDiag(val.tok, CON_ERROR, "%s", val.errmsg);
			}
			else
			{
				emitInsn(block, NULL, "retval = %s;", val.cval);
				if (isManagedType(val.type)) emitInsn(block, NULL, "Per_Up(retval);");
				if (val.type->kind == PER_TYPE_ARRAY) emitInsn(block, NULL, "Per_ArrayUp(retval);");
				emitInsn(block, NULL, "goto end;");
			};
		};
	};
};

void emitThrowStatement(ExecContext *ctx, Block *block, ThrowStatement *throws)
{
	ExprValue value = emitExpression(ctx, block, throws->expr);
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	Type *extype = (Type*) calloc(1, sizeof(Type));
	extype->kind = PER_TYPE_MANAGED;
	extype->fullname = "std.rt.Exception";
	extype->temParamTypes = hashtabNew();
	
	if (!isSubtype(extype, value.type))
	{
		lexDiag(throws->tok, CON_ERROR, "the operand to the `throw' operator must be a subtype of `std.rt.Exception'; type `%s' was given", getTypeName(value.type));
		return;
	};
	
	emitInsn(block, NULL, "Per_Up(%s);", value.cval);
	emitInsn(block, NULL, "Per_Throw(%s);", value.cval);
};

void emitVariableDefinition(ExecContext *ctx, Block *block, VariableDefinition *vardef)
{
	int sect = PER_VSECT_PRIMITIVE;
	if (isManagedType(vardef->type)) sect = PER_VSECT_MANAGED;
	if (vardef->type->kind == PER_TYPE_ARRAY) sect = PER_VSECT_ARRAY;
	
	char *cname = makeTempVar(block->func, getEquivalentCType(vardef->type), sect);
	Token *errtok = defineVar(ctx, vardef->type, vardef->id->value, cname, vardef->id, 0);
	if (errtok != NULL)
	{
		lexDiag(vardef->id, CON_ERROR, "variable `%s' is already defined in this scope", vardef->id->value);
		lexDiag(errtok, CON_NOTE, "first defined here");
		return;
	};
	
	lexTag(vardef->id, TAG_DEFINE_VAR, NULL, vardef->id->value);
	lexTag(vardef->id, TAG_VAR_NAME, NULL, vardef->id->value);
	
	char *initVal;
	if (vardef->init == NULL)
	{
		initVal = formatMsg("((%s)0)", getEquivalentCType(vardef->type));
	}
	else
	{
		ExprValue init = emitExpression(ctx, block, vardef->init);
		if (init.type == NULL)
		{
			lexDiag(init.tok, CON_ERROR, "%s", init.errmsg);
			return;
		};
		
		init = implicitCast(ctx, block, init, vardef->type);
		if (init.type == NULL)
		{
			lexDiag(init.tok, CON_ERROR, "%s", init.errmsg);
			return;
		};
		
		initVal = init.cval;
	};
	
	if (sect == PER_VSECT_PRIMITIVE)
	{
		emitInsn(block, NULL, "%s = %s;", cname, initVal);
	}
	else if (sect == PER_VSECT_MANAGED)
	{
		emitInsn(block, NULL, "Per_Down(%s); %s = %s; Per_Up(%s);", cname, cname, initVal, cname);
	}
	else
	{
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = %s; Per_ArrayUp(%s);", cname, cname, initVal, cname);
	};
};

void emitIfStatement(ExecContext *ctx, Block *block, IfStatement *ifs)
{
	ExprValue value = emitExpression(ctx, block, ifs->ctrl);
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	value = implicitCast(ctx, block, value, makePrimitiveType(PER_TYPE_BOOL));
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	Block *mainBlock;
	emitInsn(block, &mainBlock, "if (%s)", value.cval);
	
	emitStatement(ctx, mainBlock, ifs->body);
	
	if (ifs->other != NULL)
	{
		Block *elseBlock;
		emitInsn(block, &elseBlock, "else");
		
		emitStatement(ctx, elseBlock, ifs->other);
	};
};

void emitWhileStatement(ExecContext *ctx, Block *block, WhileStatement *whiles)
{
	static int whileCounter = 1;
	int labelNumber = whileCounter++;
	
	const char *oldBreak = ctx->breakTarget;
	const char *oldContinue = ctx->continueTarget;
	
	Block *body;
	emitInsn(block, &body, "while (1)");
	emitInsn(body, NULL, "while_continue_%d:;", labelNumber);
	
	ExprValue value = emitExpression(ctx, body, whiles->ctrl);
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	value = implicitCast(ctx, body, value, makePrimitiveType(PER_TYPE_BOOL));
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	emitInsn(body, NULL, "if (!%s) break;", value.cval);
	
	char *newBreak = (char*) malloc(64);
	sprintf(newBreak, "while_break_%d", labelNumber);
	char *newContinue = (char*) malloc(64);
	sprintf(newContinue, "while_continue_%d", labelNumber);
	
	ctx->breakTarget = newBreak;
	ctx->continueTarget = newContinue;

	emitStatement(ctx, body, whiles->body);
	emitInsn(block, NULL, "while_break_%d:;", labelNumber);
	
	free(newBreak);
	free(newContinue);
	
	ctx->breakTarget = oldBreak;
	ctx->continueTarget = oldContinue;
};

void emitDoWhileStatement(ExecContext *ctx, Block *block, DoWhileStatement *dos)
{
	static int dowCounter = 1;
	int labelNumber = dowCounter++;
	
	const char *oldBreak = ctx->breakTarget;
	const char *oldContinue = ctx->continueTarget;
	
	Block *body;
	emitInsn(block, &body, "while (1)");

	char *newBreak = (char*) malloc(64);
	sprintf(newBreak, "dow_break_%d", labelNumber);
	char *newContinue = (char*) malloc(64);
	sprintf(newContinue, "dow_continue_%d", labelNumber);
	
	ctx->breakTarget = newBreak;
	ctx->continueTarget = newContinue;

	emitStatement(ctx, body, dos->body);

	ctx->breakTarget = oldBreak;
	ctx->continueTarget = oldContinue;

	emitInsn(body, NULL, "dow_continue_%d:;", labelNumber);
	
	ExprValue value = emitExpression(ctx, body, dos->ctrl);
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	value = implicitCast(ctx, body, value, makePrimitiveType(PER_TYPE_BOOL));
	if (value.type == NULL)
	{
		lexDiag(value.tok, CON_ERROR, "%s", value.errmsg);
		return;
	};
	
	emitInsn(body, NULL, "if (!%s) break;", value.cval);
	emitInsn(block, NULL, "dow_break_%d:;", labelNumber);
};

void emitForStatement(ExecContext *ctx, Block *block, ForStatement *fors)
{
	static int forCounter = 1;
	int labelNumber = forCounter++;
	
	const char *oldBreak = ctx->breakTarget;
	const char *oldContinue = ctx->continueTarget;
	
	// first evaluate the initializer
	if (fors->init != NULL)
	{
		ExprValue initval = emitExpression(ctx, block, fors->init);
		if (initval.type == NULL)
		{
			lexDiag(initval.tok, CON_ERROR, "%s", initval.errmsg);
		};
	};
	
	Block *body;
	emitInsn(block, &body, "while (1)");
	
	// inside the loop, first check the condition and break out if not satisfied
	if (fors->cond != NULL)
	{
		emitInsn(body, NULL, "for_continue_%d:;", labelNumber);
		
		ExprValue condval = emitExpression(ctx, body, fors->cond);
		if (condval.type != NULL)
		{
			condval = implicitCast(ctx, body, condval, makePrimitiveType(PER_TYPE_BOOL));
		};
		
		if (condval.type == NULL)
		{
			lexDiag(condval.tok, CON_ERROR, "%s", condval.errmsg);
		}
		else
		{
			emitInsn(body, NULL, "if (!%s) break;", condval.cval);
		};
	};

	// now the body
	char *newBreak = (char*) malloc(64);
	sprintf(newBreak, "for_break_%d", labelNumber);
	char *newContinue = (char*) malloc(64);
	sprintf(newContinue, "for_continue_%d", labelNumber);
	
	ctx->breakTarget = newBreak;
	ctx->continueTarget = newContinue;
	
	emitStatement(ctx, body, fors->body);
	
	ctx->breakTarget = oldBreak;
	ctx->continueTarget = oldContinue;
	
	// at the end, evaluate the iteration expression
	if (fors->iter != NULL)
	{
		ExprValue iterval = emitExpression(ctx, body, fors->iter);
		if (iterval.type == NULL)
		{
			lexDiag(iterval.tok, CON_ERROR, "%s", iterval.errmsg);
		};
	};
	
	emitInsn(block, NULL, "for_break_%d:;", labelNumber);
};

void emitForEachStatement(ExecContext *ctx, Block *block, ForEachStatement *foreach)
{
	static int foreachTempCounter = 0;

	lexTag(foreach->id, TAG_DEFINE_VAR, NULL, foreach->id->value);
	lexTag(foreach->id, TAG_VAR_NAME, NULL, foreach->id->value);

	ExprValue subject = emitExpression(ctx, block, foreach->expr);
	if (subject.type == NULL)
	{
		lexDiag(subject.tok, CON_ERROR, "%s", subject.errmsg);
		return;
	}
	else if (subject.type->kind == PER_TYPE_ARRAY)
	{
		if (!isSameType(subject.type->elementType, foreach->type))
		{
			lexDiag(subject.tok, CON_ERROR, "array used in `foreach' loop has element type `%s' but the declared type is `%s'",
					getTypeName(subject.type->elementType), getTypeName(foreach->type));
		};
		
		Block *body;
		int tempNo = foreachTempCounter++;
		
		emitInsn(block, NULL, "Per_int foreach_%d = 0;", tempNo);
		emitInsn(block, NULL ,"Per_int foreach_len_%d = Per_ArrayLength(%s);", tempNo, subject.cval);
		emitInsn(block, &body, "while (1)");
		
		char *newBreak = (char*) malloc(64);
		sprintf(newBreak, "foreach_break_%d", tempNo);
		char *newContinue = (char*) malloc(64);
		sprintf(newContinue, "foreach_continue_%d", tempNo);
		
		ExecContext *subctx = newChildContext(ctx);
		subctx->breakTarget = newBreak;
		subctx->continueTarget = newContinue;
		
		int typesect;
		if (foreach->type->kind == PER_TYPE_ARRAY) typesect = PER_VSECT_ARRAY;
		else if (foreach->type->kind == PER_TYPE_MANAGED) typesect = PER_VSECT_MANAGED;
		else typesect = PER_VSECT_PRIMITIVE;
		
		char *loopvar = makeTempVar(block->func, getEquivalentCType(foreach->type), typesect);
		defineVar(subctx, foreach->type, foreach->id->value, loopvar, foreach->id, 1);
		
		emitInsn(body, NULL, "foreach_continue_%d:;", tempNo);
		emitInsn(body, NULL, "if (foreach_%d >= foreach_len_%d) break;", tempNo, tempNo);
		
		emitInsn(body, NULL, "Per_ArrayLock(%s);", subject.cval);
		if (typesect == PER_VSECT_ARRAY)
		{
			emitInsn(body, NULL, "Per_ArrayDown(%s); %s = ((void**)%s)[foreach_%d]; Per_ArrayUp(%s);", loopvar, loopvar, subject.cval, tempNo, loopvar);
		}
		else if (typesect == PER_VSECT_MANAGED)
		{
			emitInsn(body, NULL, "Per_Down(%s); %s = ((Per_Object**)%s)[foreach_%d]; Per_Up(%s);", loopvar, loopvar, subject.cval, tempNo, loopvar);
		}
		else
		{
			emitInsn(body, NULL, "%s = ((%s*)%s)[foreach_%d]; Per_Up(%s);", loopvar, getEquivalentCType(foreach->type), loopvar,
					tempNo, loopvar);
		};
		emitInsn(body, NULL, "Per_ArrayUnlock(%s);", subject.cval);
		emitInsn(body, NULL, "foreach_%d++;", tempNo);
		
		emitStatement(subctx, body, foreach->body);
		
		emitInsn(block, NULL, "foreach_break_%d:;", tempNo);
	}
	else if (subject.type->kind == PER_TYPE_MANAGED)
	{
		Block *body;
		int tempNo = foreachTempCounter++;
		
		Type *boolArrayType = makeArrayType(makePrimitiveType(PER_TYPE_BOOL));
		char *endRefVar = makeTempVar(block->func, "void*", PER_VSECT_ARRAY);
		ExprValue endRefValue;
		memset(&endRefValue, 0, sizeof(ExprValue));
		endRefValue.type = boolArrayType;
		endRefValue.tok = foreach->tok;
		endRefValue.cval = endRefVar;
		
		emitInsn(block, NULL, "Per_ArrayDown(%s); %s = Per_NewArray(1, 1, PER_TYPESECT_PRIMITIVE);", endRefVar, endRefVar);
		emitInsn(block, &body, "while (1)");
		emitInsn(body, NULL, "foreach_continue_%d:;", tempNo);
		
		char *newBreak = (char*) malloc(64);
		sprintf(newBreak, "foreach_break_%d", tempNo);
		char *newContinue = (char*) malloc(64);
		sprintf(newContinue, "foreach_continue_%d", tempNo);
		
		ExecContext *subctx = newChildContext(ctx);
		subctx->breakTarget = newBreak;
		subctx->continueTarget = newContinue;
		
		ExprValue opYield = emitGetMember(subctx, body, subject, foreach->tok, "opYield");
		if (opYield.type == NULL)
		{
			lexDiag(opYield.tok, CON_ERROR, "%s", opYield.errmsg);
			return;
		};
		
		ExprValue loopIntermediate = emitCall(subctx, body, foreach->tok, opYield, &endRefValue, 1);
		if (loopIntermediate.type == NULL)
		{
			lexDiag(loopIntermediate.tok, CON_ERROR, "%s", loopIntermediate.errmsg);
			return;
		};
		
		ExprValue loopvar = implicitCast(subctx, body, loopIntermediate, foreach->type);
		if (loopvar.type == NULL)
		{
			lexDiag(loopvar.tok, CON_ERROR, "%s", loopvar.errmsg);
			return;
		};
		
		defineVar(subctx, foreach->type, foreach->id->value, loopvar.cval, foreach->id, 1);
		emitInsn(body, NULL, "if (*((unsigned char*)%s)) break;", endRefVar);
		
		emitStatement(subctx, body, foreach->body);
		
		emitInsn(block, NULL, "foreach_break_%d:;", tempNo);
	}
	else
	{
		lexDiag(subject.tok, CON_ERROR, "type `%s' cannot be sequenced", getTypeName(subject.type));
	}
};

void emitTryCatchStatement(ExecContext *ctx, Block *block, TryCatchStatement *trys)
{
	block->func->volatileVars = 1;
	
	// create the exception context
	int contextNo = block->func->nextTemp++;
	emitInsn(block, NULL, "volatile Per_ExceptionContext xctx%d;", contextNo);
	emitInsn(block, NULL, "Per_Enter(&xctx%d);", contextNo);
	emitInsn(block, NULL, "setjmp(xctx%d.xcJmpBuf);", contextNo);
	
	// make an array of caught exceptions
	char *catchSpec = strdup("{");
	
	CatchList *catch;
	for (catch=trys->catchList; catch!=NULL; catch=catch->next)
	{
		Type *extype = makeSimpleManagedType("std.rt.Exception");
		if (!isSubtype(extype, catch->type))
		{
			lexDiag(catch->tok, CON_ERROR, "`%s' is not a subtype of std.rt.Exception as ought to be", getTypeName(catch->type));
			return;
		};
		
		ClassTemplate *tem = findClassByName(NULL, catch->type->fullname);
		char *newCatchSpec = (char*) malloc(strlen(catchSpec) + strlen(tem->mangledName) + 16);
		sprintf(newCatchSpec, "%s&%s, ", catchSpec, tem->mangledName);
		free(catchSpec);
		catchSpec = newCatchSpec;
	};
	
	emitInsn(block, NULL, "static Per_ClassDesc* catches%d[] = %sNULL};", contextNo, catchSpec);
	free(catchSpec);
	emitInsn(block, NULL, "xctx%d.xcCatches = catches%d;", contextNo, contextNo);
	
	// emit the `try' part first
	Block *tryBody;
	emitInsn(block, &tryBody, "if (xctx%d.xcWhich == -1)", contextNo);
	emitStatement(ctx, tryBody, trys->body);
	emitInsn(tryBody, NULL, "Per_Leave(&xctx%d);", contextNo);
	
	// now all the catch blocks
	int catchCounter = 0;
	for (catch=trys->catchList; catch!=NULL; catch=catch->next)
	{
		int catchNo = catchCounter++;
		
		// allocate the variable which will hold the exception
		char *exvar = makeTempVar(block->func, "Per_Object*", PER_VSECT_MANAGED);
		
		// make a new ExecContext to which we will add the variable
		ExecContext *catchCtx = newChildContext(ctx);
		defineVar(catchCtx, catch->type, catch->id->value, exvar, catch->id, 1);
		
		// emit the body
		Block *catchBody;
		emitInsn(block, &catchBody, "else if (xctx%d.xcWhich == %d)", contextNo, catchNo);
		emitInsn(catchBody, NULL, "Per_Down(%s); %s = xctx%d.xcThrown;", exvar, exvar, contextNo);
		emitStatement(catchCtx, catchBody, catch->body);
	};
};

void emitStatement(ExecContext *ctx, Block *block, Statement *st)
{
	if (st->comp != NULL)
	{
		emitCompoundStatement(ctx, block, st->comp);
	}
	else if (st->null != NULL)
	{
		// NOP
	}
	else if (st->vardef != NULL)
	{
		emitVariableDefinition(ctx, block, st->vardef);
	}
	else if (st->ifs != NULL)
	{
		emitIfStatement(ctx, block, st->ifs);
	}
	else if (st->whiles != NULL)
	{
		emitWhileStatement(ctx, block, st->whiles);
	}
	else if (st->fors != NULL)
	{
		emitForStatement(ctx, block, st->fors);
	}
	else if (st->foreach != NULL)
	{
		emitForEachStatement(ctx, block, st->foreach);
	}
	else if (st->breaks != NULL)
	{
		if (ctx->breakTarget == NULL)
		{
			lexDiag(st->breaks->tok, CON_ERROR, "nothing to break out of in this context");
		}
		else
		{
			emitInsn(block, NULL, "goto %s;", ctx->breakTarget);
		};
	}
	else if (st->conts != NULL)
	{
		if (ctx->continueTarget == NULL)
		{
			lexDiag(st->conts->tok, CON_ERROR, "no loop to continue in this context");
		}
		else
		{
			emitInsn(block, NULL, "goto %s;", ctx->continueTarget);
		};
	}
	else if (st->rets != NULL)
	{
		emitReturnStatement(ctx, block, st->rets);
	}
	else if (st->trys != NULL)
	{
		emitTryCatchStatement(ctx, block, st->trys);
	}
	else if (st->dos != NULL)
	{
		emitDoWhileStatement(ctx, block, st->dos);
	}
	else if (st->xs != NULL)
	{
		ExprValue val = emitExpression(ctx, block, st->xs->expr);
		if (val.type == NULL)
		{
			lexDiag(val.tok, CON_ERROR, "%s", val.errmsg);
		};
	}
	else if (st->throws != NULL)
	{
		emitThrowStatement(ctx, block, st->throws);
	}
	else
	{
		abort();	// ???
	};
};

void emitCompoundStatement(ExecContext *ctx, Block *block, CompoundStatement *stmt)
{
	ExecContext *child = newChildContext(ctx);
	
	StatementList *scan;
	for (scan=stmt->head; scan!=NULL; scan=scan->next)
	{
		emitStatement(child, block, scan->st);
	};
};

void emitArgumentSpecifications(FILE *fp, ArgumentSpecificationList *args)
{
	while (args != NULL)
	{
		fprintf(fp, "%s arg_%s", getEquivalentCType(args->type), args->name);
		if (args->next != NULL) fprintf(fp, ", ");
		args = args->next;
	};
};

ExecContext* newRootContext(int hasThis, Type *retType)
{
	ExecContext *ctx = (ExecContext*) calloc(1, sizeof(ExecContext));
	ctx->hasThis = hasThis;
	ctx->vars = hashtabNew();
	ctx->retType = retType;
	ctx->breakTarget = NULL;
	ctx->continueTarget = NULL;
	return ctx;
};

ExecContext* newChildContext(ExecContext *parent)
{
	ExecContext *ctx = (ExecContext*) calloc(1, sizeof(ExecContext));
	ctx->parent = parent;
	ctx->hasThis = parent->hasThis;
	ctx->vars = hashtabNew();
	ctx->retType = parent->retType;
	ctx->breakTarget = parent->breakTarget;
	ctx->continueTarget = parent->continueTarget;
	return ctx;
};

Token* defineVar(ExecContext *ctx, Type *type, const char *name, const char *cname, Token *tok, int isFinal)
{
	if (hashtabHasKey(ctx->vars, name))
	{
		PerunVar *var = (PerunVar*) hashtabGet(ctx->vars, name);
		return var->tok;
	};
	
	PerunVar *var = (PerunVar*) calloc(1, sizeof(PerunVar));
	var->type = type;
	var->cname = strdup(cname);
	hashtabSet(ctx->vars, name, var);
	var->tok = tok;
	var->isFinal = isFinal;
	return NULL;
};

int isExactSignature(Type *type, ArgumentSpecificationList *argspecs, ExprValue *argvals, int numArgs)
{
	int i = 0;
	
	ArgumentSpecificationList *argspec;
	for (argspec=argspecs; argspec!=NULL; argspec=argspec->next)
	{
		int argIndex = i++;
		
		if (!isSameType(argvals[argIndex].type, expandType(argspec->type, type->temParamTypes)))
		{
			return 0;
		};
	};
	
	return (i == numArgs);
};

int isSimilarSignature(Type *type, ArgumentSpecificationList *argspecs, ExprValue *argvals, int numArgs)
{
	int i = 0;
	
	ArgumentSpecificationList *argspec;
	for (argspec=argspecs; argspec!=NULL; argspec=argspec->next)
	{
		int argIndex = i++;
		
		if (!isImplicitlyCastable(argvals[argIndex], expandType(argspec->type, type->temParamTypes)))
		{
			return 0;
		};
	};
	
	return (i == numArgs);
};

char* getValueTypeNames(ExprValue *vals, int numVals)
{
	char *result = strdup("");
	
	int i;
	for (i=0; i<numVals; i++)
	{
		char *typename = getTypeName(vals[i].type);
		char *newstr = (char*) malloc(strlen(result) + strlen(typename) + 16);
		sprintf(newstr, "%s%s", result, typename);
		
		if (i != numVals-1)
		{
			strcat(newstr, ", ");
		};
		
		free(result);
		result = newstr;
	};
	
	return result;
};

ClassConstructor* findConstructor(Token *tok, Type *type, ExprValue *args, int numArgs)
{
	ClassTemplate *tem = findClassByName(NULL, type->fullname);
	
	// first try to find an exact match
	ClassConstructor *ctor;
	for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
	{
		if (isExactSignature(type, ctor->args, args, numArgs))
		{
			return ctor;
		};
	};
	
	// check if there is exactly 1 similar signature
	int numMatches = 0;
	ClassConstructor *lastMatch;
	
	for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
	{
		if (isSimilarSignature(type, ctor->args, args, numArgs))
		{
			lastMatch = ctor;
			numMatches++;
		};
	};
	
	if (numMatches == 0)
	{
		// no match for this constructor
		lexDiag(tok, CON_ERROR, "class `%s' has no constructor which can accept types (%s)", type->fullname, getValueTypeNames(args, numArgs));
		return NULL;
	}
	else if (numMatches == 1)
	{
		// exactly one match, so it's good
		return lastMatch;
	}
	else
	{
		// multiple matches
		lexDiag(tok, CON_ERROR, "class `%s' has multiple constructors which could accept types (%s); use explicit casts to select a specific constructor", type->fullname, getValueTypeNames(args, numArgs));
		
		for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
		{
			if (isSimilarSignature(type, ctor->args, args, numArgs))
			{
				lexDiag(ctor->tok, CON_NOTE, "one candidate takes (%s)", getArgumentListFormatted(ctor->args));
			};
		};
		
		return NULL;
	};
};

static FILE *emitFP;
static ClassTemplate *emitInTemplate;

FILE* emitGetOutputFile()
{
	return emitFP;
};

ClassTemplate* emitGetInTemplate()
{
	return emitInTemplate;
};

const char* getFieldMetaTypeKindName(Type *type)
{
	switch (type->kind)
	{
	case PER_TYPE_BOOL:
		return "PER_TK_BOOL";
	case PER_TYPE_FLOAT:
		return "PER_TK_F32";
	case PER_TYPE_DOUBLE:
		return "PER_TK_F64";
	case PER_TYPE_BYTE:
		return "PER_TK_U8";
	case PER_TYPE_SBYTE:
		return "PER_TK_I8";
	case PER_TYPE_WORD:
		return "PER_TK_U16";
	case PER_TYPE_SWORD:
		return "PER_TK_I16";
	case PER_TYPE_DWORD:
		return "PER_TK_U32";
	case PER_TYPE_SDWORD:
		return "PER_TK_I32";
	case PER_TYPE_QWORD:
		return "PER_TK_U64";
	case PER_TYPE_SQWORD:
		return "PER_TK_I64";
	case PER_TYPE_PTR:
		return "PER_TK_UP";
	case PER_TYPE_SPTR:
		return "PER_TK_IP";
	case PER_TYPE_MANAGED:
	case PER_TYPE_TEMPLATE_PARAM:
		return "PER_TK_OBJECT";
	default:
		return "PER_TK_VOID";
	};
};

void emitPerun(CompilationUnit *unit, FILE *fp)
{
	fprintf(fp, "/* C code emitted by the Madd Perun Compiler */\n");
	fprintf(fp, "#include <perun_abi.h>\n");
	fprintf(fp, "#include <stddef.h>\n\n");
	
	emitFP = fp;
	
	// output the declarations of all classes
	const char *key;
	HASHTAB_FOREACH(classTable, ClassTemplate*, key, tem)
	{
		fprintf(fp, "/* declare class %s */\n", tem->fullname);
		
		if ((tem->flags & PER_Q_STATIC) == 0)
		{
			char *mangledDescName = mangleName("Per_CD?", key);
			if (tem->protLevel == PER_PROT_PRIVATE)
			{
				fprintf(fp, "static Per_ClassDesc %s;\n", mangledDescName);
			}
			else
			{
				fprintf(fp, "extern Per_ClassDesc %s;\n", mangledDescName);
			};
			
			free(mangledDescName);
		};
		
		if (tem->protLevel == PER_PROT_PRIVATE)
		{
			fprintf(fp, "static Per_Mutex %s;\n", tem->staticLockName);
			fprintf(fp, "static void %s();\n", tem->staticInitName);
		}
		else
		{
			fprintf(fp, "extern Per_Mutex %s;\n", tem->staticLockName);
			fprintf(fp, "void %s();\n", tem->staticInitName);
		};
		
		ClassField *field;
		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if (field->flags & PER_Q_STATIC)
			{
				if (field->prot == PER_PROT_PRIVATE)
				{
					fprintf(fp, "static %s %s;\n", getEquivalentCType(field->type), field->mangled);
				}
				else
				{
					fprintf(fp, "extern %s %s;\n", getEquivalentCType(field->type), field->mangled);
				};
			}
			else
			{
				if (field->prot == PER_PROT_PRIVATE)
				{
					fprintf(fp, "static size_t %s;\n", field->mangled);
				}
				else
				{
					fprintf(fp, "extern size_t %s;\n", field->mangled);
				};
			};
		};
		
		ClassMethod *method;
		for (method=tem->methods; method!=NULL; method=method->next)
		{
			if (method->flags & PER_Q_STATIC)
			{
				fprintf(fp, "%s %s(", getEquivalentCType(method->retType), method->mangled);
				emitArgumentSpecifications(fp, method->args);
				fprintf(fp, ");\n");
			}
			else
			{
				const char *kind = "extern";
				if (method->prot == PER_PROT_PRIVATE)
				{
					kind = "static";
				};
				
				fprintf(fp, "%s Per_VirtFunc %s;\n", kind, method->mangled);
				fprintf(fp, "typedef %s (*FP_%s)(Per_Object *thisptr", getEquivalentCType(method->retType), method->mangled);
				if (method->args != NULL) fprintf(fp, ", ");
				emitArgumentSpecifications(fp, method->args);
				fprintf(fp, ");\n");
			};
		};
		
		ClassConstructor *ctor;
		for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
		{
			fprintf(fp, "void %s(Per_Object *thisval", ctor->mangled);
			
			ArgumentSpecificationList *args = ctor->args;
			while (args != NULL)
			{
				fprintf(fp, ", %s arg_%s", getEquivalentCType(args->type), args->name);
				args = args->next;
			};
			
			fprintf(fp, ");\n");
		};
		
		// print a newline before the next class!
		fprintf(fp, "\n");
	};
	
	// now output definitions from the compilation unit
	int i;
	for (i=0; i<unit->numClasses; i++)
	{
		tem = unit->classList[i];
		
		fprintf(fp, "/* define class %s */\n", tem->fullname);
		emitInTemplate = tem;
		
		// get the "mangled name part" of the fullname
		char *mangledFullName = mangleName("?", tem->fullname);
		
		// output static field definitions
		// we don't need to do that for private ones, because the declaration would have been
		// emitted above, and we don't need it a second time
		ClassField *field;
		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if (field->flags & PER_Q_STATIC)
			{
				if (field->prot != PER_PROT_PRIVATE)
				{
					fprintf(fp, "%s %s;\n", getEquivalentCType(field->type), field->mangled);
				};
			};
		};

		// output the component definition
		fprintf(fp, "\ntypedef struct\n{\n");
		fprintf(fp, "\tPer_Component hdr;\n");
		
		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if ((field->flags & PER_Q_STATIC) == 0)
			{
				fprintf(fp, "\t%s fld_%s;\n", getEquivalentCType(field->type), field->name);
			};
		};

		// output virtual method slots
		ClassMethod *method;
		for (method=tem->methods; method!=NULL; method=method->next)
		{
			if ((method->flags & PER_Q_STATIC) == 0 && !method->overrides)
			{
				fprintf(fp, "\t%s (*slot_%s)(Per_Object *thisval", getEquivalentCType(method->retType), method->mangled);
				if (method->args != NULL) fprintf(fp, ", ");
				emitArgumentSpecifications(fp, method->args);
				fprintf(fp, ");\n");
			};
		};
		fprintf(fp, "} Per_Component_%s;\n\n", mangledFullName);
		
		// output field offsets
		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if ((field->flags & PER_Q_STATIC) == 0)
			{
				if (field->prot == PER_PROT_PRIVATE) fprintf(fp, "static ");
				fprintf(fp, "size_t %s = offsetof(Per_Component_%s, fld_%s);\n", field->mangled, mangledFullName, field->name);
			};
		};
		
		// output virtual function definitions
		for (method=tem->methods; method!=NULL; method=method->next)
		{
			if ((method->flags & PER_Q_STATIC) == 0 && !method->overrides)
			{
				if (method->prot == PER_PROT_PRIVATE) fprintf(fp, "static ");
				fprintf(fp, "Per_VirtFunc %s = {&%s, offsetof(Per_Component_%s, slot_%s)};\n",
					method->mangled, tem->mangledName, mangledFullName, method->mangled);
			};
		};
		fprintf(fp, "\n");
		
		// output the static initializer
		EmitFunc *staticInit = newEmitFunc("void");
		emitInsn(staticInit->code, NULL, "static int siDone; if (__sync_lock_test_and_set(&siDone, 1) != 0) goto end;");
		emitInsn(staticInit->code, NULL, "Per_PreInit();");
		
		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if (field->flags & PER_Q_STATIC)
			{
				if (field->initExpr != NULL)
				{
					ExprValue val = emitExpression(NULL, staticInit->code, field->initExpr);
					if (val.type != NULL)
					{
						val = implicitCast(NULL, staticInit->code, val, field->type);
					};
					
					if (val.type == NULL)
					{
						lexDiag(val.tok, CON_ERROR, val.errmsg);
					}
					else
					{
						emitInsn(staticInit->code, NULL, "%s = %s;", field->mangled, val.cval);
						
						if (isManagedType(field->type))
						{
							emitInsn(staticInit->code, NULL, "Per_Up(%s);", field->mangled);
						}
						else if (field->type->kind == PER_TYPE_ARRAY)
						{
							emitInsn(staticInit->code, NULL, "Per_ArrayUp(%s);", field->mangled);
						};
					};
				};
			};
		};
		
		fprintf(fp, "PER_STATIC_INIT Per_SI%s()\n", mangledFullName);
		outputFunc(fp, staticInit);
		
		// now the initializer
		if ((tem->flags & PER_Q_STATIC) == 0)
		{
			EmitFunc *initFunc = newEmitFunc("void");
			emitInsn(initFunc->code, NULL, "Per_Component_%s *thiscomp = (Per_Component_%s*) Per_GetRequiredComponent(thisval, &%s);",
					mangledFullName, mangledFullName, tem->mangledName);
			
			for (method=tem->methods; method!=NULL; method=method->next)
			{
				if ((method->flags & PER_Q_STATIC) == 0 && (method->flags & PER_Q_ABSTRACT) == 0)
				{
					emitInsn(initFunc->code, NULL, "Per_SetDynamicBinding(thisval, &%s, %s);\n", method->mangled, method->implementor);
				};
			};
			
			for (field=tem->fields; field!=NULL; field=field->next)
			{
				if ((field->flags & PER_Q_STATIC) == 0)
				{
					if (field->initExpr != NULL)
					{
						ExprValue val = emitExpression(NULL, initFunc->code, field->initExpr);
						if (val.type != NULL)
						{
							val = implicitCast(NULL, initFunc->code, val, field->type);
						};
						
						if (val.type == NULL)
						{
							lexDiag(val.tok, CON_ERROR, val.errmsg);
						}
						else
						{
							emitInsn(initFunc->code, NULL, "thiscomp->fld_%s = %s;", field->name, val.cval);
							
							if (isManagedType(field->type))
							{
								emitInsn(initFunc->code, NULL, "Per_Up(thiscomp->fld_%s);", field->name);
							}
							else if (field->type->kind == PER_TYPE_ARRAY)
							{
								emitInsn(initFunc->code, NULL, "Per_ArrayUp(thiscomp->fld_%s);", field->name);
							};
						};
					};
				};
			};
			
			fprintf(fp, "static void Per_CI%s(Per_Object *thisval)\n", mangledFullName);
			outputFunc(fp, initFunc);
		};
		
		// now the static finalizer
		fprintf(fp, "PER_STATIC_FINI Per_SD%s()\n", mangledFullName);
		fprintf(fp, "{\n");

		for (field=tem->fields; field!=NULL; field=field->next)
		{
			if (field->flags & PER_Q_STATIC)
			{
				if (field->type->kind == PER_TYPE_MANAGED || field->type->kind == PER_TYPE_TEMPLATE_PARAM)
				{
					fprintf(fp, "\tPer_Down(%s);\n", field->mangled);
				}
				else if (field->type->kind == PER_TYPE_ARRAY)
				{
					fprintf(fp, "\tPer_ArrayDown(%s);\n", field->mangled);
				};
			};
		};
		
		fprintf(fp, "};\n\n");

		// now the finalizer
		if ((tem->flags & PER_Q_STATIC) == 0)
		{
			EmitFunc *finiImpl = newEmitFunc("void");
			emitInsn(finiImpl->code, NULL, "Per_Component_%s *thiscomp = (Per_Component_%s*) comp;", mangledFullName, mangledFullName);
			
			if (tem->dtor != NULL)
			{
				ExecContext *ctx = newRootContext(0, makePrimitiveType(PER_TYPE_VOID));

				for (field=tem->fields; field!=NULL; field=field->next)
				{
					if ((field->flags & PER_Q_STATIC) == 0)
					{
						defineVar(ctx, field->type, field->name, formatMsg("thiscomp->fld_%s", field->name), field->tok, 0);
					};
				};
				
				emitCompoundStatement(ctx, finiImpl->code, tem->dtor);
			};

			for (field=tem->fields; field!=NULL; field=field->next)
			{
				if ((field->flags & PER_Q_STATIC) == 0)
				{
					if (field->type->kind == PER_TYPE_MANAGED || field->type->kind == PER_TYPE_TEMPLATE_PARAM)
					{
						emitInsn(finiImpl->code, NULL, "Per_Down(thiscomp->fld_%s);", field->name);
					}
					else if (field->type->kind == PER_TYPE_ARRAY)
					{
						emitInsn(finiImpl->code, NULL, "Per_ArrayDown(thiscomp->fld_%s);", field->name);
					};
				};
			};
			
			fprintf(fp, "static void Per_CF%s(Per_Component *comp)\n", mangledFullName);
			outputFunc(fp, finiImpl);
		};
		
		// emit all defined constructors
		if ((tem->flags & PER_Q_STATIC) == 0)
		{
			ClassConstructor *ctor;
			for (ctor=tem->ctors; ctor!=NULL; ctor=ctor->next)
			{
				if (ctor->body != NULL)
				{
					ExecContext *rootctx = newRootContext(1, makePrimitiveType(PER_TYPE_VOID));
					
					int argcnt = 0;
					ArgumentSpecificationList *arg;
					for (arg=ctor->args; arg!=NULL; arg=arg->next)
					{
						int argno = argcnt++;
						
						char cname[64];
						sprintf(cname, "arg%d", argno);
						
						Token *errtok = defineVar(rootctx, arg->type, arg->name, cname, arg->tok, 1);
						if (errtok != NULL)
						{
							lexDiag(arg->tok, CON_ERROR, "argument `%s' already defined", arg->name);
							lexDiag(errtok, CON_NOTE, "first defined here");
						};
					};

					EmitFunc *ctorImpl = newEmitFunc("void");
					
					// return immediately if already constructed
					emitInsn(ctorImpl->code, NULL, "if (Per_GetRequiredComponent(thisptr, &%s)->compFlags & PER_COMP_CTED) goto end;", tem->mangledName);
					
					// now go through the list of parents and invoke constructors
					int j;
					for (j=0; j<tem->numParents; j++)
					{
						Type *parent = tem->parents[j];
						
						ClassSuperCall *call;
						for (call=ctor->superCalls; call!=NULL; call=call->next)
						{
							if (strcmp(call->parent->fullname, parent->fullname) == 0)
							{
								break;
							};
						};
						
						if (call == NULL || call->args == NULL)
						{
							// invoke the default constructor
							char *superCtorName = mangleName("Per_CC?", parent->fullname);
							emitInsn(ctorImpl->code, NULL, "%s(thisptr);", superCtorName);
							free(superCtorName);
						}
						else
						{
							// evaluate all the arguments
							int numArgs = 0;
							
							ArgumentExpressionList *arg;
							for (arg=call->args; arg!=NULL; arg=arg->next)
							{
								numArgs++;
							};
							
							int failed = 0;
							int k = 0;
							ExprValue argvals[numArgs];
							for (arg=call->args; arg!=NULL; arg=arg->next)
							{
								ExprValue val = emitAssignmentExpression(rootctx, ctorImpl->code, arg->assign);
								if (val.type == NULL)
								{
									failed = 1;
									lexDiag(val.tok, CON_ERROR, val.errmsg);
									break;
								};
								
								argvals[k++] = val;
							};
							
							if (failed) continue;
							
							// find a matching constructor
							ClassConstructor *match = findConstructor(call->tok, parent, argvals, numArgs);
							if (match != NULL)
							{
								char *callInsn = formatMsg("%s(thisptr", match->mangled);
								
								for (k=0; k<numArgs; k++)
								{
									char *newInsn = formatMsg("%s, %s", callInsn, argvals[k].cval);
									free(callInsn);
									callInsn = newInsn;
								};
								
								emitInsn(ctorImpl->code, NULL, "%s);", callInsn);
								free(callInsn);
							};
						};
					};
					
					emitCompoundStatement(rootctx, ctorImpl->code, ctor->body);
					
					// output the function
					fprintf(fp, "void %s(Per_Object *thisptr", ctor->mangled);
					
					argcnt = 0;
					for (arg=ctor->args; arg!=NULL; arg=arg->next)
					{
						int argno = argcnt++;
						fprintf(fp, ", %s arg%d", getEquivalentCType(arg->type), argno);
					};
					
					fprintf(fp, ")\n");
					outputFunc(fp, ctorImpl);
				};
			};
		};
		
		// emit all functions
		for (method=tem->methods; method!=NULL; method=method->next)
		{
			if (method->body != NULL)
			{
				ExecContext *rootctx = newRootContext(0, method->retType);
				
				int argcnt = 0;
				ArgumentSpecificationList *arg;
				for (arg=method->args; arg!=NULL; arg=arg->next)
				{
					int argno = argcnt++;
					if (strcmp(arg->name, "this") == 0)
					{
						rootctx->hasThis = 1;
					}
					else
					{
						char cname[64];
						sprintf(cname, "arg%d", argno);
						
						Token *errtok = defineVar(rootctx, arg->type, arg->name, cname, arg->tok, 1);
						if (errtok != NULL)
						{
							lexDiag(arg->tok, CON_ERROR, "argument `%s' already defined", arg->name);
							lexDiag(errtok, CON_NOTE, "first defined here");
						};
					};
				};
				
				EmitFunc *impl = newEmitFunc(getEquivalentCType(method->retType));
				
				emitCompoundStatement(rootctx, impl->code, method->body);
				
				fprintf(fp, "%s %s(", getEquivalentCType(method->retType), method->mangled);
				argcnt = 0;
				for (arg=method->args; arg!=NULL; arg=arg->next)
				{
					int argno = argcnt++;
					if (strcmp(arg->name, "this") == 0)
					{
						fprintf(fp, "%s thisptr", getEquivalentCType(arg->type));
					}
					else
					{
						fprintf(fp, "%s arg%d", getEquivalentCType(arg->type), argno);
					};
					
					if (arg->next != NULL) fprintf(fp, ", ");
				};
				fprintf(fp, ")\n");
				outputFunc(fp, impl);
			};
		};
		
		// emit the class definition itself
		if ((tem->flags & PER_Q_STATIC) == 0)
		{
			fprintf(fp, "static Per_ClassDesc* parents%s[] = {", mangledFullName);
			int j;
			for (j=0; j<tem->numParents; j++)
			{
				char *parentName = mangleName("Per_CD?", tem->parents[j]->fullname);
				fprintf(fp, "&%s, ", parentName);
				free(parentName);
			};
			fprintf(fp, "NULL};\n");
			
			fprintf(fp, "static Per_FieldMeta fieldMeta_%s[] = {\n", mangledFullName);
			ClassField *field;
			for (field=tem->fields; field!=NULL; field=field->next)
			{
				if ((field->flags & PER_Q_STATIC) == 0)
				{
					Type *type = field->type;
					int arrayDepth = 0;
					
					while (type->kind == PER_TYPE_ARRAY)
					{
						arrayDepth++;
						type = type->elementType;
					};
					
					char *fieldClass = NULL;
					if (type->kind == PER_TYPE_MANAGED)
					{
						ClassTemplate *fieldClassTemplate = findClassByName(NULL, type->fullname);
						fieldClass = formatMsg("&%s", fieldClassTemplate->mangledName);
					};
					
					fprintf(fp, "\t{\"%s\", %s, %d, offsetof(Per_Component_%s, fld_%s), %s},\n",
						field->name, getFieldMetaTypeKindName(type), arrayDepth,
						mangledFullName, field->name, fieldClass != NULL ? fieldClass : "NULL");
					free(fieldClass);
				};
			};
			fprintf(fp, "\t{NULL}\n};\n");
			
			if (tem->protLevel == PER_PROT_PRIVATE) fprintf(fp, "static ");
			fprintf(fp, "Per_ClassDesc %s = {\n", tem->mangledName);
			fprintf(fp, "\t.cdSize = sizeof(Per_ClassDesc),\n");
			fprintf(fp, "\t.cdFullName = \"%s\",\n", tem->fullname);
			fprintf(fp, "\t.cdComponentSize = sizeof(Per_Component_%s),\n", mangledFullName);
			fprintf(fp, "\t.cdParents = parents%s,\n", mangledFullName);
			fprintf(fp, "\t.cdInit = Per_CI%s,\n", mangledFullName);
			fprintf(fp, "\t.cdFini = Per_CF%s,\n", mangledFullName);
			fprintf(fp, "\t.cdDefCtor = Per_CC%s,\n", mangledFullName);
			fprintf(fp, "\t.cdLock = &Per_SL%s,\n", mangledFullName);
			fprintf(fp, "\t.cdFieldMeta = fieldMeta_%s,\n", mangledFullName);
			fprintf(fp, "\t.cdFlags = 0");
			if (tem->flags & PER_Q_FINAL) fprintf(fp, " | PER_CD_FINAL");
			if (tem->flags & PER_Q_ABSTRACT) fprintf(fp, " | PER_CD_ABSTRACT");
			fprintf(fp, "\n");
			fprintf(fp, "};\n");
		};
		
		if (tem->protLevel == PER_PROT_PRIVATE)
		{
			fprintf(fp, "static Per_Mutex %s = PER_MUTEX_INIT;\n", tem->staticLockName);
		}
		else
		{
			fprintf(fp, "Per_Mutex %s = PER_MUTEX_INIT;\n", tem->staticLockName);
		};
		
		// print a newline before the next class!
		fprintf(fp, "\n");
	};
};