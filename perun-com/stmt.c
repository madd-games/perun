/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

#include "stmt.h"
#include "parse.h"

#define	NEW_PROD(type, name)	type *name = (type*) calloc(1, sizeof(type))

static void parseError(StmtParserContext *ctx, int shouldAbort, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	
	if (ctx->aborted) return;

	char buffer[256];
	int count = vsnprintf(buffer, 256, fmt, ap);
	
	ctx->aborted = shouldAbort;
	ctx->errtok = ctx->tok;
	if (count < 256)
	{
		free(ctx->errmsg);
		ctx->errmsg = strdup(buffer);
	}
	else
	{
		ctx->errmsg = (char*) malloc((size_t)count + 1);
		vsnprintf(ctx->errmsg, (size_t)count + 1, fmt, ap);
	};
	
	va_end(ap);
};

static Token* consume(StmtParserContext *ctx)
{
	Token *result = ctx->tok;
	ctx->tok = result->next;
	return result;
};

void initStmtParser(StmtParserContext *ctx, Token *tok, ClassTemplate *inTemplate, HashTable *typeNameTable)
{
	memset(ctx, 0, sizeof(StmtParserContext));
	ctx->tok = tok;
	ctx->inTemplate = inTemplate;
	ctx->typeNameTable = typeNameTable;
};

NullStatement* parseNullStatement(StmtParserContext *ctx)
{
	if (ctx->tok->type == TOK_PER_SEMICOLON)
	{
		NEW_PROD(NullStatement, null);
		null->tok = consume(ctx);		// consume the ';'
		return null;
	};
	
	return NULL;
};

static Expression* parseSubExpr(StmtParserContext *ctx)
{
	ExprParserContext xctx;
	initExprParser(&xctx, ctx->tok, ctx->inTemplate, ctx->typeNameTable);
	
	Expression *expr = parseExpression(&xctx);

	if (!ctx->aborted)
	{
		ctx->errtok = xctx.errtok;
		ctx->errmsg = xctx.errmsg;
		
		if (xctx.aborted)
		{
			ctx->aborted = 1;
			assert(ctx->errtok != NULL);
			return NULL;
		};
	};

	ctx->tok = xctx.tok;
	if (expr == NULL) assert(ctx->errtok != NULL);
	return expr;
};

VariableDefinition* parseVariableDefinition(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		parseError(ctx, 0, "expected a type specification, not `%s'", ctx->tok->value);
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_ID)
	{
		parseError(ctx, 0, "expected an identifier not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *id = consume(ctx);		// consume the variable name
	Expression *expr = NULL;
	
	if (ctx->tok->type == TOK_PER_ASSIGN)
	{
		consume(ctx);			// consume the '='
		
		expr = parseSubExpr(ctx);
		if (expr == NULL)
		{
			// carry over the error
			assert(ctx->errtok != NULL);
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);				// consume the ';'
	
	NEW_PROD(VariableDefinition, vardef);
	vardef->type = type;
	vardef->id = id;
	vardef->init = expr;
	return vardef;
};

ForEachStatement* parseForEachStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_FOREACH) return NULL;
	
	Token *fortok = consume(ctx);		// consume the 'foreach'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);				// consume the '('
	
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		parseError(ctx, 1, "expected a type specification, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_ID)
	{
		parseError(ctx, 1, "expected an identifier, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *id = consume(ctx);		// consume the name
	
	if (ctx->tok->type != TOK_PER_COLON)
	{
		parseError(ctx, 1, "expected `:' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);				// consume the ':'
	
	Expression *expr = parseSubExpr(ctx);
	if (expr == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);				// consume the ')'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(ForEachStatement, foreach);
	foreach->tok = fortok;
	foreach->type = type;
	foreach->id = id;
	foreach->expr = expr;
	foreach->body = body;
	return foreach;
};

IfStatement* parseIfStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_IF) return NULL;
	
	Token *iftok = consume(ctx);		// consume the 'if'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the '('
	
	Expression *ctrl = parseSubExpr(ctx);
	if (ctrl == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected ')' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ')'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	Statement *other = NULL;
	if (ctx->tok->type == TOK_PER_ELSE)
	{
		consume(ctx);	// consume the 'else'
		
		other = parseStatement(ctx);
		if (other == NULL)
		{
			// carry over the error
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(IfStatement, ifs);
	ifs->tok = iftok;
	ifs->ctrl = ctrl;
	ifs->body = body;
	ifs->other = other;
	return ifs;
};

WhileStatement* parseWhileStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_WHILE) return NULL;
	
	Token *whiletok = consume(ctx);		// consume the 'while'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the '('
	
	Expression *ctrl = parseSubExpr(ctx);
	if (ctrl == NULL)
	{
		// carry error over
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ')'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry error over
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(WhileStatement, whiles);
	whiles->tok = whiletok;
	whiles->ctrl = ctrl;
	whiles->body = body;
	return whiles;
};

ForStatement* parseForStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_FOR) return NULL;
	
	Token *fortok = consume(ctx);		// consume the 'for'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the '('
	
	Expression *init = NULL;
	Expression *cond = NULL;
	Expression *iter = NULL;
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		init = parseSubExpr(ctx);
		if (init == NULL)
		{
			// carry error over
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_SEMICOLON)
		{
			parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
	};
	
	consume(ctx);		// consume the ';'
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		cond = parseSubExpr(ctx);
		if (cond == NULL)
		{
			// carry error over
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_SEMICOLON)
		{
			parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
	};
	
	consume(ctx);		// consume the ';'

	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		iter = parseSubExpr(ctx);
		if (iter == NULL)
		{
			// carry error over
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_RPAREN)
		{
			parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
	};
	
	consume(ctx);		// consume the ')'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry error over
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(ForStatement, fors);
	fors->tok = fortok;
	fors->init = init;
	fors->cond = cond;
	fors->iter = iter;
	fors->body = body;
	return fors;
};

BreakStatement* parseBreakStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_BREAK) return NULL;
	
	Token *tok = consume(ctx);
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ';'
	
	NEW_PROD(BreakStatement, breaks);
	breaks->tok = tok;
	return breaks;
};

ContinueStatement* parseContinueStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_CONTINUE) return NULL;
	
	Token *tok = consume(ctx);
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ';'
	
	NEW_PROD(ContinueStatement, conts);
	conts->tok = tok;
	return conts;
};

ReturnStatement* parseReturnStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_RETURN) return NULL;
	
	Token *tok = consume(ctx);
	Expression *expr = NULL;
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		expr = parseSubExpr(ctx);
		if (expr == NULL)
		{
			// carry over the error
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_SEMICOLON)
		{
			parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
	};
	
	consume(ctx);	// consume the ';'
	
	NEW_PROD(ReturnStatement, rets);
	rets->tok = tok;
	rets->expr = expr;
	return rets;
};

static CatchList* parseCatchList(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_CATCH)
	{
		parseError(ctx, 0, "expected `catch' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *tok = consume(ctx);	// consume the 'catch'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the '('
	
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		parseError(ctx, 1, "expected a type specification, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_ID)
	{
		parseError(ctx, 1, "expected an identifier, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *id = consume(ctx);
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ')'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	CatchList *next = parseCatchList(ctx);
	
	NEW_PROD(CatchList, list);
	list->tok = tok;
	list->body = body;
	list->type = type;
	list->id = id;
	list->next = next;
	return list;
};

TryCatchStatement* parseTryCatchStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_TRY) return NULL;
	
	Token *tok = consume(ctx);
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry over error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};

	CatchList *catchList = parseCatchList(ctx);
	if (catchList == NULL)
	{
		// carry over error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(TryCatchStatement, trys);
	trys->tok = tok;
	trys->body = body;
	trys->catchList = catchList;
	return trys;
};

ThrowStatement* parseThrowStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_THROW) return NULL;
	
	Token *tok = consume(ctx);	// consume the 'throw'
	Expression *expr = parseSubExpr(ctx);
	if (expr == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(ThrowStatement, throws);
	throws->tok = tok;
	throws->expr = expr;
	return throws;
};

DoWhileStatement* parseDoWhileStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	if (ctx->tok->type != TOK_PER_DO) return NULL;
	
	consume(ctx);		// consume the 'do'
	
	Statement *body = parseStatement(ctx);
	if (body == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_WHILE)
	{
		parseError(ctx, 1, "expected `while' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *tok = consume(ctx);	// consume the 'while'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);			// consume the '('
	
	Expression *ctrl = parseSubExpr(ctx);
	if (ctrl == NULL)
	{
		// carry over the error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);			// consume the ')'
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);			// consume the ';'
	
	NEW_PROD(DoWhileStatement, dos);
	dos->ctrl = ctrl;
	dos->body = body;
	dos->tok = tok;
	return dos;
};

ExpressionStatement* parseExpressionStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	Expression *expr = parseSubExpr(ctx);
	if (expr == NULL) return NULL;
	
	if (ctx->tok->type != TOK_PER_SEMICOLON)
	{
		parseError(ctx, 1, "expected `;' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ';'
	
	NEW_PROD(ExpressionStatement, xs);
	xs->expr = expr;
	return xs;
};

Statement* parseStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	CompoundStatement *comp = NULL;
	NullStatement *null = NULL;
	VariableDefinition *vardef = NULL;
	IfStatement *ifs = NULL;
	WhileStatement *whiles = NULL;
	ForStatement *fors = NULL;
	BreakStatement *breaks = NULL;
	ContinueStatement *conts = NULL;
	ReturnStatement *rets = NULL;
	TryCatchStatement *trys = NULL;
	ThrowStatement *throws = NULL;
	DoWhileStatement *dos = NULL;
	ForEachStatement *foreach = NULL;
	ExpressionStatement *xs = NULL;
	
	if (
			(comp = parseCompoundStatement(ctx)) != NULL
		||	(null = parseNullStatement(ctx)) != NULL
		||	(vardef = parseVariableDefinition(ctx)) != NULL
		||	(ifs = parseIfStatement(ctx)) != NULL
		||	(whiles = parseWhileStatement(ctx)) != NULL
		||	(fors = parseForStatement(ctx)) != NULL
		||	(foreach = parseForEachStatement(ctx)) != NULL
		||	(breaks = parseBreakStatement(ctx)) != NULL
		||	(conts = parseContinueStatement(ctx)) != NULL
		||	(rets = parseReturnStatement(ctx)) != NULL
		||	(trys = parseTryCatchStatement(ctx)) != NULL
		||	(throws = parseThrowStatement(ctx)) != NULL
		||	(dos = parseDoWhileStatement(ctx)) != NULL
		||	(xs = parseExpressionStatement(ctx)) != NULL
		)
	{
		NEW_PROD(Statement, st);
		st->comp = comp;
		st->null = null;
		st->vardef = vardef;
		st->ifs = ifs;
		st->whiles = whiles;
		st->fors = fors;
		st->foreach = foreach;
		st->breaks = breaks;
		st->conts = conts;
		st->rets = rets;
		st->trys = trys;
		st->throws = throws;
		st->dos = dos;
		st->xs = xs;
		return st;
	}
	else
	{
		// carry error over
		assert(ctx->errtok != NULL);
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
};

CompoundStatement* parseCompoundStatement(StmtParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_LBRACE)
	{
		parseError(ctx, 0, "expected `{' not `%s'", ctx->tok->value);
		return NULL;
	};
	
	consume(ctx);		// consume the '{'
	
	NEW_PROD(CompoundStatement, comp);
	StatementList *last = NULL;
	
	while (ctx->tok->type != TOK_PER_RBRACE)
	{
		Statement *st = parseStatement(ctx);
		if (st == NULL)
		{
			// carry error over
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		NEW_PROD(StatementList, node);
		node->st = st;
		
		if (last == NULL)
		{
			last = comp->head = node;
		}
		else
		{
			last->next = node;
			last = node;
		};
	};
	
	consume(ctx);	// consume the '}'
	return comp;
};
