/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "expr.h"
#include "parse.h"

#define	NEW_PROD(type, name)	type *name = (type*) calloc(1, sizeof(type))

static void parseError(ExprParserContext *ctx, int shouldAbort, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	
	if (ctx->aborted) return;

	char buffer[256];
	int count = vsnprintf(buffer, 256, fmt, ap);
	
	ctx->aborted = shouldAbort;
	ctx->errtok = ctx->tok;
	if (count < 256)
	{
		free(ctx->errmsg);
		ctx->errmsg = strdup(buffer);
	}
	else
	{
		ctx->errmsg = (char*) malloc((size_t)count + 1);
		vsnprintf(ctx->errmsg, (size_t)count + 1, fmt, ap);
	};
	
	va_end(ap);
};

static Token* consume(ExprParserContext *ctx)
{
	Token *result = ctx->tok;
	ctx->tok = result->next;
	return result;
};

void initExprParser(ExprParserContext *ctx, Token *tok, ClassTemplate *inTemplate, HashTable *typeNameTable)
{
	memset(ctx, 0, sizeof(ExprParserContext));
	ctx->tok = tok;
	ctx->inTemplate = inTemplate;
	ctx->typeNameTable = typeNameTable;
};

LambdaExpression* parseLambdaExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_LAMBDA) return NULL;
	Token *tok = consume(ctx);
	
	if (ctx->tok->type != TOK_PER_LARROW)
	{
		parseError(ctx, 1, "expected `<', not `%s'" , ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the '<'
	
	Type *retType = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (retType == NULL)
	{
		parseError(ctx, 1, "expected a type specification, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};

	if (ctx->tok->type != TOK_PER_RARROW)
	{
		parseError(ctx, 1, "expected `>', not `%s'" , ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the '>'

	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the '('
	
	int error = 0;
	ArgumentSpecificationList *args = parseArgumentSpecificationList(ctx->inTemplate, ctx->typeNameTable, &ctx->tok, &error);
	if (error)
	{
		ctx->tok = orig;
		parseError(ctx, 1, "`lambda' must be followed by an argument specification list");
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the ')'
	
	if (ctx->tok->type != TOK_PER_LAMBDA_ARROW)
	{
		parseError(ctx, 1, "expected `=>', not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the '=>'
	
	Expression *expr = parseExpression(ctx);
	if (expr == NULL)
	{
		parseError(ctx, 1, "expected an expression, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(LambdaExpression, lambdaExpr);
	lambdaExpr->tok = tok;
	lambdaExpr->retType = retType;
	lambdaExpr->args = args;
	lambdaExpr->expr = expr;
	return lambdaExpr;
};

ImportExpression* parseImportExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_IMPORT) return NULL;
	consume(ctx);	// consume the 'import'
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		parseError(ctx, 1, "expected `(' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the '('
	
	if (ctx->tok->type != TOK_PER_CONST_STRING)
	{
		parseError(ctx, 1, "expected a string, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	Token *str = consume(ctx);
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)', not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	consume(ctx);	// consume the ')'
	
	NEW_PROD(ImportExpression, importExpr);
	importExpr->str = str;
	return importExpr;
};

NewExpression* parseNewExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_NEW) return NULL;
	Token *newtok = consume(ctx);	// consume the 'new'
	
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		parseError(ctx, 1, "expected a type specification, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	if (type->kind == PER_TYPE_ARRAY)
	{
		if (ctx->tok->type != TOK_PER_LBRACE)
		{
			parseError(ctx, 1, "expected `{' (to initialize an array), not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the '{'
		
		ArgumentExpressionList *inits = parseArgumentExpressionList(ctx);
		
		if (ctx->tok->type != TOK_PER_RBRACE)
		{
			parseError(ctx, 1, "expected `}' or an expression, not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the '}'
		
		NEW_PROD(NewExpression, newx);
		newx->tok = newtok;
		newx->type = type;
		newx->inits = inits;
		return newx;
	}
	else if (ctx->tok->type == TOK_PER_LPAREN)
	{
		if (type->kind != PER_TYPE_MANAGED)
		{
			parseError(ctx, 1, "cannot use a constructor invocation on a primitive or array type `%s'", getTypeName(type));
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the '('
		
		ArgumentExpressionList *args = parseArgumentExpressionList(ctx);
		
		if (ctx->tok->type != TOK_PER_RPAREN)
		{
			parseError(ctx, 1, "expected `)' or an expression, not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the ')'
		
		NEW_PROD(NewExpression, newx);
		newx->tok = newtok;
		newx->type = type;
		newx->args = args;
		return newx;
	}
	else if (ctx->tok->type == TOK_PER_LSQ)
	{
		consume(ctx);	// consume the '['
		
		Expression *size = parseExpression(ctx);
		if (size == NULL)
		{
			// carry over the error
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_RSQ)
		{
			parseError(ctx, 1, "expected `]' not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the ']'
		
		NEW_PROD(NewExpression, newx);
		newx->tok = newtok;
		newx->type = type;
		newx->size = size;
		return newx;
	}
	else
	{
		parseError(ctx, 1, "expected `(', `[', or `[]', not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
};

static Token* parseConst(ExprParserContext *ctx)
{
	switch (ctx->tok->type)
	{
	case TOK_PER_CONST_DOUBLE:
	case TOK_PER_CONST_FLOAT:
	case TOK_PER_CONST_INT:
	case TOK_PER_CONST_UINT:
	case TOK_PER_CONST_LONG:
	case TOK_PER_CONST_ULONG:
	case TOK_PER_CONST_STRING:
	case TOK_PER_TRUE:
	case TOK_PER_FALSE:
	case TOK_PER_NULL:
	case TOK_PER_THIS:
	case TOK_PER_ID:
		return consume(ctx);
	default:
		return NULL;
	};
};

PrimaryExpression* parsePrimaryExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	Expression *expr = NULL;
	Token *tok = NULL;
	Token *bvar = NULL;
	NewExpression *newExpr = NULL;
	ImportExpression *importExpr = NULL;
	LambdaExpression *lambdaExpr = NULL;
	
	if (ctx->tok->type == TOK_PER_LPAREN)
	{
		consume(ctx);	// consume the '('
		expr = parseExpression(ctx);
		
		if (expr == NULL)
		{
			// carry error over
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_RPAREN)
		{
			parseError(ctx, 1, "expected `)', not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the ')'
	}
	else if (ctx->tok->type == TOK_PER_BVAR)
	{
		consume(ctx);	// consume the '$'
		
		if (ctx->tok->type != TOK_PER_ID)
		{
			parseError(ctx, 1, "expected an identifier after `$', not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		bvar = consume(ctx);
	}
	else if ((tok = parseConst(ctx)) != NULL)
	{
		// yes
	}
	else if ((newExpr = parseNewExpression(ctx)) != NULL)
	{
		// yes
	}
	else if ((importExpr = parseImportExpression(ctx)) != NULL)
	{
		// yes
	}
	else if ((lambdaExpr = parseLambdaExpression(ctx)) != NULL)
	{
		// yes
	}
	else
	{
		parseError(ctx, 0, "expected a constant, identifier, new-expression or `(', not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(PrimaryExpression, primary);
	primary->firstTok = orig;
	primary->expr = expr;
	primary->tok = tok;
	primary->bvar = bvar;
	primary->newExpr = newExpr;
	primary->importExpr = importExpr;
	primary->lambdaExpr = lambdaExpr;
	return primary;
};

SubscriptPostfix* parseSubscriptPostfix(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_LSQ) return NULL;
	
	consume(ctx);		// consume the '['
	
	Expression *expr = parseExpression(ctx);
	if (expr == NULL)
	{
		// carry over error
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RSQ)
	{
		parseError(ctx, 1, "expected `]' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ']'
	
	NEW_PROD(SubscriptPostfix, subscript);
	subscript->expr = expr;
	return subscript;
};

ArgumentExpressionList* parseArgumentExpressionList(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	AssignmentExpression *assign = parseAssignmentExpression(ctx);
	if (assign == NULL) return NULL;
	
	ArgumentExpressionList *next = NULL;
	if (ctx->tok->type == TOK_PER_COMMA)
	{
		consume(ctx);	// consume the ','
		
		next = parseArgumentExpressionList(ctx);
		if (next == NULL)
		{
			// carry the error over
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(ArgumentExpressionList, list);
	list->assign = assign;
	list->next = next;
	return list;
};

CallPostfix* parseCallPostfix(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_LPAREN) return NULL;
	
	consume(ctx);		// consume the '('
	
	ArgumentExpressionList *head = parseArgumentExpressionList(ctx);
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		parseError(ctx, 1, "expected `)' not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);		// consume the ')'
	
	NEW_PROD(CallPostfix, call);
	call->head = head;
	return call;
};

MemberPostfix* parseMemberPostfix(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_DOT) return NULL;
	
	consume(ctx);		// consume the '.'
	
	if (ctx->tok->type != TOK_PER_ID)
	{
		parseError(ctx, 1, "expected an identifier, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(MemberPostfix, member);
	member->id = consume(ctx);
	return member;
};

InstancePostfix* parseInstancePostfix(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_IS) return NULL;
	
	consume(ctx);		// consume the 'is'
	
	Token *typeTok = ctx->tok;
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		parseError(ctx, 1, "expected a type specification, not `%s'", ctx->tok->value);
		ctx->tok = orig;
		return NULL;
	};
	
	if (type->kind != PER_TYPE_MANAGED)
	{
		ctx->tok = typeTok;
		parseError(ctx, 1, "the type given to the `is' operator must be a managed non-array type, not `%s'", getTypeName(type));
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(InstancePostfix, inst);
	inst->tok = orig;
	inst->type = type;
	return inst;
};

PostfixExpression* parsePostfix(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type == TOK_PER_INC || ctx->tok->type == TOK_PER_DEC)
	{
		NEW_PROD(PostfixExpression, fix);
		fix->op = consume(ctx);		// consume the '--' or '++'
		return fix;
	}
	else
	{
		SubscriptPostfix *subscript = NULL;
		CallPostfix *call = NULL;
		MemberPostfix *member = NULL;
		InstancePostfix *inst = NULL;
		
		subscript = parseSubscriptPostfix(ctx);
		if (subscript == NULL)
		{
			call = parseCallPostfix(ctx);
			if (call == NULL)
			{
				member = parseMemberPostfix(ctx);
				if (member == NULL)
				{
					inst = parseInstancePostfix(ctx);
					if (inst == NULL)
					{
						ctx->tok = orig;
						return NULL;
					};
				};
			};
		};
		
		NEW_PROD(PostfixExpression, fix);
		fix->subscript = subscript;
		fix->call = call;
		fix->member = member;
		fix->inst = inst;
		return fix;
	};
};

PostfixExpression* parsePostfixExpression(ExprParserContext *ctx)
{
	PrimaryExpression *primary = parsePrimaryExpression(ctx);
	if (primary == NULL) return NULL;
	
	NEW_PROD(PostfixExpression, postfix);
	postfix->primary = primary;
	
	while (1)
	{
		PostfixExpression *next = parsePostfix(ctx);
		if (next == NULL) break;
		
		next->left = postfix;
		postfix = next;
	};
	
	return postfix;
};

UnaryExpression* parseUnaryExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type == TOK_PER_INC || ctx->tok->type == TOK_PER_DEC)
	{
		Token *op = consume(ctx);		// consume '++' or '--'
		PostfixExpression *sub = parsePostfixExpression(ctx);
		
		if (sub == NULL)
		{
			// carry over error from parsePostfixExpression()
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		NEW_PROD(UnaryExpression, unary);
		unary->op = op;
		unary->sub = sub;
		return unary;
	}
	else if (ctx->tok->type == TOK_PER_SUB || ctx->tok->type == TOK_PER_INV || ctx->tok->type == TOK_PER_NOT)
	{
		Token *op = consume(ctx);	// consume '-' or '~' or '!'
		CastExpression *cast = parseCastExpression(ctx);
		
		if (cast == NULL)
		{
			// carry over error from parseCastExpression()
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		NEW_PROD(UnaryExpression, unary);
		unary->op = op;
		unary->cast = cast;
		return unary;
	}
	else
	{
		PostfixExpression *postfix = parsePostfixExpression(ctx);
		if (postfix == NULL) return NULL;
		
		NEW_PROD(UnaryExpression, unary);
		unary->postfix = postfix;
		return unary;
	};
};

static CastExpression* parseCastExpressionProper(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	if (ctx->tok->type != TOK_PER_LPAREN)
	{
		return NULL;
	};
	
	consume(ctx);	// consume '('
	
	Type *type = parseTypeSpec(ctx->inTemplate, ctx->typeNameTable, &ctx->tok);
	if (type == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	if (ctx->tok->type != TOK_PER_RPAREN)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	consume(ctx);	// consume the ')'
	
	CastExpression *sub = parseCastExpression(ctx);
	if (sub == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(CastExpression, cast);
	cast->type = type;
	cast->sub = sub;
	return cast;
};

CastExpression* parseCastExpression(ExprParserContext *ctx)
{
	// first try a "proper" cast expression
	CastExpression *cast = parseCastExpressionProper(ctx);
	if (cast != NULL) return cast;
	
	// otherwise, it's unary
	UnaryExpression *unary = parseUnaryExpression(ctx);
	if (unary == NULL) return NULL;
	
	cast = (CastExpression*) calloc(1, sizeof(CastExpression));
	cast->unary = unary;
	return cast;
};

MultiplicativeExpression* parseMultiplicativeExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	CastExpression *cast = parseCastExpression(ctx);
	if (cast == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	MultiplicativeExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_AST || ctx->tok->type == TOK_PER_DIV || ctx->tok->type == TOK_PER_MOD)
	{
		op = consume(ctx);		// consume the multiplicative operator
		next = parseMultiplicativeExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(MultiplicativeExpression, mul);
	mul->cast = cast;
	mul->op = op;
	mul->next = next;
	return mul;
};

AdditiveExpression* parseAdditiveExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	MultiplicativeExpression *mul = parseMultiplicativeExpression(ctx);
	if (mul == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	AdditiveExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_ADD || ctx->tok->type == TOK_PER_SUB)
	{
		op = consume(ctx);		// consume the additive operator
		next = parseAdditiveExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(AdditiveExpression, add);
	add->mul = mul;
	add->op = op;
	add->next = next;
	return add;
};

ShiftExpression* parseShiftExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	AdditiveExpression *add = parseAdditiveExpression(ctx);
	if (add == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	ShiftExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_SHL || ctx->tok->type == TOK_PER_SHR)
	{
		op = consume(ctx);		// consume the shift operator
		next = parseShiftExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(ShiftExpression, shift);
	shift->add = add;
	shift->op = op;
	shift->next = next;
	return shift;
};

RelationalExpression* parseRelationalExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	ShiftExpression *shift = parseShiftExpression(ctx);
	if (shift == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	RelationalExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_LARROW || ctx->tok->type == TOK_PER_LESS_EQ || ctx->tok->type == TOK_PER_RARROW || ctx->tok->type == TOK_PER_MORE_EQ)
	{
		op = consume(ctx);		// consume the relational operator
		next = parseRelationalExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(RelationalExpression, rel);
	rel->shift = shift;
	rel->op = op;
	rel->next = next;
	return rel;
};

EqualityExpression* parseEqualityExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	RelationalExpression *rel = parseRelationalExpression(ctx);
	if (rel == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	EqualityExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_EQ || ctx->tok->type == TOK_PER_NEQ)
	{
		op = consume(ctx);		// consume the equality operator
		next = parseEqualityExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(EqualityExpression, equality);
	equality->rel = rel;
	equality->op = op;
	equality->next = next;
	return equality;
};

BitwiseANDExpression* parseBitwiseANDExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	EqualityExpression *equality = parseEqualityExpression(ctx);
	if (equality == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	BitwiseANDExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_AND)
	{
		op = consume(ctx);		// consume the '&'
		next = parseBitwiseANDExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(BitwiseANDExpression, bitwiseAND);
	bitwiseAND->equality = equality;
	bitwiseAND->op = op;
	bitwiseAND->next = next;
	return bitwiseAND;
};

BitwiseXORExpression* parseBitwiseXORExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	BitwiseANDExpression *bitwiseAND = parseBitwiseANDExpression(ctx);
	if (bitwiseAND == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	BitwiseXORExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_XOR)
	{
		op = consume(ctx);		// consume the '^'
		next = parseBitwiseXORExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(BitwiseXORExpression, bitwiseXOR);
	bitwiseXOR->bitwiseAND = bitwiseAND;
	bitwiseXOR->op = op;
	bitwiseXOR->next = next;
	return bitwiseXOR;
};

BitwiseORExpression* parseBitwiseORExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	BitwiseXORExpression *bitwiseXOR = parseBitwiseXORExpression(ctx);
	if (bitwiseXOR == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	BitwiseORExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_OR)
	{
		op = consume(ctx);		// consume the '|'
		next = parseBitwiseORExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(BitwiseORExpression, bitwiseOR);
	bitwiseOR->bitwiseXOR = bitwiseXOR;
	bitwiseOR->op = op;
	bitwiseOR->next = next;
	return bitwiseOR;
};

LogicalANDExpression* parseLogicalANDExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	BitwiseORExpression *bitwiseOR = parseBitwiseORExpression(ctx);
	if (bitwiseOR == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	LogicalANDExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_LAND)
	{
		op = consume(ctx);		// consume the '&&'
		next = parseLogicalANDExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(LogicalANDExpression, logicalAND);
	logicalAND->bitwiseOR = bitwiseOR;
	logicalAND->op = op;
	logicalAND->next = next;
	return logicalAND;
};

LogicalORExpression* parseLogicalORExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	LogicalANDExpression *logicalAND = parseLogicalANDExpression(ctx);
	if (logicalAND == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = NULL;
	LogicalORExpression *next = NULL;
	
	if (ctx->tok->type == TOK_PER_LOR)
	{
		op = consume(ctx);		// consume the '||'
		next = parseLogicalORExpression(ctx);
		
		if (next == NULL)
		{
			// carry error on
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(LogicalORExpression, logicalOR);
	logicalOR->logicalAND = logicalAND;
	logicalOR->op = op;
	logicalOR->next = next;
	return logicalOR;
};

ConditionalExpression* parseConditionalExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	LogicalORExpression *logicalOR = parseLogicalORExpression(ctx);
	if (logicalOR == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Expression *exprTrue = NULL;
	ConditionalExpression *exprFalse = NULL;
	Token *op = NULL;
	
	if (ctx->tok->type == TOK_PER_COND)
	{
		op = consume(ctx);	// consume the '?'
		
		exprTrue = parseExpression(ctx);
		if (exprTrue == NULL)
		{
			// carry over the error from parseExpression()
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
		
		if (ctx->tok->type != TOK_PER_COLON)
		{
			parseError(ctx, 1, "expeccted `;', not `%s'", ctx->tok->value);
			ctx->tok = orig;
			return NULL;
		};
		
		consume(ctx);	// consume the ':'
		
		exprFalse = parseConditionalExpression(ctx);
		if (exprFalse == NULL)
		{
			// carry over the error from parseConditionalExpression()
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(ConditionalExpression, cond);
	cond->logicalOR = logicalOR;
	cond->exprTrue = exprTrue;
	cond->exprFalse = exprFalse;
	cond->op = op;
	return cond;
};

static Token* parseAssignmentOperator(ExprParserContext *ctx)
{
	switch (ctx->tok->type)
	{
	case TOK_PER_ASSIGN:
		return consume(ctx);
	case TOK_PER_ASSIGN_ADD:
		return consume(ctx);
	case TOK_PER_ASSIGN_AND:
		return consume(ctx);
	case TOK_PER_ASSIGN_DIV:
		return consume(ctx);
	case TOK_PER_ASSIGN_MOD:
		return consume(ctx);
	case TOK_PER_ASSIGN_MUL:
		return consume(ctx);
	case TOK_PER_ASSIGN_OR:
		return consume(ctx);
	case TOK_PER_ASSIGN_SHL:
		return consume(ctx);
	case TOK_PER_ASSIGN_SHR:
		return consume(ctx);
	case TOK_PER_ASSIGN_SUB:
		return consume(ctx);
	case TOK_PER_ASSIGN_XOR:
		return consume(ctx);
	default:
		return NULL;
	};
};

static AssignmentExpression* parseAssignmentExpression_1(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	PostfixExpression *left = parsePostfixExpression(ctx);
	if (left == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Token *op = parseAssignmentOperator(ctx);
	if (op == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	AssignmentExpression *right = parseAssignmentExpression(ctx);
	if (right == NULL)
	{
		// carry over the error from inability to parse right expression
		ctx->aborted = 1;
		ctx->tok = orig;
		return NULL;
	};
	
	NEW_PROD(AssignmentExpression, assign);
	assign->left = left;
	assign->op = op;
	assign->right = right;
	return assign;
};

static AssignmentExpression* parseAssignmentExpression_2(ExprParserContext *ctx)
{
	ConditionalExpression *cond = parseConditionalExpression(ctx);
	if (cond == NULL) return NULL;
	
	NEW_PROD(AssignmentExpression, assign);
	assign->cond = cond;
	return assign;
};

AssignmentExpression* parseAssignmentExpression(ExprParserContext *ctx)
{
	AssignmentExpression *assign = parseAssignmentExpression_1(ctx);
	if (assign == NULL)
	{
		ctx->aborted = 0;
		return parseAssignmentExpression_2(ctx);
	};
	
	return assign;
};

Expression* parseExpression(ExprParserContext *ctx)
{
	Token *orig = ctx->tok;
	
	AssignmentExpression *assign = parseAssignmentExpression(ctx);
	if (assign == NULL)
	{
		ctx->tok = orig;
		return NULL;
	};
	
	Expression *next = NULL;
	if (ctx->tok->type == TOK_PER_COMMA)
	{
		consume(ctx);		// consume the ','
		
		next = parseExpression(ctx);
		if (next == NULL)
		{
			// error carried over from whatever reason we couldn't accept the expression
			ctx->aborted = 1;
			ctx->tok = orig;
			return NULL;
		};
	};
	
	NEW_PROD(Expression, expr);
	expr->assign = assign;
	expr->next = next;
	return expr;
};
