/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef OPT_H_
#define OPT_H_

/**
 * Describes an enum option.
 */
typedef struct
{
	/**
	 * Points to an 'int' where the value will be stored.
	 */
	int *valptr;
	
	/**
	 * The enum names, and the number of them. Each entry is a string, and
	 * the index is the value to which that string is mapped.
	 */
	int numEnums;
	const char **enumNames;
} OptionEnumSpec;

/**
 * Types of options.
 */
typedef enum
{
	/**
	 * Flag: the option, when passed, sets an 'int' at the value pointer to 1.
	 */
	OPT_FLAG,
	
	/**
	 * Short-option string, of the form "-x string" or "-xstring" (for "x"). The value
	 * pointer is assigned a pointer to the string.
	 */
	OPT_SHORT_STRING,
	
	/**
	 * Normal string option, of the form "-option=value". The value pointer is set to the
	 * string.
	 */
	OPT_STRING,
	
	/**
	 * List option, of the form "-x string" or "-xstring" (for "x"). The value pointer is expected
	 * to point to a function with prototype:
	 * 	void addOption(const char *value);
	 * And it is called for every value.
	 */
	OPT_LIST,
	
	/**
	 * Enum option, of the form "-x=y". The value pointer is expected to point to a 'OptionEnumSpec'
	 * structure, specifying the possible values and indicating where the result shall be stored.
	 */
	OPT_ENUM,
} OptionType;

/**
 * Description of a command-line option.
 */
typedef struct OptionSpec_
{
	/**
	 * Link.
	 */
	struct OptionSpec_* next;
	
	/**
	 * Name of the option, with the "-". For example "-o".
	 */
	const char *name;
	
	/**
	 * Value hint, used in help for options which take arguments.
	 */
	const char *hint;
	
	/**
	 * A help paragraph for this option.
	 */
	const char *help;
	
	/**
	 * Type of option (OPT_*).
	 */
	OptionType type;
	
	/**
	 * Pointer to the value holder.
	 */
	void *valptr;
} OptionSpec;

/**
 * List of options.
 */
typedef struct
{
	OptionSpec *head;
} OptionList;

/**
 * Initialize an option list.
 */
void optInit(OptionList *list);

/**
 * Add an option to the list.
 */
void optAdd(OptionList *list, const char *name, const char *hint, const char *help, OptionType type, void *valptr);

/**
 * Parse command line arguments according to an option list. 'argc' and 'argv' cna be taken directly
 * from main() (argv[0] is expected to be the program name and is used to print errors). Any argument
 * which does not begin with a '-' is considered an input file and is added to a separate list. For
 * all the ones that begin with '-', they are parsed according to the option list and values are
 * stored at the value pointers. If there are invalid command-line arguments, NULL is returned,
 * and an error is printed to stderr. On success, a list of strings identifying the input files,
 * terminated with a NULL pointer, is returned.
 */
char** optParse(int argc, char *argv[], OptionList *list);

/**
 * Display usage information given an option list.
 */
void optUsage(const char *progName, OptionList *list);

#endif