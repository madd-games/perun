/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "opt.h"

typedef void (*OptionHandler)(const char *value);

void optInit(OptionList *list)
{
	list->head = NULL;
};

void optAdd(OptionList *list, const char *name, const char *hint, const char *help, OptionType type, void *valptr)
{
	OptionSpec *spec = (OptionSpec*) malloc(sizeof(OptionSpec));
	memset(spec, 0, sizeof(OptionSpec));
	
	spec->name = name;
	spec->help = help;
	spec->type = type;
	spec->valptr = valptr;
	spec->hint = hint;
	
	spec->next = list->head;
	list->head = spec;
};

char** optParse(int argc, char *argv[], OptionList *list)
{
	// allocate maximum possible space for file list
	char **files = (char**) malloc(sizeof(void*) * argc);
	size_t numFiles = 0;
	
	int i;
	for (i=1; i<argc; i++)
	{
		char *opt = argv[i];
		if (strcmp(opt, "--help") == 0)
		{
			optUsage(argv[0], list);
			return NULL;
		};
		
		if (opt[0] != '-')
		{
			files[numFiles++] = opt;
		}
		else
		{
			OptionSpec *spec;
			for (spec=list->head; spec!=NULL; spec=spec->next)
			{
				if (spec->type == OPT_FLAG)
				{
					if (strcmp(opt, spec->name) == 0)
					{
						*((int*)spec->valptr) = 1;
						break;
					};
				}
				else if (spec->type == OPT_SHORT_STRING)
				{
					if (strcmp(opt, spec->name) == 0)
					{
						i++;
						if (i == argc)
						{
							fprintf(stderr, "%s: `%s' expects a paramater\n",
								argv[0], opt);
							free(files);
							return NULL;
						};
						
						*((char**)spec->valptr) = argv[i];
						break;
					}
					else if (memcmp(opt, spec->name, strlen(spec->name)) == 0)
					{
						*((char**)spec->valptr) = &opt[strlen(spec->name)];
						break;
					};
				}
				else if (spec->type == OPT_STRING)
				{
					if (memcmp(opt, spec->name, strlen(spec->name)) == 0)
					{
						char *str = &opt[strlen(spec->name)];
						if (*str != '=') continue;
						str++;
						
						*((char**)spec->valptr) = str;
						break;
					};
				}
				else if (spec->type == OPT_LIST)
				{
					if (strcmp(opt, spec->name) == 0)
					{
						i++;
						if (i == argc)
						{
							fprintf(stderr, "%s: `%s' expects a parameter\n",
								argv[0], opt);
							free(files);
							return NULL;
						};
						
						OptionHandler handler = (OptionHandler) spec->valptr;
						handler(argv[i]);
						break;
					}
					else if (memcmp(opt, spec->name, strlen(spec->name)) == 0)
					{
						OptionHandler handler = (OptionHandler) spec->valptr;
						handler(&opt[strlen(spec->name)]);
						break;
					};
				}
				else if (spec->type == OPT_ENUM)
				{
					if (memcmp(opt, spec->name, strlen(spec->name)) == 0)
					{
						char *str = &opt[strlen(spec->name)];
						if (*str != '=') continue;
						str++;
						
						OptionEnumSpec *es = (OptionEnumSpec*) spec->valptr;
						int j;
						for (j=0; j<es->numEnums; j++)
						{
							if (strcmp(str, es->enumNames[j]) == 0)
							{
								*es->valptr = j;
								break;
							};
						};
						
						if (j == es->numEnums)
						{
							fprintf(stderr, "%s: `%s' is not valid value for `%s'\n",
								argv[0], str, spec->name);
							free(files);
							return NULL;
						};
						
						break;
					};
				};
			};
			
			if (spec == NULL)
			{
				fprintf(stderr, "%s: unrecognised command-line option: `%s'\n",
					argv[0], argv[i]);
				return NULL;
			};
		};
	};
	
	files[numFiles++] = NULL;
	files = (char**) realloc(files, sizeof(void*) * numFiles);
	return files;
};

void optUsage(const char *progName, OptionList *list)
{
	fprintf(stderr, "USAGE:\t%s FILES... ", progName);
	
	OptionSpec *spec;
	for (spec=list->head; spec!=NULL; spec=spec->next)
	{
		if (spec->hint[0] == 0 && spec->type != OPT_ENUM)
		{
			fprintf(stderr, "[%s] ", spec->name);
		}
		else if (spec->type == OPT_SHORT_STRING)
		{
			fprintf(stderr, "[%s %s] ", spec->name, spec->hint);
		}
		else if (spec->type == OPT_LIST)
		{
			fprintf(stderr, "[%s%s] ", spec->name, spec->hint);
		}
		else if (spec->type == OPT_ENUM)
		{
			OptionEnumSpec *es = (OptionEnumSpec*) spec->valptr;
			fprintf(stderr, "[%s=", spec->name);
			
			int i;
			for (i=0; i<es->numEnums; i++)
			{
				fprintf(stderr, "%s%s", es->enumNames[i], i == es->numEnums-1 ? "" : "|");
			};
			
			fprintf(stderr, "] ");
		}
		else
		{
			fprintf(stderr, "[%s=%s] ", spec->name, spec->hint);
		};
	};
	
	fprintf(stderr, "\n\n");
	
	for (spec=list->head; spec!=NULL; spec=spec->next)
	{
		if (spec->type == OPT_ENUM)
		{
			OptionEnumSpec *es = (OptionEnumSpec*) spec->valptr;
			fprintf(stderr, "\t%s=", spec->name);
			
			int i;
			for (i=0; i<es->numEnums; i++)
			{
				fprintf(stderr, "%s%s", es->enumNames[i], i == es->numEnums-1 ? "" : "|");
			};
			
			fprintf(stderr, "\n");
		}
		else if (spec->type == OPT_SHORT_STRING || spec->hint[0] == 0) fprintf(stderr, "\t%s %s\n", spec->name, spec->hint);
		else if (spec->type == OPT_LIST) fprintf(stderr, "\t%s%s\n", spec->name, spec->hint);
		else fprintf(stderr, "\t%s=%s\n", spec->name, spec->hint);
		fprintf(stderr, "\t\t%s\n\n", spec->help);
	};
};