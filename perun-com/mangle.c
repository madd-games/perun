/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "mangle.h"
#include "catalog.h"

static char *mangleType(Type *type)
{
	char *out;
	char *temp;
	
	switch (type->kind)
	{
	case PER_TYPE_ARRAY:
		temp = mangleType(type->elementType);
		out = (char*) malloc(strlen(temp) + 2);
		sprintf(out, "_A%s", &temp[1]);
		free(temp);
		return out;
	case PER_TYPE_VOID:
		return strdup("_V");
	case PER_TYPE_BOOL:
		return strdup("_B");
	case PER_TYPE_BYTE:
		return strdup("_U8");
	case PER_TYPE_DOUBLE:
		return strdup("_F64");
	case PER_TYPE_DWORD:
		return strdup("_U32");
	case PER_TYPE_FLOAT:
		return strdup("_F32");
	case PER_TYPE_PTR:
		return strdup("_UP");
	case PER_TYPE_QWORD:
		return strdup("_U64");
	case PER_TYPE_SBYTE:
		return strdup("_I8");
	case PER_TYPE_SDWORD:
		return strdup("_I32");
	case PER_TYPE_SPTR:
		return strdup("_SP");
	case PER_TYPE_SQWORD:
		return strdup("_I64");
	case PER_TYPE_SWORD:
		return strdup("_I16");
	case PER_TYPE_WORD:
		return strdup("_U16");
	case PER_TYPE_TEMPLATE_PARAM:
		return mangleType(type->minBase);
	case PER_TYPE_MANAGED:
		{
			ClassTemplate *tem = findClassByName(NULL, type->fullname);
			ClassInfo *info = doAnalysis(tem->fullname);
			
			char *result = mangleName("_M?_$", type->fullname, info->numTemplateParams);
			
			int i;
			for (i=0; i<info->numTemplateParams; i++)
			{
				const char *name = info->paramNames[i];
				Type *subtype = (Type*) hashtabGet(type->temParamTypes, name);
				
				char *subname = mangleType(subtype);
				result = (char*) realloc(result, strlen(result) + strlen(subname) + 1);
				strcat(result, subname);
				free(subname);
			};
			
			return result;
		};
	case PER_TYPE_FUNCREF:
		{
			char *prefix = mangleName("_G$", type->numArgs);
			char *retMangled = mangleType(type->retType);
			
			char *result = (char*) malloc(strlen(prefix) + strlen(retMangled) + 1);
			sprintf(result, "%s%s", prefix, retMangled);
			free(prefix);
			free(retMangled);
			
			int i;
			for (i=0; i<type->numArgs; i++)
			{
				char *subname = mangleType(type->argTypes[i]);
				result = (char*) realloc(result, strlen(result) + strlen(subname) + 1);
				strcat(result, subname);
				free(subname);
			};
			
			return result;
		};
	default:
		fprintf(stderr, "CRITICAL: Cannot mangle type!\n");
		abort();
		return NULL;
	};
};

char* mangleName(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	
	int size = strlen(format) + 1;
	char *buffer = (char*) malloc(size);
	buffer[0] = 0;
	
	char c;
	while ((c = *format++) != 0)
	{
		if (c == '?')
		{
			const char *pre = va_arg(ap, const char*);
			
			while (*pre != 0)
			{
				const char *pos = strchr(pre, '.');
				if (pos == NULL) pos = &pre[strlen(pre)];
				
				char szstr[64];
				sprintf(szstr, "%d", (int) (pos-pre));
				
				size += strlen(szstr) + ((int) (pos-pre));
				buffer = (char*) realloc(buffer, size);
				strcat(buffer, szstr);
				
				char *put = &buffer[strlen(buffer)];
				memcpy(put, pre, pos-pre);
				put[pos-pre] = 0;
				
				pre = pos;
				if (*pre == '.') pre++;
			};
		}
		else if (c == '#')
		{
			char p = (char) va_arg(ap, int);
			if (p == 0) continue;
			
			char temp[3];
			sprintf(temp, "%c", p);
			
			buffer = (char*) realloc(buffer, strlen(buffer) + 2);
			strcat(buffer, temp);
		}
		else if (c == '*')
		{
			ArgumentSpecificationList *arg;
			for (arg=va_arg(ap, ArgumentSpecificationList*); arg!=NULL; arg=arg->next)
			{
				char *sub = mangleType(arg->type);
				
				buffer = (char*) realloc(buffer, strlen(buffer) + strlen(sub) + 1);
				sprintf(buffer, "%s%s", buffer, sub);
				free(sub);
			};
		}
		else if (c == '$')
		{
			int val = va_arg(ap, int);
			
			char *new = (char*) malloc(strlen(buffer) + 64);
			sprintf(new, "%s%d", buffer, val);
			free(buffer);
			buffer = new;
		}
		else
		{
			char temp[3];
			sprintf(temp, "%c", c);
			
			buffer = (char*) realloc(buffer, strlen(buffer) + 2);
			strcat(buffer, temp);
		};
	};
	
	return buffer;
};
