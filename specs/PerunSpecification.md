# Perun Programming Language Specification
![Perun logo](https://gitlab.com/madd-games/perun/raw/master/assets/logo.png)
_DRAFT SPECIFICATION_
# Introduction
Perun is an object-oriented programming language with an explicit aim of ABI stability. It is designed so that a programmer can guarantee ABI compatibility between different releases of a library with ease, unlike languages such as C++, where ABI stability is difficult to guarantee and constructs such as C++ templates cannot be properly described at the machine-code level. However, it also aims to be a compiled language and not require an interpreter, unlike Java. This means that we can achieve both good performance and ABI stability: a mixture of the good features of both of the major object-oriented languages.

This document is the formal specification of this language. The _Madd Perun Compiler_ and the _Madd Perun Standard Library_ comprise the official reference implementation of the language. Both the ABI and the language are defined in this document, as they go hand-in-hand.

# Concepts
## Data Types
Every _value_ in Perun has a _data type_. There are 4 _kinds_ of data type: _primitive_, _managed_, _array_ and _conceptual_. The primitive types are fundamental building blocks from which more complex structures are derived, and they represent integers, floating-point numbers and booleans. Furthermore, the special type `void` - which represents "no data" - is also considered to be a primitive type. An _array type_ is derived from its _element type_ and represents ordered collections of values of its element type. The name of an array type is the name of its element type followed by `[]`, and the element type itself can be an array type. For example, array types include `int[]`, `bool[][][]`, etc. The conceptual types exist only during evaluation of expressions, and can be _implicitly cast_ (converted) to specific types, as explained in the section on conceptual types. Managed types are identified by classes, as explained in the next section.

## Classes and Namespaces
Managed data types signify references to _objects_. A managed type is identified with a _class_. Classes form a hierarchy, whereby each class may derive from one or more other classes. The class which is derived from is called a _superclass_ or _ancestor class_, and the class that is derived from it is said to be its _subclass_. Classes contain _fields_, which store data related to the class or objects made from it, and _methods_, which can be invoked to perform actions on the object.

An object is created from a class using the `new` operator. This object is then said to be an _instance_ of that class. Instances of a class are also considered to be instances of all of its ancestor classes. Thus, variables of a given managed type may be assigned values of any of its subclass types, without any conversion, and thus code operating on data of a certain type can also operator on data of any subtype.

Values of managed types are said to have a _static type_ and a _dynamic type_. The static type is what the compiler sees: if a type is given to a variable or value, that is its static type. The dynamic type is available at run-time, and it is the class which was actually instantiated to create an object. If an object is determined at run-time to be of a certain type, then it can be _explicitly cast_ to that type at compile-time: this is explained later.

Fields and methods in a class may be either _virtual_ (the default) or _static_. The exact semantics are slightly different for fields and methods, but the idea is the same:

- A virtual field has a different value in every instance of the class, and can be set independently in every object.
- A static field is not tied to a specific object, and is available globally. Changing the value inside any instance causes it to change in all instances. Furthermore, it can be accessed without an instance too.
- A virtual method operates on an instance of a class, and cannot be invoked without an instance. When accessed from an object, it has _dynamic dispatch semantics_, as explained later. It can also be invoked statically, in which case it must be given an instance of the class as the first argument, and is dispatched statically. Virtual methods can also override methods of the same signature from an ancestor class, as explained in the section on dynamic dispatch.
- A static method does not operate on a specific object, and can be invoked without an instance. It is always statically dispatched, and can only access static fields of the class.

Classes have names. They have a _shortname_ and a _fullname_. The shortname is used to refer to the class when it is imported, or inside the compilation unit in which it was defined. The fullname includes the _namespace_ in which the class is placed. All classes must be placed inside a namespace, and namespaces form a hierarchy. The fullname is thus the name of the _root namespace_, followed by a period (`.`), followed by the name of a subnamespace, followed by another period, and so on, up to the shortname of the class itself. For example, a class called `String` is defined inside `rt`, a subnamespace of `std`. Thus, `String` is the shortname of said class, and `std.rt.String` is its fullname.

The general convention for namespaces is that the root namespace name should identify the vendor of the software of which the class is a part, such as `madd`, and that should include a subnamespace naming the program itself, such as `perun`. This can then be subnamespaced in any way necessary, if at all. Thus, this hypothetical package could include classes `madd.perun.Perun` and `madd.perun.utils.PerunUtil`. The following root namespaces are also reserved:

- The root namespace `std` is for Perun's _standard library_. All classes from the standard library shall be under this namespace, in subnamespaces which group classes based on the job they are designed for.
- The root namespace `doc` is reserved for documentation examples and must never be used in real programs.

All classes have a common ancestor, `std.rt.Object`, implicitly, even if it was not explicitly specified anywhere in the hierarchy. Thus, variables of type `std.rt.Object` can store any object.

## Class Templates
Each class may be  _parametrized_ by types known as _template parameters_. The class is then known as a _class template_, and types derived from it are known as _parametrized types_. Each parameter has a name, and defined a new managed type which may be used inside a class. Wherever this type is used, it is substituted by the type specified in the template parameter list whenever the parametrized is used.

An example of this would be a class called `Map`. It is a template, `Map<K, V>`, and it has methods which take types `K` and type `V` as arguments. When the `Map` type is used, these types must be specified: for example, `Map<String, InputStream>`. When a value of this static type is accessed, the `K` type is substituted for `String` wherever it appears in a member, and `V` is substituted for `InputStream`.

Template parameters must be managed types. However, if a primitive type is used, it is implicitly converted to a corresponding _implicit managed wrapper_. Array types cannot be used.

Each template parameter has a _minimum base class_, and when a type is specified for it, it must be a subtype of the minimum base class. By default, this class is `std.rt.Object`, so any type may be used.

When compiling a class, all template parameter types are substituted with their minimum base classes, and the code is compiled accordingly. The ABI guarantees that this code will work with any object of the subclasses at run-time.

## Subtypes
A type `A` is considered a _subtype_ of `B` under the following conditions:

- If `A` and `B` are the same primitive type, both are subtypes of each other.
- If `A` and `B` are managed types referring to the same class, then `A` is a subtype of `B` if all template parameters of `A` are subtypes of the corresponding paramters in `B`.
- Recursively, if a _parent type_ of `A` is a subtype of `B`, then so is `A` itself.

The _parent types_ of a type are the list of parents, with template parameters substituted in if used.

## Fields
All fields inside a class must have a unique name, and it must also not be the name of a method. It likewise must have a different name than any field or method from an ancestor class. A field can have any type other than `void`.

## Methods
A method must not have the same name as a field defined in the same class, or in an ancestor class.

Multiple methods defined in the same class may have the same name as long as they have non-matching _signautres_ as described in the last paragraph. A static method must also have a different signature than any method defined in an ancestor class. A virtual method must have a different signature than any _static_ method defined in any ancestor class. A virtual method may have the same signature as a virtual method from an ancestor class, in which case it _overrides_ that method in dynamic dispatch (as explained in the next section). However, the intention to override must be explicitly specified using the `override` keyword.

Method can take zero or more arguments, which all have types. The name of a method, and the types of arguments it takes (including their order), comprise the _signature_ of the method. For example, the signature of a method might be `methodName(int, String, float)` or `methodName(bool)`.

A signature is said to be _matching_ for the purposes of method definition if all of the new function's argument types are subtypes of an existing method's argument types, or vice versa. If an existing method has a matching signature to the new method, it is a compile-time error.

## Dynamic and Static Dispatch
In static dispatch, the function to invoke is determined at compile-time (statically). This is always the case for static functions. Virtual functions can be statically dispatched by specifically naming the class in which they are defined, as in the examples below.

In a class hierarchy, a subclass may define a method which _overrides_ a method from an ancestor class. In this case, we have two possible implementations to invoke: the definition in the superclass, A, or the definition in the subclass, B. If static dispatch is used, the class is named and the implementation from that specific class is chosen. Otherwise, an object is specified instead, and dynamic dispatch takes place: the implementation to invoke is chosen according to the dynamic type of the object.

Examples:

```java
// both of these have static type A, BUT:
A obj1 = new A();		// this one has dynamic type A
A obj2 = new B();		// this one has dynamic type B

// this one has static type B
B obj3 = new B();

// specifically invoke A's implementation on obj2 (static dispatch):
A.someMethod(obj2);

// specifically invoke B's implementation on obj3 (static dispatch):
B.someMethod(obj3);

// dynamic dispatch: A's implementation is invoked, because obj1's
// dynamic type is A:
obj1.someMethod();

// dynamic dispatch: B's implementation is invokved, because obj2's
// dynamic type is B:
obj2.someMethod();

// ERROR: cannot statically dispatch B's implementation because it is
// not a subtype of the static type of obj2:
B.someMethod(obj2);		// ERROR!

// ...but this way WILL work, because obj2 is known at run-time to be
// of type B:
B.someMethod((B) obj2);

// this throws an exception at run-time because obj1 is not of type B:
B.someMethod((B) obj1);
```

## Primitive Data Types

Perun is explicit about the sizes of all its primitive data types. In the tables below, comma-separated names in the same cell refer to the same type, and are thus aliases of each other.

### Integer Types
|                | Signed         | Unsigned       |
|----------------|----------------|----------------|
| 8-bit          | `sbyte_t`        | `byte_t`         |
| 16-bit         | `sword_t`        | `word_t`         |
| 32-bit         | `sdword_t`, `int`  | `dword_t`, `uint`  |
| 64-bit         | `sqword_t`, `long` | `qword_t`, `ulong` |
| Pointer length | `sptr_t`         | `ptr_t`          |

### The boolean type
The type `bool` can store the values `true` and `false`.

### Floating-point Types

The type name `float` shall refer to a 32-bit floating-point number. The type name `double` shall refer to a 64-bit floating-point number.

### The void type

The type name `void` is an incomplete type which can never be completed. It is mainly used to indicate that a function does not return any value.

## Implicit managed wrappers

When primitive data types are used to parametrize templates, they must be implicitly replaced by the corresponding _implicit managed wrapper_ for that type, due to the fact that when templates are compiled, the template parameter types are treated as managed types. The implicit wrappers are part of the standard library, and each primitive type and its corresponding managed wrapper can be implicitly cast to each other at compile-time. The following table lists the implicit managed wrappers for all primitive types:

|Primitive type|Implicit managed wrapper|
|--------------|------------------------|
|`byte_t`      |`std.rt.Byte`           |
|`sbyte_t`     |`std.rt.SignedByte`     |
|`word_t`      |`std.rt.Word`           |
|`sword_t`     |`std.rt.SignedWord`     |
|`uint`        |`std.rt.UnsignedInteger`|
|`int`         |`std.rt.Integer`        |
|`ulong`       |`std.rt.UnsignedLong`   |
|`long`        |`std.rt.Long`           |
|`ptr_t`       |`std.rt.Pointer`        |
|`sptr_t`      |`std.rt.SignedPointer`  |
|`bool`        |`std.rt.Boolean`        |
|`float`       |`std.rt.Float`          |
|`double`      |`std.rt.Double`         |

## Array Types
An array type can be derived from any type except `void` and that is known as its _element type_. Arrays must be instantiated much like objects. An array has a fixed length which is specified during instantiation, and arrays of any length may be assigned to a variable of the corresponding array type. Each elemetn in an array has an index, beginning with 0. The `null` value is also considered an array, and it represents the concept of no array existing, and attempting to access it results in a run-time exception.

## Implicit Casts
Whenever a value of a specific type is expected, but a value of another type is provided, an _implicit cast_ is attempted. The following types can be implicitly converted to each other:

- A managed type can be implicitly converted to any parent class. This is in practice a no-op conversion.
- A signed integer type can be implicitly converted to any wider signed integer type (but cannot be implicitly converted to a shorter type).
- An unsigned integer type can be implicitly converted to any wider unsigned integer type (but cannot be implicitly converted to a shorter type).
- The implicit manager wrapper and its corresponding primitive type can be implicitly converted to each other.
- `float` can be implicitly converted to `double`.
- The constant `null` can be implicitly converted to any managed or array type.

## Method Signature Selection
When a function call is performed, an appropriate method is chosen based on the following criteria:

- If a signature exists which takes exactly the types given, then that signature is chosen.
- If a signature exists such that all arguments can be implicitly cast to the types it expects, then that signature is chosen. If there are multiple such signatures, then the code is said to be ambiguous, and a compile-time error is raised. This can be resolved by explicitly casting to the required type, so that the first criterion is met.
- Otherwise, there are no compatible signatures, and a compile-time error is raised.

## Abstract Classes and Methods
An _abstract class_ is one which cannot be instantiated (and thus cannot be the dynamic type of an object). An abstract class may contain zero or more _abstract methods_. Methods defined as abstract are non-static methods which have no implementation and so cannot be statically dispatched. However, a non-abstract subclass may override these methods such that they may be invoked via dynamic dispatch.

If a class is not defined as abstract, then it must have no abstract methods. Any abstract methods from superclasses must be overriden. If this condition is not met, a compile-time error is raised.

## Exceptions
Exceptions are a way of interrupting the normal flow of a program, usually as the result of an error. An exception object is one which is an instance of the abstract class `std.rt.Exception`. An exception may be _thrown_. When this happens, the implementation attempts to _catch_ the exception: it moves down the call stack, until it finds itself enclosed in a `try` block, which has a `catch` block attached to it which catches exceptions of the specified type. In this case, it transfers control to this `catch` block and passes it the exception object. If it reaches the bottom of the call stack and finds no matching `catch` block, then the program is aborted, and a message (derived from the object itself) is displayed in the console.

# Tokenization
The first step in compiling a Perun compilation unit is to divide the code into a list of _tokens_, each having a type. As a special case, `Whitespace` tokens are deleted from the list after it is produced, and act only as separators where necessary.

## Integer constants
An integer constant is the prefix `0x` followed by hexadecimal digits (`0-9`, `a-f`, `A-F`), or `0` followed by octal digits (`0-7`), or decimal digits (beginning with something other than `0`). This is optionally followed by a suffix. If there is no suffix, it refers to an integer constant of type `int`; with the suffix `U`, type `uint`; with the suffix `L`, type `long`; and with suffix `UL`, type `ulong`. The suffixes are not case-sensitive.

# Syntax
## Compilation unit
```
CompilationUnit ::=
    NamespaceSpecification [ImportList] ToplevelDefinitionList

NamespaceSpecification ::=
    'namespace' NamespaceSpecifier ';'

NamespaceSpecifier ::=
    Identifier ['.' NamespaceSpecifier]

ImportList ::=
    ImportStatement [ImportList]

ImportStatement ::=
    'import' ImportPath ';'

ImportPath ::=
    Identifier [ImportPathComponent]

ImportPathComponent ::=
    '.' Identifier [ImportPathComponent]
    '.' '*'

ToplevelDefinitionList ::=
    ToplevelDefinition [ToplevelDefinitionList]

ToplevelDefinition ::=
    ClassDefinition
```

### Semantics
A compilation unit is by convention stored in a file with a file name extension of `.per`. It consists of three components: a _namespace specification_, an _import list_ and a _top-level definition list_.

The namespace specification specifies which namespace the definitions in the compilation unit belong to. The names of all top-level definitions in the compilation unit shall appear under this namespace.

The import list specifies which definitions shall be made visible to the compiler for use within the compilation unit. Each entry in the import list specifies either a fullname of a definition, in which case one definition is made available, or a wildcard, in which case all definitions under the specified namespace (but not its child namespaces) shall be made visible. In any case, when a definition is made visible, its shortname shall now refer to that definition.

The top-level definition list is a list of definitions defined by the compilation unit.

## Class Definition

```
ClassDefinition ::=
    AccessSpecifier [ClassQualifier] 'class' Identifier [TemplateSpecification]
    [ExtendsClause] ClassBody

AccessSpecifier ::=
    'private' | 'protected' | 'public'

ClassQualifier ::=
    'static' | 'abstract' | 'final'

ExtendsClause ::=
    'extends' TypeSpecificationList

TypeSpecificationList ::=
    TypeSpecification [',' TypeSpecificationList]

TemplateSpecification ::=
    '<' TemplateParameterList '>'

TemplateParameterList ::=
    Identifier ['extends' TypeSpecification] [',' TemplateParameterList]
```

### Constraints
Only one class qualifier, or none, may be applied. A class is either dynamic (the default without qualifiers) or it is `static`, or `final` or `abstract`.

If a class is defined as `static`, it must not have an `extends` clause.

No more than one class with the same name may be defined in each namespace.

All types given in the `extends` clause must be managed non-array types.

### Semantics
A class definition causes a class to become visible to the compiler. If a class is defined in a file being compiler, then an implementation of the class is emitted.

The identifier following the `class` keyword specifies the shortname of the class. The shortname shall be recognised as a type name referring to this class if used within the class body. The fullname of the class shall be its shortname inside the namespace specified in the namespace specification of the compilation unit in which it is defined.

An ABI class definition shall be emitted for every class which is not static.

The class body specifies a list of definitions of fields and methods within the class. If there is an `extends` clause, then the class shall inherit from all types specified in the `extends` clause list.

## Class Body
```
ClassBody ::=
	'{' [ClassMemberDefinitionList] '}'

ClassMemberDefinitionList ::=
	ClassMemberDefinition [ClassMemberDefinitionList]
	
ClassMemberDefinition ::=
		';'
	|	FieldDefinition
	|	MethodDefinition
	|	ConstructorDefinition
```

### Semantics
A class body contains one or more _class member definitions_. A class member definition which is simply a semicolon (`;`) is a _null definition_ and is simply ignored: thus, there may be extra semicolons before, inbetween, and after definitions.

## Type Specification
```
TypeSpecification ::=
		SingularTypeSpecification [ArrayTail]
	
ArrayTail ::=
		'[]' [ArrayTail]

SingularTypeSpecification ::=
		PrimitiveType
	|	Identifier ['<' TemplateParameterSpecificationList '>']

TemplateParameterSpecificationList ::=
		SingularTypeSpecification [',' TemplateParameterSpecificationList]

PrimitiveType ::=
		'void'
	|	'bool'
	|	'float'
	|	'double'
	|	'byte_t'
	|	'sbyte_t'
	|	'word_t'
	|	'sword_t'
	|	'dword_t'
	|	'sdword_t'
	|	'int'
	|	'uint'
	|	'qword_t'
	|	'sqword_t'
	|	'long'
	|	'ulong'
	|	'sptr_t'
	|	'ptr_t'
```

### Constraints
If an identifier is used instead of a primitive type, it must name a visible class. If the class does not have template parameters, there must not be any template parameter specification list. If it does, the number of entries in the template parameter specification list must equal the number of expected template parameters.

The types in the template parameter specification list must not be array types, and they must not be the `void` type.

The template parameter specifications must indicate types which are either subclasses of the minimum base class of the template parameter, or in the cases of primitive types, the _implicit managed wrapper_ corresponding to the primitive type used must have this property instead. If the parameter is itself a template parameter, then it must have either the same class as the minimum base, or a subclass thereof.

### Semantics
A type specification is used to specify a data type. A singular type specification is one which specifies a singular (non-array) type. Array tails may be attached to the end of any type to turn it into an array type, with the element type being the type preceding the array tail.

If the template parameter specification list contains primitive types, they are implicitly replaced with their corresponding _implicit managed types_.

## Field Definition
```
FieldDefinition ::=
	AccessSpecifier [FieldQualifierList] TypeSpecification Identifier [Initializer] ';'

FieldQualifierList ::=
	FieldQualifier [FieldQualifierList]

FieldQualifier ::=
	'static' | 'final'

Initializer ::=
	'=' Expression
```
### Constraints
A field defined in a static class must have the `static` qualifier applied.

The name of the field must be different than any field or method defined in the current class or any of its ancestors.

If a field is defined with the `final` qualifier, then it must include an initializer.

### Semantics
A field definition causes a field of a specific name to be created as a member of the class inside of which it was specified. If a field is defined as `static`, then a single copy of the field is created globally and all accesses to the field from any object (or lack thereof) refer to the same storage. Otherwise, the field exists independently inside each instance of the class.

If an initializer is present, it specifies an expression which will be evaluated at runtime and assigned as the initial value of the field. If the field is static, the expression is evaluated once before `main()` is entered. Otherwise it is evaluated prior to executing the constructor every time an instance of the class is created. This expression is not allowed to access any other fields, variables or methods, but may create instance of classes.

If a field is defined with the `final` qualifier, then its value cannot be changed after the initial value has been assigned; otherwise, the value may be updated an arbitrary number of times.

## Method Definition
```
MethodDefinition ::=
	AccessSpecifier [MethodQualifierList] TypeSpecification Identifier '(' [ArgumentSpecificationList] ')' MethodBody

MethodQualifierList ::=
	MethodQualifier [MethodQualifierList]

MethodQualifier ::=
	'static' | 'abstract' | 'final' | 'override'

ArgumentSpecificationList ::=
	TypeSpecification Identifier [',' ArgumentSpecificationList]

MethodBody ::=
		CompoundStatement
	|	';'
```

### Constraints
A method cannot be both `static` and `final`. A method cannot be both `static` and `abstract`.

There may not be multiple methods with the same signature defined in the same class. If a method with the same signature is defined in an ancestor class, then the `override` qualifier must be given. Otherwise, the `override` qualifier must not be given.

If a method is defined as `abstract`, it must not have a body.

### Semantics
A method definition causes a method to become a member of the class in which it was defined. If a method is defined as `static`, then it does not operate on an instance of a class, but is instead a globally available function, which can be called from a _static type value_. Non-static functions can only be invoked on an object, which can then be accessed by the code inside using the `this` keyword. For every non-static function, a static signature is implicitly generated, which is exactly the same, but takes the `this` value as the first argument. The static version can be used to explicitly invoke an implementation from a specific class, bypassing polymorphism.

If the method is not `abstract`, but there is no method body, then the compiler does not emit the static definition of the function at the ABI level, but still expects it to be provided elsewhere. This allows functions to be implemented in C or another language and be linked with Perun programs.

## Constructor Definition
```
ConstructorDefinition ::=
	AccessSpecifier ClassName '(' [ArgumentSpecificationList] ')' [':' SuperConstructorCallList] MethodBody

ClassName ::=
	Identifier

SuperConstructorCallList ::=
	Identifier '(' [ArgumentExpressionList] ')' [',' SuperConstructorCallList]
```

### Constraints
The `ClassName` production refers to an identifier which is the short name of the current class, and none else.

### Semantics


### Examples
```java
public class Test extends AnotherTest, ThirdTest
{
	public Test()
	{
		// default constructor, invokes default constructor
		// of AnotherTest and ThirdTest.
	};
	
	public Test(int a, float b) : AnotherTest(a, b)
	{
		// this constructor invokes AnotherTest(int a, float b), and then the
		// default constructor of ThirdTest.
	};

	public Test(int a, float b, String c) : AnotherTest(a, b), ThirdTest(c)
	{
		// this one explicitly invokes constructors of both classes.
	};
};
```

# Expression syntax
An expression evaluates to some value. In all cases, the value is "gettable": it can be used as an input elsewhere. In certain cases, the value is also settable: it can be used as the left side of an assignment. If an expression is settable, it might translate to different code to perform the setting, and different code to perform the getting. For example, the subscript expression on a managed type will invoke either the `opGet()` method or the `opSet()` method depending on whether it is get or set.

## Expression
```
Expression ::=
	AssignmentExpression [',' Expression]
```
### Semantics
An expression is a list of assignment expressions separated by commas (`,`). When an expression is evaluated, each assignment expression in the list is evaluated for its side effects, but the values are discarded, except for the last (rightmost) assignment expression, whose value becomes the value of the expression. The assignment expressions do not have to evaluate to the same type, because the type and value of the rightmost assignment expression is taken as the value of the expression.

## Assignment Expression
```
AssignmentExpression ::=
		ConditionalExpression
	|	PostfixExpression AssignmentOperator AssignmentExpression

AssignmentOperator ::=
	'=' | '+=' | '-=' | '*=' | '/=' | '<<=' | '>>=' |
	'%=' | '&=' | '|=' | '^='
```
### Constraints
The left side of an assignment expression must be a _settable value_.

### Semantics
If the assignment expression is a conditional expression, then the conditional expression is evaluated and its value is taken.

Otherwise, the assignment expression is made up of a _left side_ and a _right side_, with an assignment operator inbetween. The right side is evaluated first, thus giving the assignment operators a right-to-left grouping. Then, the left side must be a settable value which is then set to the value of the right side as a side effect, possibly after performing an operation on its original value (depending on the assignment operator used as indicated below). The expression overall takes the value which was actually set.

If `A` is the original value of the left side, and `B` is the value of the right side, then for each assignment operator, the expression behaves equivalently to the expression indicated in the table below. This includes executing any operator overloads. In the case of using the `=` operator, the left side is simply set to the value of the right side, without ever reading the left value.

|Assignment expression|Equivalent      |
|---------------------|----------------|
|`A += B`             |`A = A + B`     |
|`A -= B`             |`A = A - B`     |
|`A *= B`             |`A = A * B`     |
|`A /= B`             |`A = A / B`     |
|`A <<= B`            |`A = A << B`    |
|`A >>= B`            |`A = A >> B`    |
|`A %= B`             |`A = A % B`     |
|`A &= B`             |`A = A & B`     |
|`A |= B`             |`A = A | B`     |
|`A ^= B`             |`A = A ^ B`     |

## Conditional Expression
```
ConditionalExpression ::=
		LogicalORExpression '?' Expression ':' ConditionalExpression
	|	LogicalORExpression
```
### Constraints
The subject expression (the logical-OR-expression in the former production) must evaluate to a value of type `bool`.

The _false-value_ must be implicitly castable to the _true-value_ type.

### Semantics
If the conditional expression is a logical-OR-expression, then the logical-OR-expression is evaluated and its value is taken.

Otherwise, the conditional expression has 3 parts: the _subject_, the _true-value_ and the _false-value_. The _subject_ is an expression which must evaluate to a value of type `bool`. Both the _true-value_ and the _false-value_ are evaluated, and the value taken is _true-value_ if _subject_ evaluates to `true`, otherwise _false-value_. The _false-value_ is implicitly cast to the type of the _true-value_; if this is not possible, a compile-time error is raised. The type of the _true-value_ is also the type of the result.

## LogicalORExpression
```
LogicalORExpression ::=
	LogicalANDExpression ['||' LogicalORExpression]
```
### Constraints
If the `||` operator is used, then both operands must be values of type `bool`.
### Semantics
If only the logical-AND-expression is present (no `||` operator), then its value is taken.

Otherwise, the logical-OR-expression is a list of subexpressions separated by `||` with left-to-right grouping. For each pair of operands, both must be values of type `bool`. If the left operand has value `true`, then the result is the value `true`, and the right operand is never evaluated (and thus makes no side effects). If the left operand is `false`, then the value of the right operand is taken.

## LogicalANDExpression
```
LogicalANDExpression ::=
	BitwiseORExpression ['&&' LogicalANDExpression]
```
### Constraints
If the `&&` operator is used, then both operands must be values of type `bool`.
### Semantics
If only the bitwise-OR-expression is present (no `&&` operator), then its value is taken.

Otherwise, the logical-AND-expression is a list of subexpressions separated by `&&` with left-to-right grouping. For each pair of operands, both must be values of type `bool`. If the left operand has value `false`, then the result is the value `false`, and the right operand is never evaluated (and thus makes no side effects). If the left operand is `true`, then the value of the right operand is taken.

## Bitwise OR Expression
```
BitwiseORExpression ::=
	BitwiseXORExpression ['|' BitwiseORExpression]
```
### Semantics
If only the bitwise-XOR-expression is present, then its value is taken.

Otherwise, the bitwise-OR-expression is a list of subexpressions separated by `|` with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.
 
 After the promotions, the following actions are taken:
 
- If the left operand is an integer primitive type, then the right operand is converted to it via an implicit cast if possible, otherwise a compile-time error is raised. Then, the result has the same type as the left operand, and the result is the bitwise-OR of both operands.
- If the left operand is any other (non-integer) primitive type, then a compile-time error is raised.
- If the left operand is a managed type, then `A | B` is interpreted as `A.opBitwiseOR(B)`. If no `opBitwiseOR()` method is defined for the type of `A`, a compile-time error is raised.

## Bitwise XOR Expression
```
BitwiseXORExpression ::=
	BitwiseANDExpression ['^' BitwiseXORExpression]
```
### Semantics
If only the bitwise-AND-expression is present, then its value is taken.

Otherwise, the bitwise-XOR-expression is a list of subexpressions separated by `^` with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.
 
 After the promotions, the following actions are taken:
 
- If the left operand is an integer primitive type, then the right operand is converted to it via an implicit cast if possible, otherwise a compile-time error is raised. Then, the result has the same type as the left operand, and the result is the bitwise-XOR of both operands.
- If the left operand is any other (non-integer) primitive type, then a compile-time error is raised.
- If the left operand is a managed type, then `A ^ B` is interpreted as `A.opBitwiseXOR(B)`. If no `opBitwiseXOR()` method is defined for the type of `A`, a compile-time error is raised.

## Bitwise AND Expression
```
BitwiseANDExpression ::=
	EqualityExpression ['&' BitwiseANDExpression]
```
### Semantics
If only the equality expression is present, then its value is taken.

Otherwise, the bitwise-AND-expression is a list of subexpressions separated by `&` with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.
 
 After the promotions, the following actions are taken:
 
- If the left operand is an integer primitive type, then the right operand is converted to it via an implicit cast if possible, otherwise a compile-time error is raised. Then, the result has the same type as the left operand, and the result is the bitwise-AND of both operands.
- If the left operand is any other (non-integer) primitive type, then a compile-time error is raised.
- If the left operand is a managed type, then `A & B` is interpreted as `A.opBitwiseAND(B)`. If no `opBitwiseAND()` method is defined for the type of `A`, a compile-time error is raised.

## Equality Expression
```
EqualityExpression ::=
	RelationalExpression [EqualityOperator EqualityExpression]

EqualityOperator ::=
	'==' | '!='
```
### Semantics
If only the relational expression is present, then its value is taken.

Otherwise, the equality expression is a list of subexpressions separated by an equality operator with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.
 
 After the promotions, the following actions are taken in case of the `==` operator:
 
 - If the left type is a primitive type, then the right operand is converted to it via an implicit cast if necessary. If the conversion is not possible, a compile-time error is generated. Then, the result of the expression is a value of type `bool`, which has value `true` if both operands have the same value, otherwise `false`.
 - If the left type is a managed type, then a _managed equality test_ is performed.

A _managed equality test_ between `A` and `B` (that is, `A == B`) is performed as follows:

- If both operands are managed, and either has value `null`, then the result is `true` if and only if both are `null`, otherwise `false`.
- If both operands are the same object, the result is `true`.
- If the above did not yield a value, then `A == B` is interpreted as `A.opEquals(B)`. This is a method defined by `std.rt.Object` itself and is thus always present, and is guaranteed to return a value of type `bool`.

If the operator used is `!=`, the operation is exactly the same, except that the final result is inverted, so that `true` becomes `false` and vice versa.

The value of an equality expression always has type `bool`.

## Relational Expression
```
RelationalExpression ::=
	ShiftExpression [RelationalOperator RelationalExpression]
	
RelationalOperator ::=
	'<' | '>' | '<=' | '>='
```
### Semantics
If only the shift expression is present, then its value is taken.

Otherwise, a relational expression is a list of subexpression separated by relational operators with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.

After the promotions, a comparison is made as described below. The comparison performed depends on the operator, as follows:

|Operator|Comparison|
|--|--|
|`A < B`|`A` is strictly less than `B`|
|`A <= B`|`A` is less than, or equal to, `B`|
|`A > B`|`A` is strictly greater than `B`|
|`A >= B`|`A` is greater than or equal to `B`|

If the comparison statement is true, then the result is the value `true`, otherwise `false`. The value always has type `bool`.

The way the comparison is performed depends on the type of `A`:

- If the left operand has a primitive integer or floating-point type (this excludes `ptr_t` and `sptr_t`), then the right operand is converted to the same type by an implicit cast if necessary. If the conversion is not possible, then a compile-time error is raised. The comparison is then performed on the values of the operands.
- If the left operand is any other primitive type, then a compile-time error is raised.
- If the left operand is managed, then `A.opCompare(B)` is expected to return a value of a signed integer type. If this value is less than zero, then `A` is smaller than `B`, if it is zero then `A` equals `B`, otherwise `A` is greater than `B`. If the method does not exist or returns the wrong type, a compile-time error is raised.

The result is always a value of type `bool`.

## Shift Expression
```
ShiftExpression ::=
	AdditiveExpression [ShiftOperator ShiftExpression]

ShiftOperator ::=
	'<<' | '>>'
```

### Semantics
If only the additive expression is present, then its value is taken.

Otherwise, a shift expression is a list of subexpressions separated by shift operators, with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.

After the promotions, the following actions are taken:

- If the left operand has a primitive integer type, then the right operand must also be an integer type, or must be implicitly castable to type `uint` or `int`, in this order of preference. Then, the result has the same type as the left type, and it is shifted left (`<<`) or right (`>>`) by the number of bits indicated by the right operand. For right shifts, the shift is arithmetic if the left operand has a signed type.
- If the left operand is any other primitive type, a compile-time error is raised.
- If the left operand is managed, then `A << B` is interpreted as `A.opShiftLeft(B)`, and `A >> B` is interpreted as `A.opShiftRight(B)`. If the required method is not defined, a compile-time error is raised.

## Additive Expression
```
AdditiveExpression ::=
	MultiplicativeExpression [AdditiveOperator AdditiveExpression]

AdditiveOperator ::=
	'+' | '-'
```
### Semantics
If only the multiplicative expression is present, then its value is taken.

Otherwise, the additive expression is a list of subexpressions separated by additive operators with left-to-right grouping. For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.

After the promotions, the following actions are taken:

- If the left operand has a primitive integer or floating-point type, then the right operand is converted to the type of the left operand by an implicit cast if necessary. If the conversion is not possible, then a compile-time error is raised. Then, the result has the same type as the left operand, and it is either the sum (`+`) or difference (`-`) between the operand values.
- If the left operand has any other primitive type, then a compile-time error is raised.
- If the left operand is managed, then `A + B` is interpreted as `A.opAdd(B)`, and `A - B` is interpreted as `A.opSub(B)`. If the required method does not exist, then a compile-time error is raised.

## Multiplicative Expression
```
MultiplicativeExpression ::=
	CastExpression [MultiplicativeOperator MultiplicativeExpression]

MultiplicativeOperator ::=
	'*' | '/' | '%'
```
### Semantics
If only the cast expression is present, then its value is taken.

Otherwise, a multiplicative expression is a list of subexpressions separated by a multiplicative operator with left-to-right grouping.For each pair of operands, the left operand is first _promoted_ as follows:

 - If its type is a signed integer shorter than `int`, it is converted implicitly to `int`.
 - If its type is an unsigned integer shorter than `uint`, it is converted implicitly to `uint`.

After the promotions, the following actions are taken:

- If the left operand has a primitve integer or floating-point type, the right operand is converted to the type of the left operand by an implicit cast if necessary. If the conversion is not possible, then a compile-time error is raised. Then, the result has the same type as the left operand, and it is either the product (`*`) or ratio (`/`) between the operand values. In the case of the `%` operator, both types must be integer types, and the result is A modulo B.
- If the left operand has any other primitive type, then a compile-time error is raised.
- If the left operand is managed, then `A * B` is interpreted as `A.opMul(B)`, `A / B` is interpreted as `A.opDiv(B)`, and `A % B` is interpreted as `A.opMod(B)`. If the required method does not exist, then a compile-time error is raised.

## Cast Expression
```
CastExpression ::=
		'(' TypeSpecification ')' CastExpression
	|	UnaryExpression
```
### Semantics
If the unary expression production is used, then the value of the unary expression is taken.

Otherwise, the cast expression is made up of a subexpression and a type specification, and is grouped right-to-left. The subexpression is called the _source_ and has a _source type_, and the specified type is the _target type_. Then, the following operations are performed, known as an _explicit cast_:

- If the source type can be implicitly cast to the target type, then that is done. If both types are managed, the compiler may emit a warning about an unnecessary explicit cast.
- If both types are primitive arithmetic (integer or floating-point types, either sign), then the source is converted to the target type. If necessary, the value is truncated.
- If the target type is a subclass of the source type, then a _dynamic cast_ is performed. The source value remains unmodified but changes its type to the target type. At runtime, a check is performed: if the indicated value is not `null`, and is also not an instance of the target type, an exception of type `std.rt.DynamicCastError` is thrown.
- If the target type is managed, and the indicated class has a constructor which can take an input value of the source type as the sole argument, then a new instance of the target class is created, and this constructor is invoked on the source value, and the result is the new object.
- In all other cases, a compile-time error is raised.

## Unary Expression
```
UnaryExpression ::=
		PostfixExpression
	|	'++' UnaryExpression
	|	'--' UnaryExpression
	|	UnaryOperator CastExpression

UnaryOperator ::=
	'-' | '~' | '!'
```
### Semantics
If the postfix expression production is used, the value of the postfix expression is taken. Otherwise, the following productions are possible, with various semantics:

#### Pre-increment Operator `++`
- If the operand has a primitive integer type, then it must be settable. The value is first incremented by 1, and then the new value is the result. The value remains settable.
- If the operand has any other primitive type, a compile-time error is raised.
- If the operand has a managed type, then `++X` is interpreted as `X.opPreInc()`. If the method does not exist, then a compile-time error is raised.

#### Pre-decrement Operator `--`
- If the operand has a primitive integer type, then it must be settable. The value is first decremented by 1, and then the new value is the result. The value remains settable.
- If the operand has any other primitive type, a compile-time error is raised.
- If the operand has a managed type, then `--X` is interpreted as `X.opPreDec()`. If the method does not exist, then a compile-time error is raised.

#### Sign-negation operator `-`
The operand is first _promoted_, as follows:

- If it is a signed integer type shorter than `int`, it is converted to `int` implicitly.
- If it is an unsigned integer type shorter than `uint`, it is converted to `uint` implicitly.

After the promotions, the following actions are taken:

- If the operand is a primitive signed integer type, then the result is its sign-negation.
- If the operand is any other primitive type, a compile-time error is raised.
- If the operand is managed, then `-X` is interpreted as `X.opNegate()`.

#### Bitwise NOT Operator / Complement Operator `~`
- If the operand is a primitive integer type, the result is the bitwise-NOT of it: all bits are flipped.
- If the operand is any other primitive type, a compile-time error is raised.
- If the operand is a managed type, then `~X` is interpreted as `X.opInvert()`.

#### Logical NOT Operator `!`
- If the operand has type `bool`, then the result is its value inverted: `true` becomes `false` and vice versa.
- If the operand is any other type, a compile-time error is raised.

## Postfix Expression
```
PostfixExpression ::=
	PrimaryExpression [PostfixChain]

PostfixChain ::=
	Postfix [PostfixChain]

Postfix ::=
		SubscriptPostfix
	|	CallPostfix
	|	MemberPostfix
	|	InstancePostfix
	|	'++'
	|	'--'

SubscriptPostfix ::=
	'[' Expression ']'

CallPostfix ::=
	'(' [ArgumentExpressionList] ')'

ArgumentExpressionList ::=
	AssignmentExpression [',' ArgumentExpressionList]

MemberPostfix ::=
	'.' Identifier

InstancePostfix ::=
	'is' TypeSpecification
```
### Semantics
The value of the primary expression is taken, and then postfix operators are applied to it left-to-right. The postfix operators have semantics as defined below.

#### Subscript
The current value is known as the _subject_ and the parenthesized expression is the _subscript_. Depending on the type of the subject, the following actions are taken:

- If the subject has an array type, then the subscript is implicitly converted to either `uint` or `int` (in this order of preference). If that is not possible, a compile-time error is raised. The result is of the element type of the array, and it is the element whose index is given by the subscript. The value is settable, and the corresponding element in the array is changed if the value is assigned to. At runtime, if the index turns out to be outside the bounds of the array, an exception of type `std.rt.IndexError` is thrown.
- If the subject has any other non-managed type, then a compile-time error is raised.
- If the subject has a managed type, then `X[I]` is interpreted as `X.opGet(I)`. If the `opGet()` method is not defined, then a compile-time error is raised. Furthermore, if a method names `opSet()` exists for the subject, then the result includes a setter, such that `X[I] = V` is interpreted as `(X.opSet(I, V), V)`.

#### Calls
A call postfix can only be attached to values of the _method type_. The signature to be used for the call is found based on the types of arguments; see the section on _Method Signature Selection_ for more information.

#### Member access
The member access postfix requests access to a member of the current value, called the _subject_. The following semantics apply:

- If the subject has an array type, then accessing the member `length` results in a `uint` indicating the number of entries in the array. Attempting to access any other member results in a compile-time error.
- If the subject type is managed, then: If the identifier refers to a field in the subject class, the result is the settable value of that field. If the identifier refers to a method, a _method type_ value referring to this method is returned. The member must be visible according to its protection level. If at runtime the object is `null`, an exception of type `std.rt.NullReferenceError` is thrown.
- If the subject type is a _static class type_, then the same rules apply, but only static members can be accessed.
- In all other cases, a compile-time error is raised.

#### Instance testing
The expression `X is TypeSpecification` evaluates to `true` if `X` is an instance of the specified class at runtime, or `false` otherwise. The specified type must be managed. The test is performed by a call to the ABI function `Per_Is()`.

#### Post-decrement
- If the current object has a prititve integer type, then it must be settable. Its value is decremented by 1, and the _old_ value is returned.
- If the current object is any other primitive type, a compile-time error is raised.
- If the current object is managed, then `X--` is interpreted as `X.opPostDec()`.

#### Post-increment
- If the current object has a prititve integer type, then it must be settable. Its value is incremented by 1, and the _old_ value is returned.
- If the current object is any other primitive type, a compile-time error is raised.
- If the current object is managed, then `X++` is interpreted as `X.opPostInc()`.

## Primary Expression
```
PrimaryExpression ::=
		'(' Expression ')
	|	IntegerConstant
	|	FloatingPointConstant
	|	StringLiteral
	|	Identifier
	|	NewExpression
	|	'null'
	|	'true'
	|	'false'
	|	'this'
```
### Semantics
If the first production (parenthesized expression) is used, the value of the expression is taken.

If the integer constant is used, then an integer constant value is produced. The type of the value is `int` if there is no suffix, `uint` if the suffix is `U`, `long` if the suffix is `L`, or `ulong` if the suffix is `UL`.

If the floating-point constant is used, then a floating-point constant value is produced. The type of the value is `double` by default, or `float` if the `F` suffix is present.

If the string literal production is used, then the result is a value of type `std.rt.String` containing the string specified in the literal. This is produced by a call to the `Per_MakeString()` ABI function.

If the identifier production is used, then the result is a settable value of the named variable if present. If the identifier is the name of a visible class, then a _static type value_ for that class is produced. The _static type value_ is a special value which can have the member access postfix applied to it to access static members of the class, and can also be implicitly cast to type `std.rt.Class` referring to the named class.

If the `new`-expression production is present, then its value is taken.

The keyword `null` evaluates to the `null` value. It has the _null-type_, which can be implicitly cast to any managed type, and represents the concept of no object existing.

The keywords `true` and `false` evaluate to the corresponding constants of type `bool`.

The keyword `this` can only be used inside non-static functions, and evaluates to the object on which the method was invoked.

## `new` Expression
```
NewExpression ::=
		'new' TypeSpecification '(' [ArgumentExpressionList] ')'
	|	'new' TypeSpecification '[' Expression ']'
	|	'new' TypeSpecification '{' [ArgumentExpressionList] '}'
```
### Semantics
In the first production, the specified type must be a managed type. The expression evaluates to a new object of the specified type, and a constructor with the matching argument list is called on it. An argument list is matching if the types specified are either the types in the constructor or if they are subclasses of the types in the constructor. The constructor must be visible according to its protection level.

In the second production, the expression in square brackets must evaluate to a value which can be implicitly cast to `uint` or `int` (in this order of preference). The expression evaluates to an array, whose element type is the one given in the type specification, and whose size is given by the expression in the square brackets. The elements of the array are all set to the default values for the specified type.

In the third production, the expression evaluates to an array, and an array type must be specified. The array is initialized with values given in the braces. All expressions in the list in the braces must be implicitly castable to the element type. The array size, of course, matches the number of values given in the square braces.

### Examples
```cpp
// create a new object of type "MyClass", passing an integer to the constructor:
MyClass myObject = new MyClass(5);

// create an array of 6 floating-point values, all initially zero.
float[] myArray = new float[6];

// create an array of three integers, initializing them to specific values:
int x = 8;
int[] myInitArray = new int[] {2, x, 9};
```

# Statement syntax
```
Statement ::=
		CompoundStatement
	|	ExpressionStatement
	|	NullStatement
	|	VariableDefinition
	|	IfStatement
	|	WhileStatement
	|	ForStatement
	|	BreakStatement
	|	ContinueStatement
	|	ReturnStatement
	|	TryCatchStatement
	|	ThrowStatement
	|	DoWhileStatement
```

Statements are used in method bodies to denote actions to perform. The semantics of each statement specify what operation is performed of control reaches the statement.

All statements are executed within a _context_, which determines which variables it has access to. A context may have a _parent context_, in which case it accesses variables in the parent context (and all of its ancestors) too. When translating a method, there is a _root context_, which contains all the arguments. Then, the method body itself is executed in a subcontext of that.

A new context is created for every compound statement. Furthermore, some statements such as `try`-`catch` must create contexts even if a non-compund statement is provided. Others, such as `if`, do not create new contexts if a non-compund statements are used. This is explicitly specified in the semantics for each statement. The following examples demonstrate this:

```java
if (someValue) int a;
int b = a;		// OK, because 'a' was defined in this context

try int c;
catch (Exception e) {};

int d = c;		// ERROR: 'c' was not defined in this context
```

When we return to a parent context, the child context is _destroyed_. Any managed objects stored in its variables must be downreffed after that.

## Compound Statement
```
CompoundStatement ::=
	'{' [StatementList] '}'

StatementList ::=
	Statement [StatementList]
```
### Semantics
The compound statements puts a list of statements into a list. The compound statement is executed by executing each of the statements in the list in order. It also defines a new subcontext of the current execution context: variables defined inside a compound statement are deleted at the end of it, and are not visible outside it.

## Expression Statement
```
ExpressionStatement ::=
	Expression ';'
```
### Semantics
The expression statement contains an expression, and is executed by evaluating the expression, thus applying its side effects, and then discarding the resulting value.
 
## Null Statement
```
NullStatement ::=
	';'
```
### Semantics
The null statement specifies no actions to be performed and is ignored.

## Variable Definition
```
VariableDefinition ::=
	TypeSpecification Identifier [Initializer] ';'
```
### Semantics
A variable definition causes a new variable to be created in the current context. If the current context already has a variable with the specified name, a compile-time error is raised. If a variable with the same name is defined in a supercontext, then it is _shadowed_ by this new variable in the current context: all references to this name within the current context shall henceforth refer to this new variable.

If an initializer is present, then it is evaluated every time the variable definition is reached, and its result is stored in the variable. The value must be implicitly castable to the variable type, otherwise a compile-time error is raised. If no initializer is present, the variable is set to the default value its type whenever the definition is reached.

## `if` Statement
```
IfStatement ::=
	'if' '(' Expression ')' Statement ['else' Statement]
```
### Semantics
The _controlling expression_ is first evaluated, and it must be of type `bool`. If the value is `true` then the following statement is executed, otherwise it is skipped. If an `else` clause is present, then the statement in it is execute if the controlling expression evaluates to `false` instead.

## `while` Statement
```
WhileStatement ::=
	'while' '(' Expression ')' Statement
```

### Semantics
First, the controlling expression is evaluated, and must be a value of type `bool`. If its value is `false`, the statement is skipped. If the value is `true`, then the statement is executed, and the check is once again repeated, and this continues until the controlling expression evaluates to `false` or the loop is left using a `break` statement.

## `for` Statement
```
ForStatement ::=
	'for' '(' [Expression] ';' [Expression] ';' [Expression] ')' Statement
```
### Semantics
The `for` statement is made up of three expressions (each optional) and a statement known as the _body_. The three expressions, in order, are: the _initialization expression_, the _controlling expression_ and the _iteration expression_.

First, the _initialization expression_ is evaluated for its side effects, and value discarded, if one is present. Then, for each loop iteration, the _controlling expression_ is evaluated, and must be a value of type `bool`. If the controlling expression evaluated to `true` - or if none is present - then the _body_ is executed. After the body has been executed (including in the case where it reaches a `continue` statement), the _iteration expression_ is evaluated for its side effects, and the next loop iteration is then performed. This is repeated until the controlling expression evaluates to `false`, or until the loop is left using a `break` statement.

### Examples
```cpp
// calculate the sum of all integers up to and including 10
int sum = 0;
int i;
for (i=1; i<=10; i++)
{
	sum += i;
};

// infinite loop (never exits)
for (;;);
```

## `break` Statement
```
BreakStatement ::=
	'break' ';'
```
### Semantics
The `break` statement causes the innermost loop in which it is located to end, regardless of the value of its controlling expression. The controlling expression is not evaluated after ` break` statement. If there is no loop to leave, a compile-time error is raised.

## `continue` Statement
```
ContinueStatement ::=
	'continue' ';'
```
### Semantics
The `continue` statement causes the innermost loop in which it is located to continue to its next iteration. Any operations which would normally be performed after the loop body has executed, will be performed now. If there is no loop to continue, a compile-time error is raised.

## `return` Statement
```
ReturnStatement ::=
	'return' [Expression] ';'
```
### Semantics
The `return` statement causes a value to be returned from the function in which it appears. If the expression is present, then its type must be the same as the return type of the function, and this type must not be `void`. If there is no expression, then the return type of the function must be `void`.

## `try`-`catch` Statement
```
TryCatchStatement ::=
	'try' Statement CatchList

CatchList ::=
	'catch' '(' TypeSpecification Identifier ')' Statement [CatchList]
```
### Semantics
The statement within `try` is first executed inside a new context, and then the context is destroyed and we skip to the next statement. However, if an exception is thrown by any code inside the `try` block, then one of the `catch` blocks is executed.

Each type specification in a `catch` clause must name a type which is a subclass of `std.rt.Exception`. If the exception being thrown is an instance of the named class, then the `catch` block is executed in a new context, which includes a new variable of the named type, and the name given in the identifier, which is set to the exception that was thrown.

The `catch` clauses must be sorted such that if A is followed by B, then B must not be a subclass of A. If this constraint is not met, a compile-time error is raised.

### Examples
```java
try
{
	// do something with a 'null' value
	Object obj = null;
	obj.hash();
}
catch (NullReferenceError e)
{
	// exception has been caught!
};
```

## `throw` Statement
```
ThrowStatement ::=
	'throw' Expression ';'
```

### Semantics
The `throw` statement causes an exception to be thrown. The expression must evaluate to a value which is an instance of `std.rt.Exception`, otherwise a compile-time error is raised.

## `do..while` Statement
```
DoWhileStatement ::=
	'do' Statement 'while' '(' Expression ')' ';'
```

### Semantics
The semantics of the `do..while` statement are the same as the `while` statement, except that the body is executed at least once, without evaluating the controlling expression, and is then repeated if the controlling expression is `true`.

# Application Binary Interface (ABI)
## Object System
All Perun objects are represented by a `Per_Object` structure. Objects are passed by reference, and so all managed types in Perun are translated to a pointer to object (`Per_Object*`) type in C. Reference counting is used to implement automatic memory management. The `Per_Object` structure is opaque, and subject to change between ABI versions; only the functions provided by the library should be used to manipulate these structures. The Perun value `null` is represented by a C `NULL` pointer.

Each object contains a set of _components_, each of them being defined by a class in the hierarchy of its dynamic class. For example, if an object has type `B` which inherits `A`, there will be a component for class `A` and a component for class `B`. All objects have a component for the special class `std.rt.Object`. A component is a data structure defined separately for every class, and it contains the values of all of the class's non-static fields, and also pointers to implementations of virtual methods, as explained in the section on _Virtual Methods_. All components share the following header:

```c
typedef struct
{
    Per_ClassDesc *compClass;
    int compFlags;
    char compData[];
} Per_Component;
```

The `compClass` field indicates which class description the component is associated with; `compFlags` is initialized to zero, and is used to keep track of constructror invokations (more on that later); `compData` is data defined by the class, such as fields and virtual method slots.

When an object is first created, its reference count is set to 1. The reference count must be incremented using `Per_Up()` whenever a new reference is created which is to be manipulated by other code: typically, this happens when the object is assigned to a variable, etc. Whenever some code stops using an object, it must call `Per_Down()` to again decrement the reference count. If the reference count goes to 0, it means that nobody is using the object anymore, and the object is destroyed before `Per_Down()` returns. The prototypes for these functions are the following:

```c
void Per_Up(Per_Object *obj);
void Per_Down(Per_Object *obj);
```

## Object Creation and Destruction

Creating an object requires two steps: a call to `Per_New()`, and a call to a constructor. The `Per_New()` function has the following prototype:

```c
Per_Object* Per_New(Per_ClassDesc *desc);
```

## Class Description
Each non-static class has a _class description_ emitted, which describes the class at run-time.  Given the fullname of a class, a symbol is emitted with the name `Per_CD$(fullName)` pointing to a `Per_ClassDesc` structure. For example, the class `std.rt.String` will have a class description with the following external declaration:

```c
extern Per_ClassDesc Per_CD3std2rt7String;
```

The `Per_ClassDesc` structure has the following format:

```c
#define	PER_CD_ABSTRACT				(1 << 0)
#define	PER_CD_FINAL				(1 << 1)

typedef struct
{
	size_t cdSize;
	const char *cdFullName;
	size_t cdComponentSize;
	Per_ClassDesc **cdParents;
	void (*cdInit)(Per_Object *obj);
	void (*cdFini)(Per_Component *comp);
	void (*cdDefCtor)(Per_Object *obj);
	int cdFlags;
} Per_ClassDesc;
```

The fields have the following meanings:

- `cdSize` is the size of the structure itself, i.e. it must be set to `sizeof(Per_ClassDesc)`.
- `cdFullName` is a C string containing the fullname of the class, for example `std.rt.String`.
- `cdComponentSize` is the size of the component associated with objects of this class, including the header.
- `cdParents` is an array of pointers to parent classes, terminated with a `NULL` entry. The class `std.rt.Object` is always implicitly a parent, and does not have to be included in this list explicitly.
- `cdInit` is the _initializer_ for objects of this class, described in the section on the _Object System_.
- `cdFini` is the _finalizer_ for objects of this class, described in the section on the _Object System_.
- `cdDefCtor` is the default constructor for this class.
- `cdFlags` is a bitwise-OR of class flags (`PER_CD_*`). The `PER_CD_ABSTRACT` and `PER_CD_FINAL` bits are set for abstract and final classes, respectively.

## Calling convention and reference ownership rules
Static functions shall be invoked by a direct call from C. References to objects passed in arguments shall belong to the caller - thus, when the function returns, the reference count on these objects shall remain unchanged. If the caller no longer needs the references, it shall downref them after the callee returns.

If a function returns a managed object, the returned reference belongs to the caller, and it is the caller's responsibility to downref it. To prevent programs from linking when the caller and callee disagree on whether the return value is managed, information about whether the returned value is managed is encoded in the function's mangled name.

The mangled name for a static function has the format `Per_FD{M}$(functionPath){argumentTypeList}`. The qualifier `M` appears if the function returns a managed object. `functionPath` refers to the full path of the function (class fullname and function name), and `argumentTypeList` is the list of argument types taken by the function, mangled, each prefixed with `_`.

An array type is mangled by prefixing `A` to the mangled element type.

Primitive types are mangled as follows:

|Primitive type|Mangled name            |
|--------------|------------------------|
|`byte_t`      |`U8`                    |
|`sbyte_t`     |`I8`                    |
|`word_t`      |`U16`                   |
|`sword_t`     |`I16`                   |
|`uint`        |`U32`                   |
|`int`         |`I32`                   |
|`ulong`       |`U64`                   |
|`long`        |`I64`                   |
|`ptr_t`       |`UP`                    |
|`sptr_t`      |`SP`                    |
|`bool`        |`B`                     |
|`float`       |`F32`                   |
|`double`      |`F64`                   |

Managed types are mangled to the form `M$(classFullName)_{templateParameterCount}{templateParameterList}`. `templateParameterCount` is the number of template parameters accepted by the type, and `templateParameterList` is the list of template parameters, mangled, each prefixed with `_`. If any template parameters are primitive types, they are replaced by their implicit manager wrappers when mangling.

As an example, consider the following Perun class:

```cpp
namespace doc.example;

public class Example
{
	public static int testA(Object a, int b, String c);
	public static String testB();
	public static void testC();
	public static void testD(Map<String, int> a);
};
```

This would yield the following C prototypes:

```c
int32_t Per_FD3doc7example7Example5testA_M3std2rt7Object_0_I32_M3std2rt7String_0(Per_Object *a, int32_t b, Per_Object *c);
Per_Object* Per_FDM3doc7example7Example5testB();
void Per_FD3doc7example7Example5testC();
void Per_FD3doc7example7Example5testD_M3std4cont3Map_2_M3std2rt7String_0_3std2rt7Integer_0(Per_Object *a);
```

## Virtual methods

For every virtual method, there is a corresponding static method, which has the same argument list except that an addition first argument is used to pass the `this` value. This static version is known as the _implementor_. The component for the class in which the method is first defined (as opposed to overriden), shall contain a _virtual method slot_ for it, which is a C function pointer to the implementor. Each virtual method shall also have a _virtual indicator_ emitted for it, which is a data structure with symbol `Per_VF$(methodPath){argumentList}` (where the `argumentList` does NOT contain the `this` argument!). The virtual indicator structure is the following:

```c
typedef struct
{
    Per_ClassDesc *vfDefiner;
    off_t vfOffset;
} Per_VirtFunc;
```

The `vfDefiner` field points to the class description of the class where the virtual method was defined, and `vfOffset` is the offset into its component where the function pointer to the implementor is placed. The initializer of the class which defined the method shall call `Per_SetDynamicBinding()` to set the slot to the static implementor. This function has the following prototype:

```c
void Per_SetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf, void *implementor);
```

If a class override the method, then its initializer shall call `Per_SetDynamicBinding()` to set it to its own implementor; but this new class shall not define a new slot or virtual indicator. Thanks to this, there is only one indicator and one slot, which results in the correct implementor being chosen at runtime.

To call the method, it is necessary to call `Per_GetDynamicBinding()` to obtain the function pointer to the implementor, and then call the implementor. The argument list has the `this` value prepended to the beginning. The `Per_GetDynamicBinding()` function has the following prototype:

```c
void* Per_GetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf);
```

## Exceptions

Every thread has its own _exception context stack_, and the top entry is known as the _current exception context_. An exception context is described by the following structure, typically allocated on the stack:

```c
typedef struct
{
    Per_ExceptionContext* xcPrev;
    Per_Object* xcThrown;
    Per_ClassDesc** xcCatches;
    Per_Object*** xcRefs;
    int xcWhich;
    jmp_buf xcJmpBuf;
} Per_ExceptionContext;
```

The structure is initialized, and pushed onto the stack, by a call to `Per_Enter()`, which has the following prototype:

```c
void Per_Enter(Per_ExceptionContext *xc);
```

This function sets `xcPrev` to the previous exception context, `xcWhich` to `-1`, and`xcJmpBuf` to the current environment (via a call to `setjmp()`). All other values are set to `NULL`, and must be set up after the call to `Per_Enter()`. `Per_Enter()` may return twice: first when it is actually called (in which case `xcWhich` is `-1`), and a second time if an exception was thrown and caught (more on that later). The fields of `Per_ExceptionContext` have the following meanings:

- `xcPrev` is a link to the previous exception context.
- `xcThrown` is the exception object which was thrown, if an exception was caught.
- `xcCatches` is the list of class descriptions of exception types which are caught in this context, with the terminating entry set to `NULL`. If the value is `NULL`, it is as if the list was empty.
- `xcRefs` is a list of pointers to `Per_Object*` variables in the current context. If an exception is thrown which this context does not catch, these must be downreffed by a call to `Per_Down()`. More on this later.
- `xcWhich` is set to `-1` by `Per_Enter()`; when an exception is thrown, this is set to an index in `xcCatches` corresponding to the exception type which was caught.
- `xcJmpBuf` is the environment buffer for `setjmp()` and `longjmp()`.

A call to `Per_Leave()` pops contexts off the stack. Its prototype is the following:

```c
void Per_Leave(Per_ExceptionContext *xc);
```

The context `xc` as well as everything above it is popped off the stack. This allows you, if necessary, to leave more than one context at once.

An exception is thrown by passing it to `Per_Throw()`, which has the following prototype:

```c
void Per_Throw(Per_Object *obj);
```

The object passed in must be an instance of `std.rt.Exception`. If it is not, then an exception of type `std.rt.RuntimeError` is thrown instead. The function takes ownership of the reference. The exception context stack is traversed, with the following operations performed on each frame:

1. If the exception is an instance of one of the classes listed in `xcCatches`, then the exception is _caught_. `xcWhich` is then set to the index of the class in that array, and `xcThrown` is set to the exception object, then `longjmp()` is called to return to the environment set in `xcJmpBuf`. That environment now takes ownership of the reference to the exception object. In this case, the context is left, so the returning environment must not call `Per_Leave()`.
2. Otherwise, the variables pointed to by the `xcRefs` array are read, and passed to `Per_Down()`. This is beucase the context is being "destroyed", and any managed variables it defines must be downreffed. Then, the `xcPrev` context is taken, and these operations are repeated.
3. If we reach the bottom of the stack (i.e. `xcPrev` is `NULL`, and we cannot go further), and the exception is not caught, then a stack trace and an exception message are printed to standard error (`stderr`), and the program is aborted by a call to the C standard function `abort()`.

The following example shows how an exception context shoudl be set up, and how exceptions are to be caught:

```c
void example(Per_Object *thisval, Per_Object *anotherObj, int c)
{
    // define a variable which might hold an object
    Per_Object *msg = NULL;
    
    // make a list of variables which are managed, and must be
    // downreffered before returning, and also the list of
    // exception types that we catch.
    // note how arguments must have their reference counts
    // BALANCED (not decremented), so we don't put them here!
    Per_Object **refs[] = {&msg, NULL};
    Per_ClassDesc *catches[] = {&Per_CD3std2rt12RuntimeError, &Per_CD3std2rt9Exception, NULL};
    
    // enter the exception context
    Per_ExceptionContext ctx;
    Per_Enter(&ctx);
    ctx.xcRefs = refs;
    ctx.xcCatches = catches;
    
    if (ctx.xcWhich == -1)
    {
            // the "try" part is here, throw an exception.
            // remember that Per_Throw() takes ownership of the reference
            Per_Object *ex = Per_New(&Per_CD3std2rt12RuntimeError);
            msg = Per_MakeString("Hello, world!");
            Per_CC3std2rt12RuntimeError_M3std2rt6String_0(ex, msg);
            Per_Down(msg);
            msg = NULL;
            
            // throw the exception
            Per_Throw(ex);
    
	        // leave the context!
		    Per_Leave(&ctx);
    }
    else if (ctx.xcWhich == 0)
    {
            // caught a RuntimeError
            printf("caught a RuntimeError\n");
            
            // the reference is now ours so we must downref it
            Per_Down(ctx.xcThrown);
    }
    else
    {
            // we caught Exception (xcWhich == 1)
            printf("caught a generic Exception\n");
            
            // again we must downref
            Per_Down(ctx.xcThrown);
    }
}
```
