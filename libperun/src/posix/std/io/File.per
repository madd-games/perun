/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.io;

import std.posix.Posix;
import std.posix.PosixError;

/**
 * An I/O stream representing files.
 */
public class File extends IOStream, Seekable
{
	/**
	 * The file descriptor to use.
	 */
	private int fd;
	
	/**
	 * Hide the default constructor.
	 */
	private File()
	{
		throw new RuntimeError("default constructor of File called!");
	};
	
	/**
	 * POSIX-specific constructor: make an object referring to a specific file descriptor. When the
	 * file is closed, the descriptor is too.
	 */
	public File(int fd)
	{
		this.fd = fd;
	};
	
	/**
	 * Open a file.
	 *
	 * \param filename Path to the file.
	 * \param flags File open flags (see `FileFlags` for more info).
	 * \throws std.io.IOError If the file cannot be opened with the specified flags.
	 * \returns A `File` instance referring to the file which was opened.
	 */
	public static File open(Path filename, FileFlags flags)
	{
		int fd;
		try
		{
			fd = Posix.open(filename.toString(), flags.toPosix(), 0644);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
		
		return new File(fd);
	};
	
	protected override int readBytes(byte_t[] bytes)
	{
		try
		{
			return Posix.read(fd, bytes);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
	};
	
	protected override void writeBytes(byte_t[] bytes)
	{
		int offset = 0;
		while (offset < bytes.length)
		{
			try
			{
				offset += Posix.write(fd, offset, bytes);
			}
			catch (PosixError e)
			{
				throw new IOError(e.getMessage());
			};
		};
	};
	
	public override void seek(long pos)
	{
		try
		{
			Posix.lseek(fd, (long) pos, Posix.SEEK_SET);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
	};
	
	public override long tell()
	{
		try
		{
			return (long) Posix.lseek(fd, 0L, Posix.SEEK_CUR);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
	};
	
	public override void seekToEnd()
	{
		try
		{
			Posix.lseek(fd, 0L, Posix.SEEK_END);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
	};
	
	/**
	 * Close the file.
	 */
	public void close()
	{
		try
		{
			Posix.close(fd);
		}
		catch (PosixError e)
		{
			throw new IOError(e.getMessage());
		};
	};
	
	/**
	 * Return the file descriptor (Posix only).
	 */
	public int getFD()
	{
		return fd;
	};
};
