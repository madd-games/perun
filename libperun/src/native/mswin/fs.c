/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <windows.h>
#include <assert.h>
#include <stdio.h>

extern Per_ClassDesc Per_CD3std4time8DateTime;
void Per_CC3std4time8DateTime_I64_I64(Per_Object *obj, Per_long sec, Per_long nano);

static void getTimes(const char *path, LPFILETIME acc, LPFILETIME mod)
{
	HANDLE hFile = CreateFile(
		path,
		GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL
	);
	
	if (hFile == INVALID_HANDLE_VALUE)
	{
		Per_ThrowSimple(Per_IOError, "could not open the file");
	};
	
	GetFileTime(hFile, NULL, acc, mod);
	CloseHandle(hFile);
};

static Per_Object* makeDateTime(LPFILETIME lpTime)
{
	const int64_t UNIX_TIME_START = 0x019DB1DED53E8000;
	const int64_t TICKS_PER_SECOND = 10000000;

	LARGE_INTEGER li;
	li.LowPart  = lpTime->dwLowDateTime;
	li.HighPart = lpTime->dwHighDateTime;

	Per_Object *obj = Per_New(&Per_CD3std4time8DateTime);
	Per_CC3std4time8DateTime_I64_I64(obj,
				(li.QuadPart - UNIX_TIME_START) / TICKS_PER_SECOND,
				(li.QuadPart - UNIX_TIME_START) % TICKS_PER_SECOND * 100UL
	);
	
	return obj;	
};

Per_Object* Per_FDM3std2io10FileSystem13getAccessTime_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	FILETIME t;
	getTimes(path, &t, NULL);
	return makeDateTime(&t);
};

Per_Object* Per_FDM3std2io10FileSystem19getModificationTime_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	FILETIME t;
	getTimes(path, NULL, &t);
	return makeDateTime(&t);
};

void Per_FD3std2io10FileSystem15createDirectory_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	if (!CreateDirectoryA(path, NULL))
	{
		Per_ThrowSimple(Per_IOError, "could not create the directory");
	};
};

Per_bool Per_FD3std2io10FileSystem11isDirectory_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	DWORD attr = GetFileAttributesA(path);
	if (attr == INVALID_FILE_ATTRIBUTES)
	{
		return PER_FALSE;
	};
	
	return attr & FILE_ATTRIBUTE_DIRECTORY;
};

Per_Object** Per_FDA3std2io10FileSystem9__listdir_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	char *wildcard = (char*) malloc(strlen(path) + 8);
	sprintf(wildcard, "%s/*.*", path);
	
	Per_Object **names = NULL;
	int numNames = 0;
	
	WIN32_FIND_DATAA fd;
	HANDLE hFind = FindFirstFileA(wildcard, &fd);
	
	if (hFind == INVALID_HANDLE_VALUE)
	{
		free(wildcard);
		return NULL;
	};
	
	do
	{
		const char *entName = fd.cFileName;
		if (strcmp(entName, ".") != 0 && strcmp(entName, "..") != 0)
		{
			int index = numNames++;
			names = (Per_Object**) realloc(names, sizeof(void*) * numNames);
			names[index] = Per_MakeString(entName);
		};
	} while (FindNextFileA(hFind, &fd));
	
	FindClose(hFind);
	free(wildcard);
	
	Per_Object **array = (Per_Object**) Per_NewArray(numNames, sizeof(void*), PER_TYPESECT_MANAGED);
	memcpy(array, names, sizeof(void*) * numNames);
	free(names);
	return array;
};