/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <time.h>
#include <windows.h>

typedef struct 
{
	Per_Component head;
	Per_long sec;
	Per_long nano;
} Per_DateTime;

extern Per_ClassDesc Per_CD3std4time8DateTime;

void Per_CC3std4time8DateTime_I64_I64(Per_Object *obj, Per_long sec, Per_long nano);

Per_Object* Per_FDM3std4time8DateTime3now()
{
	const int64_t UNIX_TIME_START = 0x019DB1DED53E8000;
	const int64_t TICKS_PER_SECOND = 10000000;
	
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);

	LARGE_INTEGER li;
	li.LowPart  = ft.dwLowDateTime;
	li.HighPart = ft.dwHighDateTime;

	Per_Object *obj = Per_New(&Per_CD3std4time8DateTime);
	Per_CC3std4time8DateTime_I64_I64(obj,
				(li.QuadPart - UNIX_TIME_START) / TICKS_PER_SECOND,
				(li.QuadPart - UNIX_TIME_START) % TICKS_PER_SECOND * 100UL
	);
	
	return obj;
};

static int SystemTime2StructTM(SYSTEMTIME *st, struct tm * ptm)
{
	ptm->tm_isdst = -1; /* mktime() computes whether this is */
	/* during Standard or Daylight time. */
	ptm->tm_sec = (int)st->wSecond;
	ptm->tm_min = (int)st->wMinute;
	ptm->tm_hour = (int)st->wHour;
	ptm->tm_mday = (int)st->wDay;
	ptm->tm_mon = (int)st->wMonth - 1;
	ptm->tm_year = (int)st->wYear - 1900;
	ptm->tm_wday = (int)st->wDayOfWeek;

	/* Normalize uninitialized fields */
	if ((time_t)mktime(ptm) == (time_t)-1)
	return 0;

	return 1;
}

Per_Object* Per_FDM3std4time8DateTime6format_M3std4time8DateTime_0_M3std2rt6String_0(Per_Object *thisptr, Per_Object *formatObj)
{
	Per_DateTime *dt = (Per_DateTime*) Per_GetRequiredComponent(thisptr, &Per_CD3std4time8DateTime);
	
	LONGLONG ll;
	FILETIME ft;
	
	ll = Int32x32To64(dt->sec, 10000000) + 116444736000000000 + (dt->nano/100);
	ft.dwLowDateTime = (DWORD)ll;
	ft.dwHighDateTime = ll >> 32;
	
	SYSTEMTIME st;
	FileTimeToSystemTime(&ft, &st);
	
	struct tm tm;
	SystemTime2StructTM(&st, &tm);
	
	char buffer[1024];
	size_t sz = strftime(buffer, 1024, Per_ReadString(formatObj), &tm);
	
	if (sz == 0)
	{
		buffer[0] = 0;
	};
	
	return Per_MakeString(buffer);
};
