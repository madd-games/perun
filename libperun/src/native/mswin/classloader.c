/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <windows.h>
#include <assert.h>

extern Per_ClassDesc Per_CD3std2rt11ClassLoader;

void* Per_FD3std2rt11ClassLoader7libOpen_M3std2rt6String_0(Per_Object *pathobj)
{
	HMODULE hModule = LoadLibraryA(Per_ReadString(pathobj));
	if (hModule == NULL)
	{
		Per_ThrowFormat(Per_RuntimeError, "could not find/load %s", Per_ReadString(pathobj));
	};
	
	return hModule;
};

Per_Object* Per_FDM3std2rt11ClassLoader16libClassBySymbol_UP_M3std2rt6String_0(HMODULE hModule, Per_Object *symobj)
{
	const char *symbol = Per_ReadString(symobj);
	Per_ClassDesc *desc = (Per_ClassDesc*) GetProcAddress(hModule, symbol);
	
	if (desc == NULL) return NULL;
	else return Per_WrapClass(desc);
};
