/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <dbghelp.h>

extern Per_Object* Per_SFM3std2rt6System7ENDLINE;
extern Per_Object* Per_SFM3std2rt6System14PATH_SEPARATOR;
extern Per_Object* Per_SFM3std2rt6System10EXE_SUFFIX;
extern Per_Object* Per_SFM3std2rt6System10LIB_SUFFIX;
extern Per_Object* Per_SFM3std2rt6System5stdin;
extern Per_Object* Per_SFM3std2rt6System6stdout;
extern Per_Object* Per_SFM3std2rt6System6stderr;

static DWORD dwExceptKey;

#define	fileFromStdHandle Per_FDM3std2io4File13fromStdHandle_U32
extern Per_Object* Per_FDM3std2io4File13fromStdHandle_U32(DWORD nStdHandle);

void Per_SysInit()
{
	dwExceptKey = TlsAlloc();
	assert(dwExceptKey != TLS_OUT_OF_INDEXES);
	
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,2), &wsaData);
	
	SymInitialize(GetCurrentProcess(), NULL, TRUE);
	
	Per_SFM3std2rt6System7ENDLINE = Per_MakeString("\r\n");
	Per_SFM3std2rt6System14PATH_SEPARATOR = Per_MakeString("\\");
	
	Per_SFM3std2rt6System10EXE_SUFFIX = Per_MakeString(".exe");
	Per_SFM3std2rt6System10LIB_SUFFIX = Per_MakeString(".dll");
	
	Per_SFM3std2rt6System5stdin = fileFromStdHandle(STD_INPUT_HANDLE);
	Per_SFM3std2rt6System6stdout = fileFromStdHandle(STD_OUTPUT_HANDLE);
	Per_SFM3std2rt6System6stderr = fileFromStdHandle(STD_ERROR_HANDLE);
};

void Per_SysThreadInit()
{
	TlsSetValue(dwExceptKey, NULL);
};

void Per_SysSetExceptionContext(Per_ExceptionContext *ctx)
{
	TlsSetValue(dwExceptKey, ctx);
};

Per_ExceptionContext* Per_SysGetExceptionContext()
{
	return (Per_ExceptionContext*) TlsGetValue(dwExceptKey);
};

Per_StackFrame* Per_SysStackTrace()
{
	PVOID pvFrames[50];
	USHORT sFrameCount = CaptureStackBackTrace(1, 50, pvFrames, NULL);
	
	Per_StackFrame *first = NULL;
	Per_StackFrame *last = NULL;
	
	USHORT i;
	for (i=0; i<sFrameCount; i++)
	{
		char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
		PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)buffer;

		pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
		pSymbol->MaxNameLen = MAX_SYM_NAME;

		Per_StackFrame *frame = (Per_StackFrame*) malloc(sizeof(Per_StackFrame));
		frame->next = NULL;
		
		DWORD64 dwDisplacement;
		if (SymFromAddr(GetCurrentProcess(), (DWORD64) pvFrames[i], &dwDisplacement, pSymbol))
		{
			frame->symbolName = strdup(pSymbol->Name);
		}
		else
		{
			frame->symbolName = strdup("<?>");
		}
		
		if (first == NULL)
		{
			first = last = frame;
		}
		else
		{
			last->next = frame;
			last = frame;
		};
	};
	
	return first;
};

void Per_Sys_ExitThread()
{
	ExitThread(0);
};
