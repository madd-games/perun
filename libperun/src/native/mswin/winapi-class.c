/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <windows.h>
#include <stdio.h>

Per_int Per_FD3std5mswin6WinAPI11MessageBoxA_UP_AU8_AU8_U32(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, DWORD uType)
{
	return MessageBoxA(hWnd, lpText, lpCaption, uType);
};

Per_int Per_FD3std5mswin6WinAPI11MessageBoxW_UP_AU8_AU8_U32(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, DWORD uType)
{
	return MessageBoxW(hWnd, lpText, lpCaption, uType);
};

HANDLE Per_FD3std5mswin6WinAPI11CreateFileA_AU8_U32_U32_AU8_U32_U32_UP(
	LPCSTR                lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
)
{
	return CreateFileA(
		lpFileName,
		dwDesiredAccess,
		dwShareMode,
		lpSecurityAttributes,
		dwCreationDisposition,
		dwFlagsAndAttributes,
		hTemplateFile
	);
};

HANDLE Per_FD3std5mswin6WinAPI11CreateFileW_AU8_U32_U32_AU8_U32_U32_UP(
	LPCWSTR               lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
)
{
	return CreateFileW(
		lpFileName,
		dwDesiredAccess,
		dwShareMode,
		lpSecurityAttributes,
		dwCreationDisposition,
		dwFlagsAndAttributes,
		hTemplateFile
	);
};

Per_bool Per_FD3std5mswin6WinAPI9WriteFile_UP_AU8_U32_AU32_AU8(
	HANDLE       hFile,
	LPCVOID      lpBuffer,
	DWORD        nNumberOfBytesToWrite,
	LPDWORD      lpNumberOfBytesWritten,
	LPOVERLAPPED lpOverlapped
)
{
	return WriteFile(
		hFile,
		lpBuffer,
		nNumberOfBytesToWrite,
		lpNumberOfBytesWritten,
		lpOverlapped
	);
};

Per_bool Per_FD3std5mswin6WinAPI8ReadFile_UP_AU8_U32_AU32_AU8(
	HANDLE       hFile,
	LPVOID       lpBuffer,
	DWORD        nNumberOfBytesToWrite,
	LPDWORD      lpNumberOfBytesWritten,
	LPOVERLAPPED lpOverlapped
)
{
	return ReadFile(
		hFile,
		lpBuffer,
		nNumberOfBytesToWrite,
		lpNumberOfBytesWritten,
		lpOverlapped
	);
};

Per_bool Per_FD3std5mswin6WinAPI11CloseHandle_UP(HANDLE hObject)
{
	return CloseHandle(hObject);
};

Per_dword Per_FD3std5mswin6WinAPI12GetLastError()
{
	return GetLastError();
};

void Per_FD3std5mswin6WinAPI12SetLastError_U32(DWORD dwLastError)
{
	SetLastError(dwLastError);
};

HANDLE Per_FD3std5mswin6WinAPI12GetStdHandle_U32(DWORD nStdHandle)
{
	return GetStdHandle(nStdHandle);
};

Per_Object* Per_FDM3std5mswin6WinAPI20Madd_GetErrorMessage_U32(DWORD dwError)
{
	if (dwError == 0)
	{
		return Per_MakeString("No error");
	};

	LPSTR messageBuffer = NULL;
	FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);
	
	Per_Object *result = Per_MakeString(messageBuffer);
	LocalFree(messageBuffer);
	return result;
};

DWORD Per_FD3std5mswin6WinAPI14SetFilePointer_UP_I32_AI32_U32(
	HANDLE hFile,
	LONG   lDistanceToMove,
	PLONG  lpDistanceToMoveHigh,
	DWORD  dwMoveMethod
)
{
	return SetFilePointer(
		hFile,
		lDistanceToMove,
		lpDistanceToMoveHigh,
		dwMoveMethod
	);
};

Per_bool Per_FD3std5mswin6WinAPI14GetConsoleMode_UP_AU32(
	HANDLE  hConsoleHandle,
	LPDWORD lpMode
)
{
	return GetConsoleMode(hConsoleHandle, lpMode);
};

Per_bool Per_FD3std5mswin6WinAPI23SetConsoleTextAttribute_UP_U16(
	HANDLE hConsoleOutput,
	WORD   wAttributes
)
{
	return SetConsoleTextAttribute(hConsoleOutput, wAttributes);
};

void Per_FD3std5mswin6WinAPI17Madd_ClearConsole_UP(HANDLE hConsoleOutput)
{
	COORD coordScreen = { 0, 0 };
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	GetConsoleScreenBufferInfo( hConsoleOutput, &csbi );
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	
	FillConsoleOutputCharacter( hConsoleOutput, (TCHAR) ' ',
		dwConSize, coordScreen, &cCharsWritten );

	GetConsoleScreenBufferInfo( hConsoleOutput, &csbi );
	FillConsoleOutputAttribute( hConsoleOutput, csbi.wAttributes,
		dwConSize, coordScreen, &cCharsWritten );

	SetConsoleCursorPosition( hConsoleOutput, coordScreen );
};
