/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <windows.h>
#include <string.h>

typedef struct
{
	Per_Component header;
	HANDLE hProcess;
} Per_Process;

extern Per_ClassDesc Per_CD3std4proc7Process;

Per_Object* Per_FDM3std4proc7Process5__run_M3std2rt6String_0_AM3std2rt6String_0_AM3std2rt6String_0_M3std2rt6String_0(
	Per_Object *exeObj,
	Per_Object **argObjs,
	Per_Object **envObjs,
	Per_Object *cwdObj
)
{
	const char *exe = Per_ReadString(exeObj);
	const char *cwd = Per_ReadString(cwdObj);
	
	uint32_t argc = Per_ArrayLength(argObjs);
	unsigned long cmdlineSize = 1;
	
	uint32_t envc = Per_ArrayLength(envObjs);
	unsigned long envBlockSize = 1;
	
	uint32_t i;
	for (i=0; i<argc; i++)
	{
		cmdlineSize += 2 * strlen(Per_ReadString(argObjs[i])) + 3;
	};
	
	for (i=0; i<envc; i++)
	{
		envBlockSize += strlen(Per_ReadString(envObjs[i])) + 1;
	};
	
	// decode the command-line arguments from sensible to windows
	char *cmdline = (char*) malloc(cmdlineSize);
	char *put = cmdline;
	
	for (i=0; i<argc; i++)
	{
		const char *scan = Per_ReadString(argObjs[i]);
		
		*put++ = '"';
		
		while (*scan != 0)
		{
			int numBackslashes = 0;
			while (*scan != 0 && *scan == '\\')
			{
				scan++;
				numBackslashes++;
			};
			
			if (*scan == 0)
			{
				memset(put, '\\', numBackslashes * 2);
				put += (numBackslashes * 2);
			}
			else if (*scan == '"')
			{
				memset(put, '\\', numBackslashes * 2 + 1);
				put += (numBackslashes * 2 + 1);
				*put++ = *scan++;
			}
			else
			{
				memset(put, '\\', numBackslashes);
				put += numBackslashes;
				*put++ = *scan++;
			};
		};
		
		*put++ = '"';
		*put++ = ' ';
	};
	
	*--put = 0;	// (replace the last space with NUL)
	
	// now create the environment block
	char *envBlock = (char*) malloc(envBlockSize);
	put = envBlock;
	
	for (i=0; i<envc; i++)
	{
		const char *ent = Per_ReadString(envObjs[i]);
		strcpy(put, ent);
		put += strlen(ent) + 1;
	};
	
	*put = 0;	// (final list terminator)
	
	STARTUPINFOA si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	
	PROCESS_INFORMATION pi;
	memset(&pi, 0, sizeof(pi));
	
	BOOL success = CreateProcessA(
		exe,						// lpApplicationName
		cmdline,					// lpCommandLine
		NULL,						// lpProcessAttributes
		NULL,						// lpThreadAttributes
		TRUE,						// bInheritHandles
		NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,	// dwCreationFlags
		envBlock,					// lpEnvironment
		cwd,						// lpCurrentDirectory
		&si,						// lpStartupInfo
		&pi						// lpProcessInformation
	);
	
	free(cmdline);
	free(envBlock);
	
	if (!success)
	{
		Per_ThrowSimple(Per_ProcessError, "CreateProcess failed");
	};
	
	Per_Object *proc = Per_New(&Per_CD3std4proc7Process);
	Per_Process *comp = (Per_Process*) Per_GetRequiredComponent(proc, &Per_CD3std4proc7Process);
	comp->hProcess = pi.hProcess;
	CloseHandle(pi.hThread);
	return proc;
};


Per_int Per_FD3std4proc7Process4wait_M3std4proc7Process_0(Per_Object *proc)
{
	Per_Process *comp = (Per_Process*) Per_GetRequiredComponent(proc, &Per_CD3std4proc7Process);
	
	DWORD status;
	WaitForSingleObject(comp->hProcess, INFINITE);
	BOOL success = GetExitCodeProcess(comp->hProcess, &status);
	if (!success)
	{
		Per_ThrowSimple(Per_ProcessError, "GetExitCodeProcess() failed (did you call wait() on an already-collected process?)");
	};
	
	return status;
};