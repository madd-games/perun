/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>

extern Per_ClassDesc Per_CD3std2rt7Integer;
extern Per_ClassDesc Per_CD3std2rt15UnsignedInteger;
extern Per_ClassDesc Per_CD3std2rt7Boolean;
extern Per_ClassDesc Per_CD3std2rt5Float;

void Per_CC3std2rt7Integer_I64(Per_Object *obj, Per_long value);
void Per_CC3std2rt15UnsignedInteger_U64(Per_Object *obj, Per_qword value);
void Per_CC3std2rt7Boolean_B(Per_Object *obj, Per_bool value);
void Per_CC3std2rt5Float_F64(Per_Object *obj, Per_double value);

typedef struct
{
	Per_Component header;
	Per_long value;
} Per_SignedComponent;

typedef struct
{
	Per_Component header;
	Per_qword value;
} Per_UnsignedComponent;

typedef struct
{
	Per_Component header;
	Per_bool value;
} Per_BooleanComponent;

typedef struct
{
	Per_Component header;
	Per_double value;
} Per_FloatComponent;

Per_Object* Per_WrapSigned(Per_long value)
{
	Per_Object *wrapper = Per_New(&Per_CD3std2rt7Integer);
	Per_CC3std2rt7Integer_I64(wrapper, value);
	return wrapper;
};

Per_long Per_UnwrapSigned(Per_Object *wrapper)
{
	Per_SignedComponent *comp = (Per_SignedComponent*) Per_GetRequiredComponent(wrapper, &Per_CD3std2rt7Integer);
	return comp->value;
};

Per_Object* Per_WrapUnsigned(Per_qword value)
{
	Per_Object *wrapper = Per_New(&Per_CD3std2rt15UnsignedInteger);
	Per_CC3std2rt15UnsignedInteger_U64(wrapper, value);
	return wrapper;
};

Per_qword Per_UnwrapUnsigned(Per_Object *wrapper)
{
	Per_UnsignedComponent *comp = (Per_UnsignedComponent*) Per_GetRequiredComponent(wrapper, &Per_CD3std2rt15UnsignedInteger);
	return comp->value;
};

Per_Object* Per_WrapBoolean(Per_bool value)
{
	Per_Object *wrapper = Per_New(&Per_CD3std2rt7Boolean);
	Per_CC3std2rt7Boolean_B(wrapper, value);
	return wrapper;
};

Per_bool Per_UnwrapBoolean(Per_Object *wrapper)
{
	Per_BooleanComponent *comp = (Per_BooleanComponent*) Per_GetRequiredComponent(wrapper, &Per_CD3std2rt7Boolean);
	return comp->value;
};

Per_Object* Per_WrapFloat(Per_double value)
{
	Per_Object *wrapper = Per_New(&Per_CD3std2rt5Float);
	Per_CC3std2rt5Float_F64(wrapper, value);
	return wrapper;
};

Per_double Per_UnwrapFloat(Per_Object *wrapper)
{
	Per_FloatComponent *comp = (Per_FloatComponent*) Per_GetRequiredComponent(wrapper, &Per_CD3std2rt5Float);
	return comp->value;
};
