/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/**
 * Implement std.priv.Runtime.malloc().
 */
void* Per_FD3std4priv7Runtime6malloc_U32(Per_dword size)
{
	void *result = malloc(size);
	assert(result != NULL);
	return result;
};

/**
 * Implement std.priv.Runtime.free().
 */
void Per_FD3std4priv7Runtime4free_UP(void *ptr)
{
	free(ptr);
};

/**
 * Implement std.priv.Runtime.memset().
 */
void Per_FD3std4priv7Runtime6memset_UP_U8_U32(void *dest, Per_byte byte, Per_dword count)
{
	memset(dest, byte, count);
};

/**
 * Implement std.priv.Runtime.memcpy().
 */
void Per_FD3std4priv7Runtime6memcpy_UP_UP_U32(void *dest, void *src, Per_dword count)
{
	memcpy(dest, src, count);
};

/**
 * Implement std.priv.Runtime.strlen().
 */
Per_dword Per_FD3std4priv7Runtime6strlen_UP(const char *str)
{
	return (Per_dword) strlen(str);
};

/**
 * Implement std.priv.Runtime.getByteArrayPointer().
 */
void* Per_FD3std4priv7Runtime19getByteArrayPointer_AU8(void *ar)
{
	return ar;
};

/**
 * Implement std.priv.Runtime.strcmp().
 */
Per_int Per_FD3std4priv7Runtime6strcmp_UP_UP(const char *a, const char *b)
{
	return strcmp(a, b);
};

/**
 * Implement std.priv.Runtime.strstr().
 */
char* Per_FD3std4priv7Runtime6strstr_UP_UP(const char *haystack, const char *needle)
{
	return strstr(haystack, needle);
};

/**
 * Implement std.io.FileSystem.remove().
 */
void Per_FD3std2io10FileSystem6remove_M3std2rt6String_0(Per_Object *pathObj)
{
	const char *path = Per_ReadString(pathObj);
	if (remove(path) != 0)
	{
		Per_ThrowSimple(Per_IOError, "cannot delete file or directory");
	};
};