/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

extern Per_ClassDesc Per_CD3std2rt9Exception;
extern Per_ClassDesc Per_CD3std2rt6String;

/**
 * The Exception component. It must match the definition in Exception.per.
 */
typedef struct
{
	Per_Component hdr;
	Per_Object *msg;
} Per_Exception;

void Per_Enter(Per_ExceptionContext *ctx)
{
	ctx->xcPrev = Per_SysGetExceptionContext();
	ctx->xcThrown = NULL;
	ctx->xcCatches = NULL;
	ctx->xcRefs = NULL;
	ctx->xcArrays = NULL;
	ctx->xcWhich = -1;
	Per_SysSetExceptionContext(ctx);
};

void Per_Leave(Per_ExceptionContext *ctx)
{
	Per_SysSetExceptionContext(ctx->xcPrev);
};

void Per_Throw(Per_Object *e)
{
	Per_Exception *edata = (Per_Exception*) Per_GetRequiredComponent(e, &Per_CD3std2rt9Exception);
	
	Per_ExceptionContext *ctx;
	for (ctx=Per_SysGetExceptionContext(); ctx!=NULL; ctx=ctx->xcPrev)
	{
		// check if this context catches the exception
		if (ctx->xcCatches != NULL)
		{
			int i;
			for (i=0; ctx->xcCatches[i]!=NULL; i++)
			{
				Per_ClassDesc *cls = ctx->xcCatches[i];
				if (Per_GetComponent(e, cls) != NULL)
				{
					// caught!
					ctx->xcWhich = i;
					ctx->xcThrown = e;
					Per_SysSetExceptionContext(ctx->xcPrev);
					longjmp(ctx->xcJmpBuf, i);
				};
			};
		};
		
		// we didn't catch the exception, release the object references in this
		// context
		if (ctx->xcRefs != NULL)
		{
			int i;
			for (i=0; ctx->xcRefs[i]!=NULL; i++)
			{
				Per_Object **ptr = ctx->xcRefs[i];
				Per_Down(*ptr);
			};
		};
		
		if (ctx->xcArrays != NULL)
		{
			int i;
			for (i=0; ctx->xcArrays[i]!=NULL; i++)
			{
				void **ptr = ctx->xcArrays[i];
				Per_ArrayDown(*ptr);
			};
		};
	};
	
	// uncaught, so abort
	fprintf(stderr, "An uncaught exception has been thrown. Stack trace:\n");

	// print a stack trace
	Per_StackFrame *frame;
	for (frame=Per_SysStackTrace(); frame!=NULL; frame=frame->next)
	{
		// once we've reached main(), stop printing
		if (strcmp(frame->symbolName, "main") == 0)
		{
			break;
		};
		
		// don't print the Per_Throw() frame
		if (strcmp(frame->symbolName, "Per_Throw") != 0)
		{
			fprintf(stderr, "  from %s\n", Per_Demangle(frame->symbolName));
		};
	};
	
	// print the message
	Per_String *msg = (Per_String*) Per_GetComponent(edata->msg, &Per_CD3std2rt6String);
	fprintf(stderr, "%s: %s\n", e->objClass->cdFullName, msg->strData);
	
	abort();
};

void Per_CC3std2rt9Exception_M3std2rt6String_0(Per_Object *ex, Per_Object *msg);

void Per_ThrowSimple(Per_ClassDesc *cls, const char *msg)
{
	Per_Object *str = Per_MakeString(msg);
	Per_Object *ex = Per_New(cls);
	Per_CC3std2rt9Exception_M3std2rt6String_0(ex, str);
	Per_Throw(ex);
};

void Per_ThrowFormat(Per_ClassDesc *cls, const char *format, ...)
{
	char *result;
	char buffer[256];
	
	va_list ap, aq;
	va_start(ap, format);
	va_copy(aq, ap);
	
	int count;
	if ((count = vsnprintf(buffer, 256, format, ap)) >= 256)
	{
		result = (char*) malloc(count+1);
		vsprintf(result, format, aq);
	}
	else
	{
		result = strdup(buffer);
	};
	
	va_end(ap);
	va_end(aq);
	
	Per_Object *str = Per_MakeString(result);
	free(result);
	Per_Object *ex = Per_New(cls);
	Per_CC3std2rt9Exception_M3std2rt6String_0(ex, str);
	Per_Throw(ex);
};
