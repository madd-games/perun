/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#ifndef _WIN32
#include <unistd.h>
typedef int SOCKET;
#define	closesocket close
#define	SOCKET_ERROR -1
#endif

typedef struct
{
	Per_Component header;
	SOCKET sock;
} Per_TCPSocket;

typedef struct
{
	Per_Component header;
	struct sockaddr_in6* addr;
} Per_NetAddr;

typedef struct
{
	Per_Component header;
	SOCKET sock;
} Per_TCPServer;

typedef struct
{
	Per_Component header;
	SOCKET sock;
} Per_UDPSocket;

extern Per_ClassDesc Per_CD3std3net3tcp9TCPSocket;
extern Per_ClassDesc Per_CD3std3net4addr7NetAddr;
extern Per_ClassDesc Per_CD3std3net3tcp9TCPServer;
extern Per_ClassDesc Per_CD3std3net3udp9UDPSocket;

void Per_CC3std3net3tcp9TCPSocket_M3std2rt6String_0_I32(Per_Object *persock, Per_Object *hostObj, Per_int portno)
{
	if ((portno & 0xFFFF) != portno || portno == 0)
	{
		Per_ThrowFormat(Per_NetError, "port %d is not within the allowed range", portno);
	};
	
	Per_TCPSocket *comp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	
	const char *hostname = Per_ReadString(hostObj);
	char portstr[16];
	sprintf(portstr, "%d", portno);
	
	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
	hints.ai_protocol = IPPROTO_TCP;
	
	struct addrinfo *addrList;
	int s = getaddrinfo(hostname, portstr, &hints, &addrList);
	if (s != 0)
	{
		Per_ThrowFormat(Per_NetError, "cannot resolve %s: %s", hostname, gai_strerror(s));
	};
	
	struct addrinfo *addr;
	SOCKET sock = (SOCKET) 0;
	for (addr=addrList; addr!=NULL; addr=addr->ai_next)
	{
		sock = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
		if (sock == SOCKET_ERROR) continue;
		if (connect(sock, addr->ai_addr, addr->ai_addrlen) != SOCKET_ERROR) break;
		closesocket(sock);
	};
	
	freeaddrinfo(addrList);
	
	if (addr == NULL)
	{
		Per_ThrowSimple(Per_NetError, "could not establish a connection with host");
	};
	
	comp->sock = sock;
};

void Per_CC3std3net3tcp9TCPSocket_M3std3net4addr7NetAddr_0(Per_Object *persock, Per_Object *na)
{
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	SOCKET sock = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	if (sock == SOCKET_ERROR)
	{
		Per_ThrowSimple(Per_NetError, "could not establish a connection with host");
	};
	
	if (connect(sock, (struct sockaddr*) comp->addr, sizeof(struct sockaddr_in6)) == SOCKET_ERROR)
	{
		closesocket(sock);
		Per_ThrowSimple(Per_NetError, "could not establish a connection with host");
	};
	
	Per_TCPSocket *scomp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	scomp->sock = sock;
};

void Per_FD3std3net3tcp9TCPSocket10writeBytes_M3std3net3tcp9TCPSocket_0_AU8(Per_Object *persock, char *bytes)
{
	Per_TCPSocket *comp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	uint32_t size = Per_ArrayLength(bytes);
	
	if (send(comp->sock, bytes, size, 0) != size)
	{
		Per_ThrowSimple(Per_NetError, "failed to send all bytes");
	};
};

Per_int Per_FD3std3net3tcp9TCPSocket9readBytes_M3std3net3tcp9TCPSocket_0_AU8(Per_Object *persock, char *bytes)
{
	Per_TCPSocket *comp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	uint32_t size = Per_ArrayLength(bytes);
	
	int result = recv(comp->sock, bytes, size, 0);
	if (result == SOCKET_ERROR)
	{
		Per_ThrowSimple(Per_NetError, "failed to receive");
	};
	
	return result;
};

void Per_FD3std3net3tcp9TCPSocket5close_M3std3net3tcp9TCPSocket_0(Per_Object *persock)
{
	Per_TCPSocket *comp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	closesocket(comp->sock);
	comp->sock = (SOCKET) 0;
};

struct sockaddr_in6* Per_NetAddr_MakeStruct()
{
	struct sockaddr_in6 *addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
	memset(addr, 0, sizeof(struct sockaddr_in6));
	addr->sin6_family = AF_INET6;
	return addr;
};

void Per_NetAddr_SetPort(struct sockaddr_in6 *addr, Per_int portno)
{
	addr->sin6_port = htons(portno);
};

void Per_NetAddr_SetAddr(struct sockaddr_in6 *addr, const void *data)
{
	memcpy(&addr->sin6_addr, data, 16);
};

Per_bool Per_NetAddr_ParseAddr(const char *addrStr, char *addrBytes)
{
	// first try parsing as an IPv4 address, mapping onto the IPv6 prefix ::ffff:0:0/96
	memset(addrBytes, 0, 16);
	int i;
	for (i=10; i<12; i++)
	{
		addrBytes[i] = 0xFF;
	};
	
	if (inet_pton(AF_INET, addrStr, addrBytes + 12))
	{
		return PER_TRUE;
	};
	
	// if that fails, we try parsing as an IPv6 address (otherwise we fail)
	return !!inet_pton(AF_INET6, addrStr, addrBytes);
};

Per_Object* Per_NetAddr_ToString(struct sockaddr_in6 *addr)
{
	char addrStr[INET6_ADDRSTRLEN];
	char fullStr[INET6_ADDRSTRLEN+64];
	inet_ntop(AF_INET6, &addr->sin6_addr, addrStr, INET6_ADDRSTRLEN);
	sprintf(fullStr, "[%s]:%d", addrStr, (int) ntohs(addr->sin6_port));
	return Per_MakeString(fullStr);
};

Per_Object** Per_NetAddr_Resolve(const char *hostname, int portno)
{	
	char portstr[16];
	sprintf(portstr, "%d", portno);
	
	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = 0;
	hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
	hints.ai_protocol = 0;
	
	struct addrinfo *addrList;
	int s = getaddrinfo(hostname, portstr, &hints, &addrList);
	if (s != 0)
	{
		Per_ThrowFormat(Per_NetError, "cannot resolve %s: %s", hostname, gai_strerror(s));
	};
	
	struct addrinfo *addr;
	int count = 0;
	for (addr=addrList; addr!=NULL; addr=addr->ai_next)
	{
		if (addr->ai_addr->sa_family == AF_INET6) count++;
	};
	
	Per_Object **result = (Per_Object**) Per_NewArray(count, sizeof(void*), PER_TYPESECT_MANAGED);
	int i = 0;
	for (addr=addrList; addr!=NULL; addr=addr->ai_next)
	{
		if (addr->ai_addr->sa_family == AF_INET6)
		{
			Per_Object *na = Per_New(&Per_CD3std3net4addr7NetAddr);
			Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
			comp->addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
			memcpy(comp->addr, addr->ai_addr, sizeof(struct sockaddr_in6));
			result[i++] = na;
		};
	};
	
	freeaddrinfo(addrList);
	return result;
};

Per_Object* Per_FDM3std3net3tcp9TCPSocket12getLocalAddr_M3std3net3tcp9TCPSocket_0(Per_Object *persock)
{
	Per_TCPSocket *scomp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	struct sockaddr_in6 *addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
	socklen_t addrlen = sizeof(struct sockaddr_in6);
	if (getsockname(scomp->sock, (struct sockaddr*) addr, &addrlen) != 0)
	{
		free(addr);
		Per_ThrowFormat(Per_NetError, "failed to get socket local address");
	};
	
	Per_Object *na = Per_New(&Per_CD3std3net4addr7NetAddr);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	comp->addr = addr;
	return na;
};

Per_Object* Per_FDM3std3net3tcp9TCPSocket11getPeerAddr_M3std3net3tcp9TCPSocket_0(Per_Object *persock)
{
	Per_TCPSocket *scomp = (Per_TCPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3tcp9TCPSocket);
	struct sockaddr_in6 *addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
	socklen_t addrlen = sizeof(struct sockaddr_in6);
	if (getpeername(scomp->sock, (struct sockaddr*) addr, &addrlen) != 0)
	{
		free(addr);
		Per_ThrowFormat(Per_NetError, "failed to get socket peer address");
	};
	
	Per_Object *na = Per_New(&Per_CD3std3net4addr7NetAddr);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	comp->addr = addr;
	return na;
};

Per_bool Per_NetAddr_IsEqual(struct sockaddr_in6 *a, struct sockaddr_in6 *b)
{
	if (memcmp(&a->sin6_addr, &b->sin6_addr, 16) != 0)
	{
		return PER_FALSE;
	};
	
	return a->sin6_port == b->sin6_port;
};

void Per_CC3std3net3tcp9TCPServer_M3std3net4addr7NetAddr_0(Per_Object *perserv, Per_Object *na)
{
	static char zerobytes[16];
	
	Per_TCPServer *server = (Per_TCPServer*) Per_GetRequiredComponent(perserv, &Per_CD3std3net3tcp9TCPServer);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	
	server->sock = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	if (server->sock == SOCKET_ERROR)
	{
		Per_ThrowSimple(Per_NetError, "failed to create listening socket");
	};
	
	int v6only = (memcmp(&comp->addr->sin6_addr, zerobytes, 16) != 0);
	setsockopt(server->sock, IPPROTO_IPV6, IPV6_V6ONLY, &v6only, sizeof(v6only));
	
	if (bind(server->sock, (struct sockaddr*) comp->addr, sizeof(struct sockaddr_in6)) != 0)
	{
		int errnum = errno;
		closesocket(server->sock);
		Per_ThrowFormat(Per_NetError, "failed to bind socket: %s", strerror(errnum));
	};
	
	if (listen(server->sock, 5) != 0)
	{
		int errnum = errno;
		closesocket(server->sock);
		Per_ThrowFormat(Per_NetError, "failed to listen: %s", strerror(errnum));
	};
};

void Per_FD3std3net3tcp9TCPServer5close_M3std3net3tcp9TCPServer_0(Per_Object *perserv)
{
	Per_TCPServer *server = (Per_TCPServer*) Per_GetRequiredComponent(perserv, &Per_CD3std3net3tcp9TCPServer);
	closesocket(server->sock);
};

Per_Object* Per_FDM3std3net3tcp9TCPServer7opYield_M3std3net3tcp9TCPServer_0_AB(Per_Object *perserv, Per_bool *endRef)
{
	Per_TCPServer *server = (Per_TCPServer*) Per_GetRequiredComponent(perserv, &Per_CD3std3net3tcp9TCPServer);
	SOCKET client = accept(server->sock, NULL, NULL);
	
	if (client == SOCKET_ERROR)
	{
		Per_ThrowSimple(Per_NetError, "failed to accept a connection");
	};
	
	Per_Object *cobj = Per_New(&Per_CD3std3net3tcp9TCPSocket);
	Per_TCPSocket *scomp = (Per_TCPSocket*) Per_GetRequiredComponent(cobj, &Per_CD3std3net3tcp9TCPSocket);
	scomp->sock = client;
	
	return cobj;
};

void Per_CC3std3net3udp9UDPSocket(Per_Object *persock)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	scomp->sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
	if (scomp->sock == SOCKET_ERROR)
	{
		Per_ThrowSimple(Per_NetError, "failed to create UDP socket");
	};
	
	int v6only = 0;
	setsockopt(scomp->sock, IPPROTO_IPV6, IPV6_V6ONLY, &v6only, sizeof(v6only));
};

void Per_FD3std3net3udp9UDPSocket5close_M3std3net3udp9UDPSocket_0(Per_Object *persock)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	closesocket(scomp->sock);
};

Per_Object* Per_FDM3std3net3udp9UDPSocket12getLocalAddr_M3std3net3udp9UDPSocket_0(Per_Object *persock)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	struct sockaddr_in6 *addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
	socklen_t addrlen = sizeof(struct sockaddr_in6);
	if (getsockname(scomp->sock, (struct sockaddr*) addr, &addrlen) != 0)
	{
		free(addr);
		Per_ThrowFormat(Per_NetError, "failed to get socket local address");
	};
	
	Per_Object *na = Per_New(&Per_CD3std3net4addr7NetAddr);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	comp->addr = addr;
	return na;
};

void Per_FD3std3net3udp9UDPSocket4bind_M3std3net3udp9UDPSocket_0_M3std3net4addr7NetAddr_0(Per_Object *persock, Per_Object *na)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	
	if (bind(scomp->sock, (struct sockaddr*) comp->addr, sizeof(struct sockaddr_in6)) != 0)
	{
		Per_ThrowFormat(Per_NetError, "failed to bind socket: %s", strerror(errno));
	};
};

Per_int Per_FD3std3net3udp9UDPSocket4send_M3std3net3udp9UDPSocket_0_M3std2io6Buffer_0_M3std3net4addr7NetAddr_0(Per_Object *persock, Per_Object *bufObj, Per_Object *na)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	Per_NetAddr *ncomp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	Per_Buffer *buffer = Per_GetBufferComponent(bufObj);
	
	Per_Lock(bufObj);
	int errnum = 0;
	Per_int result = sendto(scomp->sock, buffer->bufData, buffer->bufSize, 0, (struct sockaddr*) ncomp->addr, sizeof(struct sockaddr_in6));
	if (result < 0)
	{
		errnum = errno;
	};
	Per_Unlock(bufObj);
	
	if (errnum != 0)
	{
		Per_ThrowFormat(Per_NetError, "failed to send: %s", strerror(errno));
	};
	
	return result;
};

Per_Object* Per_FDM3std3net3udp9UDPSocket4recv_M3std3net3udp9UDPSocket_0_M3std2io6Buffer_0(Per_Object *persock, Per_Object *bufObj)
{
	Per_UDPSocket *scomp = (Per_UDPSocket*) Per_GetRequiredComponent(persock, &Per_CD3std3net3udp9UDPSocket);
	Per_Buffer *buffer = Per_GetBufferComponent(bufObj);
	
	Per_Lock(bufObj);
	struct sockaddr_in6 *addr = (struct sockaddr_in6*) malloc(sizeof(struct sockaddr_in6));
	socklen_t addrlen = sizeof(struct sockaddr_in6);
	
	int errnum = 0;
	Per_int result = recvfrom(scomp->sock, buffer->bufData, buffer->bufSize, 0, (struct sockaddr*) addr, &addrlen);
	if (result < 0)
	{
		errnum = errno;
		free(addr);
	}
	else
	{
		buffer->bufData = (char*) realloc(buffer->bufData, result);
		buffer->bufSize = result;
	};
	
	Per_Unlock(bufObj);
	
	if (errnum != 0)
	{
		Per_ThrowFormat(Per_NetError, "failed to receive: %s", strerror(errno));
	};
	
	Per_Object *na = Per_New(&Per_CD3std3net4addr7NetAddr);
	Per_NetAddr *comp = (Per_NetAddr*) Per_GetRequiredComponent(na, &Per_CD3std3net4addr7NetAddr);
	comp->addr = addr;
	return na;
};