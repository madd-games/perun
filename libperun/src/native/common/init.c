/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <assert.h>

extern Per_Object** Per_SF3std2rt6System4args;
extern Per_int Per_SF3std2rt6System12POINTER_SIZE;

static int preInitDone;

void Per_FD3std2rt6System13__initEnviron_AM3std2rt6String_0(Per_Object** envp);

#ifdef LIBPERUN_DEBUG
FILE *Per_Debug_Log;
#endif

PER_STATIC_INIT Per_PreInit()
{
	if (__sync_lock_test_and_set(&preInitDone, 1) != 0)
	{
		return;
	};

#ifdef LIBPERUN_DEBUG
	Per_Debug_Log = fopen(".libperun.log", "w");
	assert(Per_Debug_Log != NULL);
#endif

	Per_SF3std2rt6System12POINTER_SIZE = sizeof(void*);
	Per_SysInit();
	Per_ThreadInit();
	srand(time(NULL));
};

void Per_Init(int argc, char *argv[], char *envp[])
{
	srand(time(NULL));
	Per_Object **args = (Per_Object**) Per_NewArray(argc, sizeof(void*), PER_TYPESECT_MANAGED);
	
	int i;
	for (i=0; i<argc; i++)
	{
		args[i] = Per_MakeString(argv[i]);
	};
	
	Per_SF3std2rt6System4args = args;
	
	int envc;
	for (envc=0; envp[envc]!=NULL; envc++);
	
	Per_Object **env = (Per_Object**) Per_NewArray(envc, sizeof(void*), PER_TYPESECT_MANAGED);
	
	for (i=0; i<envc; i++)
	{
		env[i] = Per_MakeString(envp[i]);
	};
	
	Per_FD3std2rt6System13__initEnviron_AM3std2rt6String_0(env);
};

void Per_ThreadInit()
{
	Per_SysThreadInit();
};
