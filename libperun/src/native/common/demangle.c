/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char* Per_Cat(char *a, char *b)
{
	char *result = (char*) malloc(strlen(a) + strlen(b) + 8);
	sprintf(result, "%s%s", a, b);
	
	free(a);
	free(b);
	
	return result;
};

static char* Per_CatTokenList(char *str, const char *scan, const char **out)
{
	*out = scan;
	
	if ((*scan >= '0') && (*scan <= '9'))
	{
		char *end;
		unsigned long count = strtoul(scan, &end, 10);
		if (count == 0) return str;
		
		scan = end;
		
		char *suffix = (char*) malloc(count + 1);
		memcpy(suffix, scan, count);
		suffix[count] = 0;
		
		str = Per_Cat(str, suffix);
		
		scan += count;
		*out = scan;
		if ((*scan >= '0') && (*scan <= '9'))
		{
			// Per_Cat() allocates extra space for the '.'
			strcat(str, ".");
			str = Per_CatTokenList(str, scan, out);
		};
	};
	
	return str;
};

static char* Per_DemangleStaticField(const char *scan)
{
	// skip over "SF"
	scan += 2;
	
	char *prefix = strdup("static field ");
	if (*scan == 'M')
	{
		prefix = Per_Cat(strdup("managed "), prefix);
		scan++;
	}
	else if (*scan == 'A')
	{
		prefix = Per_Cat(strdup("array "), prefix);
		scan++;
	};
	
	return Per_CatTokenList(prefix, scan, &scan);
};

static char* Per_DemangleClassDefinition(const char *scan)
{
	// skip over "CD"
	scan += 2;
	
	return Per_CatTokenList(strdup("class "), scan, &scan);
};

static char* Per_DemangleStaticInitializer(const char *scan)
{
	// skip over the "SI"
	scan += 2;
	
	return Per_CatTokenList(strdup("static initializer for "), scan, &scan);
};

static char* Per_DemangleStaticFinalizer(const char *scan)
{
	// skip over the "SD"
	scan += 2;
	
	return Per_CatTokenList(strdup("static finalizer for "), scan, &scan);
};

static char* Per_CatTypeSpec(char *str, const char *scan, const char **out);
static char* Per_CatSingularTypeSpec(char *str, const char *scan, const char **out)
{
	if (memcmp(scan, "F32", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("float"));
	}
	else if (memcmp(scan, "F64", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("double"));
	}
	else if (*scan == 'V')
	{
		*out = scan + 1;
		return Per_Cat(str, strdup("void"));
	}
	else if (*scan == 'B')
	{
		*out = scan + 1;
		return Per_Cat(str, strdup("bool"));
	}
	else if (memcmp(scan, "SP", 2) == 0)
	{
		*out = scan + 2;
		return Per_Cat(str, strdup("sptr_t"));
	}
	else if (memcmp(scan, "UP", 2) == 0)
	{
		*out = scan + 2;
		return Per_Cat(str, strdup("ptr_t"));
	}
	else if (memcmp(scan, "I64", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("long"));
	}
	else if (memcmp(scan, "U64", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("ulong"));
	}
	else if (memcmp(scan, "I32", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("int"));
	}
	else if (memcmp(scan, "U32", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("uint"));
	}
	else if (memcmp(scan, "I16", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("sword_t"));
	}
	else if (memcmp(scan, "U16", 3) == 0)
	{
		*out = scan + 3;
		return Per_Cat(str, strdup("word_t"));
	}
	else if (memcmp(scan, "I8", 2) == 0)
	{
		*out = scan + 2;
		return Per_Cat(str, strdup("sbyte_t"));
	}
	else if (memcmp(scan, "U8", 2) == 0)
	{
		*out = scan + 2;
		return Per_Cat(str, strdup("byte_t"));
	}
	else if (*scan == 'G')
	{
		scan++;
		
		char *end;
		unsigned long numArgs = strtoul(scan, &end, 10);
		scan = end;
		
		str = Per_Cat(str, strdup("function<"));
		str = Per_CatTypeSpec(str, scan, &scan);
		
		if (numArgs != 0)
		{
			str = Per_Cat(str, strdup(" : "));
		};
		
		while (numArgs--)
		{
			str = Per_CatTypeSpec(str, scan, &scan);
			if (numArgs != 0)
			{
				str = Per_Cat(str, strdup(", "));
			};
		};
		
		str = Per_Cat(str, strdup(">"));
		*out = scan;
		return str;
	}
	else if (*scan == 'M')
	{
		scan++;
		
		// first the class name
		str = Per_CatTokenList(str, scan, &scan);
		
		// if this is not followed by '_' then we have
		// an error, so return without updating *out
		// (read comment below under 'else')
		if (*scan != '_')
		{
			free(str);
			return strdup("");
		};
		scan++;
		
		// get the number of template parameters
		char *end;
		unsigned long numParams = strtoul(scan, &end, 10);
		scan = end;
		
		// if nonzero, process them here
		if (numParams != 0)
		{
			str = Per_Cat(str, strdup("<"));
			
			while (numParams--)
			{
				if (*scan != '_')
				{
					// same as above...
					free(str);
					return strdup("");
				};
				
				str = Per_CatTypeSpec(str, scan, &scan);
				
				if (numParams != 0)
				{
					str = Per_Cat(str, strdup(", "));
				};
			};
			
			str = Per_Cat(str, strdup(" >"));
		};
		
		// OK, done
		*out = scan;
		return str;
	}
	else
	{
		// we don't know what it is, so return here. since we won't stop
		// at an underscore as normally needed, the caller will terminate
		// with an error
		free(str);
		return strdup("");
	};
};

static char* Per_CatTypeSpec(char *str, const char *scan, const char **out)
{
	// skip over the '_'
	scan++;
	
	// count the array depth
	int arrayDepth = 0;
	while (*scan == 'A')
	{
		arrayDepth++;
		scan++;
	};
	
	// append the singular type spec
	str = Per_CatSingularTypeSpec(str, scan, &scan);
	
	// appand however many array suffixes we need
	while (arrayDepth--)
	{
		str = Per_Cat(str, strdup("[]"));
	};
	
	// finished
	*out = scan;
	return str;
};

static char* Per_DemangleFunctionDefinition(const char *scan)
{
	// skip over the "FD"
	scan += 2;
	
	// if this is followed by 'M' then the function returns a managed value,
	// or if followed by 'A', it returns an array
	if (*scan == 'M' || *scan == 'A')
	{
		scan++;
	};
	
	// start with the method name itself
	char *str = Per_CatTokenList(strdup(""), scan, &scan);
	
	// open the argument bracket
	str = Per_Cat(str, strdup("("));
	
	// parse the argument types
	while (*scan == '_')
	{
		str = Per_CatTypeSpec(str, scan, &scan);
		
		if (*scan == '_')
		{
			// more arguments follow
			str = Per_Cat(str, strdup(", "));
		};
	};
	
	// make sure the string was valid
	if (*scan != 0)
	{
		free(str);
		return strdup("<invalid>");
	};
	
	// close the bracket
	return Per_Cat(str, strdup(")"));
};

static char* Per_DemangleVirtualFunction(const char *scan)
{
	// skip over the "VF"
	scan += 2;
	
	// if this is followed by 'M' then the function returns a managed value,
	// or if followed by 'A', it returns an array
	if (*scan == 'M' || *scan == 'A')
	{
		scan++;
	};
	
	// start with the method name itself
	char *str = Per_CatTokenList(strdup("virtual function "), scan, &scan);
	
	// open the argument bracket
	str = Per_Cat(str, strdup("("));
	
	// parse the argument types
	while (*scan == '_')
	{
		str = Per_CatTypeSpec(str, scan, &scan);
		
		if (*scan == '_')
		{
			// more arguments follow
			str = Per_Cat(str, strdup(", "));
		};
	};
	
	// make sure the string was valid
	if (*scan != 0)
	{
		free(str);
		return strdup("<invalid>");
	};
	
	// close the bracket
	return Per_Cat(str, strdup(")"));
};

static char* Per_DemangleClassConstructor(const char *scan)
{
	// skip over the "CC"
	scan += 2;
	
	// start with the class name itself
	char *str = Per_CatTokenList(strdup("constructor "), scan, &scan);
	
	// open the argument bracket
	str = Per_Cat(str, strdup("("));
	
	// parse the argument types
	while (*scan == '_')
	{
		str = Per_CatTypeSpec(str, scan, &scan);
		
		if (*scan == '_')
		{
			// more arguments follow
			str = Per_Cat(str, strdup(", "));
		};
	};
	
	// make sure the string was valid
	if (*scan != 0)
	{
		free(str);
		return strdup("<invalid>");
	};
	
	// close the bracket
	return Per_Cat(str, strdup(")"));
};

static char* Per_DemangleClassInitializer(const char *scan)
{
	// skip over the "CI"
	scan += 2;
	
	// start with the class name itself
	return Per_CatTokenList(strdup("dynamic initializer for "), scan, &scan);
};

static char* Per_DemangleLambdaExpression(const char *scan)
{
	// skip over the "LX"
	scan += 2;
	
	// skip over the arbitrary lambda number
	while (*scan != '_' && *scan != 0)
	{
		scan++;
	};
	
	if (*scan == 0)
	{
		return strdup("<invalid>");
	};
	
	scan++;
	
	return Per_CatTokenList(strdup("lambda from "), scan, &scan);
};

char* Per_Demangle(const char *name)
{
	if (strlen(name) < 6)
	{
		// cannot fit "Per_XX" so obviously not a valid mangled name
		return strdup(name);
	};
	
	if (memcmp(name, "Per_", 4) != 0)
	{
		// no "Per_" prefix so also not a valid mangled name
		return strdup(name);
	};
	
	// skip over the 'Per_'
	const char *scan = name + 4;
	
	// ok, figure out what we are demangling
	if (memcmp(scan, "SF", 2) == 0)
	{
		return Per_DemangleStaticField(scan);
	}
	else if (memcmp(scan, "CD", 2) == 0)
	{
		return Per_DemangleClassDefinition(scan);
	}
	else if (memcmp(scan, "SI", 2) == 0)
	{
		return Per_DemangleStaticInitializer(scan);
	}
	else if (memcmp(scan, "SD", 2) == 0)
	{
		return Per_DemangleStaticFinalizer(scan);
	}
	else if (memcmp(scan, "FD", 2) == 0)
	{
		return Per_DemangleFunctionDefinition(scan);
	}
	else if (memcmp(scan, "VF", 2) == 0)
	{
		return Per_DemangleVirtualFunction(scan);
	}
	else if (memcmp(scan, "CC", 2) == 0)
	{
		return Per_DemangleClassConstructor(scan);
	}
	else if (memcmp(scan, "CI", 2) == 0)
	{
		return Per_DemangleClassInitializer(scan);
	}
	else if (memcmp(scan, "LX", 2) == 0)
	{
		return Per_DemangleLambdaExpression(scan);
	}
	else
	{
		// dunno what it is
		return strdup(name);
	};
};
