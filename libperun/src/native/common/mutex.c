/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>

typedef struct
{
	Per_Component head;
	Per_Mutex *mtx;
} Per_MutexData;

extern Per_ClassDesc Per_CD3std2mt5Mutex;

void Per_CC3std2mt5Mutex(Per_Object *thisptr)
{
	Per_MutexData *data = (Per_MutexData*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt5Mutex);
	data->mtx = (Per_Mutex*) malloc(sizeof(Per_Mutex));
	Per_InitLock(data->mtx);
};

void Per_FD3std2mt5Mutex9__release_UP(Per_Mutex *mtx)
{
	Per_DestroyLock(mtx);
	free(mtx);
};

void Per_FD3std2mt5Mutex4lock_M3std2mt5Mutex_0(Per_Object *thisptr)
{
	Per_MutexData *data = (Per_MutexData*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt5Mutex);
	Per_StaticLock(data->mtx);
};

void Per_FD3std2mt5Mutex6unlock_M3std2mt5Mutex_0(Per_Object *thisptr)
{
	Per_MutexData *data = (Per_MutexData*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt5Mutex);
	Per_StaticUnlock(data->mtx);
};
