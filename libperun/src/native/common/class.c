/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <string.h>

typedef struct
{
	Per_Component hdr;
	Per_ClassDesc *cls;
} Per_ClassComponent;

typedef struct
{
	Per_Component hdr;
	Per_FieldMeta* fieldMeta;
	Per_ClassDesc* classDesc;
	Per_Object* fieldNameStr;
	Per_int typeKind;
	Per_int arrayDepth;
	Per_ClassDesc* objectClass;
} Per_ClassFieldComponent;

extern Per_ClassDesc Per_CD3std2rt5Class;
extern Per_ClassDesc Per_CD3std2rt10ClassField;

Per_Object* Per_WrapClass(Per_ClassDesc *cls)
{
	Per_Object *obj = Per_New(&Per_CD3std2rt5Class);
	Per_ClassComponent *comp = (Per_ClassComponent*) Per_GetComponent(obj, &Per_CD3std2rt5Class);
	comp->cls = cls;
	return obj;
};

Per_bool Per_Class_IsInstance(Per_ClassDesc *cls, Per_Object *obj)
{
	return Per_Is(obj, cls);
};

// implement Class.newInstance()
Per_Object* Per_FDM3std2rt5Class11newInstance_M3std2rt5Class_0(Per_Object *cobj)
{
	Per_ClassComponent *comp = (Per_ClassComponent*) Per_GetComponent(cobj, &Per_CD3std2rt5Class);
	Per_ClassDesc *cls = comp->cls;
	
	Per_Object *result = Per_New(cls);
	cls->cdDefCtor(result);
	return result;
};

// implement Class.getFullName()
Per_Object* Per_FDM3std2rt5Class11getFullName_M3std2rt5Class_0(Per_Object *cobj)
{
	Per_ClassComponent *comp = (Per_ClassComponent*) Per_GetComponent(cobj, &Per_CD3std2rt5Class);
	Per_ClassDesc *cls = comp->cls;
	
	return Per_MakeString(cls->cdFullName);
};

// implement Class.getFields()
Per_Object** Per_Class_GetFields(Per_ClassDesc *cd)
{
	if (cd->cdFieldMeta == NULL)
	{
		// no field metadata, return the empty array
		return (Per_Object**) Per_NewArray(0, sizeof(void*), PER_TYPESECT_MANAGED);
	};
	
	// count how many fields we have and create the array
	int numFields;
	for (numFields=0; cd->cdFieldMeta[numFields].fName!=NULL; numFields++);
	Per_Object** fieldObjects = (Per_Object**) Per_NewArray(numFields, sizeof(void*), PER_TYPESECT_MANAGED);
	
	// create the ClassField objects
	int i;
	for (i=0; i<numFields; i++)
	{
		Per_FieldMeta *meta = &cd->cdFieldMeta[i];
		Per_Object *fieldObject = Per_New(&Per_CD3std2rt10ClassField);
		fieldObjects[i] = fieldObject;
		
		Per_ClassFieldComponent *fieldComp = (Per_ClassFieldComponent*) Per_GetRequiredComponent(fieldObject, &Per_CD3std2rt10ClassField);
		fieldComp->fieldMeta = meta;
		fieldComp->classDesc = cd;
		fieldComp->fieldNameStr = Per_MakeString(meta->fName);
		fieldComp->typeKind = meta->fTypeKind;
		fieldComp->arrayDepth = meta->fArrayDepth;
	};
	
	// done!
	return fieldObjects;
};

// implement ClassField._fetchValue()
void Per_ClassField_FetchValue(Per_FieldMeta *meta, Per_ClassDesc *cd, Per_Object *obj, size_t size, void *putResult)
{
	char *comp = (char*) Per_GetRequiredComponent(obj, cd);
	void *field = comp + meta->fCompOffset;
	
	Per_Lock(obj);
	memcpy(putResult, field, size);
	Per_Unlock(obj);
};

// implement ClassField._storeValue()
void Per_ClassField_StoreValue(Per_FieldMeta *meta, Per_ClassDesc *cd, Per_Object *obj, size_t size, void *data)
{
	char *comp = (char*) Per_GetRequiredComponent(obj, cd);
	void *field = comp + meta->fCompOffset;
	
	Per_Lock(obj);
	memcpy(field, data, size);
	Per_Unlock(obj);
};

// implement ClassField._getFieldClass
Per_Object* Per_ClassField_GetFieldClass(Per_FieldMeta *meta)
{
	Per_ClassDesc *cd = meta->fObjectClass;
	if (cd == NULL) return NULL;
	else return Per_WrapClass(cd);
};

// implement ClassField._getObject()
Per_Object* Per_ClassField_GetObject(Per_FieldMeta *meta, Per_ClassDesc *cd, Per_Object *obj)
{
	char *comp = (char*) Per_GetRequiredComponent(obj, cd);
	Per_Object **field = (Per_Object**) (comp + meta->fCompOffset);
	
	Per_Lock(obj);
	Per_Object *result = *field;
	Per_Up(result);
	Per_Unlock(obj);
	
	return result;
};

// implement ClassField.setObject()
void Per_ClassField_SetObject(Per_FieldMeta *meta, Per_ClassDesc *cd, Per_Object *obj, Per_Object *value)
{
	char *comp = (char*) Per_GetRequiredComponent(obj, cd);
	Per_Object **field = (Per_Object**) (comp + meta->fCompOffset);
	
	Per_Lock(obj);
	Per_Up(value);
	*field = value;
	Per_Unlock(obj);
};
