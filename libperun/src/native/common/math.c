/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <math.h>

double Per_FD3std4math4Math4acos_F64(double x)
{
	return acos(x);
};

double Per_FD3std4math4Math4asin_F64(double x)
{
	return asin(x);
};

double Per_FD3std4math4Math4atan_F64(double x)
{
	return atan(x);
};

double Per_FD3std4math4Math5atan2_F64_F64(double x, double y)
{
	return atan2(x, y);
};

double Per_FD3std4math4Math3cos_F64(double x)
{
	return cos(x);
};

double Per_FD3std4math4Math4cosh_F64(double x)
{
	return cosh(x);
};

double Per_FD3std4math4Math3sin_F64(double x)
{
	return sin(x);
};

double Per_FD3std4math4Math4sinh_F64(double x)
{
	return sinh(x);
};

double Per_FD3std4math4Math3tan_F64(double x)
{
	return tan(x);
};

double Per_FD3std4math4Math4tanh_F64(double x)
{
	return tanh(x);
};

double Per_FD3std4math4Math3exp_F64_F64(double x, double y)
{
	return pow(x, y);
};

double Per_FD3std4math4Math3exp_F64(double x)
{
	return exp(x);
};

double Per_FD3std4math4Math3log_F64(double x)
{
	return log10(x);
};

double Per_FD3std4math4Math2ln_F64(double x)
{
	return log(x);
};

double Per_FD3std4math4Math4sqrt_F64(double x)
{
	return sqrt(x);
};

double Per_FD3std4math4Math5floor_F64(double x)
{
	return floor(x);
};

double Per_FD3std4math4Math4ceil_F64(double x)
{
	return ceil(x);
};

double Per_FD3std4math4Math3abs_F64(double x)
{
	return fabs(x);
};

Per_int Per_FD3std4math4Math3abs_I32(Per_int x)
{
	if (x < 0)
	{
		return -x;
	}
	else
	{
		return x;
	};
};
