/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdlib.h>
#include <string.h>

extern Per_ClassDesc Per_CD3std2io6Buffer;

Per_Buffer* Per_GetBufferComponent(Per_Object *obj)
{
	return (Per_Buffer*) Per_GetRequiredComponent(obj, &Per_CD3std2io6Buffer);
};

void Per_CC3std2io6Buffer_I32(Per_Object *thisptr, Per_int size)
{
	Per_Buffer *buffer = Per_GetBufferComponent(thisptr);
	buffer->bufData = (char*) malloc(size);
	memset(buffer->bufData, 0, size);
	buffer->bufSize = size;
};

void Per_FD3std2io6Buffer6resize_M3std2io6Buffer_0_I32(Per_Object *thisptr, Per_int newSize)
{
	Per_Buffer *buffer = Per_GetBufferComponent(thisptr);
	
	Per_Lock(thisptr);
	buffer->bufData = (char*) realloc(buffer->bufData, newSize);
	if (newSize > buffer->bufSize)
	{
		memset(buffer->bufData + buffer->bufSize, 0, buffer->bufSize - newSize);
	};
	
	buffer->bufSize = newSize;
	Per_Unlock(thisptr);
};

void Per_FD3std2io6Buffer3put_M3std2io6Buffer_0_I32_AU8(Per_Object *thisptr, Per_int pos, char* bytes)
{
	Per_Buffer *buffer = Per_GetBufferComponent(thisptr);
	Per_int bytesLen = Per_ArrayLength(bytes);
	Per_int minSize = bytesLen + pos;
	
	Per_Lock(thisptr);
	if (buffer->bufSize < minSize)
	{
		buffer->bufData = (char*) realloc(buffer->bufData, minSize);
		buffer->bufSize = minSize;
	};
	
	memcpy(buffer->bufData + pos, bytes, bytesLen);
	Per_Unlock(thisptr);
};

Per_int Per_FD3std2io6Buffer3get_M3std2io6Buffer_0_I32_AU8(Per_Object *thisptr, Per_int pos, char *outBytes)
{
	Per_Buffer *buffer = Per_GetBufferComponent(thisptr);
	Per_int bytesLen = Per_ArrayLength(outBytes);
	
	Per_Lock(thisptr);
	if (pos + bytesLen > buffer->bufSize)
	{
		bytesLen = buffer->bufSize - pos;
	};
	
	memcpy(outBytes, buffer->bufData + pos, bytesLen);
	Per_Unlock(thisptr);
	
	return bytesLen;
};