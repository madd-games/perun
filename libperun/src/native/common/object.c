/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <perun_abi.h>

extern Per_ClassDesc Per_CD3std2rt6Object;
extern Per_VirtFunc Per_VF3std2rt6Object8opEquals_M3std2rt6Object_0;

typedef unsigned char (*equals_t)(Per_Object *a, Per_Object *b);

Per_Component* Per_GetComponent(Per_Object *obj, Per_ClassDesc *cls)
{
	if (obj == NULL) return NULL;
	
	int i;
	for (i=obj->objNumComps-1; i>=0; i--)
	{
		if (obj->objComps[i]->compClass == cls) return obj->objComps[i];
	};
	
	return NULL;
};

static void Per_Attach(Per_Object *obj, Per_ClassDesc *cls)
{
	// see if the component is already attached
	if (Per_GetComponent(obj, cls) != NULL) return;
	
	// it's not, so we must attach. start by attaching all parents
	Per_ClassDesc **scan;
	for (scan=cls->cdParents; *scan!=NULL; scan++)
	{
		Per_Attach(obj, *scan);
	};
	
	// attach it
	Per_Component *comp = (Per_Component*) malloc(cls->cdComponentSize);
	memset(comp, 0, cls->cdComponentSize);
	comp->compClass = cls;
	
	// add to the end of the list
	int index = obj->objNumComps++;
	obj->objComps = (Per_Component**) realloc(obj->objComps, sizeof(void*) * obj->objNumComps);
	obj->objComps[index] = comp;
};

#ifdef LIBPERUN_DEBUG
void Per_Debug_StackTrace(FILE *fp)
{
	Per_StackFrame *frame;
	for (frame=Per_SysStackTrace(); frame!=NULL; frame=frame->next)
	{
		// once we've reached main(), stop printing
		if (strcmp(frame->symbolName, "main") == 0)
		{
			break;
		};
		
		fprintf(fp, "  from %s\n", Per_Demangle(frame->symbolName));
	};
	
	fprintf(fp, "End of trace.\n");
};
#endif

static inline Per_Object* Per_Alloc(Per_ClassDesc *cls)
{
	Per_StaticLock(cls->cdLock);
	Per_Object *obj;
	
	if (cls->cdReuseStack == NULL)
	{
		Per_StaticUnlock(cls->cdLock);
		
		// set up the object structure itself first
		obj = (Per_Object*) calloc(1, sizeof(Per_Object));
		obj->objRefs = 1;
		obj->objClass = cls;
		Per_InitLock(&obj->lock);
		
		// all classes implicitly inherit from std.rt.Object, so attach it first
		Per_Attach(obj, &Per_CD3std2rt6Object);
		
		// now attach the specified class
		Per_Attach(obj, cls);
	}
	else
	{
		obj = cls->cdReuseStack;
		cls->cdReuseStack = obj->reuse;
		Per_StaticUnlock(cls->cdLock);
		
		obj->objRefs = 1;
		int i;
		for (i=0; i<obj->objNumComps; i++)
		{
			Per_Component *comp = obj->objComps[i];
			Per_ClassDesc *compClass = comp->compClass;
			memset(comp, 0, compClass->cdComponentSize);
			comp->compClass = compClass;
		};
	};
	
	return obj;
};

Per_Object* Per_New(Per_ClassDesc *cls)
{
	Per_Object *obj = Per_Alloc(cls);
	
	// invoke initializers
	int i;
	for (i=0; i<obj->objNumComps; i++)
	{
		Per_Component *comp = obj->objComps[i];
		comp->compClass->cdInit(obj);
	};

#ifdef LIBPERUN_DEBUG
	if (getenv("LIBPERUN_DEBUG_LOG_REFS") != NULL)
	{
		fprintf(Per_Debug_Log, "Per_New: Created %p with refcount=%d\n", obj, obj->objRefs);
		Per_Debug_StackTrace(Per_Debug_Log);
		fflush(Per_Debug_Log);
	};
#endif

	// done!
	return obj;
};

#ifdef LIBPERUN_DEBUG
void Per_Debug_OpOnDestroyed(Per_Object *obj, const char *funcName)
{
	if (getenv("LIBPERUN_DEBUG_SANITY") != NULL)
	{
		fprintf(stderr, "libperun: object system corruption: %s() called on a destroyed object\n", funcName);
		fprintf(stderr, "Object at memory address: %p\n", obj);
		fprintf(stderr, "I will now attempt to perform the requested debug actions.\n");
		fprintf(stderr, "Actions can be triggered by setting the following environment variables:\n");
		fprintf(stderr, "	LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_MAINCLASS - Attempt to dump the main class\n");
		fprintf(stderr, "	LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_COMPS - Attempt to dump the component table\n");
		fprintf(stderr, "	LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_STACK - Attempt to dump a stack trace\n");
		fprintf(stderr, "These actions may trigger a SIGSEGV. Attempting the selected ones now...\n");
		
		if (getenv("LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_STACK") != NULL)
		{
			Per_Debug_StackTrace(stderr);
		};
		
		if (getenv("LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_MAINCLASS") != NULL)
		{
			fprintf(stderr, "Main class dump requested. Attempting...\n");
			fprintf(stderr, "Main class: %s\n", obj->objClass->cdFullName);
		};
		
		if (getenv("LIBPERUN_DEBUG_CORRUPTOBJ_DUMP_COMPS") != NULL)
		{
			fprintf(stderr, "Component table dump requested. Attempting...\n");
			fprintf(stderr, "Components:\n");
			
			int i;
			for (i=0; i<obj->objNumComps; i++)
			{
				Per_Component *comp = obj->objComps[i];
				fprintf(stderr, "	[*] %s\n", comp->compClass->cdFullName);
			};
			
			fprintf(stderr, "End.\n");
		};
		
		fprintf(stderr, "All requested actions completed. Aborting.\n");
		abort();
	};
};
#endif

void Per_Up(Per_Object *obj)
{
	if (obj == NULL) return;
#ifdef LIBPERUN_DEBUG
	int newRefs = __sync_add_and_fetch(&obj->objRefs, 1);
	if (newRefs < 2)
	{
		Per_Debug_OpOnDestroyed(obj, "Per_Up");
	};
	
	if (getenv("LIBPERUN_DEBUG_LOG_REFS") != NULL)
	{
		fprintf(Per_Debug_Log, "Per_Up: incremented refcount of %p, refcount is now %d\n", obj, obj->objRefs);
		Per_Debug_StackTrace(Per_Debug_Log);
		fflush(Per_Debug_Log);
	};
#else
	__sync_fetch_and_add(&obj->objRefs, 1);
#endif
};

void Per_Down(Per_Object *obj)
{
	if (obj == NULL) return;
	int newRefs = __sync_add_and_fetch(&obj->objRefs, -1);

#ifdef LIBPERUN_DEBUG
	if (newRefs < 0)
	{
		Per_Debug_OpOnDestroyed(obj, "Per_Down");
	};
	
	if (getenv("LIBPERUN_DEBUG_LOG_REFS") != NULL)
	{
		fprintf(Per_Debug_Log, "Per_Down: decremented refcount of %p, refcount is now %d\n", obj, obj->objRefs);
		Per_Debug_StackTrace(Per_Debug_Log);
		fflush(Per_Debug_Log);
	};
#endif
	if (newRefs == 0)
	{
		// invoke destructors
		int i;
		for (i=obj->objNumComps-1; i>=0; i--)
		{
			Per_Component *comp = obj->objComps[i];
			comp->compClass->cdFini(comp);
		};

#ifdef LIBPERUN_DEBUG
		if (getenv("LIBPERUN_DEBUG_LOG_REFS") != NULL)
		{
			fprintf(Per_Debug_Log, "Per_Down: destroying %p\n", obj);
			fflush(Per_Debug_Log);
		};
#endif
		Per_StaticLock(obj->objClass->cdLock);
		obj->reuse = obj->objClass->cdReuseStack;
		obj->objClass->cdReuseStack = obj;
		Per_StaticUnlock(obj->objClass->cdLock);
	};
};

Per_Component* Per_GetRequiredComponent(Per_Object *obj, Per_ClassDesc *cls)
{
	if (obj == NULL)
	{
		Per_ThrowSimple(Per_NullReferenceError, "dereferencing the `null' object");
	};

	Per_Component *comp = Per_GetComponent(obj, cls);
	if (comp == NULL)
	{
		Per_ThrowFormat(Per_TypeError, "object of type `%s' cannot be cast to `%s'", obj->objClass->cdFullName, cls->cdFullName);
	};
	return comp;
};

void* Per_GetFieldPtr(Per_Object *obj, Per_ClassDesc *cls, size_t offset)
{
	Per_Component *comp = Per_GetRequiredComponent(obj, cls);
	return (char*) comp + offset;
};

void Per_SetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf, void *implementor)
{
	void **ptr = (void**) Per_GetFieldPtr(obj, vf->vfDefiner, vf->vfOffset);
	*ptr = implementor;
};

void* Per_GetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf)
{
	void **ptr = (void**) Per_GetFieldPtr(obj, vf->vfDefiner, vf->vfOffset);
	void *result = *ptr;
	
	if (result == NULL)
	{
		Per_ThrowSimple(Per_RuntimeError, "accessing an unimplemented abstract method");
	};
	
	return result;
};

void Per_Lock(Per_Object *obj)
{
	if (obj != NULL) Per_StaticLock(&obj->lock);
};

void Per_Unlock(Per_Object *obj)
{
	if (obj != NULL) Per_StaticUnlock(&obj->lock);
};

Per_bool Per_Is(Per_Object *obj, Per_ClassDesc *cls)
{
	if (Per_GetComponent(obj, cls) != NULL)
	{
		return 1;
	};
	
	return 0;
};

Per_bool Per_Equals(Per_Object *a, Per_Object *b)
{
	if (a == NULL) return (b == NULL);
	if (b == NULL) return (a == NULL);
	
	equals_t eq = (equals_t) Per_GetDynamicBinding(a, &Per_VF3std2rt6Object8opEquals_M3std2rt6Object_0);
	return eq(a, b);
};

// implements Object.hash()
uint32_t Per_FD3std2rt6Object4hash_M3std2rt6Object_0(Per_Object *obj)
{
	return 0;
};

// implements Object.opEquals()
unsigned char Per_FD3std2rt6Object8opEquals_M3std2rt6Object_0_M3std2rt6Object_0(Per_Object *a, Per_Object *b)
{
	return (a == b);
};

// implements Object.getDynamicClass()
Per_Object* Per_FDM3std2rt6Object15getDynamicClass_M3std2rt6Object_0(Per_Object *obj)
{
	return Per_WrapClass(obj->objClass);
};

// implements Object.getComponentClasses()
Per_Object** Per_Object_GetComponentClasses(Per_Object *obj)
{
	Per_Object **result = (Per_Object**) Per_NewArray(obj->objNumComps, sizeof(void*), PER_TYPESECT_MANAGED);
	
	int i;
	for (i=0; i<obj->objNumComps; i++)
	{
		result[i] = Per_WrapClass(obj->objComps[i]->compClass);
	};
	
	return result;
};