/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdlib.h>
#include <string.h>

#define	PER_ARRAY_HEADER(ar)		((Per_ArrayHeader*)(ar) - 1)

void* Per_NewArray(size_t len, size_t elementSize, uint32_t sect)
{
	size_t size = len * elementSize;
	
	Per_ArrayHeader *head = (Per_ArrayHeader*) malloc(sizeof(Per_ArrayHeader) + size);
	memset(head, 0, sizeof(Per_ArrayHeader) + size);
	
	head->arRefs = 1;
	head->arLength = len;
	head->arTypeSection = sect;
	Per_InitLock(&head->lock);
	
	return &head[1];
};

void Per_ArrayUp(void *ar)
{
	if (ar == NULL) return;
	
	Per_ArrayHeader *head = PER_ARRAY_HEADER(ar);
	__sync_fetch_and_add(&head->arRefs, 1);
};

void Per_ArrayDown(void *ar)
{
	if (ar == NULL) return;
	
	Per_ArrayHeader *head = PER_ARRAY_HEADER(ar);
	if (__sync_add_and_fetch(&head->arRefs, -1) == 0)
	{
		if (head->arTypeSection == PER_TYPESECT_ARRAY)
		{
			uint64_t i;
			void **ents = (void**) ar;
			
			for (i=0; i<head->arLength; i++)
			{
				Per_ArrayDown(ents[i]);
			};
		}
		else if (head->arTypeSection == PER_TYPESECT_MANAGED)
		{
			uint64_t i;
			Per_Object **ents = (Per_Object**) ar;
			
			for (i=0; i<head->arLength; i++)
			{
				Per_Down(ents[i]);
			};
		};
		
		free(head);
	};
};

uint32_t Per_ArrayLength(void *ar)
{
	if (ar == NULL)
	{
		Per_ThrowSimple(Per_NullReferenceError, "attempting to read the length of a `null' array");
	};
	
	Per_ArrayHeader *head = PER_ARRAY_HEADER(ar);
	return head->arLength;
};

void Per_BoundsCheck(void *ar, Per_int index)
{
	uint32_t len = Per_ArrayLength(ar);
	if (index < 0 || index >= len)
	{
		Per_ThrowFormat(Per_IndexError, "array index `%d' is out-of-bounds", (int) index);
	};
};

void Per_ArrayLock(void *ar)
{
	Per_ArrayHeader *head = PER_ARRAY_HEADER(ar);
	Per_StaticLock(&head->lock);
};

void Per_ArrayUnlock(void *ar)
{
	Per_ArrayHeader *head = PER_ARRAY_HEADER(ar);
	Per_StaticUnlock(&head->lock);
};
