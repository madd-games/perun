/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
	Per_Component head;
	Per_Object *str;
	const char *scan;
} Per_StringReader;

extern Per_ClassDesc Per_CD3std2rt6String;
extern Per_ClassDesc Per_CD3std2rt12StringReader;

Per_Object* Per_MakeString(const char *str)
{
	Per_Object *obj = Per_New(&Per_CD3std2rt6String);
	Per_String *strdata = (Per_String*) Per_GetComponent(obj, &Per_CD3std2rt6String);
	strdata->strData = strdup(str);
	return obj;
};

const char* Per_ReadString(Per_Object *str)
{
	Per_String *strdata = (Per_String*) Per_GetComponent(str, &Per_CD3std2rt6String);
	return strdata->strData;
};

/**
 * Implement std.rt.String.hash().
 */
Per_dword Per_FD3std2rt6String4hash_M3std2rt6String_0(Per_Object *thisptr)
{
	Per_String *data = (Per_String*) Per_GetComponent(thisptr, &Per_CD3std2rt6String);
	const unsigned char *name = (const unsigned char*) data->strData;
	unsigned long h = 0;
	int c;

	while ((c = *name++))
	{
		h = c + (h << 6) + (h << 16) - h;
	};
    
	return h;
};

/**
 * Implement std.rt.String.fromCodepoint().
 */
Per_Object* Per_FDM3std2rt6String13fromCodepoint_U64(Per_qword codepoint)
{
	char buffer[9];
	char *put = buffer;
	
	if (codepoint < 128)
	{
		*put++ = (char) codepoint;
		*put = 0;
	}
	else
	{
		char temp[9];
		put = &temp[8];
		*put = 0;
		
		uint64_t len = 0;
		while (codepoint != 0)
		{
			// calculate the number of bits that we could now encode using only 2 bytes
			uint64_t maxBits = 6 + (6 - (len+1));
			long mask = (1L << maxBits)-1;
			
			if ((codepoint & mask) == codepoint)
			{
				// finishing sequence
				*--put = (uint8_t) ((codepoint & 0x3F) | 0x80);
				codepoint >>= 6;
				
				uint8_t mask = ~((1 << (8-(len+2)))-1);
				*--put = (uint8_t) (codepoint | mask);
				
				break;
			}
			else
			{
				// not yet; encode the next 6 bits
				*--put = (uint8_t) ((codepoint & 0x3F) | 0x80);
				codepoint >>= 6;
				len++;
			};
		};
		
		strcpy(buffer, temp);
	};
	
	return Per_MakeString(buffer);
};

static Per_qword readUTF8(const char **strptr)
{
	if (**strptr == 0)
	{
		return 0;
	};
	
	if ((**strptr & 0x80) == 0)
	{
		long ret = (long) (uint8_t) **strptr;
		(*strptr)++;
		return ret;
	};
	
	long result = 0;
	uint8_t c = (uint8_t) **strptr;
	(*strptr)++;
	int len = 0;
		
	while (c & 0x80)
	{
		c <<= 1;
		len++;
	};
		
	result = (long) (c >> len);
	
	while (--len)
	{
		c = **strptr;
		(*strptr)++;
		result <<= 6;
		result |= (c & 0x3F);	
	};
	
	return result;
};

/**
 * Implement std.rt.StringReader.getNext().
 */
Per_qword Per_FD3std2rt12StringReader7getNext_M3std2rt12StringReader_0(Per_Object *thisptr)
{
	Per_StringReader *sr = (Per_StringReader*) Per_GetRequiredComponent(thisptr, &Per_CD3std2rt12StringReader);
	
	if (sr->scan == NULL)
	{
		sr->scan = Per_ReadString(sr->str);
	};
	
	return readUTF8(&sr->scan);
};