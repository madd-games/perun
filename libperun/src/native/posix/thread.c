/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <pthread.h>

typedef struct
{
	Per_Component head;
	pthread_t *pthread;
} Per_Thread;

extern Per_ClassDesc Per_CD3std2mt6Thread;
extern Per_VirtFunc Per_VF3std2mt6Thread5begin;

typedef void (*Per_ThreadBegin)(Per_Object *thisptr);

static void* threadEntry(void *data)
{
	Per_Object *thisptr = (Per_Object*) data;
	Per_ThreadInit();
		
	Per_ThreadBegin begin = (Per_ThreadBegin) Per_GetDynamicBinding(thisptr, &Per_VF3std2mt6Thread5begin);
	begin(thisptr);
	
	// we must downref thisptr because it was upreffered when being passed to us
	Per_Down(thisptr);
	return NULL;
};

void Per_FD3std2mt6Thread3run_M3std2mt6Thread_0(Per_Object *thisptr)
{
	Per_Thread *th = (Per_Thread*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt6Thread);
	
	Per_Lock(thisptr);
	if (th->pthread != NULL)
	{
		Per_Unlock(thisptr);
		Per_ThrowSimple(Per_RuntimeError, "run() called on an already running thread");
	};
	
	th->pthread = (pthread_t*) malloc(sizeof(pthread_t));
	Per_Unlock(thisptr);
	
	// upref 'thisptr' before thread creation, because the new thread needs a new reference to it
	Per_Up(thisptr);
	
	if (pthread_create(th->pthread, NULL, threadEntry, thisptr) != 0)
	{
		Per_Down(thisptr);
		Per_ThrowSimple(Per_RuntimeError, "thread creation failed");
	};
};

void Per_FD3std2mt6Thread4wait_M3std2mt6Thread_0(Per_Object *thisptr)
{
	Per_Thread *th = (Per_Thread*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt6Thread);
	if (th->pthread == NULL)
	{
		Per_ThrowSimple(Per_RuntimeError, "attempting to wait on a thread which was not ran yet");
	};
	
	if (pthread_join(*th->pthread, NULL) != 0)
	{
		Per_ThrowSimple(Per_RuntimeError, "the wait operation was not possible. did you attempt to wait for the same thread more than once?");
	};
};

void Per_FD3std2mt6Thread9__release_UP(void *data)
{
	free(data);
};

void Per_FD3std2mt6Thread6detach_M3std2mt6Thread_0(Per_Object *thisptr)
{
	Per_Thread *th = (Per_Thread*) Per_GetRequiredComponent(thisptr, &Per_CD3std2mt6Thread);
	if (th->pthread == NULL)
	{
		Per_ThrowSimple(Per_RuntimeError, "attempted to detach a thread which was not ran yet");
	};
	
	if (pthread_detach(*th->pthread) != 0)
	{
		Per_ThrowSimple(Per_RuntimeError, "the detach operation was not possible. did you attempt to detach a thread more than once?");
	};
};
