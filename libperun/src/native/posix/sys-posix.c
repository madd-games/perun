/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define UNW_LOCAL_ONLY
#include <perun_abi.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <libunwind.h>

#include "posix.h"

static pthread_key_t exceptKey;

extern Per_ClassDesc Per_CD3std2io4File;
void Per_CC3std2io4File_I32(Per_Object *obj, Per_int fd);

extern Per_Object* Per_SFM3std2rt6System7ENDLINE;
extern Per_Object* Per_SFM3std2rt6System14PATH_SEPARATOR;
extern Per_Object* Per_SFM3std2rt6System10EXE_SUFFIX;
extern Per_Object* Per_SFM3std2rt6System10LIB_SUFFIX;
extern Per_Object* Per_SFM3std2rt6System5stdin;
extern Per_Object* Per_SFM3std2rt6System6stdout;
extern Per_Object* Per_SFM3std2rt6System6stderr;
extern Per_int Per_SF3std5posix5Posix6O_RDWR;
extern Per_int Per_SF3std5posix5Posix8O_WRONLY;
extern Per_int Per_SF3std5posix5Posix8O_RDONLY;
extern Per_int Per_SF3std5posix5Posix8O_APPEND;
extern Per_int Per_SF3std5posix5Posix7O_CREAT;
extern Per_int Per_SF3std5posix5Posix6O_EXCL;
extern Per_int Per_SF3std5posix5Posix8O_NOCTTY;
extern Per_int Per_SF3std5posix5Posix7O_TRUNC;
extern Per_int Per_SF3std5posix5Posix8SEEK_SET;
extern Per_int Per_SF3std5posix5Posix8SEEK_CUR;
extern Per_int Per_SF3std5posix5Posix8SEEK_END;

void Per_SysInit()
{
	int error = pthread_key_create(&exceptKey, NULL);
	if (error != 0)
	{
		fprintf(stderr, "libperun: CRITICAL: cannot allocate exception key: %s\n", strerror(error));
		abort();
	};
	
	Per_SFM3std2rt6System7ENDLINE = Per_MakeString("\n");
	Per_SFM3std2rt6System14PATH_SEPARATOR = Per_MakeString("/");
	
	Per_SFM3std2rt6System10EXE_SUFFIX = Per_MakeString("");
	Per_SFM3std2rt6System10LIB_SUFFIX = Per_MakeString(".so");
	
	Per_SFM3std2rt6System5stdin = Per_Posix_Stream(0);
	Per_SFM3std2rt6System6stdout = Per_Posix_Stream(1);
	Per_SFM3std2rt6System6stderr = Per_Posix_Stream(2);
	
	Per_SF3std5posix5Posix6O_RDWR = O_RDWR;
	Per_SF3std5posix5Posix8O_RDONLY = O_RDONLY;
	Per_SF3std5posix5Posix8O_WRONLY = O_WRONLY;
	Per_SF3std5posix5Posix8O_APPEND = O_APPEND;
	Per_SF3std5posix5Posix7O_CREAT = O_CREAT;
	Per_SF3std5posix5Posix6O_EXCL = O_EXCL;
	Per_SF3std5posix5Posix8O_NOCTTY = O_NOCTTY;
	Per_SF3std5posix5Posix7O_TRUNC = O_TRUNC;
	
	Per_SF3std5posix5Posix8SEEK_SET = SEEK_SET;
	Per_SF3std5posix5Posix8SEEK_CUR = SEEK_CUR;
	Per_SF3std5posix5Posix8SEEK_END = SEEK_END;
};

void Per_SysThreadInit()
{
	pthread_setspecific(exceptKey, NULL);
};

void Per_SysSetExceptionContext(Per_ExceptionContext *ctx)
{
	pthread_setspecific(exceptKey, ctx);
};

Per_ExceptionContext* Per_SysGetExceptionContext()
{
	return (Per_ExceptionContext*) pthread_getspecific(exceptKey);
};

Per_StackFrame* Per_SysStackTrace()
{
	unw_cursor_t cursor;
	unw_context_t context;
	
	unw_getcontext(&context);
	unw_init_local(&cursor, &context);
	
	Per_StackFrame *first = NULL;
	Per_StackFrame *last = NULL;
	
	while (unw_step(&cursor) > 0)
	{
		unw_word_t offset, pc;
		unw_get_reg(&cursor, UNW_REG_IP, &pc);
		if (pc == 0) break;
		
		char sym[256];
		if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) != 0)
		{
			strcpy(sym, "<cannot find function name>");
		};
		
		Per_StackFrame *frame = (Per_StackFrame*) malloc(sizeof(Per_StackFrame));
		frame->next = NULL;
		frame->symbolName = strdup(sym);
		
		if (first == NULL)
		{
			first = last = frame;
		}
		else
		{
			last->next = frame;
			last = frame;
		};
	};
	
	return first;
};

Per_Object* Per_Posix_Stream(int fd)
{
	Per_Object *obj = Per_New(&Per_CD3std2io4File);
	Per_CC3std2io4File_I32(obj, fd);
	return obj;
};

void Per_Sys_ExitThread()
{
	pthread_exit(NULL);
};
