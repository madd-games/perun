/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <time.h>

typedef struct 
{
	Per_Component head;
	Per_long sec;
	Per_long nano;
} Per_DateTime;

extern Per_ClassDesc Per_CD3std4time8DateTime;

void Per_CC3std4time8DateTime_I64_I64(Per_Object *obj, Per_long sec, Per_long nano);

Per_Object* Per_FDM3std4time8DateTime3now()
{
	struct timespec tv;
	clock_gettime(CLOCK_REALTIME, &tv);
	
	Per_Object *obj = Per_New(&Per_CD3std4time8DateTime);
	Per_CC3std4time8DateTime_I64_I64(obj, tv.tv_sec, tv.tv_nsec);
	return obj;
};

Per_Object* Per_FDM3std4time8DateTime6format_M3std4time8DateTime_0_M3std2rt6String_0(Per_Object *thisptr, Per_Object *formatObj)
{
	Per_DateTime *dt = (Per_DateTime*) Per_GetRequiredComponent(thisptr, &Per_CD3std4time8DateTime);
	struct tm tm;
	time_t unixTime = dt->sec;
	localtime_r(&unixTime, &tm);
	
	char buffer[1024];
	size_t sz = strftime(buffer, 1024, Per_ReadString(formatObj), &tm);
	
	if (sz == 0)
	{
		buffer[0] = 0;
	};
	
	return Per_MakeString(buffer);
};
