/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

extern Per_ClassDesc Per_CD3std5posix10PosixError;

void Per_CC3std5posix10PosixError_I32_M3std2rt6String_0(Per_Object *ex, Per_int errnum, Per_Object *str);

void Per_Posix_ThrowPosixError(int errnum)
{
	char errbuf[1024];
	if (strerror_r(errnum, errbuf, 1024) != 0)
	{
		strcpy(errbuf, "(message too long)");
	};
	
	Per_Object *msgstr = Per_MakeString(errbuf);
	Per_Object *ex = Per_New(&Per_CD3std5posix10PosixError);
	Per_CC3std5posix10PosixError_I32_M3std2rt6String_0(ex, errnum, msgstr);
	Per_Down(msgstr);
	
	Per_Throw(ex);
};

// implements Posix.open()
Per_int Per_FD3std5posix5Posix4open_M3std2rt6String_0_I32_I32(Per_Object *filenameObj, Per_int flags, Per_int mode)
{
	const char *filename = Per_ReadString(filenameObj);
	
	int fd = open(filename, flags, mode);
	if (fd == -1)
	{
		Per_Posix_ThrowPosixError(errno);
	};
	
	return fd;
};

// implements Posix.write()
Per_int Per_FD3std5posix5Posix5write_I32_I32_AU8(Per_int fd, Per_int offset, char *data)
{
	Per_int totalSize = Per_ArrayLength(data);
	if (offset >= totalSize)
	{
		Per_ThrowSimple(Per_IndexError, "offset out of bounds");
	};
	
	size_t numBytes = totalSize - offset;
	ssize_t result = write(fd, data + offset, numBytes);
	
	if (result == -1)
	{
		Per_Posix_ThrowPosixError(errno);
	};
	
	return result;
};

// implements Posix.read()
Per_int Per_FD3std5posix5Posix4read_I32_AU8(Per_int fd, void *buffer)
{
	Per_int size = Per_ArrayLength(buffer);
	ssize_t result = read(fd, buffer, size);
	
	if (result == -1)
	{
		Per_Posix_ThrowPosixError(errno);
	};
	
	return result;
};

// implements Posix.close()
void Per_FD3std5posix5Posix5close_I32(Per_int fd)
{
	if (close(fd) != 0)
	{
		Per_Posix_ThrowPosixError(errno);
	};
};

// implements Posix.isatty()
Per_bool Per_FD3std5posix5Posix6isatty_I32(Per_int fd)
{
	return !!isatty(fd);
};

// implements Posix.lseek()
Per_long Per_FD3std5posix5Posix5lseek_I32_I64_I32(Per_int fd, Per_long offset, Per_int whence)
{
	off_t result = lseek(fd, offset, whence);
	if (result == (off_t) -1)
	{
		Per_Posix_ThrowPosixError(errno);
	};
	
	return result;
};
