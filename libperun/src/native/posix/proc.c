/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <unistd.h>
#include <sys/wait.h>

typedef struct
{
	Per_Component header;
	pid_t pid;
} Per_Process;

extern Per_ClassDesc Per_CD3std4proc7Process;

Per_Object* Per_FDM3std4proc7Process5__run_M3std2rt6String_0_AM3std2rt6String_0_AM3std2rt6String_0_M3std2rt6String_0(
	Per_Object *exeObj,
	Per_Object **argObjs,
	Per_Object **envObjs,
	Per_Object *cwdObj
)
{
	int argc = Per_ArrayLength(argObjs);
	
	char **argv = (char**) malloc(sizeof(void*) * (argc+1));
	int i;
	for (i=0; i<argc; i++)
	{
		argv[i] = (char*) Per_ReadString(argObjs[i]);
	};
	
	argv[argc] = NULL;
	
	int envc = Per_ArrayLength(envObjs);
	
	char **envp = (char**) malloc(sizeof(void*) * (envc+1));
	for (i=0; i<envc; i++)
	{
		envp[i] = (char*) Per_ReadString(envObjs[i]);
	};
	
	envp[envc] = NULL;
	
	pid_t pid = fork();
	if (pid == -1)
	{
		free(argv);
		free(envp);
		Per_ThrowSimple(Per_ProcessError, "fork error");
		return NULL;
	}
	else if (pid == 0)
	{
		if (chdir(Per_ReadString(cwdObj)) != 0)
		{
			_exit(1);
		};
		
		execve(Per_ReadString(exeObj), argv, envp);
		_exit(1);
		return NULL;
	}
	else
	{
		free(argv);
		free(envp);
		
		Per_Object *proc = Per_New(&Per_CD3std4proc7Process);
		Per_Process *comp = (Per_Process*) Per_GetRequiredComponent(proc, &Per_CD3std4proc7Process);
		comp->pid = pid;
		return proc;
	};
};

Per_int Per_FD3std4proc7Process4wait_M3std4proc7Process_0(Per_Object *proc)
{
	Per_Process *comp = (Per_Process*) Per_GetRequiredComponent(proc, &Per_CD3std4proc7Process);
	
	int status;
	if (waitpid(comp->pid, &status, 0) == -1)
	{
		Per_ThrowSimple(Per_ProcessError, "waitpid returning an error (did you call wait() on an already-collected process?)");
	};
	
	if (WIFEXITED(status))
	{
		return WEXITSTATUS(status);
	}
	else
	{
		return -WTERMSIG(status);
	};
};