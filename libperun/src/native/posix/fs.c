/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>

extern Per_ClassDesc Per_CD3std4time8DateTime;

void Per_CC3std4time8DateTime_I64_I64(Per_Object *obj, Per_long sec, Per_long nano);

void Per_Posix_ThrowIOError(int errnum)
{
	char errbuf[1024];
	if (strerror_r(errnum, errbuf, 1024) != 0)
	{
		strcpy(errbuf, "(message too long)");
	};
	
	Per_ThrowSimple(Per_IOError, errbuf);
};

Per_Object* Per_FDM3std2io10FileSystem13getAccessTime_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	struct stat st;
	if (stat(path, &st) != 0)
	{
		Per_Posix_ThrowIOError(errno);
	};
	
	Per_Object *result = Per_New(&Per_CD3std4time8DateTime);
	Per_CC3std4time8DateTime_I64_I64(result, st.st_atime, 0);
	return result;
};

Per_Object* Per_FDM3std2io10FileSystem19getModificationTime_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	struct stat st;
	if (stat(path, &st) != 0)
	{
		Per_Posix_ThrowIOError(errno);
	};
	
	Per_Object *result = Per_New(&Per_CD3std4time8DateTime);
	Per_CC3std4time8DateTime_I64_I64(result, st.st_mtime, 0);
	return result;
};

void Per_FD3std2io10FileSystem15createDirectory_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	if (mkdir(path, 0755) != 0)
	{
		Per_Posix_ThrowIOError(errno);
	};
};

Per_bool Per_FD3std2io10FileSystem11isDirectory_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	struct stat st;
	if (stat(path, &st) != 0)
	{
		return PER_FALSE;
	};
	
	if (S_ISDIR(st.st_mode))
	{
		return PER_TRUE;
	};
	
	return PER_FALSE;
};

Per_Object** Per_FDA3std2io10FileSystem9__listdir_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	DIR *dirp = opendir(path);
	if (dirp == NULL)
	{
		return NULL;
	};
	
	Per_Object **names = NULL;
	int numNames = 0;
	
	struct dirent *ent;
	while ((ent = readdir(dirp)) != NULL)
	{
		if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0)
		{
			int index = numNames++;
			names = (Per_Object**) realloc(names, sizeof(void*) * numNames);
			names[index] = Per_MakeString(ent->d_name);
		};
	};
	
	closedir(dirp);
	
	Per_Object **array = (Per_Object**) Per_NewArray(numNames, sizeof(void*), PER_TYPESECT_MANAGED);
	memcpy(array, names, sizeof(void*) * numNames);
	free(names);
	return array;
};