/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <dlfcn.h>
#include <pthread.h>
#include <perun_abi.h>
#include <string.h>

extern Per_ClassDesc Per_CD3std2rt11ClassLoader;
static pthread_mutex_t libLock = PTHREAD_MUTEX_INITIALIZER;

void* Per_FD3std2rt11ClassLoader7libOpen_M3std2rt6String_0(Per_Object *pathobj)
{
	const char *path = Per_ReadString(pathobj);
	
	pthread_mutex_lock(&libLock);
	void *lib = dlopen(path, RTLD_NOW | RTLD_LOCAL);
	if (lib == NULL)
	{
		char *err = dlerror();
		char msg[strlen(err)+1];
		strcpy(msg, err);
		free(err);
		
		pthread_mutex_unlock(&libLock);
		Per_ThrowSimple(Per_RuntimeError, msg);
	};
	
	pthread_mutex_unlock(&libLock);
	return lib;
};

Per_Object* Per_FDM3std2rt11ClassLoader16libClassBySymbol_UP_M3std2rt6String_0(void *lib, Per_Object *symobj)
{
	const char *symbol = Per_ReadString(symobj);
	void *ptr = dlsym(lib, symbol);
	
	if (ptr == NULL) return NULL;
	else return Per_WrapClass((Per_ClassDesc*) ptr);
};
