/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.net.addr;

import std.priv.Runtime;

/**
 * Represents a network transport-layer address.
 * 
 * This stores an IPv6 address (IPv4 addresses are mapped onto the ::ffff:0:0/96 prefix),
 * and a port number. Internally, this is the `struct sockaddr_in6` C structure. 
 */
public final class NetAddr extends StringFormattable
{
	private static ptr_t _makeStruct() extern Per_NetAddr_MakeStruct;
	private static void _setPort(ptr_t sa, int portno) extern Per_NetAddr_SetPort;
	private static void _setAddr(ptr_t sa, byte_t[] addrBytes) extern Per_NetAddr_SetAddr;
	private static bool _parseAddr(byte_t[] addrStr, byte_t[] addrBytes) extern Per_NetAddr_ParseAddr;
	private static String _toString(ptr_t sa) extern Per_NetAddr_ToString;
	private static NetAddr[] _resolve(byte_t[] hostname, int portno) extern Per_NetAddr_Resolve;
	private static bool _isEqual(ptr_t sa1, ptr_t sa2) extern Per_NetAddr_IsEqual;
	
	private ptr_t structPtr;
	
	private NetAddr()
	{
		structPtr = _makeStruct();
	};	
	
	destructor
	{
		Runtime.free(structPtr);
	};
	
	/**
	 * Construct the specified address.
	 * 
	 * If the specified address is IPv4, it is mapped onto the IPv6 prefix ::ffff:0:0/96,
	 * which allows one to pass IPv4 addresses to IPv6 APIs.
	 * 
	 * The port number must not be negative, and must not be larger than 0xFFFF. 0 is allowed.
	 * 
	 * \param addr The address string (either an IPv4 or IPv6 address).
	 * \param portno The port number.
	 * 
	 * \throws std.rt.IllegalArgumentError If the address string or the port number is invalid.
	 */
	public NetAddr(String addr, int portno)
	{
		byte_t[] addrBytes = new byte_t[16];
		if (!_parseAddr(addr.toByteArrayWithTerminator(), addrBytes))
		{
			throw new IllegalArgumentError("invalid IP address: " + addr);
		};
		
		if (portno < 0 || portno > 0xFFFF)
		{
			throw new IllegalArgumentError("invalid port number: " + portno);
		};
		
		structPtr = _makeStruct();
		_setPort(structPtr, portno);
		_setAddr(structPtr, addrBytes);
	};
	
	/**
	 * Return a wildcard address (::) with the specified port number.
	 * 
	 * \throws std.rt.IllegalArgumentError If `port` is negative or larger than `0xFFFF`.
	 */
	public static NetAddr wildcard(int port)
	{
		if (port < 0 || port > 0xFFFF)
		{
			throw new IllegalArgumentError("invalid port number: " + port);
		};
		
		NetAddr result = new NetAddr();
		_setPort(result.structPtr, port);
		return result;
	};
	
	/**
	 * Return the loopback address (::1) with the specified port number.
	 * 
	 * \throws std.rt.IllegalArgumentError If `port` is negative or larger than `0xFFFF`.
	 */
	public static NetAddr loopback(int portno)
	{
		return new NetAddr("::1", portno);
	};
	
	/**
	 * Returns an array of possible network addresses for the specified host name and port.
	 * 
	 * \throws std.net.NetError If the name could not be resolved.
	 */
	public static NetAddr[] resolve(String hostname, int portno)
	{
		return _resolve(hostname.toByteArrayWithTerminator(), portno);
	};
	
	/**
	 * Convert the address into a string of the form "[addr]:portno".
	 */
	public override String toString()
	{
		return _toString(structPtr);
	};
	
	/**
	 * Compare two addresses.
	 * 
	 * Addresses are said to be equal if both the IP address and the port number match.
	 */
	public override bool opEquals(Object b)
	{
		if (!(b is NetAddr))
		{
			return false;
		};
		
		NetAddr other = (NetAddr) b;
		return _isEqual(structPtr, other.structPtr);
	};
};