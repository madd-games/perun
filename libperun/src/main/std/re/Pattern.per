/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.re;

import std.cont.List;
import std.cont.LinkedList;
import std.cont.LinkedStack;
import std.cont.HashSet;
import std.cont.Iterator;

/**
 * Represents a compiled regular expression.
 */
public final class Pattern
{
	private List<Atom> atoms = new LinkedList<Atom>();
	
	/**
	 * List of atoms making up this
	 */
	private Pattern() {};
	
	/**
	 * Construct a pattern.
	 */
	public Pattern(String regex)
	{
		LinkedStack<Pattern> stack = new LinkedStack<Pattern>();
		stack.push(this);
		
		String expr = regex;
		while (expr.size() != 0)
		{
			if (expr.startsWith("\\"))
			{
				if (expr.size() < 2)
				{
					throw new RegexError("'\\' cannot be the end of an expression");
				};
				
				String c = expr.substr(1, 1);
				expr = expr.substr(2);
				
				if (c == "n") c = "\n";
				if (c == "t") c = "\t";
				
				Atom atom = new Atom();
				atom.mode = Atom.EXACT;
				atom.exactString = c;
				
				stack[0].atoms.append(atom);
			}
			else if (expr.startsWith("*"))
			{
				if (stack[0].atoms.size() == 0)
				{
					throw new RegexError("the '*' modifier can only appear after an atom");
				};
				
				stack[0].atoms[stack[0].atoms.size()-1].mode |= Atom.ASTERISK;
				expr = expr.substr(1);
			}
			else if (expr.startsWith("+"))
			{
				if (stack[0].atoms.size() == 0)
				{
					throw new RegexError("the '+' modifier can only appear after an atom");
				};
				
				stack[0].atoms[stack[0].atoms.size()-1].mode |= Atom.PLUS;
				expr = expr.substr(1);
			}
			else if (expr.startsWith("?"))
			{
				if (stack[0].atoms.size() == 0)
				{
					throw new RegexError("the '?' modifier can only appear after an atom");
				};
				
				stack[0].atoms[stack[0].atoms.size()-1].mode |= Atom.NOGREED;
				expr = expr.substr(1);
			}
			else if (expr.startsWith("["))
			{
				expr = expr.substr(1);
				
				Atom atom = new Atom();
				stack[0].atoms.append(atom);
				
				atom.mode = Atom.SET;
				atom.set = new HashSet<String>();
				
				if (expr.startsWith("^"))
				{
					atom.oppositeSet = true;
				};
				
				while (!expr.startsWith("]"))
				{
					if (expr.size() == 0)
					{
						throw new RegexError("unterminated ']'");
					};
					
					if (expr.startsWith("\\"))
					{
						if (expr.size() < 2)
						{
							throw new RegexError("'\\' cannot appear at the end of an expression");
						};
						
						String c = expr.substr(1, 1);
						expr = expr.substr(2);
						
						atom.set.add(c);
					}
					else
					{
						byte_t start = expr.substr(0, 1).toByteArray()[0];
						byte_t end = start;
						expr = expr.substr(1);
						
						if (expr.startsWith("-"))
						{
							if (expr.size() < 2)
							{
								throw new RegexError("unterminated ']'");
							};
							
							end = expr.substr(1, 1).toByteArray()[0];
							expr = expr.substr(2);
						};
						
						if (end < start)
						{
							throw new RegexError("inverted range inside []");
						};
						
						byte_t codepoint;
						for (codepoint=start; codepoint<=end; codepoint++)
						{
							atom.set.add(String.fromCodepoint(codepoint));
						};
					};
				};
				
				expr = expr.substr(1);	// skip over the ']'
			}
			else if (expr.startsWith("."))
			{
				expr = expr.substr(1);
				
				Atom atom = new Atom();
				atom.mode = Atom.ANY_CHAR;
				stack[0].atoms.append(atom);
			}
			else if (expr.startsWith("$"))
			{
				expr = expr.substr(1);
				
				Atom atom = new Atom();
				atom.mode = Atom.SET;
				
				atom.set = new HashSet<String>();
				atom.set.add(" ");
				atom.set.add("\t");
				atom.set.add("\n");
				atom.set.add("\r");
				
				stack[0].atoms.append(atom);
			}
			else if (expr.startsWith("("))
			{
				expr = expr.substr(1);
				
				Pattern sub = new Pattern();
				Atom atom = new Atom();
				stack[0].atoms.append(atom);
				
				atom.mode = Atom.BRACKET;
				atom.options = new LinkedList<Pattern>();
				atom.options.append(sub);

				stack.push(sub);
			}
			else if (expr.startsWith("|"))
			{
				expr = expr.substr(1);
				stack.pop();
				if (stack.size() == 0)
				{
					throw new RegexError("'|' must be inside brackets");
				};
				
				Pattern sub = new Pattern();
				stack[0].atoms[stack[0].atoms.size()-1].options.append(sub);
				stack.push(sub);
			}
			else if (expr.startsWith(")"))
			{
				expr = expr.substr(1);
				stack.pop();
				if (stack.size() == 0)
				{
					throw new RegexError("'|' must be inside brackets");
				};
			}
			else if (expr.startsWith("%"))
			{
				expr = expr.substr(1);
				
				Atom atom = new Atom();
				atom.mode = Atom.END;
				stack[0].atoms.append(atom);
			}
			else
			{
				// unknown, so exact match
				String c = expr.substr(0, 1);
				expr = expr.substr(1);
				
				Pattern current = stack[0];
				
				Atom atom = new Atom();
				atom.mode = Atom.EXACT;
				atom.exactString = c;
				
				current.atoms.append(atom);
			};
		};
		
		if (stack.size() != 1)
		{
			throw new RegexError("unterminated '()'");
		};
	};
	
	private int matchSingle(Atom atom, String str, int startPos, List<Match> subMatches)
	{
		int mode = (atom.mode & 0xFF);
		if (mode == Atom.EXACT)
		{
			if (str.substr(startPos).startsWith(atom.exactString))
			{
				return (int) atom.exactString.size();
			};
			
			return -1;
		}
		else if (mode == Atom.SET)
		{
			if (str.size() == startPos)
			{
				// end of string
				return -1;
			};
			
			if (atom.set.contains(str.substr(startPos, 1)))
			{
				return atom.oppositeSet ? -1 : 1;
			}
			else
			{
				return atom.oppositeSet ? 1 : -1;
			};
		}
		else if (mode == Atom.BRACKET)
		{
			Iterator<Pattern> it;
			for (it=atom.options.iterate(); !it.end(); it.next())
			{
				Pattern pat = it.get();
				Match m = pat.match(str, startPos);
				
				if (m != null)
				{
					if (subMatches != null) subMatches.append(m);
					return m.getSize();
				};
			};
			
			return -1;
		}
		else if (mode == Atom.END)
		{
			if (str.size() == startPos)
			{
				// end of string, match 0 bytes successfully
				return 0;
			};
			
			return -1;
		}
		else if (mode == Atom.ANY_CHAR)
		{
			if (str.size() != startPos)
			{
				// not end of string, match one byte
				return 1;
			};
			
			return -1;
		}
		else
		{
			throw new RegexError("INTERNAL ERROR: Unknown atom type");
		};
	};
	
	/**
	 * Attempt a match at the beginning of the string.
	 * 
	 * If there is any match at the beginning, including a zero-size match,
	 * returns a `std.re.Match` object; otherwise, returns `null`.
	 */
	public Match match(String str)
	{
		return match(str, 0);
	};
	
	/**
	 * Attempt to match at the specified position.
	 * 
	 * If there is any match at the indicated position in the string,
	 * including a zero-size match, returns a `std.re.Match` object;
	 * otherwise, returns `null`.
	 */
	public Match match(String str, int startPos)
	{
		int index = 0;
		int currentMatchSize = 0;
		List<Match> subMatches = new LinkedList<Match>();
		bool caughtOne = false;
		
		int pos = startPos;
		while (index != atoms.size())
		{
			Atom atom = atoms[index];
			if ((atom.mode & Atom.NOGREED) != 0)
			{
				if (index != atoms.size()-1)
				{
					if (matchSingle(atoms[index+1], str, pos, null) != -1)
					{
						index++;
						continue;
					};
				};
			};
			
			int matchSize = matchSingle(atom, str, pos, subMatches);
			if (matchSize == -1)
			{
				if ((atom.mode & Atom.ASTERISK) != 0)
				{
					index++;
					continue;
				};
				
				if ((atom.mode & Atom.PLUS) != 0 && caughtOne)
				{
					caughtOne = false;
					index++;
					continue;
				};
				
				if ((atom.mode & Atom.NOGREED) != 0)
				{
					index++;
					continue;
				};
				
				return null;
			};
			
			currentMatchSize += matchSize;
			pos += matchSize;
			
			if ((atom.mode & Atom.PLUS) != 0)
			{
				caughtOne = true;
			}
			else if ((atom.mode & Atom.ASTERISK) == 0)
			{
				index++;
			};
		};
		
		return new Match(str, startPos, currentMatchSize, subMatches);
	};
	
	/**
	 * Attempt to find a match in the string.
	 */
	public Match search(String str, int startFrom)
	{
		int offset;
		for (offset=startFrom; offset<=str.size(); offset++)
		{
			Match m = match(str, offset);
			if (m != null) return m;
		};
		
		return null;
	};
	
	/**
	 * Attempt to find a match in the string.
	 */
	public Match search(String str)
	{
		return search(str, 0);
	};
};