/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.re;

import std.cont.List;
import std.cont.LinkedList;
import std.cont.Iterator;

private class TokenType
{
	public Pattern pat;
	public bool whitespace;
};

/**
 * Lexer (lexical analyzer).
 * 
 * This is an abstract class, to which you can make subclasses to parse other languages.
 * In each subclass, there would be a constructor which calls `addTokenType()` to define
 * the types of tokens in the language it is parsing. Then at some point later, or possibly
 * in the constructor itself, you would call `feed()` to provide data to be parsed. Then, for
 * each token spotted, `onToken()` will be called, and your subclass decides what to do with
 * it.
 */
public abstract class Lexer
{
	/**
	 * The list of token types.
	 */
	private List<TokenType> tokenTypes = new LinkedList<TokenType>();
	
	/**
	 * Add a new token type.
	 * 
	 * \param regex The regular expression identifying this token type.
	 * \param whitespace If `true`, then this is a whitespace token and should be ignored and
	 *                   not passed to `onToken()`.
	 * \returns An object which will later be passed `onToken()` when identifying this token type.
	 */
	protected final Object addTokenType(String regex, bool whitespace)
	{
		TokenType tt = new TokenType();
		tt.pat = new Pattern(regex);
		tt.whitespace = whitespace;
		tokenTypes.append(tt);
		
		return tt;
	};

	/**
	 * Add a new non-whitespace token type.
	 * 
	 * \param regex The regular expression identifying this token type.
	 * \returns An object which will later be passed `onToken()` when identifying this token type.
	 */
	protected final Object addTokenType(String regex)
	{
		return addTokenType(regex, false);
	};
	
	/**
	 * Add a whitespace token type.
	 * 
	 * \param regex The regular expression identifying this token type.
	 * \returns An object which will later be passed `onToken()` when identifying this token type.
	 */
	public final Object addWhitespaceTokenType(String regex)
	{
		return addTokenType(regex, true);
	};
	
	/**
	 * This function is called every time a non-whitespace token is encountered by the parser.
	 * 
	 * \param type The token type (previously returned by `addTokenType()`).
	 * \param lineno The line number within the `feed()` data.
	 * \param match The `std.re.Match` object representing the matched token.
	 */
	protected abstract void onToken(Object type, int lineno, Match match);
	
	/**
	 * Feed the parser.
	 * 
	 * \param data The data to be parsed.
	 */
	protected final void feed(String data)
	{
		int pos = 0;
		int totalSize = (int) data.size();
		int lineno = 1;
		
		while (pos < totalSize)
		{
			Iterator<TokenType> it;
			Match longestMatch = null;
			TokenType longestTT = null;
			
			for (it=tokenTypes.iterate(); !it.end(); it.next())
			{
				TokenType tt = it.get();
				Match m = tt.pat.match(data, pos);
				
				if (m != null)
				{
					if (longestMatch == null)
					{
						longestMatch = m;
						longestTT = tt;
					}
					else
					{
						if (m.getSize() > longestMatch.getSize())
						{
							longestMatch = m;
							longestTT = tt;
						};
					};
				};
			};
			
			if (longestMatch == null)
			{
				throw new RegexError("parse failure at " + pos);
			};
			
			if (!longestTT.whitespace) onToken(longestTT, lineno, longestMatch);
			lineno += longestMatch.getValue().count("\n");
			pos += longestMatch.getSize();
		};
	};
};