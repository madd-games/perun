/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.json;

import std.io.InputStream;
import std.io.OutputStream;
import std.cont.Map;
import std.cont.List;
import std.cont.Iterator;
import std.cont.KeyValuePair;

/**
 * Private class to help with parsing JSON.
 */
private class JSONParser
{
	/**
	 * "Saved" character. This is set by `rewind()`, and returned by `next()` if set. This is
	 * to allow us to go back if we found a terminating character.
	 */
	private String saved;
	
	/**
	 * The input stream.
	 */
	private InputStream strm;
	
	public JSONParser(InputStream strm)
	{
		this.strm = strm;
	};
	
	/**
	 * Get the next character.
	 */
	public String next()
	{
		if (saved != null)
		{
			String result = saved;
			saved = null;
			return result;
		};
		
		return strm.read(1);
	};
	
	/**
	 * Rewind the specified character. It'll be returned by the next call to `next()`.
	 */
	public void rewind(String c)
	{
		saved = c;
	};
};

/**
 * Represents any JSON value, and also provides static methods to parse JSON.
 */
public abstract class JSON
{
	private static final String DIGITS = "0123456789";
	
	/**
	 * Encode this value and write to the specified output stream.
	 *
	 * \param strm The output stream.
	 * \param depth Current depth (for indentation).
	 */
	protected abstract void outputTo(OutputStream strm, int depth);
	
	/**
	 * Encode this value and write to the specified output stream.
	 */
	public final void outputTo(OutputStream strm)
	{
		outputTo(strm, 1);
	};
	
	/**
	 * Escape a string for writing to a JSON file. This does NOT add quotes to each end!
	 */
	public static String escapeString(String str)
	{
		return str.replace("\\", "\\\\").replace("\"", "\\\"").replace("\n", "\\n").replace("\r", "\\r");
	};
	
	private static JSONString parseString(JSONParser parser)
	{
		String c = parser.next();
		if (c != "\"")
		{
			parser.rewind(c);
			return null;
		};
		
		String result = "";
		while (true)
		{
			c = parser.next();
			if (c == "\"")
			{
				return new JSONString(result);
			}
			else if (c == "\\")
			{
				c = parser.next();
				
				// TODO: the \u escape sequence!
				if (c == "\"")
				{
					result += "\"";
				}
				else if (c == "\\")
				{
					result += "\\";
				}
				else if (c == "/")
				{
					result += "/";
				}
				else if (c == "b")
				{
					result += "\b";
				}
				else if (c == "f")
				{
					result += "\f";
				}
				else if (c == "n")
				{
					result += "\n";
				}
				else if (c == "r")
				{
					result += "\r";
				}
				else if (c == "t")
				{
					result += "\t";
				}
				else
				{
					throw new JSONError("illegal escape sequence: \\" + c);
				};
			}
			else
			{
				result += c;
			};
		};
	};
	
	private static JSONNumber parseNumber(JSONParser parser)
	{
		String numRepr = "";
		
		String c = parser.next();
		if (DIGITS.find(c) == -1)
		{
			parser.rewind(c);
			return null;
		};
		
		numRepr = c;
		while (DIGITS.find(c = parser.next()) != -1)
		{
			numRepr += c;
		};
		
		if (c != ".")
		{
			parser.rewind(c);
		}
		else
		{
			numRepr += ".";
			
			int numDigits = 0;
			while (DIGITS.find(c = parser.next()) != -1)
			{
				numRepr += c;
				numDigits++;
			};
			
			if (numDigits == 0)
			{
				throw new JSONError("no digits after decimal point in a number");
			};
			
			if (c != "e")
			{
				parser.rewind(c);
			}
			else
			{
				numRepr += c;
				
				c = parser.next();
				if (c != "+" && c != "-")
				{
					throw new JSONError("invalid number format");
				};
				
				numRepr += c;
				
				int numDigits = 0;
				while (DIGITS.find(c = parser.next()) != -1)
				{
					numRepr += c;
					numDigits++;
				};
				
				if (numDigits == 0)
				{
					throw new JSONError("no digits after exponent in a number");
				};
				
				parser.rewind(c);
			};
		};
		
		return new JSONNumber(numRepr.parseFloat());
	};
	
	private static JSONObject parseObject(JSONParser parser)
	{
		String c = parser.next();
		if (c != "{")
		{
			parser.rewind(c);
			return null;
		};

		skipWhitespace(parser);
		c = parser.next();
		if (c == "}")
		{
			return new JSONObject();
		};
		parser.rewind(c);
		
		JSONObject obj = new JSONObject();
		while (true)
		{
			skipWhitespace(parser);
			
			JSONString key = parseString(parser);
			if (key == null)
			{
				throw new JSONError("expected a string for object key");
			};
			
			skipWhitespace(parser);
			
			c = parser.next();
			if (c != ":")
			{
				parser.rewind(c);
				throw new JSONError("expected `:' after object key");
			};
			
			skipWhitespace(parser);
			
			JSON value = parseValue(parser);
			obj[key.toString()] = value;
			
			skipWhitespace(parser);
			
			c = parser.next();
			if (c == ",")
			{
				// NOP
			}
			else if (c == "}")
			{
				return obj;
			}
			else
			{
				parser.rewind(c);
				throw new JSONError("expected `,' or `}' not `" + c + "'");
			};
		};
	};
	
	private static JSONArray parseArray(JSONParser parser)
	{
		String c = parser.next();
		if (c != "[")
		{
			parser.rewind(c);
			return null;
		};
		
		skipWhitespace(parser);
		c = parser.next();
		if (c == "]")
		{
			return new JSONArray();
		};
		parser.rewind(c);
		
		JSONArray array = new JSONArray();
		while (true)
		{
			skipWhitespace(parser);
			
			JSON value = parseValue(parser);
			array.append(value);
			
			skipWhitespace(parser);
			
			c = parser.next();
			if (c == ",")
			{
				// NOP
			}
			else if (c == "]")
			{
				return array;
			}
			else
			{
				parser.rewind(c);
				throw new JSONError("expected `,' or `]' not `" + c + "'");
			};
		};
	};
	
	private static JSONBoolean parseBoolean(JSONParser parser)
	{
		String c = parser.next();
		
		if (c == "t")
		{
			if (parser.next() == "r" && parser.next() == "u" && parser.next() == "e")
			{
				return new JSONBoolean(true);
			};
			
			throw new JSONError("invalid sequence beginning in `t'");
		}
		else if (c == "f")
		{
			if (parser.next() == "a" && parser.next() == "l" && parser.next() == "s" && parser.next() == "e")
			{
				return new JSONBoolean(false);
			};
			
			throw new JSONError("invalid sequence beginning in `f'");
		}
		else
		{
			parser.rewind(c);
			return null;
		};
	};
	
	private static bool parseNull(JSONParser parser)
	{
		// this one returns "true" if there was a null, false otherwise
		String c = parser.next();
		if (c != "n")
		{
			parser.rewind(c);
			return false;
		};
		
		if (parser.next() == "u" && parser.next() == "l" && parser.next() == "l")
		{
			return true;
		};
		
		throw new JSONError("invalid sequence beginning in `n'");
	};
	
	private static void skipWhitespace(JSONParser parser)
	{
		while (true)
		{
			String c = parser.next();
			if (c != " " && c != "\n" && c != "\r" && c != "\t")
			{
				parser.rewind(c);
				return;
			};
		};
	};
	
	private static JSON parseValue(JSONParser parser)
	{
		skipWhitespace(parser);
		
		JSONString str;
		JSONNumber num;
		JSONObject obj;
		JSONArray array;
		JSONBoolean boolean;
		
		if ((str = parseString(parser)) != null)
		{
			return str;
		}
		else if ((num = parseNumber(parser)) != null)
		{
			return num;
		}
		else if ((obj = parseObject(parser)) != null)
		{
			return obj;
		}
		else if ((array = parseArray(parser)) != null)
		{
			return array;
		}
		else if ((boolean = parseBoolean(parser)) != null)
		{
			return boolean;
		}
		else if (parseNull(parser))
		{
			return null;
		}
		else
		{
			throw new JSONError("invalid JSON data");
		};
	};
	
	/**
	 * Parse JSON from the specified stream, and return the resulting JSON object. This might be `null` if the stream
	 * simply says `null`.
	 *
	 * \param strm The input stream to read from.
	 * \returns A JSON instance referring to the parsed data.
	 * \throws std.json.JSONError If the input data is invalid.
	 */
	public static JSON parse(InputStream strm)
	{
		JSONParser parser = new JSONParser(strm);
		return parseValue(parser);
	};
	
	/**
	 * Serialize an object into JSON.
	 * 
	 * The object must be serializable; which means it must be a subtype of the following:
	 * 
	 * If the object is a `Map<String, Object>`, it is serialized into a `JSONObject`.
	 * 
	 * If the object is a `List<Object>`, it is serialized into a `JSONArray`.
	 * 
	 * If the object is a `String` or a `StringFormattable`, it is serialized into a `JSONString`.
	 * 
	 * If the object is an `Integer`, `UnsignedInteger` or `Float`, it is serialized into a `JSONNumber`.
	 * 
	 * If the object is a `Boolean`, it is serialized into a `JSONBoolean`.
	 * 
	 * If the object is a subtype of `JSONSerializable`, the `toJSON()` method is called.
	 * 
	 * All other objects are serialized into a `JSONObject`, based on fields with available metadata.
	 * 
	 * If the object is `null`, this method returns `null`.
	 * 
	 * \param obj The object to serialize.
	 * \returns The object serialized into a `JSON` instance (`null` if `obj` is `null`).
	 * \throws std.json.JSONError If `obj` cannot be serialized.
	 */
	public static JSON serialize(Object obj)
	{
		try
		{
			if (obj == null)
			{
				return null;
			}
			else if (obj is Integer)
			{
				return new JSONNumber((double) (sqword_t) (Integer) obj);
			}
			else if (obj is UnsignedInteger)
			{
				return new JSONNumber((double) (qword_t) (UnsignedInteger) obj);
			}
			else if (obj is Float)
			{
				return new JSONNumber((double) (Float) obj);
			}
			else if (obj is String)
			{
				return new JSONString((String) obj);
			}
			else if (obj is StringFormattable)
			{
				return new JSONString(((StringFormattable) obj).toString());
			}
			else if (obj is Boolean)
			{
				return new JSONBoolean((Boolean) obj);
			}
			else if (obj is JSONSerializable)
			{
				return ((JSONSerializable)obj).toJSON();
			}
			else if (obj is List<Object>)
			{
				JSONArray result = new JSONArray();
				Iterator<Object> it;
				for (it=((List<Object>) obj).iterate(); !it.end(); it.next())
				{
					result.append(serialize(it.get()));
				};
				return result;
			}
			else if (obj is Map<String, Object>)
			{
				JSONObject result = new JSONObject();
				Iterator< KeyValuePair<String, Object> > it;
				for (it=((Map<String, Object>) obj).iterate(); !it.end(); it.next())
				{
					result[it.get().getKey()] = serialize(it.get().getValue());
				};
				return result;
			}
			else
			{
				JSONObject result = new JSONObject();
				
				foreach (Class cls : obj.getComponentClasses())
				{
					if (cls.getFullName() != "std.rt.Object")
					{
						foreach (ClassField field : cls.getFields())
						{
							result[field.getName()] = serialize(field.getObjectValue(obj));
						};
					};
				};
				
				return result;
			};
		}
		catch (Exception e)
		{
			throw new JSONError("Cannot serialize object: " + e.getMessage());
		};
	};
	
	/**
	 * Create a copy of the JSON value.
	 * 
	 * The returned value is identical but deep-copied, so updated to the returned value or any of
	 * its descendants (e.g. array members, array-member-object members, etc) do not affect this one.
	 * 
	 * \returns A deep-copy of this value.
	 */
	public final JSON copy()
	{
		return serialize(this);
	};
	
	/**
	 * Deserialize into an instance of the specified class.
	 * 
	 * This method may throw errors caused by instantiating the class.
	 * 
	 * \returns An instance of `cls`, with fields filled in from this JSON value.
	 * \throws std.json.JSONError If we cannot deserialize into an instance of this class.
	 */
	public abstract Object deserialize(Class cls);
};
