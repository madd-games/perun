/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.time;

/**
 * Represents an interval in time, with nanosecond precision.
 */
public final class TimeInterval
{
	private static final long NANO_PER_SEC = 1000000000L;
	
	private long sec;
	private long nano;
	
	/**
	 * Construct the time interval with the specified seconds and nanoseconds.
	 */
	public TimeInterval(long sec, long nano)
	{
		this.sec = sec;
		this.nano = nano;
	}
	
	/**
	 * Construct the time interval in seconds.
	 */
	public TimeInterval(long sec)
	{
		this.sec = sec;
		this.nano = 0;
	}
	
	/**
	 * Get the number of nanoseconds represented by this object. Note that this might overflow, because
	 * internally, two `long`s are used to represent the seconds+nanoseconds value, instead of just a single
	 * `long`.
	 */
	public long toNanoSeconds()
	{
		return sec * NANO_PER_SEC * nano;
	};
	
	/**
	 * Get the number of seconds represented by this object (rounded down to the nearest integer).
	 */
	public long toSeconds()
	{
		return sec;
	};
	
	/**
	 * Get the number of minutes represented by this object.
	 */
	public long toMinutes()
	{
		return sec / 60L;
	};
	
	/**
	 * Get the number of hours represented by this object.
	 */
	public long toHours()
	{
		return toMinutes() / 60L;
	};
	
	/**
	 * Get the number of days represented by this object.
	 */
	public long toDays()
	{
		return toHours() / 24L;
	};
	
	/**
	 * Get the number of weeks represented by this object.
	 */
	public long toWeeks()
	{
		return toDays() / 7L;
	};
	
	/**
	 * Find the sum of two intervals.
	 */
	public TimeInterval opAdd(TimeInterval b)
	{
		long sumNano = nano + b.nano;
		
		long nano = sumNano % NANO_PER_SEC;
		long carry = sumNano / NANO_PER_SEC;
		
		return new TimeInterval(sec + b.sec + carry, nano);
	};
	
	/**
	 * Find the difference between two intervals.
	 */
	public TimeInterval opSub(TimeInterval b)
	{
		long nanodiff = nano - b.nano;
		long carry = 0;
		
		while (nanodiff < 0)
		{
			carry--;
			nanodiff += NANO_PER_SEC;
		};
		
		return new TimeInterval(sec - b.sec + carry, nanodiff);
	};

	/**
	 * Compare two `TimeInterval` objects. Returns -1 if the second object is larger, 0 if equal, 1 otherwise.
	 */
	public int opCompare(TimeInterval other)
	{
		long diff;
		
		if (other.sec == sec)
		{
			diff = nano - other.nano;
		}
		else
		{
			diff = sec - other.sec;
		};
		
		if (diff < 0L)
		{
			return -1;
		}
		else if (diff == 0L)
		{
			return 0;
		}
		else
		{
			return 1;
		};
	};
	
	/**
	 * Returns `true` if equal.
	 */
	public override bool opEquals(Object b)
	{
		if (b is TimeInterval)
		{
			return opCompare((TimeInterval) b) == 0;
		};
		
		return false;
	};
	
	/**
	 * Return a `TimeInterval` instance corresponding to the value of this one, plus the specified
	 * number of seconds.
	 */
	public TimeInterval seconds(long secs)
	{
		return new TimeInterval(this.sec+secs, this.nano);
	};
	
	/**
	 * Return a `TimeInterval` instance corresponding to the value of this one, plus the specified
	 * number of minutes.
	 */
	public TimeInterval minutes(long m)
	{
		return seconds(60L * m);
	};

	/**
	 * Return a `TimeInterval` instance corresponding to the value of this one, plus the specified
	 * number of hours.
	 */
	public TimeInterval hours(long h)
	{
		return minutes(60L * h);
	};

	/**
	 * Return a `TimeInterval` instance corresponding to the value of this one, plus the specified
	 * number of days.
	 */
	public TimeInterval days(long d)
	{
		return hours(24L * d);
	};
};
