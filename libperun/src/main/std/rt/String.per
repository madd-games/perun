/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.rt;

import std.priv.Runtime;
import std.re.Pattern;
import std.re.Match;

/**
 * Represents strings of characters.
 */
public final class String extends Comparable<String>
{
	// pointer to the array of bytes
	private ptr_t strData;
	
	/**
	 * Type representing Unicode codepoints (64-bit unsigned integer).
	 */
	public typedef Codepoint = qword_t;
	
	/**
	 * True/false strings for `fromBool()`.
	 */
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	
	/**
	 * Lower-case digits for `fromInt()`.
	 *
	 * There are 36 digits in this array, thus making the maximum radix 36.
	 */
	public static final String[] LOWERCASE_DIGITS = new String[]
	{
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
		"k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
		"u", "v", "w", "x", "y", "z"
	};
	
	/**
	 * Upper-case digits for `fromInt()`.
	 *
	 * There are 36 digits in this array, thus making the maximum radix 36.
	 */
	public static final String[] UPPERCASE_DIGITS = new String[]
	{
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z"
	};
	
	/**
	 * Make a string containing a single UTF-8 character.
	 */
	public static String fromCodepoint(String::Codepoint point);
	
	/**
	 * Make a string from a boolean.
	 *
	 * \param value The boolean value.
	 * \returns Either the string "true" or the string "false".
	 */
	public static String fromBool(bool value)
	{
		if (value)
		{
			return TRUE;
		};
		
		return FALSE;
	};

	/**
	 * Make a string from a floating-point number.
	 *
	 * \param value The value to be converted into a string.
	 * \param minPrecision The minimum number of digits after the demical point to display.
	 * \param maxPrecision The mximum number of digits after the decimal point to display.
	 * \param allowExp If `true`, write the number in standard form if very small.
	 * \returns The resulting string.
	 */
	public static String fromFloat(double value, int minPrecision, int maxPrecision, bool allowExp)
	{
		if (value < 0.0)
		{
			return "-" + fromFloat(-value, minPrecision, maxPrecision, allowExp);
		};

		int minDigits = minPrecision;
		if (minPrecision < 1)
		{
			minDigits = 1;
		};
		
		int maxDigits = maxPrecision;
		if (maxDigits < minDigits)
		{
			maxDigits = minDigits;
		};
		
		if (allowExp && value < 0.001 && value > 0.0)
		{
			int exponent = 0;
			long factor = 1;
			
			double expanded = value;
			while (expanded < 1.0)
			{
				exponent++;
				factor *= 10;
				expanded = value * (double) factor;
			};
			
			return fromFloat(expanded, minPrecision, maxPrecision, false) + "e-" + exponent;
		}
		else if (allowExp && value > -0.001 && value < 0.0)
		{
			int exponent = 0;
			long factor = 1;
			
			double expanded = value;
			while (expanded > -1.0)
			{
				exponent++;
				factor *= 10;
				expanded = value * (double) factor;
			};
			
			return fromFloat(expanded, minPrecision, maxPrecision, false) + "e-" + exponent;
		}
		
		String intPart = fromInt((int) value);
		
		int factor = 1;
		int i;
		for (i=0; i<maxDigits; i++)
		{
			factor *= 10;
		};
		
		String decimalPart = fromInt((dword_t) ((value - (double) (int) value) * (double) factor));
		while (decimalPart.size() < maxDigits)
		{
			decimalPart = "0" + decimalPart;
		};
		
		while (decimalPart.endsWith("0") && decimalPart.size() > minDigits)
		{
			decimalPart = decimalPart.substr(0, (int) decimalPart.size() - 1);
		};
		
		return intPart + "." + decimalPart;
	};

	/**
	 * Make a string from a floating-point number. If the number is very small, it'll be converted
	 * to standard form.
	 *
	 * \param value The value to be converted into a string.
	 * \param minPrecision The minimum number of digits after the demical point to display.
	 * \param maxPrecision The mximum number of digits after the decimal point to display.
	 * \returns The resulting string.
	 */
	public static String fromFloat(double value, int minPrecision, int maxPrecision)
	{
		return fromFloat(value, minPrecision, maxPrecision, true);
	};

	/**
	 * Make a string from a floating-point number. If the number is very small, it'll be converted
	 * to standard form.
	 *
	 * \param value The value to be converted into a string.
	 * \returns The resulting string.
	 */
	public static String fromFloat(double value)
	{
		return fromFloat(value, 1, 9);
	};
	
	/**
	 * Make a string from an integer.
	 *
	 * \param value The number to be converted into a string.
	 * \param radix The radix (base) of the number system to use.
	 * \param digits The array of digits to use. The lenght must be at least `radix`.
	 */
	public static String fromInt(int value, int radix, String[] digits)
	{
		if (value < 0)
		{
			return "-" + fromInt((dword_t) (-value), radix, digits);
		}
		else
		{
			return fromInt((dword_t) value, radix, digits);
		};
	};
	
	/**
	 * Make a string from an integer.
	 *
	 * If the radix is larger than 10, then lowercase letters will be used as the extra digits. The
	 * maximum radix is thus 36.
	 *
	 * \param value The number to be converted into a string.
	 * \param radix The radix (base) of the number system to use.
	 */
	public static String fromInt(int value, int radix)
	{
		return fromInt(value, radix, LOWERCASE_DIGITS);
	};
	
	/**
	 * Make a base-10 string from an integer.
	 *
	 * \param value The value to be converted into a string.
	 */
	public static String fromInt(int value)
	{
		return fromInt(value, 10);
	};
	
	/**
	 * Make a string from an unsigned integer.
	 *
	 * \param value The value to be converted into a string.
	 * \param radix The radix (base) of the number system to use.
	 * \param digits The digits to use; the length must be at least `radix`.
	 */
	public static String fromInt(dword_t value, int radix, String[] digits)
	{
		if (radix > digits.length)
		{
			throw new IllegalArgumentError("radix for integer to string conversion is higher than the available number of digits");
		};
		
		if (radix < 2)
		{
			throw new IllegalArgumentError("radix for integer to string conversion is less than 2");
		};
		
		String result = "";
		dword_t rem = value;
		
		do
		{
			dword_t digitno = rem % (dword_t) radix;
			rem = rem / (dword_t) radix;
			
			result = digits[digitno] + result;
		} while (rem > 0U);
		
		return result;
	};

	/**
	 * Make a string from an integer.
	 *
	 * If the radix is larger than 10, then lowercase letters will be used as the extra digits. The
	 * maximum radix is thus 36.
	 *
	 * \param value The number to be converted into a string.
	 * \param radix The radix (base) of the number system to use.
	 */
	public static String fromInt(dword_t value, int radix)
	{
		return fromInt(value, radix, LOWERCASE_DIGITS);
	};

	/**
	 * Make a base-10 string from an integer.
	 *
	 * \param value The value to be converted into a string.
	 */
	public static String fromInt(dword_t value)
	{
		return fromInt(value, 10);
	};
	
	/**
	 * Make a string from an array of bytes.
	 *
	 * \param bytes The array of bytes.
	 * \param offset Offset into the array to begin copying characters from.
	 * \param count Number of characters to copy.
	 */
	public static String fromByteArray(byte_t[] bytes, int offset, int count)
	{
		if (offset + count > bytes.length || offset < 0 || count < 0)
		{
			throw new IndexError("offset+count out of bounds");
		};
		
		ptr_t newData = Runtime.malloc((dword_t)count + 1U);
		Runtime.memcpy(newData, Runtime.getByteArrayPointer(bytes) + (ptr_t) offset, (dword_t) count);
		Runtime.memset(newData + (ptr_t) count, (byte_t) 0, 1U);
		
		return new String(newData);
	};

	private String()
	{
		throw new RuntimeError("the default constructor of String was invoked!");
	};
	
	private String(ptr_t strData)
	{
		this.strData = strData;
	};
	
	destructor
	{
		Runtime.free(strData);
	};
	
	/**
	 * Return the size of the string.
	 *
	 * \returns The size of the string (in bytes).
	 */
	public int size()
	{
		return Runtime.strlen(strData);
	};
	
	/**
	 * Concatenate two strings; implements the `+` operator.
	 */
	public String opAdd(String other)
	{
		int sizeA = size();
		int sizeB = other.size();
		int newSize = sizeA + sizeB + 1;
		
		ptr_t newData = Runtime.malloc((dword_t) newSize);
		Runtime.memcpy(newData, strData, (dword_t) sizeA);
		Runtime.memcpy(newData + (ptr_t) sizeA, other.strData, (dword_t) sizeB);
		Runtime.memset(newData + (ptr_t) sizeA + (ptr_t) sizeB, (byte_t) 0, 1U);
		
		return new String(newData);
	};
	
	/**
	 * Concatenate a string with an integer.
	 *
	 * The integer is converted to a string in base 10 before being concatenated.
	 */
	public String opAdd(int value)
	{
		return this + String.fromInt(value);
	};
	
	/**
	 * Concatenate a string with an integer.
	 *
	 * The integer is converted to a string in base 10 before being concatenated.
	 */
	public String opAdd(dword_t value)
	{
		return this + String.fromInt(value);
	};
	
	/**
	 * Concatenate a string with a boolean.
	 */
	public String opAdd(bool value)
	{
		return this + String.fromBool(value);
	};
	
	/**
	 * Concatenate a string with a floating-point number.
	 */
	public String opAdd(double value)
	{
		return this + String.fromFloat(value);
	};
	
	/**
	 * Check if two strings are equal.
	 */
	public override bool opEquals(Object b)
	{
		if (!b is String)
		{
			return false;
		};
		
		String other = (String) b;
		if (Runtime.strcmp(strData, other.strData) == 0)
		{
			return true;
		};
		
		return false;
	};

	public override int opCompare(String s)
	{
		return (int) Runtime.strcmp(strData, s.strData);
	};	

	/**
	 * Compute the hash of a string.
	 */
	public override dword_t hash();
	
	/**
	 * Concatenate a string with a `StringFormattable` object.
	 */
	public String opAdd(StringFormattable formattable)
	{
		return this + formattable.toString();
	};
	
	/**
	 * Convert the string to an array of bytes making up the string.
	 */
	public byte_t[] toByteArray()
	{
		int mySize = size();
		byte_t[] byteArray = new byte_t[mySize];
		
		Runtime.memcpy(Runtime.getByteArrayPointer(byteArray), strData, (dword_t) mySize);
		
		return byteArray;
	};
	
	/**
	 * Convert the string to an array of bytes making up the string, NUL-terminated.
	 */
	public byte_t[] toByteArrayWithTerminator()
	{
		int mySize = size();
		byte_t[] byteArray = new byte_t[mySize+1];
		
		Runtime.memcpy(Runtime.getByteArrayPointer(byteArray), strData, (dword_t) mySize);
		byteArray[mySize] = (byte_t) 0;
		
		return byteArray;
	};
	
	/**
	 * Extract a substring from this string.
	 *
	 * \param start The starting position (zero-based) to begin extracting from.
	 * \param size Number of bytes to extract.
	 * \throws IndexError If the range is out of bounds.
	 */
	public String substr(int start, int size)
	{
		int mySize = (int) this.size();
		
		if (start < 0 || size < 0)
		{
			throw new IllegalArgumentError("negative start or size");
		};
		
		if ((start+size) > mySize)
		{
			throw new IndexError("trying to extract a substring out of bounds");
		};
		
		if (size == 0)
		{
			return "";
		};
		
		ptr_t newData = Runtime.malloc((dword_t) size + 1u);
		Runtime.memcpy(newData, strData + (ptr_t) start, (dword_t) size);
		Runtime.memset(newData + (ptr_t) size, (byte_t) 0, 1u);
		return new String(newData);
	};
	
	/**
	 * Extract a substring from `start` to the end of this string.
	 *
	 * \param start The starting position (zero-based) to begin extracting from.
	 * \throws IndexError If the starting position is out of bounds.
	 */
	public String substr(int start)
	{
		if (start < 0 || start > size())
		{
			throw new IndexError("trying to extract a substring out of bounds");
		};
		
		return substr(start, size() - start);
	};
	
	/**
	 * Find a substring inside this string, and return its offset, but only if found after `start`.
	 * Returns -1 if the string is not found.
	 */
	public int find(String sub, int start)
	{
		if (start < 0 || start > size())
		{
			throw new IndexError("start position " + start + " for string search is out of bounds");
		};
		
		ptr_t ptr = Runtime.strstr(strData + (ptr_t) start, sub.strData);
		if (ptr == (ptr_t)0) return -1;
		else return (int) (ptr - strData);
	};
	
	/**
	 * Find a substring inside this string, and return its offset. Returns -1 if the string is
	 * not found.
	 */
	public int find(String sub)
	{
		ptr_t ptr = Runtime.strstr(strData, sub.strData);
		if (ptr == (ptr_t)0) return -1;
		else return (int) (ptr - strData);
	};
	
	/**
	 * Find a substring matching the specified pattern inside this string, and return its offset, but only if
	 * found after `start`. Returns -1 if the string is not found.
	 */
	public int find(Pattern pat, int start)
	{
		if (start < 0 || start > (int) size())
		{
			throw new IndexError("start position " + start + " for string search is out of bounds");
		};
		
		int offset;
		for (offset=start; offset<=(int)size(); offset++)
		{
			if (pat.match(this, offset) != null)
			{
				return offset;
			};
		};
		
		return -1;
	};
	
	/**
	 * Find a substring matching the specified pattern inside this string. Returns -1 if not found.
	 */
	public int find(Pattern pat)
	{
		return find(pat, 0);
	};
	
	/**
	 * Count the number of occurences of `sub` within this string.
	 */
	public int count(String sub)
	{
		if (sub.size() == 0)
		{
			throw new IllegalArgumentError("cannot count empty substrings");
		};
		
		int pos = 0;
		int count = 0;
		
		while (true)
		{
			int matchPos = find(sub, pos);
			if (matchPos == -1)
			{
				return count;
			};
			
			pos = matchPos + (int) sub.size();
			count++;
		};
	};
	
	/**
	 * Returns `true` if this string starts with `prefix`.
	 */
	public bool startsWith(String prefix)
	{
		if (prefix.size() > size())
		{
			return false;
		};
		
		return substr(0, (int) prefix.size()) == prefix;
	};
	
	/**
	 * Returns `true` if this strings ends with `suffix`.
	 */
	public bool endsWith(String suffix)
	{
		if (suffix.size() > size())
		{
			return false;
		};
		
		return substr((int) (size() - suffix.size()), (int) suffix.size()) == suffix;
	};
	
	/**
	 * Return this string, but with all occurences of `search` replaced with `replaceWith`.
	 */
	public String replace(String search, String replaceWith)
	{
		String result = "";
		String subject = this;
		
		int pos;
		while ((pos = subject.find(search)) != -1)
		{
			result += subject.substr(0, pos);
			result += replaceWith;
			subject = subject.substr(pos + (int) search.size());
		};
		
		result += subject;
		return result;
	};
	
	/**
	 * Return this string, but with all substrings matching `pat` replaced with `format`,
	 * which is a regex format string. See `std.re.Match.format()` for more information.
	 */
	public String replace(Pattern pat, String format)
	{
		String result = "";
		String subject = this;
		
		Match m;
		while ((m = pat.search(subject)) != null)
		{
			result += subject.substr(0, m.getStartPos());
			result += m.format(format);
			subject = subject.substr(m.getStartPos() + m.getSize());
		};
		
		result += subject;
		return result;
	};
	
	/**
	 * Split the string at the specified delimiter, up to the maximum number of splits, and return
	 * an array with all tokens which were delimited by the specified delimiter in the original string.
	 * The final token contains the remainder of the string, if there were more occurences of the delimeter
	 * than the maximum.
	 *
	 * \param delimiter The delimiter which is to split the tokens.
	 * \param maxSplits Maximum number of splits; the returned array will have one more elements than this number.
	 *                  A value of 0 indicates that there should not be any splits, and the full string is returned
	 *                  as the sole member of the array. Negative values indicate that there is no limit to splits.
	 * \throws std.rt.IllegalArgumentError If `delimeter` is the empty string.
	 * \returns An array containing the delimited tokens.
	 */
	public String[] split(String delimiter, int maxSplits)
	{
		int actualSplits = count(delimiter);
		if (actualSplits > maxSplits && maxSplits >= 0)
		{
			actualSplits = maxSplits;
		};
		
		String[] tokens = new String[actualSplits + 1];
		String subject = this;
		
		int i;
		for (i=0; i<actualSplits; i++)
		{
			int pos = subject.find(delimiter);
			tokens[i] = subject.substr(0, pos);
			subject = subject.substr(pos + delimiter.size());
		};
		
		tokens[actualSplits] = subject;
		return tokens;
	};

	/**
	 * Split the string at the specified delimiter, up to the maximum number of splits, and return
	 * an array with all tokens which were delimited by the specified delimiter in the original string.
	 *
	 * \param delimiter The delimiter which is to split the tokens.
	 * \throws std.rt.IllegalArgumentError If `delimeter` is the empty string.
	 * \returns An array containing the delimited tokens.
	 */
	public String[] split(String delimiter)
	{
		return split(delimiter, -1);
	};
	
	/**
	 * Convert the string to an unsigned integer.
	 */
	public dword_t parseUnsignedInt()
	{
		dword_t result = 0U;
		
		String working = this;
		while (working.size() != 0)
		{
			String digit = working.substr(0, 1);
			working = working.substr(1);
			
			dword_t val;
			if (digit == "0")
			{
				val = 0U;
			}
			else if (digit == "1")
			{
				val = 1U;
			}
			else if (digit == "2")
			{
				val = 2U;
			}
			else if (digit == "3")
			{
				val = 3U;
			}
			else if (digit == "4")
			{
				val = 4U;
			}
			else if (digit == "5")
			{
				val = 5U;
			}
			else if (digit == "6")
			{
				val = 6U;
			}
			else if (digit == "7")
			{
				val = 7U;
			}
			else if (digit == "8")
			{
				val = 8U;
			}
			else if (digit == "9")
			{
				val = 9U;
			}
			else
			{
				throw new IllegalArgumentError(this + " is not a valid unsigned integer");
			};
			
			if (result >= System.MAX_DWORD/10U)
			{
				dword_t maxDigit = System.MAX_DWORD % 10U;
				if (val > maxDigit)
				{
					throw new OverflowError(this + " is not representable as a dword_t");
				};
			};
			
			result = result * 10U + val;
		};
		
		return result;
	};
	
	/**
	 * Convert the string to a signed integer.
	 */
	public int parseInt()
	{
		bool negative;
		String subject;
		
		if (startsWith("-"))
		{
			negative = true;
			subject = substr(1);
		}
		else
		{
			negative = false;
			subject = this;
		};
		
		dword_t asUnsigned;
		try
		{
			asUnsigned = subject.parseUnsignedInt();
		}
		catch (IllegalArgumentError e)
		{
			throw new IllegalArgumentError(this + " is not an integer");
		}
		catch (OverflowError e)
		{
			throw new OverflowError(this + " is not representable as an int");
		};
		
		if (negative)
		{
			if (asUnsigned > (dword_t) (-System.MIN_INT))
			{
				throw new OverflowError(this + " is not representable as an int");
			};
			
			return -(int)asUnsigned;
		}
		else
		{
			if (asUnsigned > (dword_t) System.MAX_INT)
			{
				throw new OverflowError(this + " is not representable as an int");
			};
			
			return (int) asUnsigned;
		};
	};
	
	/**
	 * Convert the string to a floating-point number, of type `double`.
	 */
	public double parseFloat()
	{
		try
		{
			String[] pieces = split(".", 1);
			int intPart = pieces[0].parseInt();
			
			if (pieces.length == 1)
			{
				return (double) intPart;
			};
			
			pieces = pieces[1].split("e", 1);
			String fracStr = pieces[0];
			if (fracStr.size() > 6)
			{
				fracStr = fracStr.substr(0, 6);
			};
			
			int fracPart = (int) fracStr.parseUnsignedInt();
			
			int factor = 1;
			int i;
			for (i=0; i<fracStr.size(); i++)
			{
				factor *= 10;
			};
			
			double result = (double) intPart + (double) fracPart / (double) factor;
			if (pieces.length == 1)
			{
				return result;
			};
			
			int exp;
			String expSpec = pieces[1];
			if (expSpec.startsWith("+"))
			{
				exp = (int) expSpec.substr(1).parseUnsignedInt();
			}
			else if (expSpec.startsWith("-"))
			{
				exp = -(int) expSpec.substr(1).parseUnsignedInt();
			}
			else
			{
				throw new RuntimeError("doesn't matter, it's caught by the catch below");
			};
			
			if (exp < 0)
			{
				exp = -exp;
				int i;
				for (i=0; i<exp; i++)
				{
					result /= 10.0;
				};
			}
			else
			{
				int i;
				for (i=0; i<exp; i++)
				{
					result *= 10.0;
				};
			};
			
			return result;
		}
		catch (RuntimeError e)
		{
			throw new IllegalArgumentError(this + " is not representable as a floating-point number");
		};
	};
	
	/**
	 * Join an array of strings, using this one as a delimiter between them.
	 */
	public String join(String[] array)
	{
		if (array.length == 0)
		{
			return "";
		};
		
		String result = array[0];
		
		int i;
		for (i=1; i<array.length; i++)
		{
			result += this + array[i];
		};
		
		return result;
	};
	
	/**
	 * Return a sequence of code points making up this string.
	 */
	public StringReader codepoints()
	{
		return new StringReader(this);
	};
};
