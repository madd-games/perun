/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.rt;

import std.io.InputStream;
import std.io.OutputStream;
import std.io.Path;
import std.io.FlatDirectoryIterator;
import std.cont.Map;
import std.cont.HashMap;
import std.cont.Iterator;

/**
 * A static class containing routines which allow interaction with the system.
 */
public static class System
{	
	/**
	 * A string which marks line breaks on the host platform.
	 */
	public static final String ENDLINE;
	
	/**
	 * A string which separates the components of a path.
	 */
	public static final String PATH_SEPARATOR;
	
	/**
	 * The executable suffix on the host platform. For example, this is the empty string
	 * on POSIX, and ".exe" on Windows.
	 */
	public static final String EXE_SUFFIX;
	
	/**
	 * The library suffix on the host platform. For example, this is ".dll" on Windows,
	 * or ".so" on POSIX.
	 */
	public static final String LIB_SUFFIX;
	
	/**
	 * Command-line arguments passed to the program. The first argument is the program
	 * name itself.
	 */
	public static final String[] args;
	
	/**
	 * Environment variables.
	 */
	public static final Map<String, String> environ = new HashMap<String, String>();
	
	/**
	 * Standard input stream.
	 */
	public static final InputStream stdin;
	
	/**
	 * Standard output stream.
	 */
	public static final OutputStream stdout;
	
	/**
	 * Standard error output stream.
	 */
	public static final OutputStream stderr;

	/**
	 * Maximum value that can be stored in a `byte_t`.
	 */
	public static final byte_t MAX_BYTE = (byte_t) 0xFFU;
	
	/**
	 * Minimum value that can be stored in a `byte_t`.
	 */
	public static final byte_t MIN_BYTE = (byte_t) 0U;
	
	/**
	 * Maximum value that can be stored in a `sbyte_t`.
	 */
	public static final sbyte_t MAX_SBYTE = (sbyte_t) 0x7F;
	
	/**
	 * Minimum value that can be stored in a `sbyte_t`.
	 */
	public static final sbyte_t MIN_SBYTE = (sbyte_t) -0x80;
	
	/**
	 * Maximum value that can be stored in a `word_t`.
	 */
	public static final word_t MAX_WORD = (word_t) 0xFFFFU;
	
	/**
	 * Minimum value that can be stored in a `word_t`.
	 */
	public static final word_t MIN_WORD = (word_t) 0U;
	
	/**
	 * Maximum value that can be stored in a `sword_t`.
	 */
	public static final sword_t MAX_SWORD = (sword_t) 0x7FFF;
	
	/**
	 * Minimum value that can be stored in a `sword_t`.
	 */
	public static final sword_t MIN_SWORD = (sword_t) -0x8000;
	
	/**
	 * Maximum value that can be stored in a `dword_t`.
	 */
	public static final dword_t MAX_DWORD = 0xFFFFFFFFU;
	
	/**
	 * Minimum value that can be stored in a `dword_t`.
	 */
	public static final dword_t MIN_DWORD = 0U;
	
	/**
	 * Maximum value that can be stored in a `sdword_t`.
	 */
	public static final sdword_t MAX_SDWORD = 0x7FFFFFFF;
	public static final int MAX_INT = 0x7FFFFFFF;
	
	/**
	 * Minimum value that can be stored in a `sdword_t`.
	 */
	public static final sdword_t MIN_SDWORD = -0x80000000;
	public static final int MIN_INT = -0x80000000;
	
	/**
	 * Maximum value that can be stored in a `qword_t`.
	 */
	public static final qword_t MAX_QWORD = 0xFFFFFFFFFFFFFFFFUL;
	
	/**
	 * Minimum value that can be stored in a `qword_t`.
	 */
	public static final qword_t MIN_QWORD = 0UL;
	
	/**
	 * Maximum value that can be stored in a `sqword_t`.
	 */
	public static final sqword_t MAX_SQWORD = 0x7FFFFFFFFFFFFFFFL;
	public static final long MAX_LONG = 0x7FFFFFFFFFFFFFFFL;
	
	/**
	 * Minimum value that can be stored in a `sqword_t`.
	 */
	public static final sqword_t MIN_SQWORD = -0x8000000000000000L;
	public static final long MIN_LONG = -0x8000000000000000L;
	
	/**
	 * Size of a pointer in bytes.
	 */
	public static final int POINTER_SIZE;
	
	/**
	 * Exit the program with a success status.
	 */
	public static void exit();
	
	/**
	 * Exit the program with the specified status. A value of 0 indicates the program
	 * eneded successfully; any other value indicates an error. Note that only values
	 * in the 0-127 range are guaranteed to work correctly on all systems.
	 */
	public static void exit(int status);
	
	/**
	 * Throw `std.rt.AssertionError` if `cond` is not `true`.
	 * 
	 * \param cond The condition to check for.
	 * \param msg The error message to be associated with the `AssertionError`.
	 */
	public static void assert(bool cond, String msg)
	{
		if (!cond)
		{
			throw new AssertionError(msg);
		};
	};
	
	/**
	 * Throw `std.rt.AssertionError` if `cond` is not `true`.
	 */
	public static void assert(bool cond)
	{
		assert(cond, "assertion failed");
	};
	
	/**
	 * Internal, do not call.
	 * 
	 * During initialisation, this function is given an array of environment variables in the
	 * form "name=value". This is simply because manipulating that map is WAY easier in Perun
	 * than in C ;)
	 */
	protected static void __initEnviron(String[] envp)
	{
		int i;
		for (i=0; i<envp.length; i++)
		{
			String spec = envp[i];
			String[] parts = spec.split("=", 1);
			
			if (parts.length == 2)
			{
				environ[parts[0]] = parts[1];
			};
		};
	};
	
	/**
	 * Prompt for user input.
	 * 
	 * This is a utility method, which calls `System.stdout.write()` to output the specified prompt,
	 * then returns the result of `System.stdin.readLine()`. It is of course not guaranteed that
	 * stdin/stdout will actually point to a console. Use with caution.
	 * 
	 * \param promptText The prompt text to write to `stdout`.
	 * \returns The line read from `stdin`.
	 */
	public static String prompt(String promptText)
	{
		stdout.write(promptText);
		return stdin.readLine();
	};
	
	/**
	 * Return an iterator which provides path to all direct entries in a directory.
	 * 
	 * \param dir Path to a directory.
	 * \throws std.io.IOError If the directory could not be read.
	 * \returns An iterator which returns a path to every direct child of the directory.
	 */
	public static Iterator<Path> scandir(Path dir)
	{
		return new FlatDirectoryIterator(dir);
	};
	
	/**
	 * Return an iterator which provides path to all direct entries in a directory.
	 * 
	 * \param dir Path to a directory.
	 * \throws std.io.IOError If the directory could not be read.
	 * \returns An iterator which returns a path to every direct child of the directory.
	 */
	public static Iterator<Path> scandir(String dir)
	{
		scandir(new Path(dir));
	};
};
