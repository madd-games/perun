/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.rt;

/**
 * Field metadata extracted from a class description.
 * 
 * Instances of this class are returned by the `getFields()` method of `Class`; they
 * can be used to read and writes values of named fields in objects dynamically, as
 * well as extract other useful information, for purposes such as serialization.
 */
public final class ClassField
{
	/**
	 * Type kinds.
	 */
	private static final int PER_TK_VOID = 0;
	private static final int PER_TK_I8 = 1;
	private static final int PER_TK_I16 = 2;
	private static final int PER_TK_I32 = 3;
	private static final int PER_TK_I64 = 4;
	private static final int PER_TK_U8 = 5;
	private static final int PER_TK_U16 = 6;
	private static final int PER_TK_U32 = 7;
	private static final int PER_TK_U64 = 8;
	private static final int PER_TK_F32 = 9;
	private static final int PER_TK_F64 = 10;
	private static final int PER_TK_OBJECT = 11;
	private static final int PER_TK_BOOL = 12;
	private static final int PER_TK_IP = 13;
	private static final int PER_TK_UP = 14;

	/**
	 * Pointer to Per_FieldMeta.
	 */
	private ptr_t fieldMetaPtr;
	
	/**
	 * Pointer to the class description.
	 */
	private ptr_t classDesc;
	
	/**
	 * Name of this field.
	 */
	private String name;
	
	/**
	 * Type kind (one of the PER_TK_* constants).
	 */
	private int typeKind;
	
	/**
	 * Array depth.
	 */
	private int arrayDepth;
	
	private ClassField() {};
	
	/**
	 * Fetch a value: reads the value indicated by the field metadata and class description
	 * from the specified object, and stores it in `putResult`.
	 */
	private static void _fetchValue(ptr_t meta, ptr_t cd, Object obj, int size, ptr_t putResult) extern Per_ClassField_FetchValue;
	
	/**
	 * Same as above except stores the value.
	 */
	private static void _storeValue(ptr_t meta, ptr_t cd, Object obj, int size, ptr_t data) extern Per_ClassField_StoreValue;
	
	/**
	 * Get the class object for the specified metadata pointer.
	 */
	private static Class _getFieldClass(ptr_t meta) extern Per_ClassField_GetFieldClass;
	
	/**
	 * Get the object value (and increment the refcount).
	 */
	private static Object _getObject(ptr_t meta, ptr_t cd, Object obj) extern Per_ClassField_GetObject;
	
	/**
	 * Same as above, except stores the object.
	 */
	private static void _setObject(ptr_t meta, ptr_t cd, Object obj, Object value) extern Per_ClassField_SetObject;
	
	/**
	 * Get the name of this field.
	 */
	public String getName()
	{
		return name;
	};
	
	/**
	 * Get the size, in bytes, of this field's value.
	 */
	public int getSize()
	{
		if (typeKind == PER_TK_VOID)
		{
			return 0;
		}
		else if (typeKind == PER_TK_I8 || typeKind == PER_TK_U8 || typeKind == PER_TK_BOOL)
		{
			return 1;
		}
		else if (typeKind == PER_TK_I16 || typeKind == PER_TK_U16)
		{
			return 2;
		}
		else if (typeKind == PER_TK_I32 || typeKind == PER_TK_U32 || typeKind == PER_TK_F32)
		{
			return 4;
		}
		else if (typeKind == PER_TK_I64 || typeKind == PER_TK_U64 || typeKind == PER_TK_F64)
		{
			return 8;
		}
		else if (typeKind == PER_TK_IP || typeKind == PER_TK_UP || typeKind == PER_TK_OBJECT)
		{
			return System.POINTER_SIZE;
		}
		else
		{
			// ???
		};
	};
	
	/**
	 * Returns `true` if this field is of primitive `bool` type.
	 */
	public bool isBool()
	{
		return typeKind == PER_TK_BOOL && arrayDepth == 0;
	};
	
	/**
	 * Returns `true` if this field is of primitive `sbyte_t` type.
	 */
	public bool isSignedByte()
	{
		return typeKind == PER_TK_I8 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `sword_t` type.
	 */
	public bool isSignedWord()
	{
		return typeKind == PER_TK_I16 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `sdword_t` type.
	 */
	public bool isSignedDWord()
	{
		return typeKind == PER_TK_I32 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `sqword_t` type.
	 */
	public bool isSignedQWord()
	{
		return typeKind == PER_TK_I64 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `byte_t` type.
	 */
	public bool isUnsignedByte()
	{
		return typeKind == PER_TK_U8 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `word_t` type.
	 */
	public bool isUnsignedWord()
	{
		return typeKind == PER_TK_U16 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `dword_t` type.
	 */
	public bool isUnsignedDWord()
	{
		return typeKind == PER_TK_U32 && arrayDepth == 0;
	};

	/**
	 * Returns `true` if this field is of primitive `qword_t` type.
	 */
	public bool isUnsignedQWord()
	{
		return typeKind == PER_TK_U64 && arrayDepth == 0;
	};
	
	/**
	 * Returns `true` if this field is of primitive `float` type.
	 */
	public bool isFloat()
	{
		return typeKind == PER_TK_F32 && arrayDepth == 0;
	};
	
	/**
	 * Returns `true` if this field is of primitive `double` type.
	 */
	public bool isDouble()
	{
		return typeKind == PER_TK_F64 && arrayDepth == 0;
	};
	
	/**
	 * Returns `true` if this field is of an array type.
	 */
	public bool isArray()
	{
		return arrayDepth > 0;
	};
	
	/**
	 * Returns `true` if this field is of a managed type (object).
	 */
	public bool isObject()
	{
		return typeKind == PER_TK_OBJECT && arrayDepth == 0;
	};

	/**
	 * Get the boolean value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a boolean field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public bool getBoolValue(Object obj)
	{
		if (isBool())
		{
			bool[] buf = new bool[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a boolean field");
		};
	};
	
	/**
	 * Get the signed byte value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed byte field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public sbyte_t getSignedByteValue(Object obj)
	{
		if (isSignedByte())
		{
			sbyte_t[] buf = new sbyte_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a signed byte field");
		};
	};

	/**
	 * Get the signed word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public sword_t getSignedWordValue(Object obj)
	{
		if (isSignedWord())
		{
			sword_t[] buf = new sword_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 2, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a signed word field");
		};
	};

	/**
	 * Get the signed double-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed double-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public sdword_t getSignedDWordValue(Object obj)
	{
		if (isSignedDWord())
		{
			sdword_t[] buf = new sdword_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a signed double-word field");
		};
	};
	

	/**
	 * Get the signed quad-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed quad-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public sqword_t getSignedQWordValue(Object obj)
	{
		if (isSignedQWord())
		{
			sqword_t[] buf = new sqword_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a signed quad-word field");
		};
	};
	
	/**
	 * Get the `float` value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a `float` field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public float getFloatValue(Object obj)
	{
		if (isFloat())
		{
			float[] buf = new float[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a float field");
		};
	};
	
	/**
	 * Get the `double` value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a `double` field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public double getDoubleValue(Object obj)
	{
		if (isDouble())
		{
			double[] buf = new double[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not a double field");
		};
	};

	/**
	 * Get the unsigned byte value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned byte field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public byte_t getUnsignedByteValue(Object obj)
	{
		if (isUnsignedByte())
		{
			byte_t[] buf = new byte_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned byte field");
		};
	};

	/**
	 * Get the unsigned word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public word_t getUnsignedWordValue(Object obj)
	{
		if (isUnsignedWord())
		{
			word_t[] buf = new word_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 2, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned word field");
		};
	};

	/**
	 * Get the unsigned double-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned double-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public dword_t getUnsignedDWordValue(Object obj)
	{
		if (isUnsignedDWord())
		{
			dword_t[] buf = new dword_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned double-word field");
		};
	};
	
	/**
	 * Get the unsigned quad-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned quad-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public qword_t getUnsignedQWordValue(Object obj)
	{
		if (isUnsignedQWord())
		{
			qword_t[] buf = new qword_t[1];
			_fetchValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
			return buf[0];
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned quad-word field");
		};
	};
	
	/**
	 * Get the object value of this field. This works for any non-array type.
	 * 
	 * If this field is of a primitive type, then the managed wrapper for its value is
	 * returned.
	 * 
	 * \throws std.rt.IllegalArgumentError If this field is of an array type.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public Object getObjectValue(Object obj)
	{
		if (isBool()) return (Boolean) getBoolValue(obj);
		else if (isSignedByte()) return (Integer) getSignedByteValue(obj);
		else if (isSignedWord()) return (Integer) getSignedWordValue(obj);
		else if (isSignedDWord()) return (Integer) getSignedDWordValue(obj);
		else if (isSignedQWord()) return (Integer) getSignedQWordValue(obj);
		else if (isUnsignedByte()) return (UnsignedInteger) getUnsignedByteValue(obj);
		else if (isUnsignedWord()) return (UnsignedInteger) getUnsignedWordValue(obj);
		else if (isUnsignedDWord()) return (UnsignedInteger) getUnsignedDWordValue(obj);
		else if (isUnsignedQWord()) return (UnsignedInteger) getUnsignedQWordValue(obj);
		else if (isFloat()) return (Float) getFloatValue(obj);
		else if (isDouble()) return (Float) getDoubleValue(obj);
		else if (isObject()) return _getObject(fieldMetaPtr, classDesc, obj);
		else throw new IllegalArgumentError("the field value cannot be read as an object");
	};
	
	/**
	 * Set the boolean value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a boolean field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setBoolValue(Object obj, bool value)
	{
		if (isBool())
		{
			bool[] buf = new bool[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a boolean field");
		};
	};

	/**
	 * Set the signed byte value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed byte field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setSignedByteValue(Object obj, sbyte_t value)
	{
		if (isSignedByte())
		{
			sbyte_t[] buf = new sbyte_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a signed byte field");
		};
	};
	
	/**
	 * Set the signed word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setSignedWordValue(Object obj, sword_t value)
	{
		if (isSignedWord())
		{
			sword_t[] buf = new sword_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 2, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a signed word field");
		};
	};

	/**
	 * Set the signed double-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed double-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setSignedDWordValue(Object obj, sdword_t value)
	{
		if (isSignedDWord())
		{
			sdword_t[] buf = new sdword_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a signed double-word field");
		};
	};

	/**
	 * Set the signed quad-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a signed quad-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setSignedQWordValue(Object obj, sqword_t value)
	{
		if (isSignedQWord())
		{
			sqword_t[] buf = new sqword_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a signed quad-word field");
		};
	};

	/**
	 * Set the unsigned byte value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned byte field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setUnsignedByteValue(Object obj, byte_t value)
	{
		if (isUnsignedByte())
		{
			byte_t[] buf = new byte_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 1, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned byte field");
		};
	};

	/**
	 * Set the unsigned word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setUnsignedWordValue(Object obj, word_t value)
	{
		if (isUnsignedWord())
		{
			word_t[] buf = new word_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 2, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned word field");
		};
	};
	
	/**
	 * Set the unsigned double-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned double-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setUnsignedDWordValue(Object obj, dword_t value)
	{
		if (isUnsignedDWord())
		{
			dword_t[] buf = new dword_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned double-word field");
		};
	};

	/**
	 * Set the unsigned quad-word value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a unsigned quad-word field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setUnsignedQWordValue(Object obj, qword_t value)
	{
		if (isUnsignedQWord())
		{
			qword_t[] buf = new qword_t[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not an unsigned quad-word field");
		};
	};

	/**
	 * Set the `float` value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a `float` field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setFloatValue(Object obj, float value)
	{
		if (isFloat())
		{
			float[] buf = new float[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 4, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a float field");
		};
	};

	/**
	 * Set the `double` value of this field in the specified object.
	 * 
	 * \throws std.rt.IllegalArgumentError If this is not a `double` field.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class.
	 */
	public void setDoubleValue(Object obj, double value)
	{
		if (isDouble())
		{
			double[] buf = new double[] {value};
			_storeValue(fieldMetaPtr, classDesc, obj, 8, (ptr_t) buf);
		}
		else
		{
			throw new IllegalArgumentError("not a double field");
		};
	};
	
	/**
	 * Set the object value of this field. Works for any non-array type.
	 * 
	 * If the field is of a primitive type, the `value` must be the managed wrapper for
	 * its type.
	 * 
	 * \throws std.rt.IllegalArgumentError If this field is of an array type.
	 * \throws std.rt.TypeError If `obj` is not an instance of the class, or `value` cannot
	 *                          be stored in this field.
	 */
	public void setObjectValue(Object obj, Object value)
	{
		Class fieldClass;
		
		if (isBool()) setBoolValue(obj, (bool) (Boolean) value);
		else if (isSignedByte()) setSignedByteValue(obj, (sbyte_t) (Integer) value);
		else if (isSignedWord()) setSignedWordValue(obj, (sword_t) (Integer) value);
		else if (isSignedDWord()) setSignedDWordValue(obj, (sdword_t) (Integer) value);
		else if (isSignedQWord()) setSignedQWordValue(obj, (sqword_t) (Integer) value);
		else if (isUnsignedByte()) setUnsignedByteValue(obj, (byte_t) (UnsignedInteger) value);
		else if (isUnsignedWord()) setUnsignedWordValue(obj, (word_t) (UnsignedInteger) value);
		else if (isUnsignedDWord()) setUnsignedDWordValue(obj, (dword_t) (UnsignedInteger) value);
		else if (isUnsignedQWord()) setUnsignedQWordValue(obj, (qword_t) (UnsignedInteger) value);
		else if (isFloat()) setFloatValue(obj, (float) (Float) value);
		else if (isDouble()) setDoubleValue(obj, (double) (Float) value);
		else if (isObject() && (fieldClass = getFieldClass()) != null)
		{
			if (!fieldClass.isInstance(value))
			{
				throw new TypeError("the new value is not an instance of the field type");
			};
			
			_setObject(fieldMetaPtr, classDesc, obj, value);
		}
		else
		{
			throw new IllegalArgumentError("an object value cannot be assigned to this field");
		};
	};
	
	/**
	 * Get the object class of this field.
	 * 
	 * Returns `null` if the class cannot be determined. Returns the managed wrapper class if this field
	 * is primitive.
	 */
	public Class getFieldClass()
	{
		if (isBool()) return Boolean;
		else if (isSignedByte()) return Integer;
		else if (isSignedWord()) return Integer;
		else if (isSignedDWord()) return Integer;
		else if (isSignedQWord()) return Integer;
		else if (isUnsignedByte()) return UnsignedInteger;
		else if (isUnsignedWord()) return UnsignedInteger;
		else if (isUnsignedDWord()) return UnsignedInteger;
		else if (isUnsignedQWord()) return UnsignedInteger;
		else if (isFloat()) return Float;
		else if (isDouble()) return Float;
		else return _getFieldClass(fieldMetaPtr);
	};
};