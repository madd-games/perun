/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.io;

import std.time.DateTime;
import std.cont.Iterator;

/**
 * A static class containing methods relating to the file system.
 */
public static class FileSystem
{
	/**
	 * Get the last access time of the specified file or directory.
	 * 
	 * \param path Path to the file or directory, in the system's native path format.
	 * \returns A `DateTime` object indicating the last access time.
	 */
	public static DateTime getAccessTime(String path);
	
	/**
	 * Get the last access time of the specified file or direcotry.
	 * 
	 * \param path Path to the file or directory.
	 * \returns A `DateTime` object indicating the last access time.
	 */
	public static DateTime getAccessTime(Path path)
	{
		return getAccessTime(path.toString());
	};
	
	/**
	 * Get the last modification time of the specified file or directory.
	 * 
	 * \param path Path to the file or directory, in the system's native path format.
	 * \returns A `DateTime` object indicating the last access time.
	 */
	public static DateTime getModificationTime(String path);
	
	/**
	 * Get the last modification time of the specified file or directory.
	 * 
	 * \param path Path to the file or directory.
	 * \returns A `DateTime` object indicating the last access time.
	 */
	public static DateTime getModificationTime(Path path)
	{
		return getModificationTime(path.toString());
	};
	
	/**
	 * Create a new directory.
	 * 
	 * The parent directory must already exist, and the directory itself must not.
	 * 
	 * \param path Path to the directory to be created.
	 * \throws std.io.IOError If the creation failed.
	 */
	public static void createDirectory(String path);
	
	/**
	 * Create a new directory.
	 * 
	 * The parent directory must already exist, and the directory itself must not.
	 * 
	 * \param path Path to the directory to be created.
	 * \throws std.io.IOError If the creation failed.
	 */
	public static void createDirectory(Path path)
	{
		createDirectory(path.toString());
	};
	
	/**
	 * Determine if the specified path names an (accessible) directory.
	 * 
	 * This function returns `true` if the path refers to a directory, and `false` is ALL
	 * other cases, including if the path cannot be accessed, or does not exist, etc. It
	 * never throws exceptions!
	 * 
	 * \param path Path to be checked.
	 * \returns `true` if the path refers to a directory.
	 */
	public static bool isDirectory(String path);
	
	/**
	 * Determine if the specified path names an (accessible) directory.
	 * 
	 * This function returns `true` if the path refers to a directory, and `false` is ALL
	 * other cases, including if the path cannot be accessed, or does not exist, etc. It
	 * never throws exceptions!
	 * 
	 * \param path Path to be checked.
	 * \returns `true` if the path refers to a directory.
	 */
	public static bool isDirectory(Path path)
	{
		return isDirectory(path.toString());
	};
	
	/**
	 * Determine if the specified path refers to an existing accessible object.
	 * 
	 * This function returns `true` if the path is either a directory, or a file which could
	 * be opened. It returns `false` in all other cases, and never throws exceptions.
	 * 
	 * \param path Path to be checked.
	 * \returns `true` if the path refers to an existing accessible object.
	 */
	public static bool exists(Path path)
	{
		if (isDirectory(path)) return true;
		
		try
		{
			File.open(path, FileFlags.readable()).close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		};
	};

	/**
	 * Determine if the specified path refers to an existing accessible object.
	 * 
	 * This function returns `true` if the path is either a directory, or a file which could
	 * be opened. It returns `false` in all other cases, and never throws exceptions.
	 * 
	 * \param path Path to be checked.
	 * \returns `true` if the path refers to an existing accessible object.
	 */
	public static bool exists(String path)
	{
		return exists(new Path(path));
	};
	
	/**
	 * Create a directory, and any parent directories if they don't yet exist.
	 * 
	 * If the directory already exists, nothing happens. If one of the would-be
	 * parents exists but is not a directory, an `IOError` is thrown.
	 * 
	 * \path path The path to the new directory.
	 * \throws std.io.IOError If one of the parents is not a directory.
	 */
	public static void createDirectoryProgressively(Path path)
	{
		Iterator<Path> iter;
		for (iter=new ProgressivePathIterator(path); !iter.end(); iter.next())
		{
			Path sub = iter.get();
			if (!isDirectory(sub))
			{
				createDirectory(sub);
			};
		};
	};

	/**
	 * Create a directory, and any parent directories if they don't yet exist.
	 * 
	 * If the directory already exists, nothing happens. If one of the would-be
	 * parents exists but is not a directory, an `IOError` is thrown.
	 * 
	 * \path path The path to the new directory.
	 * \throws std.io.IOError If one of the parents is not a directory.
	 */
	public static void createDirectoryProgressively(String path)
	{
		createDirectoryProgressively(new Path(path));
	};
	
	/**
	 * INTERNAL.
	 * 
	 * Returns an array of file names within the specified directory, or null if there was an
	 * error. The array does not include "." and "..".
	 */
	public static String[] __listdir(String dirname);
	
	/**
	 * Remove a file or directory.
	 * 
	 * If `path` refers to a directory, it is only deleted if it is empty.
	 * 
	 * \param path Path to the file or directory to remove.
	 * \throws std.io.IOError If the removal is not possible, e.g. due to permissions,
	 *                        or attempting to remove a non-empty directory.
	 */
	public static void remove(String path);		// in runtime.c
	
	/**
	 * Remove a file or directory.
	 * 
	 * If `path` refers to a directory, it is only deleted if it is empty.
	 * 
	 * \param path Path to the file or directory to remove.
	 * \throws std.io.IOError If the removal is not possible, e.g. due to permissions,
	 *                        or attempting to remove a non-empty directory.
	 */
	public static void remove(Path path)
	{
		remove(path.toString());
	};
};
