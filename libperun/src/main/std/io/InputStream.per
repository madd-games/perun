/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.io;

/**
 * An input stream is an object which allows data to be received from some source.
 */
public abstract class InputStream
{
	/**
	 * This method must be implemented by child classes to implement the actual
	 * input machanism. Bytes read from the stream are used to fill the array.
	 *
	 * \param bytes The array of bytes to fill.
	 * \throws std.io.IOError If an I/O error occured.
	 * \returns The number of bytes successfully read; 0 indicating end of stream.
	 */
	protected abstract int readBytes(byte_t[] bytes);
	
	/**
	 * Read the up to the specified number of bytes from the stream, and return them in a string.
	 */
	public final String read(int count)
	{
		byte_t[] bytes = new byte_t[count];
		int actualCount = readBytes(bytes);
		return String.fromByteArray(bytes, 0, actualCount);
	};
	
	/**
	 * Read a line of text from the stream, and return it as a string, without the line terminator.
	 * Returns `null` at the end of the stream.
	 */
	public final String readLine()
	{
		String data = "";
		
		while (!data.endsWith(System.ENDLINE))
		{
			String nextBit = read(1);
			if (nextBit == "")
			{
				if (data == "")
				{
					return null;
				}
				else
				{
					return data;
				};
			};
			
			data += nextBit;
		};
		
		return data.substr(0, data.size() - System.ENDLINE.size());
	};
	
	/**
	 * Read a little-endian integer from the stream.
	 * 
	 * \param size The size of the integer, in bytes (typically 1, 2, 4 or 8. Less than 1 or more than 8
	 *             is not allowed).
	 * \returns The resulting integer as a `qword_t`, zero-extended.
	 * \throws std.io.IOError If the requested amount of bytes could not be read.
	 * \throws std.rt.IllegalArgumentError If the `size` argument contains an invalid value.
	 */
	public qword_t readLE(int size)
	{
		if (size < 1 || size > 8)
		{
			throw new IllegalArgumentError("invalid size: " + size);
		};
		
		byte_t[] bytes = new byte_t[size];
		if (readBytes(bytes) != size)
		{
			throw new IOError("could not read sufficiently many bytes");
		};
		
		qword_t result = (qword_t) 0;
		int i;
		for (i=0; i<size; i++)
		{
			result |= ((qword_t) bytes[i] << (i * 8));
		};
		
		return result;
	};

	/**
	 * Read a big-endian integer from the stream.
	 * 
	 * \param size The size of the integer, in bytes (typically 1, 2, 4 or 8. Less than 1 or more than 8
	 *             is not allowed).
	 * \returns The resulting integer as a `qword_t`, zero-extended.
	 * \throws std.io.IOError If the requested amount of bytes could not be read.
	 * \throws std.rt.IllegalArgumentError If the `size` argument contains an invalid value.
	 */
	public qword_t readBE(int size)
	{
		if (size < 1 || size > 8)
		{
			throw new IllegalArgumentError("invalid size: " + size);
		};
		
		byte_t[] bytes = new byte_t[size];
		if (readBytes(bytes) != size)
		{
			throw new IOError("could not read sufficiently many bytes");
		};
		
		qword_t result = (qword_t) 0;
		int i;
		for (i=0; i<size; i++)
		{
			result = (result << 8) | (qword_t) bytes[i];
		};
		
		return result;
	};
	
	/**
	 * Read data from the stream and place in a buffer.
	 * 
	 * This function reads up to `len` bytes from the stream, places the bytes in the specified
	 * buffer, and resizes the buffer to fit the contents exactly.
	 * 
	 * \param buf The buffer to place the data in.
	 * \param len Maximum number of bytes to read.
	 */
	public void read(Buffer buf, int len)
	{
		byte_t[] byteArray = new byte_t[len];
		int gotBytes = readBytes(byteArray);
		
		buf.put(0, byteArray);
		buf.resize(gotBytes);
	};
};
