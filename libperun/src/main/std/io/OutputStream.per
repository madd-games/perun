/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.io;

/**
 * An output stream is an object which allows one send data to some destination.
 * This class also performs formatting of the data.
 */
public abstract class OutputStream
{
	/**
	 * This method must be implemented in child classes to implement the actual
	 * output mechanism. The bytes stored in the byte array given are written to
	 * the stream.
	 *
	 * \param bytes The bytes to be written to the stream.
	 * \throws std.io.IOError If an error occured trying to write the data.
	 */
	protected abstract void writeBytes(byte_t[] bytes);
	
	/**
	 * Write the specified string to the stream.
	 *
	 * \param str The string to be written.
	 * \throws std.io.IOError If an error occured trying to write the data.
	 */
	public final void write(String str)
	{
		writeBytes(str.toByteArray());
	};
	
	/**
	 * Write the specified bytes to the stream.
	 * 
	 * \param bytes The bytes to write.
	 */
	public final void write(byte_t[] bytes)
	{
		writeBytes(bytes);
	};
	
	/**
	 * Write a line of text to the stream.
	 *
	 * \param line The line to be written.
	 * \throws std.io.IOError If an error occured trying to write the data.
	 */
	public final void writeLine(String line)
	{
		write(line + System.ENDLINE);
	};
	
	/**
	 * Write a single byte to this stream. 
	 */
	public final void writeLE(byte_t byte)
	{
		writeBytes(new byte_t[] {byte});
	};
	
	/**
	 * Write a word to this stream in little-endian byte order.
	 */
	public final void writeLE(word_t word)
	{
		writeBytes(new byte_t[] {
			(byte_t) (word),
			(byte_t) (word >> 8)
		});
	};
	
	/**
	 * Write a double-word to this stream in little-endian byte order.
	 */
	public final void writeLE(dword_t dword)
	{
		writeBytes(new byte_t[] {
			(byte_t) (dword),
			(byte_t) (dword >> 8),
			(byte_t) (dword >> 16),
			(byte_t) (dword >> 24)
		});
	};
	
	/**
	 * Write a quad-word to this stream in little-endian byte order.
	 */
	public final void writeLE(qword_t qword)
	{
		writeBytes(new byte_t[] {
			(byte_t) (qword),
			(byte_t) (qword >> 8),
			(byte_t) (qword >> 16),
			(byte_t) (qword >> 24),
			(byte_t) (qword >> 32),
			(byte_t) (qword >> 40),
			(byte_t) (qword >> 48),
			(byte_t) (qword >> 56)
		});
	};

	/**
	 * Write a single byte to this stream. 
	 */
	public final void writeBE(byte_t byte)
	{
		writeBytes(new byte_t[] {byte});
	};
	
	/**
	 * Write a word to this stream in big-endian byte order.
	 */
	public final void writeBE(word_t word)
	{
		writeBytes(new byte_t[] {
			(byte_t) (word >> 8),
			(byte_t) (word)
		});
	};
	
	/**
	 * Write a double-word to this stream in big-endian byte order.
	 */
	public final void writeBE(dword_t dword)
	{
		writeBytes(new byte_t[] {
			(byte_t) (dword >> 24),
			(byte_t) (dword >> 16),
			(byte_t) (dword >> 8),
			(byte_t) (dword)
		});
	};
	
	/**
	 * Write a quad-word to this stream in big-endian byte order.
	 */
	public final void writeBE(qword_t qword)
	{
		writeBytes(new byte_t[] {
			(byte_t) (qword >> 56),
			(byte_t) (qword >> 48),
			(byte_t) (qword >> 40),
			(byte_t) (qword >> 32),
			(byte_t) (qword >> 24),
			(byte_t) (qword >> 16),
			(byte_t) (qword >> 8),
			(byte_t) (qword)
		});
	};
	
	/**
	 * Write the contents of a buffer to the stream.
	 * 
	 * Reads `len` bytes from the beginning of the buffer and writes to this stream.
	 * 
	 * \param buf The buffer.
	 * \param len Number of bytes to write.
	 */
	public void write(Buffer buf, int len)
	{
		byte_t[] bytes = new byte_t[len];
		buf.get(0, bytes);
		writeBytes(bytes);
	};
};