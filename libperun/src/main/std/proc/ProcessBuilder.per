/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace std.proc;

import std.cont.List;
import std.cont.ArrayList;
import std.cont.Map;
import std.cont.KeyValuePair;
import std.cont.Iterator;

import std.io.Path;
import std.io.FileSystem;

/**
 * Process builder.
 * 
 * This class contains methods which can be used to construct processes.
 */
public class ProcessBuilder
{
	/**
	 * List of command-line arguments passed to the program (including the program name).
	 */
	private List<String> args = new ArrayList<String>();
	
	/**
	 * Path to the executable; or `null` to find automatically.
	 */
	private Path exePath;
	
	/**
	 * Path to the new working directory.
	 */
	private Path cwd = new Path(".");
	
	/**
	 * Environment variable for child process (by default, `System.environ`).
	 */
	private Map<String, String> environ;
	
	/**
	 * Default constructor.
	 */
	public ProcessBuilder()
	{
		environ = System.environ;
	};
	
	/**
	 * Append a command-line argument to the end of the current list.
	 */
	public void append(String arg)
	{
		args.append(arg);
	};
	
	/**
	 * Append an array of command-line arguments to the end of the current list.
	 */
	public void append(String[] args)
	{
		int i;
		for (i=0; i<args.length; i++)
		{
			this.args.append(args[i]);
		};
	};
	
	/**
	 * Set the executable path.
	 * 
	 * The specified file will be the one actually executed. If a path is not specified, or is
	 * set to `null` using this method, then the executable will be found automatically based on
	 * the `PATH` environment variable.
	 * 
	 * \param path Path to the executable, or `null` to find automatically.
	 */
	public void setExePath(Path path)
	{
		this.exePath = path;
	};
	
	/**
	 * Set the working directory for the child process.
	 * 
	 * \param path Path to the working directory to set.
	 */
	public void setWorkingDir(Path path)
	{
		this.cwd = path;
	};
	
	/**
	 * Set the environment for the child process.
	 * 
	 * \param environ A mapping of environ variable names to their values.
	 */
	public void setEnviron(Map<String, String> environ)
	{
		this.environ = environ;
	};
	
	/**
	 * Run the process.
	 * 
	 * \returns A `Process` object referring to the newly-created process.
	 * \throws std.proc.ProcessError If an error occured trying to start the process.
	 * \throws std.rt.IllegalArgumentError If the builder state is invalid.
	 */
	public Process run()
	{
		if (args.size() == 0)
		{
			throw new IllegalArgumentError("no arguments passed to process (must be at least process name)");
		};
		
		String exePath;
		
		if (this.exePath == null)
		{
			if (args[0].count(System.PATH_SEPARATOR) != 0 || args[0].count("/") != 0)
			{
				exePath = args[0];
			}
			else
			{
				Path nameComponent = new Path(args[0] + System.EXE_SUFFIX);
				
				String delimiter = ":";
				if (System.PATH_SEPARATOR == "\\") delimiter = ";";
				
				String[] paths = System.environ["PATH"].split(delimiter);
				
				int i;
				for (i=0; i<paths.length; i++)
				{
					Path candidate = new Path(paths[i]) + nameComponent;
					if (FileSystem.exists(candidate))
					{
						exePath = candidate.toString();
						break;
					};
				};
				
				if (i == paths.length)
				{
					throw new ProcessError("cannot find program: " + args[0]);
				};
			};
		}
		else
		{
			exePath = this.exePath.toString();
		};
		
		String[] argv = new String[args.size()];
		int i;
		for (i=0; i<args.size(); i++)
		{
			argv[i] = args[i];
		};
		
		int environSize = 0;
		Iterator< KeyValuePair<String, String> > it;
		for (it=environ.iterate(); !it.end(); it.next())
		{
			environSize++;
		};
		
		String[] envp = new String[environSize];
		int put = 0;
		for (it=environ.iterate(); !it.end(); it.next())
		{
			String str = it.get().getKey() + "=" + it.get().getValue();
			envp[put++] = str;
		};
		
		return Process.__run(exePath, argv, envp, cwd.toString());
	};
};