/*
	Perun

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PERUN_ABI_H_
#define PERUN_ABI_H_

#include <stdint.h>
#include <stdlib.h>
#include <setjmp.h>

#ifdef _WIN32
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0601
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#ifdef LIBPERUN_DEBUG
#include <stdio.h>
extern FILE* Per_Debug_Log;
#endif

#ifdef _WIN32
#	include <windows.h>
typedef HANDLE Per_Mutex;
#define	PER_MUTEX_INIT 0
#else
#	include <pthread.h>
typedef pthread_mutex_t Per_Mutex;
#define	PER_MUTEX_INIT PTHREAD_MUTEX_INITIALIZER
#endif

/**
 * Mark a function as a static initializer.
 */
#define	PER_STATIC_INIT				__attribute__ ((constructor)) void

/**
 * Mark a function as a static finalizer.
 */
#define	PER_STATIC_FINI				__attribute__ ((destructor)) void

/**
 * Class flags.
 */
#define	PER_CD_ABSTRACT				(1 << 0)
#define	PER_CD_FINAL				(1 << 1)

/**
 * Component flags.
 */
#define	PER_COMP_CTED				(1 << 0)

/**
 * Type sections.
 */
#define	PER_TYPESECT_PRIMITIVE			0
#define	PER_TYPESECT_MANAGED			1
#define	PER_TYPESECT_ARRAY			2

/**
 * Boolean macros for readability.
 */
#define	PER_TRUE				1
#define	PER_FALSE				0

/**
 * Type kinds, used to specify the type of field in metadata.
 */
#define	PER_TK_VOID				0
#define	PER_TK_I8				1
#define	PER_TK_I16				2
#define	PER_TK_I32				3
#define	PER_TK_I64				4
#define	PER_TK_U8				5
#define	PER_TK_U16				6
#define	PER_TK_U32				7
#define	PER_TK_U64				8
#define	PER_TK_F32				9
#define	PER_TK_F64				10
#define	PER_TK_OBJECT				11
#define	PER_TK_BOOL				12
#define	PER_TK_IP				13
#define	PER_TK_UP				14

/**
 * Typedef C types to Perun types.
 */
typedef	unsigned char Per_bool;
typedef float Per_float;
typedef double Per_double;
typedef int8_t Per_sbyte;
typedef uint8_t Per_byte;
typedef int16_t Per_sword;
typedef uint16_t Per_word;
typedef int32_t Per_sdword;
typedef uint32_t Per_dword;
typedef int64_t Per_sqword;
typedef uint64_t Per_qword;
typedef Per_sdword Per_int;
typedef Per_sqword Per_long;

/**
 * Typedef structs here.
 */
typedef struct Per_Object_ Per_Object;
typedef struct Per_Component_ Per_Component;
typedef struct Per_ExceptionContext_ Per_ExceptionContext;
typedef struct Per_StackFrame_ Per_StackFrame;
typedef struct Per_FieldMeta_ Per_FieldMeta;
typedef struct Per_ClassDesc_ Per_ClassDesc;

/**
 * Represents metadata about a class field.
 */
struct Per_FieldMeta_
{
	/**
	 * Name of this field; NULL indicates the end of the field list.
	 */
	const char* fName;
	
	/**
	 * The type kind (one of the PER_TK_* constants). If the field contains an array,
	 * then this is set to the base elements (e.g. PER_TK_I32 for int[][]).
	 * 
	 * If this is PER_TK_VOID (zero), ignore this entry; this is used if the type cannot
	 * be currently encoded.
	 */
	int fTypeKind;
	
	/**
	 * Array depth. If the field is of a non-array type, this is zero. Otherwise, it is
	 * the depth of this array; e.g. 2 for int[][].
	 */
	int fArrayDepth;
	
	/**
	 * Offset into the component, where this field is defined.
	 */
	size_t fCompOffset;
	
	/**
	 * If fTypeKind is PER_TK_OBJECT, points to the class description for the declared class
	 * type of this field.
	 */
	Per_ClassDesc* fObjectClass;
	
	/**
	 * Reserved for future use (currently must be set to all zeroes).
	 */
	char fResv[sizeof(void*) * 12];
};

/**
 * Represents a class description, as emitted by the compiler for all non-static classes,
 * specifying dynamic type information.
 */
struct Per_ClassDesc_
{
	/**
	 * Size of this structure.
	 */
	size_t cdSize;
	
	/**
	 * Full name of this class (namespace.name) as a string.
	 */
	const char *cdFullName;
	
	/**
	 * Size of the component for this class, including the header.
	 */
	size_t cdComponentSize;
	
	/**
	 * Pointer to an array of parent classes, terminated by a NULL pointer.
	 */
	Per_ClassDesc **cdParents;
	
	/**
	 * Initializer for this object. Sets up virtual table entries and initializes all dynamic fields.
	 * This function must not change the reference count of the object.
	 */
	void (*cdInit)(Per_Object *obj);
	
	/**
	 * Class destructor. This is invoked when an object has a reference count of zero, and so the
	 * object itself cannot actually be used, so only the component is passed!
	 */
	void (*cdFini)(Per_Component *comp);
	
	/**
	 * Default constuctor. This is here so that it may be used by Class.newInstance() at runtime.
	 */
	void (*cdDefCtor)(Per_Object *obj);
	
	/**
	 * Class flags.
	 */
	int cdFlags;
	int cdPadding;
	
	/**
	 * Pointer to the static lock.
	 */
	Per_Mutex *cdLock;
	
	/**
	 * Pointer to an array of field metadata entries. If this is NULL, then no field metadata is
	 * available. If non-NULL, then the array is terminated with an entry where `fName` is set to
	 * NULL.
	 */
	Per_FieldMeta* cdFieldMeta;
	
	char cdReservedForFutureUse[sizeof(void*) * 7];
	
	// === AREA RESERVED FOR RUNTIME ===
	
	/**
	 * Stack of reusable objects of this type.
	 */
	Per_Object *cdReuseStack;
	
	char cdReservedForRuntime[sizeof(void*) * 7];
};

/**
 * Represents a component attached to a Per_Object.
 */
struct Per_Component_
{
	/**
	 * The class definition form which this component is derived.
	 */
	Per_ClassDesc *compClass;
	
	/**
	 * Flags.
	 */
	int compFlags;
	
	/**
	 * Data area, meaning defined entirely by the class.
	 */
	char compData[];
};

/**
 * Represents virtual function binding information.
 */
typedef struct
{
	/**
	 * The class whose component contains this binding.
	 */
	Per_ClassDesc *vfDefiner;
	
	/**
	 * Offset into the component (includes the header).
	 */
	size_t vfOffset;
} Per_VirtFunc;

/**
 * Represents an exception context.
 */
struct Per_ExceptionContext_
{
	Per_ExceptionContext* xcPrev;
	Per_Object* xcThrown;
	Per_ClassDesc** xcCatches;
	Per_Object*** xcRefs;
	void*** xcArrays;
	volatile int xcWhich;
	jmp_buf xcJmpBuf;
};

/**
 * Represents Perun objects.
 */
struct Per_Object_
{
	/**
	 * Reference count.
	 */
	int objRefs;
	
	/**
	 * Number of components attached to this object.
	 */
	int objNumComps;
	
	/**
	 * The dynamic class of this object: the class that was actually given to the 'new'
	 * operator to create this.
	 */
	Per_ClassDesc *objClass;
	
	/**
	 * Array of components (the number is objNumComps). They are given in the order in which
	 * they were intialized, with accordance to ABI rules.
	 */
	Per_Component **objComps;
	
	/**
	 * The lock for Per_Lock() and Per_Unlock().
	 */
	Per_Mutex lock;
	
	/**
	 * Next object in the reuse stack (if applicable).
	 */
	Per_Object* reuse;
};

/**
 * The String component; it must match the definition in String.per!
 */
typedef struct
{
	Per_Component strHdr;
	char *strData;
} Per_String;

/**
 * The Buffer component; it must match the definition in Buffer.per!
 */
typedef struct
{
	Per_Component bufHdr;
	char* bufData;
	Per_int bufSize;
} Per_Buffer;

/**
 * Header prepended to Perun arrays.
 */
typedef struct
{
	/**
	 * Reference count of the array. Must be accessed atomically.
	 */
	uint64_t arRefs;
	
	/**
	 * Number of elements in the array.
	 */
	uint32_t arLength;
	
	/**
	 * Entry type-section, one of PER_TYPESECT_*.
	 */
	uint32_t arTypeSection;
	
	/**
	 * The lock.
	 */
	Per_Mutex lock;
} Per_ArrayHeader;

/**
 * Stack frame, as returned by Per_SysStackTrace().
 */
struct Per_StackFrame_
{
	Per_StackFrame *next;
	char *symbolName;
};

/**
 * Some useful classes and macros to name them more easily.
 */
#define	Per_NullReferenceError (&Per_CD3std2rt18NullReferenceError)
#define	Per_RuntimeError (&Per_CD3std2rt12RuntimeError)
#define	Per_TypeError (&Per_CD3std2rt9TypeError)
#define	Per_IndexError (&Per_CD3std2rt10IndexError)
#define	Per_IOError (&Per_CD3std2io7IOError)
#define	Per_NetError (&Per_CD3std3net8NetError)
#define	Per_ProcessError (&Per_CD3std4proc12ProcessError)
extern Per_ClassDesc Per_CD3std2rt18NullReferenceError;
extern Per_ClassDesc Per_CD3std2rt12RuntimeError;
extern Per_ClassDesc Per_CD3std2rt9TypeError;
extern Per_ClassDesc Per_CD3std2rt10IndexError;
extern Per_ClassDesc Per_CD3std2io7IOError;
extern Per_ClassDesc Per_CD3std3net8NetError;
extern Per_ClassDesc Per_CD3std4proc12ProcessError;

/**
 * Pre-initialize the Perun runtime. This is called before any static initializers.
 */
void Per_PreInit();

/**
 * Initialize the Perun runtime. This is called before invoking the main program's entry point.
 */
void Per_Init(int argc, char *argv[], char *envp[]);

/**
 * System-specific initialization.
 */
void Per_SysInit();

/**
 * Per-thread initialization.
 */
void Per_ThreadInit();

/**
 * System-specific per-thread initialization.
 */
void Per_SysThreadInit();

/**
 * Get the specified component of a Perun object. Returns NULL if not found.
 */
Per_Component* Per_GetComponent(Per_Object *obj, Per_ClassDesc *cls);

/**
 * Get the specified component of a Perun object. Throws an exception if not found.
 */
Per_Component* Per_GetRequiredComponent(Per_Object *obj, Per_ClassDesc *cls);

/**
 * Create a new object from the specified class. The returned object has a reference count of 1.
 */
Per_Object* Per_New(Per_ClassDesc *cls);

/**
 * Increment the reference count of the specified object.
 */
void Per_Up(Per_Object *obj);

/**
 * Decrement the reference count of the specified object. If there are no more references, also deletes the
 * object.
 */
void Per_Down(Per_Object *obj);

/**
 * Get a pointer to the offset element in the specified component.
 */
void* Per_GetFieldPtr(Per_Object *obj, Per_ClassDesc *cls, size_t offset);

/**
 * Change the dynamic binding of a virtual method.
 */
void Per_SetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf, void *implementor);

/**
 * Get a function pointer for the current dynamic binding of a virtual method.
 */
void* Per_GetDynamicBinding(Per_Object *obj, Per_VirtFunc *vf);

/**
 * Get/set the current exception context. System-specific.
 */
void Per_SysSetExceptionContext(Per_ExceptionContext *ctx);
Per_ExceptionContext* Per_SysGetExceptionContext();

/**
 * Enter an exception context.
 */
void Per_Enter(Per_ExceptionContext *ctx);

/**
 * Leave an exception context (+ any above it).
 */
void Per_Leave(Per_ExceptionContext *ctx);

/**
 * Throw an exception.
 */
void Per_Throw(Per_Object *except);

/**
 * Make a Perun std.rt.String object out of a C string.
 */
Per_Object* Per_MakeString(const char *str);

/**
 * Return a pointer to the data held by a Perun string.
 */
const char* Per_ReadString(Per_Object *str);

/**
 * Demangle a name, and return the unmangled string on the heap. The string must thus later
 * be passed to free(). This function never fails.
 */
char* Per_Demangle(const char *name);

/**
 * Get a stack trace. Return a linked list of Per_StackFrame.
 */
Per_StackFrame* Per_SysStackTrace();

/**
 * Allocate an array of the specified size, with the specified type-section. The returned array
 * has a reference count of 1.
 */
void* Per_NewArray(size_t len, size_t elementSize, uint32_t sect);

/**
 * Increment the reference count of an array.
 */
void Per_ArrayUp(void *ar);

/**
 * Decrement the reference count of an array. Destroys the array if the reference count becomes
 * zero.
 */
void Per_ArrayDown(void *ar);

/**
 * Return the length of an array. Throws a NullReferenceError if the array is NULL.
 */
uint32_t Per_ArrayLength(void *ar);

/**
 * Perform a bounds check. Throws an IndexError if the index is out of bounds of the array, or a
 * NullReferenceError if the array is null.
 */
void Per_BoundsCheck(void *ar, Per_int index);

/**
 * Throw a "simple exception" with the specified message. A simple exception is one whose constructor
 * does NOTHING other than simply invoke the Exception constructor with a message; hence, we can construct
 * the class in question by just calling the Exception constructor.
 */
void Per_ThrowSimple(Per_ClassDesc *cls, const char *msg);

/**
 * Throw a "simple exception" (see definition in Per_ThrowSimple() ) with a message created by formatting.
 */
void Per_ThrowFormat(Per_ClassDesc *cls, const char *format, ...);

/**
 * Wrap a signed integer into an `std.rt.Integer` instance. It is returned with a refcount of 1.
 */
Per_Object* Per_WrapSigned(Per_long value);

/**
 * Unwrap a signed integer from an `std.rt.Integer` instance.
 */
Per_long Per_UnwrapSigned(Per_Object *wrapper);

/**
 * Wrap an unsigned integer into an `std.rt.UnsignedInteger` instance. Is is returned with a refcount of 1.
 */
Per_Object* Per_WrapUnsigned(Per_qword value);

/**
 * Unwrap an unsigned integer from an `std.rt.UnsignedInteger` instace.
 */
Per_qword Per_UnwrapUnsigned(Per_Object *wrapper);

/**
 * Wrap a boolean into an `std.rt.Boolean` instance. It is returned with a refcount of 1.
 */
Per_Object* Per_WrapBoolean(Per_bool value);

/**
 * Unwrap a boolean from an `std.rt.Boolean` instance.
 */
Per_bool Per_UnwrapBoolean(Per_Object *wrapper);

/**
 * Wrap a double into an `std.rt.Float` instance. Is is returned with a refcount of 1.
 */
Per_Object* Per_WrapFloat(Per_double value);

/**
 * Unwrap a double from an `std.rt.Float` instance.
 */
Per_double Per_UnwrapFloat(Per_Object *wrapper);

/**
 * Returns `true` if `obj` is an instance of class `cls`.
 */
Per_bool Per_Is(Per_Object *obj, Per_ClassDesc *cls);

/**
 * Return an `std.rt.Class` object for the specified class.
 */
Per_Object* Per_WrapClass(Per_ClassDesc *cls);

/**
 * Return true if the two objects compare equal.
 */
Per_bool Per_Equals(Per_Object *a, Per_Object *b);

/**
 * Initialize a mutex. This is needed in all non-static contexts, since PER_MUTEX_INIT might
 * only be assignable in static contexts.
 */
void Per_InitLock(Per_Mutex *lock);

/**
 * Destroys a mutex.
 */
void Per_DestroyLock(Per_Mutex *lock);

/**
 * Lock or unlock a mutex.
 */
void Per_StaticLock(Per_Mutex *lock);
void Per_StaticUnlock(Per_Mutex *lock);

/**
 * Lock or unlock the controlling mutex of an object.
 */
void Per_Lock(Per_Object *obj);
void Per_Unlock(Per_Object *obj);

/**
 * Lock or unlock the mutex associated with an array.
 */
void Per_ArrayLock(void *ar);
void Per_ArrayUnlock(void *ar);

/**
 * Exit from the calling thread. System-specific.
 */
void Per_Sys_ExitThread();

/**
 * Get the Buffer component of an object. Throws an exception if not a buffer.
 */
Per_Buffer* Per_GetBufferComponent(Per_Object *obj);

#endif
